import os
import shlex
from pathlib import Path

import nox

nox.options.reuse_venv = True


AUTHENTIC_VERSION = os.getenv('AUTHENTIC_VERSION', 'main')
PASSERELLE_VERSION = os.getenv('PASSERELLE_VERSION', 'main')
DJANGO_TENANT_SCHEMAS_VERSION = os.getenv('DJANGO_TENANT_SCHEMAS_VERSION', 'main')


def run_hook(name, *args, **kwargs):
    for file in [Path(__name__).parent / '.nox-hooks.py', Path('~/.config/nox/eo-hooks.py').expanduser()]:
        if not file.exists():
            continue

        globals_ = {}
        exec(file.read_text(), globals_)
        hook = globals_.get(name, None)
        if hook:
            hook(*args, **kwargs)


def get_lasso3(session):
    src_dir = Path('/usr/lib/python3/dist-packages/')
    venv_dir = Path(session.virtualenv.location)
    for dst_dir in venv_dir.glob('lib/**/site-packages'):
        files_to_link = [src_dir / 'lasso.py'] + list(src_dir.glob('_lasso.cpython-*.so'))

        for src_file in files_to_link:
            dst_file = dst_dir / src_file.name
            if dst_file.exists():
                dst_file.unlink()
            session.log('%s => %s', dst_file, src_file)
            dst_file.symlink_to(src_file)


def setup_venv(
    session,
    *packages,
    django_version='>=4.2,<4.3',
    drf_version='>=3.12,<3.15',
    django_mellon_version='>=1.34',
):
    packages = [
        'Markdown<3',
        'Pillow',
        'WebTest',
        'coverage',
        'cssselect',
        'git+https://git.entrouvert.org/entrouvert/django-mellon.git',
        'django-webtest',
        f'djangorestframework{drf_version}',
        'enum34<=1.1.6',
        'httmock',
        (
            'https://git.entrouvert.org/entrouvert/'
            f'debian-django-tenant-schemas/archive/{DJANGO_TENANT_SCHEMAS_VERSION}.tar.gz'
        ),
        'mock<4',
        'psycopg2',
        'psycopg2-binary',
        'pyquery',
        'pytest!=6.0.0',
        'pytest-cov',
        'pytest-django',
        'pytest-freezer',
        'pytest-mock',
        'requests',
        'responses',
        'sorl-thumbnail',
        f'django{django_version}',
        *packages,
    ]
    run_hook('setup_venv', session, packages)
    session.install('-e', '.', *packages, silent=False)
    get_lasso3(session)


def hookable_run(session, *args, **kwargs):
    args = list(args)
    run_hook('run', session, args, kwargs)
    session.run(*args, **kwargs)


TEST_ENVIRONMENTS = {
    'hobo': {
        'test_directory': 'tests/',
        'env': {
            'DJANGO_SETTINGS_MODULE': 'hobo.settings',
            'HOBO_SETTINGS_FILE': 'tests/settings.py',
        },
    },
    'schemas': {
        'test_directory': 'tests_schemas/',
        'env': {
            'DJANGO_SETTINGS_MODULE': 'hobo.settings',
            'HOBO_SETTINGS_FILE': 'tests_schemas/settings.py',
        },
    },
    'multitenant': {
        'test_directory': 'tests_multitenant/',
        'env': {
            'PYTHONPATH': 'tests_multitenant',
            'DJANGO_SETTINGS_MODULE': 'settings',
        },
        'packages': ['systemd-python'],
    },
    'multipublik': {
        'test_directory': 'tests_multipublik/',
        'env': {
            'PYTHONPATH': 'tests_multipublik',
            'DJANGO_SETTINGS_MODULE': 'settings',
        },
    },
    'authentic': {
        'test_directory': 'tests_authentic/',
        'env': {
            'DEBIAN_CONFIG_COMMON': 'debian/debian_config_common.py',
            'DJANGO_SETTINGS_MODULE': 'authentic2.settings',
            'AUTHENTIC2_SETTINGS_FILE': 'tests_authentic/settings.py',
        },
        'packages': [f'https://git.entrouvert.org/entrouvert/authentic/archive/{AUTHENTIC_VERSION}.tar.gz'],
    },
    'passerelle': {
        'test_directory': 'tests_passerelle/',
        'env': {
            'DEBIAN_CONFIG_COMMON': 'debian/debian_config_common.py',
            'PASSERELLE_SETTINGS_FILE': 'tests_passerelle/settings.py',
            'DJANGO_SETTINGS_MODULE': 'passerelle.settings',
        },
        'packages': [
            f'https://git.entrouvert.org/entrouvert/passerelle/archive/{PASSERELLE_VERSION}.tar.gz',
            'pymemcache',
        ],
    },
}


@nox.session
@nox.parametrize('environment_name', tuple(TEST_ENVIRONMENTS.keys()), ids=TEST_ENVIRONMENTS.keys())
@nox.parametrize('django,drf', [('>=4.2,<4.3', '>=3.13,<3.15')], ids=['django4'])
def tests(session, environment_name, django, drf):
    test_environment = TEST_ENVIRONMENTS[environment_name]
    setup_venv(
        session,
        'git+https://git.entrouvert.org/publik-django-templatetags.git',
        *test_environment.get('packages', []),
        django_version=django,
        drf_version=drf,
    )

    session.run('python', 'manage.py', 'compilemessages', silent=True)

    args = ['py.test']
    if '--coverage' in session.posargs or not session.interactive:
        while '--coverage' in session.posargs:
            session.posargs.remove('--coverage')
        args += [
            f'--junitxml=junit-django-{environment_name}.xml',
            '--cov=hobo/',
            '--cov-config',
            '.coveragerc',
            '--cov-report=',
            '--cov-branch',
        ]

    args += [test_environment['test_directory']] + session.posargs

    if not session.interactive:
        args += ['-v']

    hookable_run(
        session,
        *args,
        env={
            'DISABLE_GLOBAL_HANDLERS': '1',
            'DB_ENGINE': 'django.db.backends.postgresql_psycopg2',
            'COVERAGE_FILE': f'.coverage.{environment_name}',
            'BRANCH_NAME': f'{os.environ.get("BRANCH_NAME")}',
        }
        | test_environment['env'],
    )


@nox.session
def pylint(session):
    os.environ['SETUPTOOLS_USE_DISTUTILS'] = 'stdlib'
    setup_venv(
        session,
        f'https://git.entrouvert.org/entrouvert/authentic/archive/{AUTHENTIC_VERSION}.tar.gz',
        'pylint<3',
        'pylint-django',
        'nox',
        'uwsgidecorators',
    )
    pylint_command = ['pylint', '--jobs', '6', '-f', 'parseable', '--rcfile', 'pylint.rc']

    if not session.posargs:
        pylint_command += [
            'hobo/',
            'hobo',
            'tests',
            'tests_authentic',
            'tests_multipublik',
            'tests_multitenant',
            'tests_passerelle',
            'tests_schemas',
            'noxfile.py',
        ]
    else:
        pylint_command += session.posargs

    if not session.interactive:
        session.run(
            'bash',
            '-c',
            f'{shlex.join(pylint_command)} | tee pylint.out ; test $PIPESTATUS -eq 0',
            external=True,
        )
    else:
        session.run(*pylint_command)


@nox.session
def codestyle(session):
    session.install('pre-commit')
    session.run('pre-commit', 'run', '--all-files', '--show-diff-on-failure')


@nox.session
def coverage_report(session):
    if session.interactive:
        return

    session.install('coverage')
    session.run('python3', '-m', 'coverage', 'erase', '--data-file=.coverage')
    session.run('python3', '-m', 'coverage', 'combine')
    session.run('python3', '-m', 'coverage', 'html', '-d', 'htmlcov', '--show-contexts')
    session.run('python3', '-m', 'coverage', 'xml')


@nox.session
def check_manifest(session):
    # django is only required to compile messages
    session.install('django', 'check-manifest')
    # compile messages and css
    ignores = [
        'VERSION',
        'hobo/static/css/*.css',
    ]
    session.run('check-manifest', '--ignore', ','.join(ignores))
