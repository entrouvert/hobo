import os

import pytest
from django.core.management import call_command
from django.db import connection
from tenant_schemas.test.client import TenantClient
from tenant_schemas.utils import tenant_context

from hobo.environment.management.commands.cook import Command
from hobo.multitenant.middleware import TenantMiddleware


@pytest.fixture(scope='session')
def client(request, tenant_schema):
    name = 'hobo.foobar'
    tenant = TenantMiddleware.get_tenant_by_hostname(name)
    with tenant_context(tenant):
        return TenantClient(tenant)


@pytest.fixture(scope='session')
def tenant_schema(django_db_setup, django_db_blocker):
    name = 'hobo.foobar'
    with django_db_blocker.unblock():
        call_command('create_hobo_tenant', name)
    tenant = TenantMiddleware.get_tenant_by_hostname(name)
    connection.set_tenant(tenant)


@pytest.fixture
def fake_notify(monkeypatch):
    services = {}

    def notify_agents_mock(*args, **kwargs):
        from hobo.deploy.utils import get_hobo_json

        environment = get_hobo_json()
        for service in environment.get('services', []):
            if service and service['slug'] not in services:
                services[service['slug']] = service

    def fake_wait(*args, **kwargs):
        pass

    monkeypatch.setattr('hobo.environment.management.commands.cook.notify_agents', notify_agents_mock)
    monkeypatch.setattr('hobo.deploy.signals.notify_agents', notify_agents_mock)
    monkeypatch.setattr(Command, 'wait_operationals', fake_wait)

    return services


@pytest.fixture
def fake_themes(settings, tmpdir):
    THEMES = """
[
  {
    "id": "publik",
    "label": "Publik",
    "variables": {
      "css_variant": "publik",
      "no_extra_js": false,
      "theme_color": "#E80E89"
    }
  }
]
"""
    base_dir = str(tmpdir.mkdir('themes'))
    settings.THEMES_DIRECTORY = base_dir

    themes_dir = os.path.join(base_dir, 'publik-base')
    os.mkdir(themes_dir)
    with open(os.path.join(themes_dir, 'themes.json'), 'w') as handler:
        handler.write(THEMES)
