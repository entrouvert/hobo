import os
from unittest import mock

import pytest
from django.core.management import call_command, load_command_class
from tenant_schemas.utils import tenant_context

from hobo.environment.models import Variable
from hobo.multitenant.middleware import TenantMiddleware, TenantNotFound


def assert_deployed(domain):
    tenant = TenantMiddleware.get_tenant_by_hostname(domain)
    tenant_hobo_json = os.path.join(tenant.get_directory(), 'hobo.json')
    assert os.path.exists(tenant_hobo_json)


@mock.patch('hobo.agent.common.management.commands.hobo_deploy.call_command')
@mock.patch('hobo.agent.common.management.commands.hobo_deploy.get_commands')
def test_import_template(mocked_get_commands, mocked_call_command, db):
    """check 'hobo-deploy' result in '$ chrono-manage import_template' call
    $ chrono-manage hobo_deploy env.json  # simulate this bash query

    warning:
    this test must be the first one executed else 'get_commands' fails to be mocked
    """
    command = load_command_class('hobo.agent.common', 'hobo_deploy')
    domain = 'chrono.dev.publik.love'

    def my_call_command(command, parameter, **kwargs):
        if command == 'import_template':
            my_call_command.import_template_was_called = True
            return
        call_command(command, parameter, **kwargs)

    mocked_get_commands.return_value = ['import_template']
    mocked_call_command.side_effect = my_call_command
    my_call_command.import_template_was_called = False

    command.handle('https://%s/' % domain, 'tests_schemas/env.json')
    assert_deployed(domain)

    # assert the 'import_template' command was called
    assert my_call_command.import_template_was_called is True


def test_deploy_on_common_agent(db):
    """deploy basic case (for chrono here):
    $ chrono-manage hobo_deploy env.json  # simulate this bash query
    """
    command = load_command_class('hobo.agent.common', 'hobo_deploy')
    domain = 'chrono.dev.publik.love'
    command.handle('https://%s/' % domain, 'tests_schemas/env.json')
    assert_deployed(domain)


def test_deploy_specifics_on_hobo_agent(db):
    """overloaded case for hobo:
    $ hobo-manage hobo-deploy env.json  # simulate this bash query
    """
    command = load_command_class('hobo.agent.hobo', 'hobo_deploy')
    domain = 'hobo.dev.publik.love'

    command.handle('https://%s/' % domain, 'tests_schemas/env.json')
    assert_deployed(domain)

    # reach deploy_specifics() in hobo/agent/hobo/management/commands/hobo_deploy.py
    tenant = TenantMiddleware.get_tenant_by_hostname(domain)
    with tenant_context(tenant):
        assert Variable.objects.get(name='ou-label').value == 'HOBO'


# fails to simulate call from authentic2 agent, that overload deploy_specifics()
# $ authentic-multitenant-manage hobo-deploy
# here, because this agent need to import authentic2 modules


# fails to simulate call from bijoe agent, that overload deploy_specifics()
# $ bijoe-mange hobo-deploy
# here, because this code is not implemented here


def test_deploy_with_legacy_urls(db):
    command = load_command_class('hobo.agent.common', 'hobo_deploy')
    domain = 'chrono.dev.publik.love'
    command.handle('https://%s/' % domain, 'tests_schemas/env.json')
    assert_deployed(domain)
    tenant_directory = TenantMiddleware.get_tenant_by_hostname(domain).get_directory()

    # change domain
    new_domain = 'new-chrono.dev.publik.love'
    command.handle('https://%s/' % new_domain, 'tests_schemas/legacy_urls_chrono_env.json')
    assert_deployed(new_domain)

    # check old tenant is gone
    with pytest.raises(TenantNotFound):
        TenantMiddleware.get_tenant_by_hostname(domain)
    assert not os.path.exists(tenant_directory)
