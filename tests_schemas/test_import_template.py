import os
from unittest import mock

from django.core.management import load_command_class


@mock.patch('hobo.agent.common.management.commands.import_template.call_command')
@mock.patch('hobo.agent.common.management.commands.import_template.get_commands')
def test_import_template_on_common_agent(mocked_get_commands, mocked_call_command, tmpdir):
    """import-template basic case (for passerelle here):
    $ passerelle-manage import_template my-template  # simulate this bash query
    check this call result in '$ passerelle-manage import_site my-template'
    """
    command = load_command_class('hobo.agent.common', 'import_template')

    mocked_get_commands.return_value = ['import_site']
    template_path = os.path.join(str(tmpdir), 'my-template.json')
    with open(template_path, 'w') as handler:
        handler.write('...')

    command.handle(basepath=str(tmpdir), template_name='my-template')
    assert mocked_call_command.mock_calls == [mock.call('import_site', template_path)]


@mock.patch('hobo.agent.common.management.commands.import_template.call_command')
@mock.patch('hobo.agent.common.management.commands.import_template.get_commands')
def test_import_template_on_combo_agent(mocked_get_commands, mocked_call_command, tmpdir):
    """overloaded case for combo
    $ combo-manage import_template my-template  # simulate this bash query
    only check we reach the hobo.agent.common's code here
    """
    command = load_command_class('hobo.agent.combo', 'import_template')

    mocked_get_commands.return_value = ['import_site']
    template_path = os.path.join(str(tmpdir), 'my-template.json')
    with open(template_path, 'w') as handler:
        handler.write('...')

    command.handle(basepath=str(tmpdir), template_name='my-template')
    assert mocked_call_command.mock_calls == [mock.call('import_site', template_path)]
