from unittest.mock import Mock

import pytest
from django.core.management import call_command
from django.core.management.base import CommandError
from tenant_schemas.utils import tenant_context

from hobo.environment import models as environment_models
from hobo.environment.utils import get_installed_services
from hobo.multitenant.middleware import TenantMiddleware


@pytest.fixture()
def hobo_tenant(db, fake_notify, monkeypatch, fake_themes):
    monkeypatch.setattr(environment_models, 'is_resolvable', lambda x: True)
    monkeypatch.setattr(environment_models, 'has_valid_certificate', lambda x: True)
    yield call_command('cook', 'tests_schemas/example_recipe.json')
    call_command('delete_tenant', 'hobo-instance-name.dev.signalpublik.com')


def test_unknown_service(hobo_tenant):
    tenant = TenantMiddleware.get_tenant_by_hostname('hobo-instance-name.dev.signalpublik.com')
    with tenant_context(tenant):
        assert get_installed_services()
        with pytest.raises(CommandError) as e_info:
            call_command(
                'rename_service',
                'https://unkown.dev.signalpublik.com/',
                'https://new-passerelle-instance-name.dev.signalpublik.com',
            )
        assert 'No service matches https://unkown.dev.signalpublik.com/' in str(e_info.value)


def test_secondary_service(hobo_tenant):
    tenant = TenantMiddleware.get_tenant_by_hostname('hobo-instance-name.dev.signalpublik.com')
    with tenant_context(tenant):
        assert get_installed_services()
        environment_models.Passerelle.objects.create(
            title='other passerelle',
            slug='other-passerelle',
            base_url='https://other-passerelle-instance-name.dev.signalpublik.com',
            secondary=True,
        )
        with pytest.raises(CommandError) as e_info:
            call_command(
                'rename_service',
                'https://other-passerelle-instance-name.dev.signalpublik.com',
                'https://new-other-passerelle-instance-name.dev.signalpublik.com',
            )
        assert (
            'Cannot rename a secondary service, you must run the command against the hobo tenant that is primary holding it'
            in str(e_info.value)
        )


def test_rename_service_succes(hobo_tenant, monkeypatch):
    tenant = TenantMiddleware.get_tenant_by_hostname('hobo-instance-name.dev.signalpublik.com')
    with tenant_context(tenant):
        assert environment_models.Passerelle.objects.count() == 1
        passerelle_service = environment_models.Passerelle.objects.first()
        assert (
            passerelle_service.get_base_url_path() == 'https://passerelle-instance-name.dev.signalpublik.com/'
        )
        assert get_installed_services()
        monkeypatch.setattr('hobo.environment.management.commands.rename_service.notify_agents', Mock())
        monkeypatch.setattr('hobo.environment.management.commands.rename_service.wait_operationals', Mock())
        call_command(
            'rename_service',
            'https://passerelle-instance-name.dev.signalpublik.com/',
            'https://new-passerelle-instance-name.dev.signalpublik.com/',
        )
        assert environment_models.Passerelle.objects.count() == 1
        passerelle_service = environment_models.Passerelle.objects.first()
        assert (
            passerelle_service.get_base_url_path()
            == 'https://new-passerelle-instance-name.dev.signalpublik.com/'
        )
        assert len(passerelle_service.legacy_urls) == 1
        assert (
            passerelle_service.legacy_urls[0]['base_url']
            == 'https://passerelle-instance-name.dev.signalpublik.com/'
        )
