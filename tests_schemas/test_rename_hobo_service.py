import pytest
from django.core.management import call_command
from django.core.management.base import CommandError
from tenant_schemas.utils import tenant_context

from hobo.environment import models as environment_models
from hobo.environment.utils import get_installed_services, get_or_create_local_hobo
from hobo.multitenant.middleware import TenantMiddleware, TenantNotFound


@pytest.fixture()
def hobo_tenant(db, fake_notify, monkeypatch, fake_themes):
    monkeypatch.setattr(environment_models, 'is_resolvable', lambda x: True)
    monkeypatch.setattr(environment_models, 'has_valid_certificate', lambda x: True)
    yield call_command('cook', 'tests_schemas/example_recipe.json')
    call_command('delete_tenant', 'hobo-instance-name.dev.signalpublik.com')


def test_unknown_service(hobo_tenant):
    tenant = TenantMiddleware.get_tenant_by_hostname('hobo-instance-name.dev.signalpublik.com')
    with tenant_context(tenant):
        assert get_installed_services()
    with pytest.raises(CommandError) as e_info:
        call_command(
            'rename_hobo_service',
            'https://unkown-hobo-instance-name.dev.signalpublik.com/',
            'https://new-hobo-instance-name.dev.signalpublik.com/',
        )
        assert 'No service matches https://unkown-hobo-instance-name.dev.signalpublik.com/' in str(
            e_info.value
        )


def test_rename_hobo_service_succes(db, fake_notify, monkeypatch, fake_themes):
    monkeypatch.setattr(environment_models, 'is_resolvable', lambda x: True)
    monkeypatch.setattr(environment_models, 'has_valid_certificate', lambda x: True)
    call_command('cook', 'tests_schemas/example_recipe.json')
    assert TenantMiddleware.get_tenant_by_hostname('hobo-instance-name.dev.signalpublik.com')
    call_command(
        'rename_hobo_service',
        'https://hobo-instance-name.dev.signalpublik.com/',
        'https://new-hobo-instance-name.dev.signalpublik.com/',
    )
    with pytest.raises(TenantNotFound):
        TenantMiddleware.get_tenant_by_hostname('hobo-instance-name.dev.signalpublik.com')

    tenant = TenantMiddleware.get_tenant_by_hostname('new-hobo-instance-name.dev.signalpublik.com')
    with tenant_context(tenant):
        local_hobo = get_or_create_local_hobo()
        assert local_hobo.get_base_url_path() == 'https://new-hobo-instance-name.dev.signalpublik.com/'
        assert len(local_hobo.legacy_urls) == 1
        assert local_hobo.legacy_urls[0]['base_url'] == 'https://hobo-instance-name.dev.signalpublik.com/'

    # cleanup
    call_command('delete_tenant', 'new-hobo-instance-name.dev.signalpublik.com')
