import os
import tempfile

import hobo.test_utils

TENANT_BASE = tempfile.mkdtemp('hobo-tenant-base')
TENANT_MODEL = 'multitenant.Tenant'
# noqa pylint: disable=used-before-assignment
MIDDLEWARE = ('hobo.multitenant.middleware.TenantMiddleware',) + MIDDLEWARE
BRANCH_NAME = (
    os.environ.get('BRANCH_NAME', '').replace('/', '_').replace('-', '_').encode('ascii', 'ignore').decode()
)[:15]
DATABASES = {
    'default': {
        'ENGINE': 'tenant_schemas.postgresql_backend',
        'NAME': hobo.test_utils.get_safe_db_name(),
    }
}
DATABASE_ROUTERS = ('tenant_schemas.routers.TenantSyncRouter',)
# noqa pylint: disable=used-before-assignment
INSTALLED_APPS = ('hobo.multitenant', 'hobo') + INSTALLED_APPS
SHARED_APPS = ()
TENANT_APPS = INSTALLED_APPS
PROJECT_NAME = 'testing'
