import json

import pytest
from django.core.management import call_command
from django.core.management.base import CommandError

from hobo.deploy.utils import get_hobo_json
from hobo.environment import models as environment_models


def test_cook(db, fake_notify, monkeypatch, fake_themes):
    monkeypatch.setattr(environment_models, 'is_resolvable', lambda x: True)
    monkeypatch.setattr(environment_models, 'has_valid_certificate', lambda x: True)
    call_command('cook', 'tests_schemas/recipe.json')
    assert len(fake_notify) == 3
    assert get_hobo_json().get('variables').get('theme') == 'publik'


def test_cook_unresolvable(db, fake_notify, monkeypatch):
    monkeypatch.setattr(environment_models, 'is_resolvable', lambda x: False)
    with pytest.raises(CommandError) as e_info:
        call_command('cook', 'tests_schemas/recipe.json')
    assert 'is not resolvable' in str(e_info.value)


def test_cook_example(db, fake_notify, monkeypatch, fake_themes):
    """hobo/cook (before rabbitmq) scenario having templates.
    the resulting JSON may be helpfull to manually invoque hobo-deploy (after rabbitmq)
    """
    monkeypatch.setattr(environment_models, 'is_resolvable', lambda x: True)
    monkeypatch.setattr(environment_models, 'has_valid_certificate', lambda x: True)
    call_command('cook', 'tests_schemas/example_recipe.json')

    # notify_agents was call
    assert len(fake_notify) == 6

    # here is the JSON environment spread by notify_agents
    environment = get_hobo_json()

    # below JSON file was created by this instruction
    # json.dump(environment, open('tests_schemas/example_env.json', 'w'),
    #          sort_keys=True, indent=4, separators=(',', ': '))
    with open('tests_schemas/example_env.json') as fd:
        expected_env = json.load(fd)

    # remove secret_key and timestamp values that alway change
    environment['timestamp'] = 'XXXXXXXXXX.XX'
    expected_env['timestamp'] = 'XXXXXXXXXX.XX'
    for service in environment['services']:
        service['secret_key'] = 'XXX'
    for service in expected_env['services']:
        service['secret_key'] = 'XXX'

    dump = json.dumps(environment, sort_keys=True, indent=4, separators=(',', ': '))
    expected_dump = json.dumps(expected_env, sort_keys=True, indent=4, separators=(',', ': '))
    assert dump == expected_dump
    call_command('delete_tenant', 'hobo-instance-name.dev.signalpublik.com')
