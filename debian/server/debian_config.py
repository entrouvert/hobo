# This file is sourced by "exec" from hobo.settings

import os

PROJECT_NAME = 'hobo'
exec(open('/usr/lib/hobo/debian_config_common.py').read())

# hobo don't use multitenant mellon adapter: IdP is detected in the local
# environnment
MELLON_ADAPTER = ('hobo.utils.MellonAdapter',)
MELLON_ADD_AUTHNREQUEST_NEXT_URL_EXTENSION = True

# add custom hobo agent module
INSTALLED_APPS = ('hobo.agent.hobo',) + INSTALLED_APPS

exec(open(os.path.join(ETC_DIR, 'settings.py')).read())

# run additional settings snippets
exec(open('/usr/lib/hobo/debian_config_settings_d.py').read())
