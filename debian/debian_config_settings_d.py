import glob
import os

for filename in sorted(glob.glob(os.path.join(ETC_DIR, 'settings.d', '*.py'))):
    exec(open(filename).read())
