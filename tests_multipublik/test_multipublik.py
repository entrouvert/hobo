import json
import os
import random

from django.conf import settings
from tenant_schemas.utils import tenant_context

from hobo.agent.hobo.management.commands.hobo_deploy import Command as HoboDeployCommand
from hobo.deploy.utils import get_hobo_json
from hobo.environment.models import Combo, Hobo
from hobo.multitenant.middleware import TenantMiddleware


def get_hobo_json_filename(tenant):
    with tenant_context(tenant):
        hobo_json = get_hobo_json()
        json_filename = os.path.join(settings.TENANT_BASE, 'tmp%s.json' % random.random())
        with open(json_filename, 'w') as fd:
            json.dump(hobo_json, fd, indent=2)
    return json_filename


def count_hobo_services(hobo_json):
    return len([service for service in hobo_json['services'] if service['service-id'] == 'hobo'])


def assert_hobo1_service_in_hobo1(hobo_json):
    for service in hobo_json['services']:
        if service['slug'] == 'hobo':
            assert service['service-id'] == 'hobo'
            assert service['title'] == 'Hobo'
            assert service['backoffice-menu-url'] == 'https://tenant1.example.net/menu.json'
            assert service['base_url'] == 'https://tenant1.example.net/'
            assert service['provisionning-url'] == 'https://tenant1.example.net/__provision__/'
            assert service['saml-sp-metadata-url'] == 'https://tenant1.example.net/accounts/mellon/metadata/'
            assert service['secret_key'] == '7e5e1778ee87fbbcfc9f10f0978bdd9e1b74d300'
            assert 'id' not in service
            return
    assert False, "Can't find hobo1 service"


def assert_hobo1_service_in_hobo2(hobo_json):
    for service in hobo_json['services']:
        if service['slug'] == '_interco_hobo':
            assert service['service-id'] == 'hobo'
            assert service['title'] == 'Hobo'
            assert 'backoffice-menu-url' not in service
            assert service['base_url'] == 'https://tenant1.example.net/'
            assert service['provisionning-url'] == 'https://tenant1.example.net/__provision__/'
            assert service['saml-sp-metadata-url'] == 'https://tenant1.example.net/accounts/mellon/metadata/'
            assert service['secret_key'] == '7e5e1778ee87fbbcfc9f10f0978bdd9e1b74d300'
            assert 'id' in service
            assert service['secondary'] is True
            assert service['template_name'] == ''
            assert service['variables'] == {}
            return
    assert False, "Can't find hobo1 service"


def assert_hobo1_service_in_hobo3(hobo_json):
    for service in hobo_json['services']:
        if service['slug'] == '_interco_hobo':
            assert service['service-id'] == 'hobo'
            assert service['title'] == 'Hobo'
            assert 'backoffice-menu-url' not in service
            assert service['base_url'] == 'https://tenant1.example.net/'
            assert service['provisionning-url'] == 'https://tenant1.example.net/__provision__/'
            assert service['saml-sp-metadata-url'] == 'https://tenant1.example.net/accounts/mellon/metadata/'
            assert service['secret_key'] == '7e5e1778ee87fbbcfc9f10f0978bdd9e1b74d300'
            assert 'id' in service
            assert service['secondary'] is True
            assert service['template_name'] == ''
            assert service['variables'] == {}
            return
    assert False, "Can't find hobo1 service"


def assert_hobo2_service_in_hobo1(hobo_json):
    for service in hobo_json['services']:
        if service['slug'] == 'hobo-coll2':
            assert service['service-id'] == 'hobo'
            assert service['title'] == 'Coll2'
            assert 'backoffice-menu-url' not in service
            assert 'base-url' not in service
            assert service['base_url'] == 'https://hobo2.example.net/'
            assert service['provisionning-url'] == 'https://hobo2.example.net/__provision__/'
            assert service['saml-sp-metadata-url'] == 'https://hobo2.example.net/accounts/mellon/metadata/'
            assert service['secondary'] is False
            assert service['template_name'] == ''
            assert service['variables'] == {}
            assert 'secret_key' in service
            assert 'id' in service
            return
    assert False, "Can't find hobo2 service"


def assert_hobo2_service_in_hobo2(hobo_json):
    for service in hobo_json['services']:
        if service['slug'] == 'hobo':
            assert service['service-id'] == 'hobo'
            assert service['title'] == 'Hobo'
            assert service['backoffice-menu-url'] == 'https://hobo2.example.net/menu.json'
            assert service['base_url'] == 'https://hobo2.example.net/'
            assert service['provisionning-url'] == 'https://hobo2.example.net/__provision__/'
            assert service['saml-sp-metadata-url'] == 'https://hobo2.example.net/accounts/mellon/metadata/'
            assert 'secret_key' in service
            assert 'id' not in service
            return
    assert False, "Can't find hobo2 service"


def assert_hobo2_service_in_hobo3(hobo_json):
    for service in hobo_json['services']:
        if service['slug'] == '_interco_hobo-coll2':
            assert service['service-id'] == 'hobo'
            assert service['title'] == 'Coll2'
            assert 'backoffice-menu-url' not in service
            assert 'base-url' not in service
            assert service['base_url'] == 'https://hobo2.example.net/'
            assert service['provisionning-url'] == 'https://hobo2.example.net/__provision__/'
            assert service['saml-sp-metadata-url'] == 'https://hobo2.example.net/accounts/mellon/metadata/'
            assert service['secondary'] is True
            assert service['template_name'] == ''
            assert service['variables'] == {}
            assert 'secret_key' in service
            assert 'id' in service
            return
    assert False, "Can't find hobo2 service"


def assert_hobo3_service_in_hobo1(hobo_json):
    for service in hobo_json['services']:
        if service['slug'] == 'hobo-coll3':
            assert service['service-id'] == 'hobo'
            assert service['title'] == 'Coll3'
            assert 'backoffice-menu-url' not in service
            assert 'base-url' not in service
            assert service['base_url'] == 'https://hobo3.example.net/'
            assert service['provisionning-url'] == 'https://hobo3.example.net/__provision__/'
            assert service['saml-sp-metadata-url'] == 'https://hobo3.example.net/accounts/mellon/metadata/'
            assert service['secondary'] is False
            assert service['template_name'] == ''
            assert service['variables'] == {}
            assert 'secret_key' in service
            assert 'id' in service
            return
    assert False, "Can't find hobo3 service"


def assert_hobo3_service_in_hobo2(hobo_json):
    for service in hobo_json['services']:
        if service['slug'] == '_interco_hobo-coll3':
            assert service['service-id'] == 'hobo'
            assert service['title'] == 'Coll3'
            assert 'backoffice-menu-url' not in service
            assert 'base-url' not in service
            assert service['base_url'] == 'https://hobo3.example.net/'
            assert service['provisionning-url'] == 'https://hobo3.example.net/__provision__/'
            assert service['saml-sp-metadata-url'] == 'https://hobo3.example.net/accounts/mellon/metadata/'
            assert service['secondary'] is True
            assert service['template_name'] == ''
            assert service['variables'] == {}
            assert 'secret_key' in service
            assert 'id' in service
            return
    assert False, "Can't find hobo3 service"


def assert_hobo3_service_in_hobo3(hobo_json):
    for service in hobo_json['services']:
        if service['slug'] == 'hobo':
            assert service['service-id'] == 'hobo'
            assert service['title'] == 'Hobo'
            assert service['backoffice-menu-url'] == 'https://hobo3.example.net/menu.json'
            assert service['base_url'] == 'https://hobo3.example.net/'
            assert service['provisionning-url'] == 'https://hobo3.example.net/__provision__/'
            assert service['saml-sp-metadata-url'] == 'https://hobo3.example.net/accounts/mellon/metadata/'
            assert 'secret_key' in service
            assert 'id' not in service
            return
    assert False, "Can't find hobo3 service"


def test_multipublik(tenants, mocker):
    hobo1 = tenants[0]
    hobo1.base_url = 'https://tenant1.example.net/'
    with tenant_context(hobo1):
        hobo_json = get_hobo_json()
        assert count_hobo_services(hobo_json) == 1
        assert_hobo1_service_in_hobo1(hobo_json)

        hobo2 = Hobo(title='Coll2', slug='hobo-coll2', base_url='https://hobo2.example.net')
        hobo2.save()
        hobo_json = get_hobo_json()
        assert count_hobo_services(hobo_json) == 2
        assert_hobo1_service_in_hobo1(hobo_json)
        assert_hobo2_service_in_hobo1(hobo_json)

        hobo3 = Hobo(title='Coll3', slug='hobo-coll3', base_url='https://hobo3.example.net')
        hobo3.save()
        hobo_json = get_hobo_json()
        assert count_hobo_services(hobo_json) == 3
        assert_hobo1_service_in_hobo1(hobo_json)
        assert_hobo2_service_in_hobo1(hobo_json)
        assert_hobo3_service_in_hobo1(hobo_json)

        combo = Combo(title='Portail', slug='portal', base_url='https://combo1.example.net')
        combo.save()
        assert count_hobo_services(hobo_json) == 3
        assert_hobo1_service_in_hobo1(hobo_json)
        assert_hobo2_service_in_hobo1(hobo_json)
        assert_hobo3_service_in_hobo1(hobo_json)

    # inform coll2 about interco environment
    HoboDeployCommand().handle(hobo2.base_url, get_hobo_json_filename(hobo1))

    hobo2 = TenantMiddleware.get_tenant_by_hostname('hobo2.example.net')
    with tenant_context(hobo2):
        hobo_json = get_hobo_json()
        assert count_hobo_services(hobo_json) == 3
        assert_hobo1_service_in_hobo2(hobo_json)
        assert_hobo2_service_in_hobo2(hobo_json)
        assert_hobo3_service_in_hobo2(hobo_json)

        # interco combo created
        assert Combo.objects.count() == 1
        combo = Combo.objects.first()
        assert combo.slug == '_interco_portal'
        assert combo.base_url == 'https://combo1.example.net/'
        assert combo.secondary is True

        assert Hobo.objects.count() == 3
        # interco hobo
        hobo = Hobo.objects.get(slug='_interco_hobo')
        assert hobo.title == 'Hobo'
        assert hobo.base_url == 'https://tenant1.example.net/'
        assert hobo.secondary is True

        # coll3 Hobo
        hobo = Hobo.objects.get(slug='_interco_hobo-coll3')
        assert hobo.title == 'Coll3'
        assert hobo.base_url == 'https://hobo3.example.net/'
        assert hobo.secondary is True

    # notify_agents will be called for secondary services: as celery
    # is not running we just block it
    mocker.patch('hobo.agent.hobo.management.commands.hobo_deploy.notify_agents')

    hobo2 = TenantMiddleware.get_tenant_by_hostname('hobo2.example.net')
    hobo2.base_url = 'https://hobo2.example.net/'
    with tenant_context(hobo2):
        combo = Combo(title='Portail', slug='portal', base_url='https://combo2.example.net')
        combo.save()

    # inform interco about coll2 environment
    HoboDeployCommand().handle(hobo1.base_url, get_hobo_json_filename(hobo2))

    with tenant_context(hobo1):
        assert Combo.objects.filter(secondary=True).count() == 1
        assert Combo.objects.filter(secondary=False).count() == 1

        # coll2 hobo
        combo = Combo.objects.get(secondary=True)
        assert combo.slug == '_hobo-coll2_portal'
        assert combo.title == 'Portail'
        assert combo.base_url == 'https://combo2.example.net/'

    # interco environment has changed (secondary Combo coming from coll2)
    # inform coll2 about interco environment and check that nothing changes
    # (no service creation recursion)
    HoboDeployCommand().handle(hobo2.base_url, get_hobo_json_filename(hobo1))
    with tenant_context(hobo2):
        assert Hobo.objects.filter().count() == 3
        assert Hobo.objects.filter(secondary=True).count() == 2
        assert Combo.objects.filter().count() == 2
        assert Combo.objects.filter(secondary=True).count() == 1

    # what are we checking here ?
    with tenant_context(hobo1):
        assert Hobo.objects.filter(secondary=True).count() == 0
        assert Combo.objects.filter(secondary=False).count() == 1

    # inform coll3 about interco environment
    HoboDeployCommand().handle(hobo3.base_url, get_hobo_json_filename(hobo1))
    hobo3 = TenantMiddleware.get_tenant_by_hostname('hobo3.example.net')
    hobo3.base_url = 'https://hobo3.example.net/'
    with tenant_context(hobo3):
        hobo_json = get_hobo_json()
        assert count_hobo_services(hobo_json) == 3
        assert_hobo1_service_in_hobo3(hobo_json)
        assert_hobo2_service_in_hobo3(hobo_json)
        assert_hobo3_service_in_hobo3(hobo_json)

        # interco combo created
        assert Combo.objects.count() == 1
        combo = Combo.objects.first()
        assert combo.slug == '_interco_portal'
        assert combo.base_url == 'https://combo1.example.net/'
        assert combo.secondary is True

        assert Hobo.objects.count() == 3
        # interco hobo
        hobo = Hobo.objects.get(slug='_interco_hobo')
        assert hobo.title == 'Hobo'
        assert hobo.base_url == 'https://tenant1.example.net/'
        assert hobo.secondary is True

        # coll2 Hobo
        hobo = Hobo.objects.get(slug='_interco_hobo-coll2')
        assert hobo.title == 'Coll2'
        assert hobo.base_url == 'https://hobo2.example.net/'
        assert hobo.secondary is True

        # Add a portal in coll3
        combo = Combo(title='Portail', slug='portal', base_url='https://combo3.example.net')
        combo.save()

    # inform interco about coll3 environment
    HoboDeployCommand().handle(hobo1.base_url, get_hobo_json_filename(hobo3))

    with tenant_context(hobo1):
        assert Combo.objects.filter(secondary=True).count() == 2
        assert Combo.objects.filter(secondary=False).count() == 1
        # coll3 Combo
        combo = Combo.objects.get(slug='_hobo-coll3_portal')
        assert combo.title == 'Portail'
        assert combo.base_url == 'https://combo3.example.net/'
        assert combo.secondary is True

    # inform coll2 about interco environment
    # nothing changed, coll2 does not care about secondary services in interco
    HoboDeployCommand().handle(hobo2.base_url, get_hobo_json_filename(hobo1))
    with tenant_context(hobo2):
        assert Combo.objects.filter(secondary=True).count() == 1
        assert Combo.objects.filter(secondary=False).count() == 1

    # inform coll2 about coll3 environment
    # nothing changed, coll2 does not have to know anything about coll3
    # (except its hobo, which is a primary service in interco)
    HoboDeployCommand().handle(hobo2.base_url, get_hobo_json_filename(hobo3))
    with tenant_context(hobo2):
        assert Combo.objects.filter(secondary=True).count() == 1
        assert Combo.objects.filter(secondary=False).count() == 1

    # check hobo secret keys
    with tenant_context(hobo1):
        hobo_secret_keys = {
            hobo1.base_url: Hobo.objects.filter(base_url=hobo1.base_url)[0].secret_key,
            hobo2.base_url: Hobo.objects.filter(base_url=hobo2.base_url)[0].secret_key,
            hobo3.base_url: Hobo.objects.filter(base_url=hobo3.base_url)[0].secret_key,
        }
    for hobo in (hobo2, hobo3):
        with tenant_context(hobo):
            for hobo_instance in Hobo.objects.all():
                assert hobo_instance.secret_key == hobo_secret_keys.get(hobo_instance.base_url)

    # URL change in interco portal
    with tenant_context(hobo1):
        combo = Combo.objects.get(slug='portal')
        assert combo.base_url == 'https://combo1.example.net/'
        combo.change_base_url('https://new-combo1.example.net')
        combo.save()

        # check the interco hobo json
        hobo_json = get_hobo_json()
        for service in hobo_json['services']:
            if service['slug'] == 'portal':
                assert service['base_url'] == 'https://new-combo1.example.net/'
                assert len(service['legacy_urls']) == 1
                assert service['legacy_urls'][0]['base_url'] == 'https://combo1.example.net/'
                break
        else:
            assert False, 'no portal found'

    # inform coll2 about interco environment
    HoboDeployCommand().handle(hobo2.base_url, get_hobo_json_filename(hobo1))
    with tenant_context(hobo2):
        # no extra combo created in coll 2
        assert Combo.objects.filter().count() == 2
        # interco portal url changed
        combo = Combo.objects.get(slug='_interco_portal')
        assert combo.base_url == 'https://new-combo1.example.net/'
        assert len(combo.legacy_urls) == 1
        assert combo.legacy_urls[0]['base_url'] == 'https://combo1.example.net/'

    # inform coll2 about interco environment a second time, check that nothing changes
    HoboDeployCommand().handle(hobo2.base_url, get_hobo_json_filename(hobo1))
    with tenant_context(hobo2):
        assert Combo.objects.filter().count() == 2
        combo = Combo.objects.get(slug='_interco_portal')
        assert combo.base_url == 'https://new-combo1.example.net/'
        assert len(combo.legacy_urls) == 1
        assert combo.legacy_urls[0]['base_url'] == 'https://combo1.example.net/'

    # URL change in coll2 portal
    with tenant_context(hobo2):
        combo = Combo.objects.get(slug='portal')
        assert combo.base_url == 'https://combo2.example.net/'
        combo.change_base_url('https://new-combo2.example.net')
        combo.save()

        # check the coll2 hobo json
        hobo_json = get_hobo_json()
        for service in hobo_json['services']:
            if service['slug'] == 'portal':
                assert service['base_url'] == 'https://new-combo2.example.net/'
                assert len(service['legacy_urls']) == 1
                assert service['legacy_urls'][0]['base_url'] == 'https://combo2.example.net/'
                break
        else:
            assert False, 'no portal found'

    # inform interco about coll2 environment
    HoboDeployCommand().handle(hobo1.base_url, get_hobo_json_filename(hobo2))
    with tenant_context(hobo1):
        # no extra combo created in interco
        assert Combo.objects.filter().count() == 3
        # coll2 portal url changed
        combo = Combo.objects.get(slug='_hobo-coll2_portal')
        assert combo.base_url == 'https://new-combo2.example.net/'
        assert len(combo.legacy_urls) == 1
        assert combo.legacy_urls[0]['base_url'] == 'https://combo2.example.net/'

    # URL change on the primary hobo
    with tenant_context(hobo1):
        hobo = Hobo.objects.get(slug='hobo')
        assert hobo.base_url == 'https://tenant1.example.net/'
        hobo.change_base_url('https://new-tenant1.example.net')
        hobo.save()

        # check the interco hobo json
        hobo_json = get_hobo_json()
        for service in hobo_json['services']:
            if service['slug'] == 'hobo':
                assert service['base_url'] == 'https://new-tenant1.example.net/'
                assert len(service['legacy_urls']) == 1
                assert service['legacy_urls'][0]['base_url'] == 'https://tenant1.example.net/'
                break
        else:
            assert False, 'no hobo found'

    # inform coll2 about interco environment
    HoboDeployCommand().handle(hobo2.base_url, get_hobo_json_filename(hobo1))
    with tenant_context(hobo2):
        # no extra hobo created in coll 2
        assert Hobo.objects.count() == 3
        # interco hobo url changed
        hobo = Hobo.objects.get(slug='_interco_hobo')
        assert hobo.base_url == 'https://new-tenant1.example.net/'
        assert len(hobo.legacy_urls) == 1
        assert hobo.legacy_urls[0]['base_url'] == 'https://tenant1.example.net/'

    # URL change on coll2 hobo (initiated by the interco tenant)
    with tenant_context(hobo1):
        hobo2 = Hobo.objects.get(slug='hobo-coll2')
        assert hobo2.base_url == 'https://hobo2.example.net/'
        hobo2.change_base_url('https://new-hobo2.example.net')
        hobo2.save()

    # inform coll2 about interco environment
    HoboDeployCommand().handle(hobo2.base_url, get_hobo_json_filename(hobo1))

    hobo2 = TenantMiddleware.get_tenant_by_hostname('new-hobo2.example.net')
    with tenant_context(hobo2):
        # no extra hobo created in coll 2
        assert Hobo.objects.count() == 3
        # coll2 hobo url changed
        hobo = Hobo.objects.get(slug='hobo')
        assert hobo.base_url == 'https://new-hobo2.example.net/'
        assert len(hobo.legacy_urls) == 1
        assert hobo.legacy_urls[0]['base_url'] == 'https://hobo2.example.net/'
