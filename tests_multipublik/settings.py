import builtins
import os.path
from unittest.mock import mock_open, patch

import hobo.test_utils
from hobo.settings import *  # noqa pylint: disable=wildcard-import,unused-wildcard-import

LANGUAGE_CODE = 'en-us'

PROJECT_NAME = 'multipublik'

open_backup = open
with patch.object(builtins, 'open', mock_open(read_data=b'xxx')):
    with open_backup(os.path.join(os.path.dirname(__file__), '../debian/debian_config_common.py')) as fd:
        exec(fd.read())

# noqa pylint: disable=undefined-variable
DATABASES['default']['NAME'] = hobo.test_utils.get_safe_db_name()

CACHES = {
    'default': {
        'BACKEND': 'hobo.multitenant.cache.TenantCache',
        'REAL_BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    }
}
STATIC_URL = '/static/'

MELLON_IDENTITY_PROVIDERS = []
LOGGING = None

TENANT_SETTINGS_LOADERS = (
    'hobo.multitenant.settings_loaders.TemplateVars',
    'hobo.multitenant.settings_loaders.KnownServices',
    'hobo.multitenant.settings_loaders.CORSSettings',
    'hobo.multitenant.settings_loaders.SharedThemeSettings',
    'hobo.multitenant.settings_loaders.Mellon',
    'hobo.multitenant.settings_loaders.SiteBaseUrl',
    'hobo.multitenant.settings_loaders.CookieNames',
    'hobo.multitenant.settings_loaders.SettingsJSON',
)
