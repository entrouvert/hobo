import os
import shutil
import tempfile

import pytest


@pytest.fixture(scope='function')
def tenants(transactional_db, request, settings):
    from hobo.multitenant.models import Tenant

    base = tempfile.mkdtemp('combo-tenant-base')
    settings.TENANT_BASE = base

    @pytest.mark.django_db
    def make_tenant(name):
        tenant_dir = os.path.join(base, name)
        os.mkdir(tenant_dir)
        t = Tenant(domain_url=name, schema_name=name.replace('-', '_').replace('.', '_'))
        t.create_schema()
        return t

    tenants = [
        make_tenant('tenant1.example.net'),
        make_tenant('hobo2.example.net'),
        make_tenant('hobo3.example.net'),
    ]

    def fin():
        from django.db import connection

        connection.set_schema_to_public()
        for t in tenants:
            if os.path.exists(t.get_directory()):
                t.delete(True)
        shutil.rmtree(base)

    request.addfinalizer(fin)
    return tenants
