class PatchDefaultSettings:
    empty = object()

    def __init__(self, settings, **kwargs):
        self.settings = settings
        self.patch = kwargs
        self.old = {}

    def __enter__(self):
        for key, value in self.patch.items():
            self.old[key] = getattr(self.settings.default_settings, key, self.empty)
            setattr(self.settings.default_settings, key, value)

    def __exit__(self, *args, **kwargs):
        for key, value in self.old.items():
            if value is self.empty:
                delattr(self.settings.default_settings, key)
            else:
                setattr(self.settings.default_settings, key, value)


def patch_default_settings(settings, **kwargs):
    return PatchDefaultSettings(settings, **kwargs)
