from django.db import connection


def test_xforwardedfor(settings, tenants, client):
    settings.USE_X_FORWARDED_FOR = True
    settings.USE_X_FORWARDED_FOR_HEADERS = None
    for tenant in tenants:
        settings.ALLOWED_HOSTS.append(tenant.domain_url)
        response = client.get(
            '/',
            SERVER_NAME=tenant.domain_url,
            HTTP_X_FORWARDED_FOR='99.99.99.99, 127.0.0.1',
            HTTP_X_REAL_IP='5.6.7.8',
        )
        assert '99.99.99.99' in response.content.decode()
    connection.set_schema_to_public()

    settings.USE_X_FORWARDED_FOR_HEADERS = ('X-Real-IP', 'X-Forwarded-For')
    for tenant in tenants:
        response = client.get(
            '/',
            SERVER_NAME=tenant.domain_url,
            HTTP_X_FORWARDED_FOR='99.99.99.99, 127.0.0.1',
            HTTP_X_REAL_IP='5.6.7.8',
        )
        assert '5.6.7.8' in response.content.decode()
    connection.set_schema_to_public()

    settings.USE_X_FORWARDED_FOR = False
    for tenant in tenants:
        response = client.get(
            '/',
            SERVER_NAME=tenant.domain_url,
            HTTP_X_FORWARDED_FOR='99.99.99.99, 127.0.0.1',
            HTTP_X_REAL_IP='5.6.7.8',
        )
        assert '99.99.99.99' not in response.content.decode()
        assert '5.6.7.8' not in response.content.decode()
    connection.set_schema_to_public()
