# hobo - portal to configure and deploy applications
# Copyright (C) 2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import logging.config
import unittest.mock

from hobo.logger import ClampLogLevel, DebugLogFilter, RequestContextFilter


def test_debug_log_filter(settings, tmp_path):
    log_path = tmp_path / 'debug.log'

    settings.DEBUG_LOG = False

    logger = logging.getLogger('test')
    handler = logging.FileHandler(str(log_path))
    handler.setLevel(logging.DEBUG)
    handler.addFilter(DebugLogFilter())
    logger.addHandler(handler)
    logger.propagate = False
    logger.setLevel(logging.DEBUG)

    logger.info('barfoo')
    logger.debug('foobar')
    assert log_path.read_text().count('barfoo') == 1
    assert log_path.read_text().count('foobar') == 0

    settings.DEBUG_LOG = True
    logger.debug('foobar')
    assert log_path.read_text().count('foobar') == 1

    settings.DEBUG_LOG = 'test.foobar,test.foobar2'
    logger.debug('foobar')
    assert log_path.read_text().count('foobar') == 1

    logging.getLogger('test.foobar').debug('foobar')
    logging.getLogger('test.foobar2').debug('foobar')
    logging.getLogger('test.foobar3').debug('foobar')
    assert log_path.read_text().count('foobar') == 3


def test_clamp_log_level(caplog):
    logger = logging.getLogger('django.security.SuspiciousFileOperation')
    logger.addFilter(ClampLogLevel(level='WARNING'))
    logger.error('test')
    assert caplog.records[-1].levelname == 'WARNING'


def test_request_context_filter(caplog, rf):
    request = rf.get('/')

    logger = logging.getLogger('mytest')
    rc_filter = RequestContextFilter()
    logger.addFilter(rc_filter)
    try:
        logger.warning('message')
        record = caplog.records[0]
        caplog.clear()
        assert record.application == 'fake-agent'
        assert record.request_id == '-'
        assert record.session_id == '-'
        assert record.user == '-'
        assert record.user_display_name == '-'
        assert record.user_email == '-'
        assert record.user_uuid == '-'

        logger.warning('message', extra={'request': request})
        record = caplog.records[0]
        caplog.clear()
        assert record.application == 'fake-agent'
        assert record.request_id.startswith('r:')
        assert record.session_id == '-'
        assert record.user == '-'
        assert record.user_display_name == '-'
        assert record.user_email == '-'
        assert record.user_uuid == '-'

        user = unittest.mock.Mock(email='john.doe@example.com', username='john.doe', is_authenticated=True)
        user.saml_identifiers.first.return_value.name_id = '1234'
        user.original_get_full_name.return_value = 'John Doe'
        request.user = user
        request.session = unittest.mock.Mock(_session_key='abcd1234')
        logger.warning('message', extra={'request': request})
        record = caplog.records[0]
        caplog.clear()
        assert record.application == 'fake-agent'
        assert record.request_id.startswith('r:')
        assert record.session_id == 's:abcd'
        assert record.user == 'john.doe'
        assert record.user_display_name == 'John Doe'
        assert record.user_email == 'john.doe@example.com'
        assert record.user_name == 'john.doe'
        assert record.user_uuid == '1234'

        user = unittest.mock.Mock(uuid='45678', is_authenticated=True, spec=['uuid'])
        request.user = user
        logger.warning('message', extra={'request': request})
        record = caplog.records[0]
        caplog.clear()
        assert record.user == '4567'
        assert record.user_uuid == '45678'
    finally:
        logger.removeFilter(rc_filter)
