import importlib
import pickle
from unittest import mock

import pytest

import hobo.multitenant.uwsgidecorators


@pytest.fixture
def uwsgi():
    import sys

    uwsgi = mock.Mock()
    uwsgi.SPOOL_OK = -2
    sys.modules['uwsgi'] = uwsgi
    importlib.reload(hobo.multitenant.uwsgidecorators)
    yield uwsgi
    del sys.modules['uwsgi']
    importlib.reload(hobo.multitenant.uwsgidecorators)


def test_basic():
    @hobo.multitenant.uwsgidecorators.spool
    def function(a, b):
        pass

    function(1, 2)
    with pytest.raises(AttributeError):
        function.spool(1, 2)


def test_mocked_uwsgi(uwsgi):
    @hobo.multitenant.uwsgidecorators.spool
    def function(a, b):
        pass

    function(1, 2)
    function.spool(1, 2)
    assert set(uwsgi.spool.call_args[1].keys()) == {'body', 'tenant', 'name'}
    assert pickle.loads(uwsgi.spool.call_args[1]['body']) == {'args': (1, 2), 'kwargs': {}}
    assert uwsgi.spool.call_args[1]['name'] == b'tests_multitenant.test_uwsgidecorators.function'
    assert uwsgi.spool.call_args[1]['tenant'] == b''


def test_mocked_uwsgi_tenant(uwsgi, tenant):
    from tenant_schemas.utils import tenant_context

    @hobo.multitenant.uwsgidecorators.spool
    def function(a, b):
        pass

    with tenant_context(tenant):
        function.spool(1, 2)

    assert set(uwsgi.spool.call_args[1].keys()) == {'body', 'tenant', 'name'}
    assert pickle.loads(uwsgi.spool.call_args[1]['body']) == {'args': (1, 2), 'kwargs': {}}
    assert uwsgi.spool.call_args[1]['name'] == b'tests_multitenant.test_uwsgidecorators.function'
    assert uwsgi.spool.call_args[1]['tenant'] == b'tenant.example.net'
