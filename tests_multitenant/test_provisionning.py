from hobo.provisionning.utils import NotificationProcessing


def test_truncate_role_name():
    seen = set()
    max_length = NotificationProcessing.group_name_max_length

    assert max_length == 150  # value on Django 2.2

    for length in range(max_length - 10, max_length):
        name = 'a' * length
        truncated = NotificationProcessing.truncate_role_name(name)
        assert len(truncated) == length
        assert truncated not in seen
        seen.add(truncated)

    for length in range(max_length + 1, max_length + 10):
        name = 'a' * length
        truncated = NotificationProcessing.truncate_role_name(name)
        assert len(truncated) == max_length
        assert truncated not in seen
        seen.add(truncated)
