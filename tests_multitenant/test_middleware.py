from hobo.multitenant.middleware import TenantMiddleware


def test_hostname2schema():
    assert TenantMiddleware.hostname2schema('x' * 63) == ('x' * 63)
    shortened = TenantMiddleware.hostname2schema('x' * 64)
    # no more than 63 characters
    assert len(shortened) == 63
    # it's different than just the 63 first characters
    assert shortened != ('x' * 63)
    # but it matches the prefix
    assert shortened[:20] == ('x' * 20)
    # and it matches the suffix
    assert shortened[-20:] == ('x' * 20)


def test_internalipmiddleware(app, tenants, settings):
    settings.INTERNAL_IPS = []
    settings.ALLOWED_HOSTS = ['*']
    settings.DEBUG_PROPAGATE_EXCEPTIONS = False
    app.get('/?raise', status=404)
    response = app.get('/?raise', status=500, extra_environ={'HTTP_HOST': tenants[0].domain_url})
    assert 'Server Error (500)' in response.text

    settings.INTERNAL_IPS = ['127.0.0.1']

    response = app.get('/?raise', status=500, extra_environ={'HTTP_HOST': tenants[0].domain_url})
    assert 'seeing this error because you have' in response.text


def test_samesite_settings(app, tenants, settings):
    settings.ALLOWED_HOSTS = [tenants[0].domain_url]
    response = app.get('/', extra_environ={'HTTP_HOST': tenants[0].domain_url})
    assert ' SameSite=None;' in response.headers['Set-Cookie']
    app.cookiejar.clear()
    settings.CSRF_COOKIE_SAMESITE = 'Lax'
    response = app.get('/', extra_environ={'HTTP_HOST': tenants[0].domain_url})
    assert ' SameSite=Lax;' in response.headers['Set-Cookie']
