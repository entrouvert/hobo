import builtins
import os
from unittest.mock import mock_open, patch

import hobo.test_utils

LANGUAGE_CODE = 'en-us'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.sessions',
    'django.contrib.contenttypes',
    'mellon',
    'hobo.environment',
    'hobo.profile',
)

PROJECT_NAME = 'fake-agent'

MIDDLEWARE = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]


open_backup = open
with patch.object(builtins, 'open', mock_open(read_data=b'xxx')):
    with open_backup(os.path.join(os.path.dirname(__file__), '../debian/debian_config_common.py')) as fd:
        exec(fd.read())

# noqa pylint: disable=undefined-variable
DATABASES['default']['NAME'] = hobo.test_utils.get_safe_db_name()

TENANT_APPS = (
    'django.contrib.auth',
    'django.contrib.sessions',
    'django.contrib.contenttypes',
    'hobo.agent.common',
    'mellon',
)

ROOT_URLCONF = 'hobo.test_urls'
CACHES = {
    'default': {
        'BACKEND': 'hobo.multitenant.cache.TenantCache',
        'REAL_BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    }
}
STATIC_URL = '/static/'

MELLON_IDENTITY_PROVIDERS = []
LOGGING = None

UPDATE_ME = {'x': 1}
EXTEND_ME = [1]

THEMES_DIRECTORY = os.path.join(os.path.dirname(__file__), 'files')

TENANT_SETTINGS_LOADERS = (
    'hobo.multitenant.settings_loaders.TemplateVars',
    'hobo.multitenant.settings_loaders.KnownServices',
    'hobo.multitenant.settings_loaders.CORSSettings',
    'hobo.multitenant.settings_loaders.SharedThemeSettings',
    'hobo.multitenant.settings_loaders.Mellon',
    'hobo.multitenant.settings_loaders.SiteBaseUrl',
    'hobo.multitenant.settings_loaders.CookieNames',
    'hobo.multitenant.settings_loaders.SettingsJSON',
    'hobo.multitenant.settings_loaders.SettingsVars',
    'hobo.multitenant.settings_loaders.LegacyURLSSettings',
)

GLOBAL1 = 0
GLOBAL2 = [1, 2, 3]
GLOBAL3 = {'z': 1}

OVERRIDE1 = 0
OVERRIDE2 = [1, 2, 3]
OVERRIDE3 = {'z': 1}
