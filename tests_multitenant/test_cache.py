from unittest import mock

from django.core.cache import cache

from hobo.multitenant.cache import MemcacheServerError


def test_memcache_error_log(settings, caplog):
    with mock.patch('django.core.cache.backends.locmem.LocMemCache.set', side_effect=MemcacheServerError):
        settings.DEBUG_CACHE_FAILURE_LOG = False
        cache.set('test', 1)
    assert caplog.messages == []

    with mock.patch('django.core.cache.backends.locmem.LocMemCache.set', side_effect=MemcacheServerError):
        settings.DEBUG_CACHE_FAILURE_LOG = True
        cache.set('test', 1)
    assert caplog.messages == ['cache failure']
