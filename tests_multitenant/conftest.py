import pytest

from hobo.multitenant.apps import clear_tenants_settings


@pytest.fixture
def make_tenant(tmp_path, transactional_db, settings, request):
    import json

    from hobo.multitenant.models import Tenant

    base = tmp_path / 'combo-tenant-base'
    base.mkdir()
    settings.TENANT_BASE = str(base)

    def make_tenant(name):
        tenant_dir = base / name
        tenant_dir.mkdir()
        with (tenant_dir / 'settings.json').open(mode='w') as fd:
            json.dump(
                {
                    'HOBO_TEST_VARIABLE': name,
                    'UPDATE_ME.update': {'y': 2},
                    'EXTEND_ME.extend': [2],
                },
                fd,
            )
        with (tenant_dir / 'hobo.json').open(mode='w') as fd:
            json.dump(
                {
                    'variables': {
                        'hobo_test_variable': True,
                        'other_variable': 'foo',
                        'theme': 'publik',
                        'SETTING_GLOBAL1': True,
                        'SETTING_GLOBAL2.extend': [2, 3, 4],
                        'SETTING_GLOBAL3.update': {'x': 1, 'y': 2},
                        'SETTING_OVERRIDE1': False,
                        'SETTING_OVERRIDE2.extend': [6, 7, 8],
                        'SETTING_OVERRIDE3.update': {'a': 1, 'b': 2},
                    },
                    'services': [
                        {
                            'slug': 'test',
                            'title': 'Test',
                            'service-id': 'welco',
                            'this': True,
                            'secret_key': '12345',
                            'base_url': 'https://%s' % name,
                            'saml-sp-metadata-url': 'https://%s/saml/metadata' % name,
                            'variables': {
                                'other_variable': 'bar',
                                'SETTING_OVERRIDE1': True,
                                'SETTING_OVERRIDE2.extend': [name, 7, 8],
                                'SETTING_OVERRIDE3.update': {'a': name, 'b': 2},
                                'SETTING_LOCAL1': False,
                                'SETTING_LOCAL2.extend': [name, 7, 8],
                                'SETTING_LOCAL3.update': {'a': name, 'b': 2},
                            },
                        },
                        {
                            'slug': 'slug-with-hyphen',
                            'title': 'Hyphen',
                            'base_url': 'http://slug-with-hyphen.example.net',
                        },
                        {
                            'slug': 'other',
                            'title': 'Other',
                            'secret_key': 'abcde',
                            'service-id': 'authentic',
                            'base_url': 'http://other.example.net',
                            'legacy_urls': [{'base_url': 'http://olda2.example.net'}],
                        },
                        {
                            'slug': 'another',
                            'title': 'Agent portal',
                            'service-id': 'combo',
                            'template_name': '...portal-agent...',
                            'base_url': 'http://portal-agent.example.net',
                        },
                        {
                            'slug': 'another2',
                            'title': 'User portal',
                            'service-id': 'combo',
                            'template_name': '...portal-user...',
                            'base_url': 'http://portal-user.example.net',
                        },
                        {
                            'slug': 'payment',
                            'title': 'Lingo',
                            'service-id': 'lingo',
                            'secret_key': '123456',
                            'base_url': 'https://payment.example.net',
                        },
                    ],
                },
                fd,
            )
        t = Tenant(domain_url=name, schema_name=name.replace('-', '_').replace('.', '_'))
        t.create_schema()

        def drop_tenant():
            from django.db import connection

            from hobo.multitenant.middleware import TenantMiddleware, TenantNotFound

            connection.set_schema_to_public()
            try:
                TenantMiddleware.get_tenant_by_hostname(t.domain_url)
            except TenantNotFound:
                pass
            else:
                t.delete(True)

        request.addfinalizer(drop_tenant)
        return t

    return make_tenant


@pytest.fixture
def tenants(make_tenant):
    clear_tenants_settings()
    return [make_tenant('tenant1.example.net'), make_tenant('tenant2.example.net')]


@pytest.fixture
def tenant(make_tenant):
    clear_tenants_settings()
    return make_tenant('tenant.example.net')


@pytest.fixture
def app(request):
    import django_webtest

    wtm = django_webtest.WebTestMixin()
    wtm._patch_settings()
    yield django_webtest.DjangoTestApp()
    wtm._unpatch_settings()
