import pytest

pytestmark = pytest.mark.django_db


def test_user_creation(tenants):
    from django.contrib.auth import models
    from tenant_schemas.utils import tenant_context

    for tenant in tenants:
        with tenant_context(tenant):
            models.User.objects.create(username=tenant.domain_url)
            assert models.User.objects.count() == 1
    for tenant in tenants:
        with tenant_context(tenant):
            assert models.User.objects.get().username == tenant.domain_url
