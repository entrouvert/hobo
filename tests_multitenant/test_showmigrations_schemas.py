import pytest
from django.core.management import call_command

from .test_create_tenant import schema_exists


@pytest.fixture(autouse=True)
def configuration(settings, tmpdir):
    settings.TENANT_BASE = str(tmpdir.mkdir('tenants'))


def test_showmigrations_schemas(db, capsys):
    assert not schema_exists('www_example_com')
    call_command('create_tenant', 'www.example.com')

    call_command('showmigrations_schemas', 'common')
    captured = capsys.readouterr()
    assert '[X] 0001_initial' in captured.out
    assert '[X] 0002_auto_20160105_1702' in captured.out

    call_command('migrate_schemas', 'common', '0001_initial')
    call_command('showmigrations_schemas')
    captured = capsys.readouterr()
    assert '[X] 0001_initial' in captured.out
    assert '[ ] 0002_auto_20160105_1702' in captured.out

    call_command('migrate_schemas', 'common')
    call_command('showmigrations_schemas')
    captured = capsys.readouterr()
    assert '[X] 0001_initial' in captured.out
    assert '[X] 0002_auto_20160105_1702' in captured.out
