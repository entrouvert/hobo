import functools
import json
import logging

from django.db import close_old_connections, connection
from uwsgidecorators import spool

# noqa pylint: disable=unused-import
import hobo.multitenant.settings_loaders  # this will get imported via importlib but fail for some reason in a uwsgi job
from hobo.provisionning.utils import NotificationProcessing


def ensure_db(func):
    """Emulate Django"s setup/teardown of database connections before/after
    each request"""

    @functools.wraps(func)
    def f(*args, **kwargs):
        close_old_connections()
        try:
            return func(*args, **kwargs)
        finally:
            close_old_connections()

    return f


def set_connection(domain):
    from hobo.multitenant.middleware import TenantMiddleware

    tenant = TenantMiddleware.get_tenant_by_hostname(domain)
    connection.set_tenant(tenant)


@spool
@ensure_db
def provision(args):
    try:
        set_connection(args['domain'])
        NotificationProcessing.provision(
            object_type=args['object_type'],
            issuer=args['issuer'],
            action=args['action'],
            data=json.loads(args['body']),
            full=bool(args['full'] == 'true'),
        )
    except Exception:
        # we need to catch every exceptions otherwise the task will be re scheduled for ever and ever
        logging.getLogger(__name__).exception('provisionning failed')
