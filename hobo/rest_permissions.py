# hobo - portal to configure and deploy applications
# Copyright (C) 2015-2024 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.conf import settings
from rest_framework import permissions
from rest_framework.exceptions import PermissionDenied

from .rest_authentication import APIClientUser


class PermissionContextError(PermissionDenied):
    pass


class APIClientBasePermission:
    @staticmethod
    def is_apiclient(user):
        return isinstance(user, APIClientUser)

    @staticmethod
    def this_service():
        for services in settings.KNOWN_SERVICES.values():
            for slug, service in services.items():
                if service.get('this', False):
                    return slug, service
        raise PermissionContextError('No active service')

    @classmethod
    def is_service_superuser(cls, user):
        service_slug, service = cls.this_service()
        ou_slug = service.get('variables', {}).get('ou-slug', None)

        if not ou_slug:
            if not user.service_superuser:
                return False
            if len(user.service_superuser.keys()) > 1:
                raise PermissionContextError('Multiple OU in permissions but no OU defined in service')
            ou_slug = list(user.service_superuser.keys())[0]

        service_superuser = user.service_superuser.get(ou_slug, {})
        is_service_superuser = service_superuser.get(service_slug, False)
        return is_service_superuser is True


class IsAPIClient(APIClientBasePermission, permissions.BasePermission):
    def has_permission(self, request, view):
        return self.is_apiclient(request.user)

    def has_object_permission(self, request, view, obj):
        return self.is_apiclient(request.user)


class IsAdminUser(APIClientBasePermission, permissions.IsAdminUser):
    def has_permission(self, request, view):
        if self.is_apiclient(request.user):
            if self.is_service_superuser(request.user):
                return True
        else:
            return super().has_permission(request, view)

    def has_object_permission(self, request, view, obj):
        return self.has_permission(request, view)


class RoleBasePermission(APIClientBasePermission, permissions.IsAuthenticated):
    get_object_func = None
    group_attr_name = None

    @classmethod
    def user_in_group(cls, user, group):
        if cls.is_apiclient(user):
            return group.role.uuid in user.roles
        groups = getattr(user, 'groups', False)
        return groups and group.id in (g.id for g in groups.all())

    @classmethod
    def _get_groups(cls, obj):
        if cls.get_object_func:
            perm_objs = cls.get_object_func(obj)
        else:
            perm_objs = [obj]

        groups = []
        for perm_obj in perm_objs:
            if not hasattr(perm_obj, cls.group_attr_name):
                raise PermissionContextError(
                    'Object do not have any attribute named %s : not able to determine permission groups'
                    % cls.group_attr_name
                )
            groups.append(getattr(perm_obj, cls.group_attr_name))
        return groups

    def has_object_permission(self, request, view, obj):
        for group in self._get_groups(obj):
            if not group or not self.user_in_group(request.user, group):
                break
        else:
            return True
        return False


def role_permission(group_attr_name, get_object_func=None):
    class _RolePerm(RoleBasePermission):
        pass

    _RolePerm.group_attr_name = group_attr_name
    _RolePerm.get_object_func = get_object_func
    return _RolePerm
