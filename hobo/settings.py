"""
Django settings file; it loads the default settings, and local settings
(from a local_settings.py file, or a configuration file set in the
HOBO_SETTINGS_FILE environment variable).

The local settings file should exist, at least to set a suitable SECRET_KEY,
and to disable DEBUG mode in production.
"""

import os

from django.conf import global_settings
from django.utils.translation import gettext_lazy as _

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'hc^g)m7+*n+!8ej5i4*5iiv21s-y#+lpgje1w8d1jw5cyd+g%s'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

HOBO_VALIDATE_EMAIL_WITH_SMTP = True
ALLOWED_SPF_RECORDS = []
# EMAIL_FROM_ALLOWED_DOMAINS: list of allowed domains for default_from_email.
# Use ['*'] to allow all domains.
# Note: all get_operational_services() url domains are always allowed
EMAIL_FROM_ALLOWED_DOMAINS = []

# Application definition

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'mellon',
    'gadjo',
    'sorl.thumbnail',
    'hobo.applications',
    'hobo.debug',
    'hobo.environment',
    'hobo.maintenance',
    'hobo.matomo',
    'hobo.profile',
    'hobo.security',
    'hobo.seo',
    'hobo.theme',
    'hobo.emails',
    'hobo.deploy',
    'hobo.sms',
)

MIDDLEWARE = (
    'hobo.middleware.xforwardedfor.XForwardedForMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'hobo.middleware.utils.StoreRequestMiddleware',
)

# Templates
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'hobo', 'templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.request',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'hobo.context_processors.template_vars',
                'hobo.context_processors.hobo_json',
                'publik_django_templatetags.wcs.context_processors.cards',
            ],
            'builtins': [
                'publik_django_templatetags.publik.templatetags.publik',
            ],
        },
    },
]

ROOT_URLCONF = 'hobo.urls'

WSGI_APPLICATION = 'hobo.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True


USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

STATICFILES_FINDERS = list(global_settings.STATICFILES_FINDERS) + ['gadjo.finders.XStaticFinder']

STATICFILES_DIRS = (os.path.join(BASE_DIR, 'hobo', 'static'),)

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'

# from solr.thumbnail -- https://sorl-thumbnail.readthedocs.io/en/latest/reference/settings.html
THUMBNAIL_PRESERVE_FORMAT = True
THUMBNAIL_FORCE_OVERWRITE = False

LOCALE_PATHS = (os.path.join(BASE_DIR, 'hobo', 'locale'),)

AUTHENTICATION_BACKENDS = (
    'mellon.backends.SAMLBackend',
    'django.contrib.auth.backends.ModelBackend',
)

# SERVICE_TEMPLATES: possible "flavours" for the various service types.
# This variable expects a dictionary mapping service identifiers
# to a list of (template name, template title) tuples.
#
# Note: Template names are opaque identifiers, it's up to the deployment
# agents to assign some meaning to them.
#
# Example:
#    SERVICE_TEMPLATES = {
#        'wcs': [('export-auquo-light.wcs', u'Au quotidien light'),
#                ('export-auquo.wcs', u'Au quotidien'),
#                ('export-demo.wcs', u'Au quotidien Demo')],
#    }
SERVICE_TEMPLATES = {
    'combo': [
        ('', ''),
        ('portal-user', _('User Portal')),
        ('portal-agent', _('Agent Portal')),
    ]
}

# SERVICE_EXTRA_VARIABLES: variables to create automatically for the
# given service types; they can be simple strings, or a dictionary with
# name and label keys.
#
# Example:
#    SERVICE_EXTRA_VARIABLES = {
#        'wcs': ['legal_url', 'commune_url', 'domain_key'],
#        'combo': [{'name': 'theme', 'label': 'Theme'}],
#    }
SERVICE_EXTRA_VARIABLES = None

# AMQP message broker (rabbitmq is recommended)
BROKER_URL = 'amqp://'
# Task expiry time, as seconds after task publish
# http://docs.celeryproject.org/en/latest/userguide/calling.html#expiration
BROKER_TASK_EXPIRES = 600

LOGIN_REDIRECT_URL = '/'
LOGIN_URL = '/login/'
LOGOUT_URL = '/logout/'

# timeout used in python-requests call, in seconds
# we use 28s by default: timeout just before web server, which is usually 30s
REQUESTS_TIMEOUT = 28

# mellon authentication params
MELLON_ADAPTER = ('hobo.utils.MellonAdapter',)

MELLON_ATTRIBUTE_MAPPING = {
    'email': '{attributes[email][0]}',
    'first_name': '{attributes[first_name][0]}',
    'last_name': '{attributes[last_name][0]}',
}

MELLON_SUPERUSER_MAPPING = {
    'is_superuser': 'true',
}

MELLON_ADD_AUTHNREQUEST_NEXT_URL_EXTENSION = True

THEMES_DIRECTORY = '/usr/share/publik/themes/'

MELLON_USERNAME_TEMPLATE = '{attributes[name_id_content]}'

# MATOMO_SERVER: allow automatic configuration on a matomo server.
# This variable excepts:
# - the URL of the matomo server to connect
# - an authentication token for a matomo admin user
# - an email template for new emails associated with new matomo users
# The token is available on the matomo GUI into the personal parameters
# of the user.
#
# Example:
# MATOMO_SERVER = {
#     'URL': 'https://matomo.domain.org',
#     'TOKEN_AUTH': '0123456789abcdef0123456789abcdef',
#     'EMAIL_TEMPLATE': 'noreply+%(user_login)s@domain.org'
# }

MATOMO_SERVER = {}

# List of service class names to hide from the create service menu.
HOBO_SERVICES_DISABLED = [
    'bijoe',
    'fargo',
    'lingo',
    'welco',
]
# List of service to show in the create service menu, it overrides HOBO_SERVICES_DISABLED.
HOBO_SERVICES_ENABLED = []

# Phone prefixes by country for phone number as authentication identifier
PHONE_COUNTRY_CODES = {
    '32': {'region': 'BE', 'region_desc': _('Belgium')},
    '33': {'region': 'FR', 'region_desc': _('Metropolitan France')},
    '262': {'region': 'RE', 'region_desc': _('Réunion')},
    '508': {'region': 'PM', 'region_desc': _('Saint Pierre and Miquelon')},
    '590': {'region': 'GP', 'region_desc': _('Guadeloupe')},
    '594': {'region': 'GF', 'region_desc': _('French Guiana')},
    '596': {'region': 'MQ', 'region_desc': _('Martinique')},
}

local_settings_file = os.environ.get(
    'HOBO_SETTINGS_FILE', os.path.join(os.path.dirname(__file__), 'local_settings.py')
)
if os.path.exists(local_settings_file):
    with open(local_settings_file) as f:
        exec(f.read())
