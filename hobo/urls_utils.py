# Decorating URL includes, <https://djangosnippets.org/snippets/2532/>

try:
    from django.urls import URLPattern, URLResolver
except ImportError:
    # django < 2.0 compatibility
    from django.urls import RegexURLPattern as URLPattern
    from django.urls import RegexURLResolver as URLResolver


class DecoratedURLPattern(URLPattern):
    def resolve(self, *args, **kwargs):
        result = super().resolve(*args, **kwargs)
        if result:
            result.func = self._decorate_with(result.func)
        return result


class DecoratedRegexURLResolver(URLResolver):
    def resolve(self, *args, **kwargs):
        result = super().resolve(*args, **kwargs)
        if result:
            result.func = self._decorate_with(result.func)
        return result


def decorated_includes(func, includes, *args, **kwargs):
    urlconf_module, app_name, namespace = includes

    for item in urlconf_module:
        if isinstance(item, URLPattern):
            item.__class__ = DecoratedURLPattern
            item._decorate_with = func

        elif isinstance(item, URLResolver):
            item.__class__ = DecoratedRegexURLResolver
            item._decorate_with = func

    return urlconf_module, app_name, namespace
