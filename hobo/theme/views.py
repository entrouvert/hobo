# hobo - portal to configure and deploy applications
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import colorsys
import random

from django.contrib import messages
from django.urls import reverse
from django.utils.translation import gettext as _
from django.views.generic import RedirectView, TemplateView

from hobo.environment.forms import VariablesFormMixin

from .forms import ThemeOptionsForm
from .utils import get_selected_theme, get_theme, get_themes, set_theme


class HomeView(TemplateView):
    template_name = 'hobo/theme_home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        def hsv(theme):
            theme_colour = theme.get('variables').get('theme_color') or '#FFFFFF'
            if theme_colour == '#FFFFFF':
                return (1, 1, 1)
            theme_rgb = [int(x, 16) for x in (theme_colour[1:3], theme_colour[3:5], theme_colour[5:])]
            theme_hsv = colorsys.rgb_to_hsv(*(1.0 * x / 255 for x in theme_rgb))
            return theme_hsv

        context['themes'] = get_themes()
        if random.random() < 0.1:
            # easter egg, sometimes it gets sorted by colour
            context['themes'].sort(key=hsv)
        context['selected_theme'] = get_theme(get_selected_theme())

        return context


home = HomeView.as_view()


class SelectView(RedirectView):
    permanent = False

    def get_redirect_url(self):
        if self.request.method == 'POST' and 'theme' in self.request.POST:
            set_theme(self.request.POST['theme'])
            messages.info(
                self.request, _('The theme has been changed, it will soon be visible on the sites.')
            )
        return reverse('theme-home')


select = SelectView.as_view()


class OptionsView(VariablesFormMixin, TemplateView):
    template_name = 'hobo/theme_options.html'
    variables = ['global_title']
    form_class = ThemeOptionsForm


options = OptionsView.as_view()
