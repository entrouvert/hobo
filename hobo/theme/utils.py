# hobo - portal to configure and deploy applications
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import os
import unicodedata

from django.conf import settings


def get_themes():
    themes = []
    if not getattr(settings, 'THEMES_DIRECTORY', None):
        return []
    if not os.path.exists(settings.THEMES_DIRECTORY):
        return []
    for dirname in os.listdir(settings.THEMES_DIRECTORY):
        filename = os.path.join(settings.THEMES_DIRECTORY, dirname, 'themes.json')
        if not os.path.exists(filename):
            continue
        with open(filename) as fd:
            themes_data = json.load(fd)
        if not isinstance(themes_data, dict):
            themes_data = {'themes': themes_data}
        for theme in themes_data['themes']:
            if not 'module' in theme:
                theme['module'] = dirname
            themes.append(theme)

    def key_function(theme):
        label = theme.get('label')
        return unicodedata.normalize('NFKD', label.upper()).encode('ascii', 'ignore').decode()

    themes.sort(key=key_function)
    return themes


def get_selected_theme():
    from hobo.environment.models import Variable

    try:
        selected_theme = Variable.objects.get(name='theme', service_pk__isnull=True).value
    except Variable.DoesNotExist:
        selected_theme = None
    return selected_theme


def set_theme(theme_id):
    from hobo.environment.models import Variable

    selected_theme, dummy = Variable.objects.get_or_create(name='theme', service_pk__isnull=True)
    old_theme = get_theme(selected_theme.value) or {}
    old_variables = old_theme.get('variables', {})
    theme = get_theme(theme_id)
    for variable in theme.get('variables', {}).keys():
        theme_variable, dummy = Variable.objects.get_or_create(name=variable, service_pk__isnull=True)
        theme_variable.auto = True
        if isinstance(theme['variables'][variable], str):
            theme_variable.value = theme['variables'][variable]
        else:
            theme_variable.value = json.dumps(theme['variables'][variable])
        theme_variable.save()
        if variable in old_variables:
            del old_variables[variable]

    for old_variable in old_variables.keys():
        Variable.objects.filter(name=old_variable, service_pk__isnull=True).update(value='')

    selected_theme.value = theme_id
    selected_theme.auto = True
    selected_theme.save()


def get_theme(theme_id):
    themes = get_themes()
    for theme in themes:
        if theme.get('id') != theme_id:
            continue
        return theme
    return None
