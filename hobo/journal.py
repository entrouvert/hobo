#
#
#  Copyright 2012 David Strauss <david@davidstrauss.net>
#  Copyright 2012 Zbigniew Jędrzejewski-Szmek <zbyszek@in.waw.pl>
#  Copyright 2012 Marti Raudsepp <marti@juffo.org>
#
#  python-systemd is free software; you can redistribute it and/or modify it
#  under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation; either version 2.1 of the License, or
#  (at your option) any later version.
#
#  python-systemd is distributed in the hope that it will be useful, but
#  WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public License
#  along with python-systemd; If not, see <http://www.gnu.org/licenses/>.

import logging as _logging
import sys as _sys
import traceback as _traceback
from syslog import LOG_ALERT, LOG_CRIT, LOG_DEBUG, LOG_ERR, LOG_INFO, LOG_WARNING

try:
    # more recent pythong binding is named cysystemd
    from cysystemd import journal  # noqa pylint: disable=import-error
except ImportError:
    from systemd import journal  # noqa pylint: disable=import-error

_IDENT_CHARACTER = set('ABCDEFGHIJKLMNOPQRTSUVWXYZ_0123456789')


def _valid_field_name(s):
    return not (set(s) - _IDENT_CHARACTER)


class JournalHandler(_logging.Handler):
    """Journal handler class for the Python logging framework.

    Please see the Python logging module documentation for an overview:
    http://docs.python.org/library/logging.html.

    To create a custom logger whose messages go only to journal:

    >>> import logging
    >>> log = logging.getLogger('custom_logger_name')
    >>> log.propagate = False
    >>> log.addHandler(JournalHandler())
    >>> log.warning("Some message: %s", 'detail')

    Note that by default, message levels `INFO` and `DEBUG` are ignored by the
    logging framework. To enable those log levels:

    >>> log.setLevel(logging.DEBUG)

    To redirect all logging messages to journal regardless of where they come
    from, attach it to the root logger:

    >>> logging.root.addHandler(JournalHandler())

    For more complex configurations when using `dictConfig` or `fileConfig`,
    specify `systemd.journal.JournalHandler` as the handler class.  Only
    standard handler configuration options are supported: `level`, `formatter`,
    `filters`.

    To attach journal MESSAGE_ID, an extra field is supported:

    >>> import uuid
    >>> mid = uuid.UUID('0123456789ABCDEF0123456789ABCDEF')
    >>> log.warning("Message with ID", extra={'MESSAGE_ID': mid})

    Fields to be attached to all messages sent through this handler can be
    specified as keyword arguments. This probably makes sense only for
    SYSLOG_IDENTIFIER and similar fields which are constant for the whole
    program:

    >>> JournalHandler(SYSLOG_IDENTIFIER='my-cool-app')
    <...JournalHandler ...>

    The following journal fields will be sent: `MESSAGE`, `PRIORITY`,
    `THREAD_NAME`, `CODE_FILE`, `CODE_LINE`, `CODE_FUNC`, `LOGGER` (name as
    supplied to getLogger call), `MESSAGE_ID` (optional, see above),
    `SYSLOG_IDENTIFIER` (defaults to sys.argv[0]).

    The function used to actually send messages can be overridden using
    the `sender_function` parameter.
    """

    def __init__(self, level=_logging.NOTSET, **kwargs):
        super().__init__(level)

        for name in kwargs:
            if not _valid_field_name(name):
                raise ValueError('Invalid field name: ' + name)
        if 'SYSLOG_IDENTIFIER' not in kwargs:
            kwargs['SYSLOG_IDENTIFIER'] = _sys.argv[0]

        self._extra = kwargs

    def emit(self, record):
        """Write `record` as a journal event.

        MESSAGE is taken from the message provided by the user, and PRIORITY,
        LOGGER, THREAD_NAME, CODE_{FILE,LINE,FUNC} fields are appended
        automatically. In addition, record.MESSAGE_ID will be used if present.
        """
        try:
            msg = self.format(record)
            pri = self.map_priority(record.levelno)
            # defaults
            extras = self._extra.copy()

            # higher priority
            if record.exc_text:
                extras['EXCEPTION_TEXT'] = record.exc_text

            if record.exc_info:
                extras['EXCEPTION_INFO'] = record.exc_info

            if record.args:
                extras['CODE_ARGS'] = str(record.args)

            # explicit arguments — highest priority
            for key, value in record.__dict__.items():
                new_key = key.upper()
                if new_key in [
                    'PRIORITY',
                    'LOGGER',
                    'THREAD_NAME',
                    'PROCESS_NAME',
                    'CODE_FILE',
                    'MESSAGE',
                    'CODE_LINE',
                    'CODE_FUNC',
                ]:
                    continue
                if key in ['threadName', 'processName', 'pathname', 'lineno', 'funcName']:
                    continue
                # prevent logging of WSGIRequest object which attached to the log record
                if not isinstance(value, (str, int)):
                    continue
                extras[key.upper()] = value

            journal.send(
                MESSAGE=msg,
                PRIORITY=format(pri),
                LOGGER=record.name,
                THREAD_NAME=record.threadName,
                PROCESS_NAME=record.processName,
                CODE_FILE=record.pathname,
                CODE_LINE=record.lineno,
                CODE_FUNC=record.funcName,
                **extras,
            )
        except Exception:
            self.handleError(record)

    @staticmethod
    def map_priority(levelno):
        """Map logging levels to journald priorities.

        Since Python log level numbers are "sparse", we have to map numbers in
        between the standard levels too.
        """
        if levelno <= _logging.DEBUG:
            return LOG_DEBUG
        elif levelno <= _logging.INFO:
            return LOG_INFO
        elif levelno <= _logging.WARNING:
            return LOG_WARNING
        elif levelno <= _logging.ERROR:
            return LOG_ERR
        elif levelno <= _logging.CRITICAL:
            return LOG_CRIT
        else:
            return LOG_ALERT

    mapPriority = map_priority
