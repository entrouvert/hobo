# hobo - portal to configure and deploy applications
# Copyright (C) 2015-2020 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.utils.translation import gettext as _
from django.views.generic import TemplateView

from hobo.environment.forms import VariablesFormMixin

from .forms import SMSForm


class HomeView(VariablesFormMixin, TemplateView):
    template_name = 'hobo/sms_home.html'
    variables = ['sms_url', 'sms_sender', 'local_country_code']
    form_class = SMSForm
    success_message = _('SMS settings have been updated. It will take a few seconds to be effective.')


home = HomeView.as_view()
