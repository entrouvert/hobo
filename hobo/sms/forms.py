# hobo - portal to configure and deploy applications
# Copyright (C) 2015-2020 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from django import forms
from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.utils.translation import gettext_lazy as _

from hobo.utils import TemplateError, get_templated_url


class SMSForm(forms.Form):
    sms_url = forms.CharField(
        label=_('SMS URL'),
        help_text=_('URL that can receive POST data to send SMS.'),
        required=False,
    )
    sms_sender = forms.CharField(
        label=_('Sender'),
        max_length=11,
        validators=[
            RegexValidator(
                '^[A-Za-z0-9_ .]{0,11}$', _('Only alphanumeric characters, dots and spaces are allowed.')
            )
        ],
        help_text=_(
            'Sender name or phone number. It must neither exceed 11 characters nor contain special characters.'
        ),
        required=False,
    )
    local_country_code = forms.ChoiceField(
        label=_('Local country code'),
        help_text=_(
            'Local country code, used to identify local mobile phone numbers and '
            'as international prefix while sending SMS messages.'
        ),
        required=False,
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        country_code_choices = [
            (key, f"+{key} ({value['region_desc']})") for key, value in settings.PHONE_COUNTRY_CODES.items()
        ]
        self.fields['local_country_code'].choices = [('', '')] + country_code_choices

    def clean_sms_url(self):
        value = self.cleaned_data['sms_url']
        try:
            get_templated_url(value)
        except TemplateError as e:
            raise ValidationError(_('Invalid template: %(error)s'), params={'error': e})
        return value
