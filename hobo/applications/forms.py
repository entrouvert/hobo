# hobo - portal to configure and deploy applications
# Copyright (C) 2015-2022 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import forms
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _

from hobo.applications.models import Application, Relation, RelationConfig, Version


class GenerateForm(forms.Form):
    number = forms.RegexField(
        label=_('Version Number'),
        max_length=100,
        regex=r'^\d+\.\d+$',
        help_text=_('The version number consists of two numbers separated by a dot. Example: 1.0'),
    )
    notes = forms.CharField(label=_('Version notes'), widget=forms.Textarea, required=False)

    def __init__(self, *args, **kwargs):
        self.latest_version = kwargs.pop('latest_version')
        super().__init__(*args, **kwargs)
        if self.latest_version:
            try:
                old_version = [int(n) for n in self.latest_version.number.split('.')]
            except ValueError:
                old_version = None
            if old_version:
                self.initial['number'] = '.'.join(str(n) for n in old_version[:2])
            self.initial['notes'] = self.latest_version.notes

    def clean_number(self):
        number = self.cleaned_data['number']
        if not self.latest_version:
            return number
        try:
            old_number = [int(n) for n in self.latest_version.number.split('.')]
        except ValueError:
            return number
        new_number = [int(n) for n in self.cleaned_data['number'].split('.')]
        if old_number[:2] > new_number:
            raise forms.ValidationError(
                _('The version number must be equal to or greater than the previous one.')
            )
        return number

    def get_cleaned_number(self):
        number = [int(n) for n in self.cleaned_data['number'].split('.')]
        number.append(int(now().strftime('%Y%m%d')))
        if not self.latest_version:
            return '%s.%s.%s.0' % tuple(number)
        try:
            old_number = [int(n) for n in self.latest_version.number.split('.')]
        except ValueError:
            return '%s.%s.%s.0' % tuple(number)
        if number != old_number[:3]:
            return '%s.%s.%s.0' % tuple(number)
        last_part = old_number[3]
        return '%s.%s.%s.%s' % (*number, last_part + 1)


class InstallForm(forms.Form):
    bundle = forms.FileField(label=_('Application'))


class MetadataForm(forms.ModelForm):
    class Meta:
        model = Application
        fields = ['name', 'slug', 'description', 'documentation_url', 'authors', 'license', 'icon', 'visible']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['icon'].widget.attrs = {'accept': 'image/jpeg,image/png'}

    def clean_slug(self):
        slug = self.cleaned_data['slug']

        if Application.objects.filter(slug=slug).exclude(pk=self.instance.pk).exists():
            raise forms.ValidationError(_('Another application exists with the same identifier.'))

        return slug

    def clean_icon(self):
        value = self.cleaned_data.get('icon')
        if hasattr(value, 'content_type') and value.content_type not in ('image/jpeg', 'image/png'):
            raise forms.ValidationError(_('The icon must be in JPEG or PNG format.'))
        return value


class RelationConfigForm(forms.ModelForm):
    class Meta:
        model = Relation
        fields = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        field_classes = {
            'bool': forms.BooleanField,
        }
        widget_classes = {}
        for field in self.instance.config_option_fields:
            field_class = field_classes[field['field_type']]
            field_name = field['varname'].replace('-', '_')
            field_initial = field['default_value']
            try:
                config_options = RelationConfig.objects.get(
                    application=self.instance.application,
                    element_type=self.instance.element.type,
                    element_slug=self.instance.element.slug,
                ).options
            except RelationConfig.DoesNotExist:
                config_options = {}
            if field['varname'] in config_options:
                field_initial = config_options[field['varname']]
            self.fields[field_name] = field_class(
                label=field['label'],
                help_text=field.get('help_text'),
                required=field.get('required'),
                initial=field_initial,
                widget=widget_classes.get(field['field_type']),
            )

    def clean(self):
        super().clean()
        config_options = {}
        for field in self.instance.config_option_fields:
            field_name = field['varname'].replace('-', '_')
            config_options[field['varname']] = self.cleaned_data.get(field_name)
            if field_name in self.cleaned_data:
                del self.cleaned_data[field_name]
        self.cleaned_data['config_options'] = config_options

    def save(self, *args, **kwargs):
        if 'config_options' in self.cleaned_data:
            config, dummy = RelationConfig.objects.get_or_create(
                application=self.instance.application,
                element_type=self.instance.element.type,
                element_slug=self.instance.element.slug,
            )
            config.options = self.cleaned_data['config_options']
            config.save()
        return self.instance


class VersionSelectForm(forms.Form):
    version = forms.ModelChoiceField(queryset=Version.objects.none(), empty_label=None)

    def __init__(self, *args, **kwargs):
        self.application = kwargs.pop('application')
        self.version = kwargs.pop('version')
        super().__init__(*args, **kwargs)
        self.fields['version'].label = _('Compare version %s to') % self.version.number
        self.fields['version'].queryset = (
            Version.objects.filter(application=self.application)
            .exclude(pk=self.version.pk)
            .order_by('-last_update_timestamp')
        )
