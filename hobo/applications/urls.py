# hobo - portal to configure and deploy applications
# Copyright (C) 2015-2022 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import path, re_path

from . import views

urlpatterns = [
    path('', views.home, name='applications-home'),
    path('create/', views.init, name='application-init'),
    path('install/', views.install, name='application-install'),
    re_path(r'^manifest/(?P<slug>[\w-]+)/delete/$', views.delete, name='application-delete'),
    re_path(r'^manifest/(?P<app_slug>[\w-]+)/$', views.manifest, name='application-manifest'),
    re_path(r'^manifest/(?P<app_slug>[\w-]+)/update/$', views.update, name='application-update'),
    re_path(r'^manifest/(?P<app_slug>[\w-]+)/refresh/$', views.refresh, name='application-refresh'),
    re_path(r'^manifest/(?P<app_slug>[\w-]+)/versions/$', views.versions, name='application-versions'),
    re_path(
        r'^manifest/(?P<app_slug>[\w-]+)/version/(?P<version_pk>\d+)/select/$',
        views.version_select,
        name='application-version-select',
    ),
    re_path(
        r'^manifest/(?P<app_slug>[\w-]+)/version/compare/$',
        views.version_compare,
        name='application-version-compare',
    ),
    re_path(r'^manifest/(?P<app_slug>[\w-]+)/metadata/$', views.metadata, name='application-metadata'),
    re_path(r'^manifest/(?P<app_slug>[\w-]+)/scandeps/$', views.scandeps, name='application-scandeps'),
    re_path(r'^manifest/(?P<app_slug>[\w-]+)/generate/$', views.generate, name='application-generate'),
    re_path(r'^manifest/(?P<app_slug>[\w-]+)/download/$', views.download, name='application-download'),
    re_path(
        r'^manifest/(?P<app_slug>[\w-]+)/download/(?P<version_pk>\d+)/$',
        views.download,
        name='application-download',
    ),
    re_path(
        r'^manifest/(?P<app_slug>[\w-]+)/confirm/(?P<version_pk>\d+)/$',
        views.confirm_install,
        name='application-confirm-install',
    ),
    re_path(
        r'^manifest/(?P<app_slug>[\w-]+)/add/(?P<type>[\w-]+)/$',
        views.add_element,
        name='application-add-element',
    ),
    re_path(
        r'^manifest/(?P<app_slug>[\w-]+)/delete/(?P<pk>\d+)/$',
        views.delete_element,
        name='application-delete-element',
    ),
    re_path(
        r'^manifest/(?P<app_slug>[\w-]+)/origins/(?P<pk>\d+)/$',
        views.element_origins,
        name='application-element-origins',
    ),
    re_path(
        r'^manifest/(?P<app_slug>[\w-]+)/config/(?P<pk>\d+)/$',
        views.element_config,
        name='application-element-config',
    ),
    re_path(
        r'^manifest/(?P<app_slug>[\w-]+)/add-parameter/$',
        views.add_parameter,
        name='application-add-parameter',
    ),
    re_path(
        r'^manifest/(?P<app_slug>[\w-]+)/edit-parameter/(?P<pk>\d+)/$',
        views.edit_parameter,
        name='application-edit-parameter',
    ),
    re_path(
        r'^manifest/(?P<app_slug>[\w-]+)/delete-parameter/(?P<pk>\d+)/$',
        views.delete_parameter,
        name='application-delete-parameter',
    ),
    re_path(
        r'^manifest/(?P<app_slug>[\w-]+)/parameter-value/(?P<name>[\w_]+)/$',
        views.change_parameter_value,
        name='application-change-parameter-value',
    ),
    re_path(
        r'^manifest/(?P<app_slug>[\w-]+)/job/(?P<pk>\d+)/$',
        views.async_job,
        name='application-async-job',
    ),
    re_path(
        r'^manifest/(?P<app_slug>[\w-]+)/job/(?P<pk>\d+)/check-diffs/$',
        views.async_job_diffs,
        name='application-async-job-diffs',
    ),
]
