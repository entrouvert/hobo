# hobo - portal to configure and deploy applications
# Copyright (C) 2015-2022 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import collections
import copy
import io
import json
import os
import sys
import tarfile
import traceback
import urllib.parse

from django.conf import settings
from django.core.files.base import ContentFile
from django.db import connection, models
from django.db.models import JSONField
from django.utils.text import slugify
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _
from django.utils.translation import ngettext

from hobo.deploy.signals import notify_agents
from hobo.environment.models import Variable
from hobo.environment.utils import get_installed_services

from .utils import Requests

requests = Requests()


def get_object_types():
    object_types = []
    for service_id, services in getattr(settings, 'KNOWN_SERVICES', {}).items():
        if service_id not in Application.get_supported_modules():
            continue
        service_objects = {x.get_base_url_path(): x for x in get_installed_services(types=[service_id])}
        for service in services.values():
            if service['url'] not in service_objects:
                continue
            if service_objects[service['url']].secondary:
                continue
            url = urllib.parse.urljoin(service['url'], 'api/export-import/')
            response = requests.get(url)
            if not response.ok:
                continue
            for object_type in response.json()['data']:
                object_type['service'] = service
                object_types.append(object_type)
    return object_types


def get_object_type_index(object_type, types):
    try:
        return types.index(object_type)
    except ValueError:
        return 99999


class ApplicationError(Exception):
    def __init__(self, msg, details=None):
        self.msg = msg
        self.details = details


class ScanError(ApplicationError):
    pass


class DeploymentError(ApplicationError):
    pass


class UnlinkError(ApplicationError):
    pass


class Application(models.Model):
    name = models.CharField(max_length=100, verbose_name=_('Name'))
    slug = models.SlugField(max_length=100, unique=True)
    icon = models.FileField(
        verbose_name=_('Icon'),
        help_text=_(
            'Icon file must be in JPEG or PNG format, and should be a square of at least 512×512 pixels.'
        ),
        upload_to='applications/icons/',
        blank=True,
        null=True,
    )
    description = models.TextField(verbose_name=_('Description'), blank=True)
    documentation_url = models.URLField(_('Documentation URL'), blank=True)
    authors = models.TextField(verbose_name=_('Authors'), blank=True)
    license = models.CharField(
        verbose_name=_('License'),
        max_length=200,
        choices=[
            ('', _('Unspecified')),
            # from trove_classifiers License values
            ('agplv3+', _('GNU Affero General Public License v3 or later (AGPLv3+)')),
            ('gplv3+', _('GNU General Public License v3 or later (GPLv3+)')),
            ('mit', _('MIT License')),
        ],
        blank=True,
    )
    editable = models.BooleanField(default=True)
    visible = models.BooleanField(
        verbose_name=_('Visible'),
        help_text=_('If enabled, the application will be visible in the services where it is installed.'),
        default=True,
    )
    elements = models.ManyToManyField('Element', blank=True, through='Relation')
    creation_timestamp = models.DateTimeField(default=now)
    last_update_timestamp = models.DateTimeField(auto_now=True)

    def __repr__(self):
        return '<Application %s>' % self.slug

    @classmethod
    def get_supported_modules(cls):
        return ['wcs', 'combo', 'chrono', 'lingo']

    def save(self, *args, **kwargs):
        if not self.slug:
            base_slug = slugify(self.name)[:95]
            slug = base_slug
            i = 1
            while Application.objects.filter(slug=slug).exists():
                slug = '%s-%s' % (base_slug, i)
                i += 1
            self.slug = slug
        super().save(*args, **kwargs)

    def refresh_elements(self, cache_only=False):
        if not cache_only:
            self.relation_set.filter(auto_dependency=True).delete()
            for relation in self.relation_set.all():
                relation.origins.clear()
        remote_elements = {}
        relations = self.relation_set.select_related('element')
        elements = {(x.element.type, x.element.slug): (x.element, x) for x in relations}
        current_object_types = {t for t, s in elements}
        for object_type in get_object_types():
            if object_type['id'] not in current_object_types:
                continue
            if not cache_only and object_type.get('minor'):
                continue
            url = object_type['urls']['list']
            response = requests.get(url)
            if not response.ok:
                raise ScanError(
                    _('Failed to get elements of type %s (%s)' % (object_type['id'], response.status_code)),
                    details=response.text,
                )
            remote_elements[object_type['id']] = {x['id']: x for x in response.json()['data']}
        for element, relation in elements.values():
            if not remote_elements.get(element.type):
                continue
            if not remote_elements[element.type].get(element.slug):
                continue
            remote_element = remote_elements[element.type][element.slug]
            relation.reset_error()
            if cache_only:
                if element.cache == remote_element:
                    continue
                element.cache = remote_element
                element.save()
                elements[(element.type, element.slug)] = (element, relation)
                continue
            if element.name == remote_element['text'] and element.cache == remote_element:
                continue
            element.name = remote_element['text']
            element.cache = remote_element
            element.save()
            elements[(element.type, element.slug)] = (element, relation)
        return elements

    def scandeps(self):
        elements = self.refresh_elements()
        dependencies_url_cache = set()
        finished = False
        while not finished:
            finished = True
            for el, rel in list(elements.values()):
                dependencies_url = el.cache['urls'].get('dependencies')
                if not dependencies_url:
                    continue
                if dependencies_url in dependencies_url_cache:
                    continue
                response = requests.get(dependencies_url)
                if not response.ok:
                    rel.set_error_from_http_code(response.status_code)
                    raise ScanError(
                        _('Failed to scan "%s" (type %s, slug %s) dependencies (%s)')
                        % (el.name, el.type, el.slug, response.status_code),
                        details=response.text,
                    )
                rel.reset_error()
                for dependency in response.json()['data']:
                    if (dependency['type'], dependency['id']) in elements:
                        continue
                    finished = False
                    element, created = Element.objects.get_or_create(
                        type=dependency['type'], slug=dependency['id'], defaults={'name': dependency['text']}
                    )
                    element.name = dependency['text']
                    element.cache = dependency
                    element.save()
                    relation, created = Relation.objects.get_or_create(application=self, element=element)
                    if created:
                        relation.auto_dependency = True
                        relation.save()
                    if self.editable:
                        relation.origins.add(rel)
                    elements[(element.type, element.slug)] = (element, relation)
                dependencies_url_cache.add(dependencies_url)
        return elements

    def unlink(self):
        for service_id, services in getattr(settings, 'KNOWN_SERVICES', {}).items():
            if service_id not in Application.get_supported_modules():
                continue
            service_objects = {x.get_base_url_path(): x for x in get_installed_services(types=[service_id])}
            for service in services.values():
                if service['url'] not in service_objects:
                    continue
                if service_objects[service['url']].secondary:
                    continue
                url = urllib.parse.urljoin(service['url'], 'api/export-import/unlink/')
                response = requests.post(url, data={'application': self.slug})
                if not response.ok:
                    raise UnlinkError(
                        _('Failed to unlink application in module %s (%s)')
                        % (service_id, response.status_code)
                    )

    def get_latest_deployed_version(self):
        last_job = (
            AsyncJob.objects.select_related('version')
            .filter(application=self, action__in=('deploy', 'create_bundle'), status='completed')
            .order_by('last_update_timestamp')
            .last()
        )
        return last_job.version if last_job else None


class Parameter(models.Model):
    application = models.ForeignKey(Application, on_delete=models.CASCADE)
    label = models.CharField(max_length=100, verbose_name=_('Label'))
    name = models.CharField(
        max_length=100,
        verbose_name=_('Identifier'),
        help_text=_('Variable name, it is useful to prefix it with an unique application identifier.'),
    )
    default_value = models.CharField(max_length=100, verbose_name=_('Default value'), blank=True, null=True)

    def as_dict(self):
        return {'label': self.label, 'name': self.name, 'default_value': self.default_value}

    @property
    def variable(self):
        variable, _ = Variable.objects.get_or_create(
            name=self.name, defaults={'value': self.default_value or '', 'auto': True, 'label': self.label}
        )
        return variable


class Element(models.Model):
    type = models.CharField(max_length=100, verbose_name=_('Type'))
    slug = models.SlugField(max_length=500, verbose_name=_('Slug'))
    name = models.CharField(max_length=500, verbose_name=_('Name'))
    cache = JSONField(blank=True, default=dict)

    def __repr__(self):
        return '<Element %s/%s>' % (self.type, self.slug)

    def get_redirect_url(self):
        if self.type == 'roles':
            return
        if not self.cache.get('urls'):
            return
        if self.cache['urls'].get('redirect'):
            return self.cache['urls']['redirect']
        if self.cache['urls'].get('export'):
            return '%sredirect/' % self.cache['urls']['export']


class Relation(models.Model):
    application = models.ForeignKey(Application, on_delete=models.CASCADE)
    element = models.ForeignKey(Element, on_delete=models.CASCADE)
    auto_dependency = models.BooleanField(default=False)
    origins = models.ManyToManyField('self', blank=True, symmetrical=False)
    error = models.BooleanField(default=False)
    error_status = models.CharField(
        max_length=100,
        choices=[
            ('notfound', _('Not Found')),
            ('error', _('Error')),
            ('not-installed', _('Not installed')),
        ],
        null=True,
    )

    def __repr__(self):
        return '<Relation %s - %s/%s>' % (self.application.slug, self.element.type, self.element.slug)

    def set_error(self, status):
        self.error = True
        self.error_status = status
        self.save()

    def set_error_from_http_code(self, http_status_code):
        self.set_error('notfound' if http_status_code == 404 else 'error')

    def reset_error(self):
        self.error = False
        self.error_status = None
        self.save()


class RelationConfig(models.Model):
    application = models.ForeignKey(Application, on_delete=models.CASCADE)
    # on scandeps, auto relations are deleted, so keep config in another model
    element_type = models.CharField(max_length=100, verbose_name=_('Type'))
    element_slug = models.SlugField(max_length=500, verbose_name=_('Slug'))

    options = models.JSONField(blank=True, default=dict)

    class Meta:
        unique_together = ['application', 'element_type', 'element_slug']


class Version(models.Model):
    application = models.ForeignKey(Application, on_delete=models.CASCADE)
    number = models.CharField(max_length=100, verbose_name=_('Number'))
    notes = models.TextField(verbose_name=_('Notes'), blank=True)
    bundle = models.FileField(upload_to='applications', blank=True, null=True)
    creation_timestamp = models.DateTimeField(default=now)
    last_update_timestamp = models.DateTimeField(auto_now=True)

    def __repr__(self):
        return '<Version %s - %s>' % (self.application.slug, self.number)

    def __str__(self):
        return str(self.number)

    def create_bundle(self, job=None):
        app = self.application
        elements = app.scandeps()
        tar_io = io.BytesIO()
        with tarfile.open(mode='w', fileobj=tar_io) as tar:
            manifest_json = {
                'application': app.name,
                'slug': app.slug,
                'description': app.description,
                'documentation_url': app.documentation_url,
                'license': app.license,
                'authors': app.authors,
                'icon': os.path.basename(app.icon.name) if app.icon.name else None,
                'visible': app.visible,
                'version_number': self.number,
                'version_notes': self.notes,
                'elements': [],
                'parameters': [x.as_dict() for x in self.application.parameter_set.all()],
                'config_options': collections.defaultdict(dict),
            }

            relation_config_options = {
                (r.element_type, r.element_slug): r
                for r in RelationConfig.objects.filter(application=self.application)
            }

            for element, relation in elements.values():
                manifest_json['elements'].append(
                    {
                        'type': element.type,
                        'slug': element.slug,
                        'name': element.name,
                        'auto-dependency': relation.auto_dependency,
                    }
                )
                config_options = relation_config_options.get((element.type, element.slug))
                if config_options:
                    for key, value in config_options.options.items():
                        manifest_json['config_options'][key][f'{element.type}/{element.slug}'] = value

                tarinfo = tarfile.TarInfo('%s/%s' % (element.type, element.slug))
                tarinfo.mtime = self.last_update_timestamp.timestamp()
                if element.type == 'roles':
                    role_element = json.dumps({'name': element.name, 'slug': element.slug}).encode()
                    tarinfo.size = len(role_element)
                    tar.addfile(tarinfo, fileobj=io.BytesIO(role_element))
                else:
                    response = requests.get(element.cache['urls']['export'])
                    tarinfo.size = int(response.headers['content-length'])
                    tar.addfile(tarinfo, fileobj=io.BytesIO(response.content))

            manifest_fd = io.BytesIO(json.dumps(manifest_json, indent=2).encode())
            tarinfo = tarfile.TarInfo('manifest.json')
            tarinfo.size = len(manifest_fd.getvalue())
            tarinfo.mtime = self.last_update_timestamp.timestamp()
            tar.addfile(tarinfo, fileobj=manifest_fd)

            if app.icon.name:
                icon_fd = app.icon.file
                tarinfo = tarfile.TarInfo(manifest_json['icon'])
                tarinfo.size = icon_fd.size
                tarinfo.mtime = self.last_update_timestamp.timestamp()
                tar.addfile(tarinfo, fileobj=icon_fd)

        self.bundle.save('%s.tar' % app.slug, content=ContentFile(tar_io.getvalue()))
        self.save()

        bundle_content = self.bundle.read()
        self.do_something_with_bundle(bundle_content, 'declare', job=job)

    def get_version_to_check(self):
        # search for version to check
        if not self.asyncjob_set.filter(action='deploy', status='completed').exists():
            # new version, check the previous one, if exists
            previous_version = (
                self.application.version_set.exclude(pk=self.pk)
                .filter(asyncjob__action='deploy', asyncjob__status='completed')
                .order_by('last_update_timestamp')
                .last()
            )
            return previous_version
        # this version was already successfully installed, check itself
        return self

    def save_progression(self, job, service_id, service_title, response_json):
        job.save_values(service_id, service_title, response_json, 'progression_urls', 'url')

    def save_details(self, job, service_id, service_title, response_json):
        job.save_values(service_id, service_title, response_json, 'details', 'data')

    def check_install(self, initial=False, job=None):
        version_to_check = self
        if not initial:
            # search for version to check
            version_to_check = self.get_version_to_check()
            if version_to_check is None:
                return

        # in addition to the previous bundle, we send the list of elements contained in the new bundle,
        # so that modules can detect elements no longer contained in the application
        next_bundle = self.bundle.read()
        tar_io = io.BytesIO(next_bundle)
        with tarfile.open(fileobj=tar_io) as tar:
            manifest = json.loads(tar.extractfile('manifest.json').read().decode())
            payload = {
                'elements_from_next_bundle': json.dumps(
                    [f"{element['type']}/{element['slug']}" for element in manifest.get('elements')]
                )
            }
        self.bundle.seek(0)

        # call modules
        bundle_content = version_to_check.bundle.read()
        self.do_something_with_bundle(bundle_content, 'check', job=job, payload=payload)

    def deploy(self, job=None):
        bundle_content = self.bundle.read()
        self.deploy_parameters(bundle_content)
        self.deploy_roles(bundle_content)
        self.do_something_with_bundle(bundle_content, 'deploy', job=job)
        self.application.refresh_elements(cache_only=True)

    def do_something_with_bundle(self, bundle_content, action, job=None, payload=None):
        if action == 'check':
            target_url = 'api/export-import/bundle-check/'
            exception_message = _('Failed to check local modifications for module %(module)s (%(error)s)')
        elif action == 'deploy':
            target_url = 'api/export-import/bundle-import/'
            exception_message = _('Failed to deploy module %(module)s (%(error)s)')
        elif action == 'declare':
            target_url = 'api/export-import/bundle-declare/'
            exception_message = _('Failed to declare elements for module %(module)s (%(error)s)')

        for service_id, services in getattr(settings, 'KNOWN_SERVICES', {}).items():
            if service_id not in Application.get_supported_modules():
                continue
            service_objects = {x.get_base_url_path(): x for x in get_installed_services(types=[service_id])}
            for service in services.values():
                if service['url'] not in service_objects:
                    continue
                if service_objects[service['url']].secondary:
                    continue
                url = urllib.parse.urljoin(service['url'], target_url)
                response = requests.post(url, data=payload or {}, files=[('bundle', bundle_content)])
                if not response.ok:
                    raise DeploymentError(
                        exception_message % {'module': service_id, 'error': response.status_code},
                        details=response.text,
                    )
                if not job:
                    continue
                try:
                    response_json = response.json()
                except json.JSONDecodeError:
                    continue
                self.save_details(job, service_id, service['title'], response_json)
                self.save_progression(job, service_id, service['title'], response_json)

    def get_authentic_service(self):
        for service_id, services in getattr(settings, 'KNOWN_SERVICES', {}).items():
            if service_id == 'authentic':
                for service in services.values():
                    return service
        return None

    def deploy_parameters(self, bundle):
        had_parameters = False
        tar_io = io.BytesIO(bundle)
        with tarfile.open(fileobj=tar_io) as tar:
            manifest = json.loads(tar.extractfile('manifest.json').read().decode())
            for parameter in manifest.get('parameters') or []:
                param, _ = Parameter.objects.get_or_create(
                    name=parameter.get('name'), application=self.application
                )
                param.label = parameter.get('label')
                param.default_value = parameter.get('default_value')
                param.save()
                variable, _ = Variable.objects.get_or_create(
                    name=parameter.get('name'),
                    defaults={'auto': True, 'value': parameter.get('default_value') or ''},
                )
                variable.label = parameter.get('label')
                variable.save()
                had_parameters = True

        if had_parameters:
            notify_agents(None)

    def deploy_roles(self, bundle):
        tar_io = io.BytesIO(bundle)
        service = self.get_authentic_service()
        if not service:
            return
        roles_api_url = urllib.parse.urljoin(service['url'], 'api/roles/?update_or_create=slug')
        role_ou = None
        provision_api_url = urllib.parse.urljoin(service['url'], 'api/provision/')
        ou_slug_variable = Variable.objects.filter(name='ou-slug').first()
        if ou_slug_variable:
            roles_api_url += '&update_or_create=ou'
            if ou_slug_variable.service:  # variable on main instance has service defined
                role_ou = 'default'
            else:
                role_ou = ou_slug_variable.value
        with tarfile.open(fileobj=tar_io) as tar:
            manifest = json.loads(tar.extractfile('manifest.json').read().decode())
            for element in manifest.get('elements'):
                if element.get('type') != 'roles':
                    continue
                role_info = json.loads(tar.extractfile('%s/%s' % (element['type'], element['slug'])).read())
                if role_ou:
                    role_info['ou'] = role_ou
                # create or update
                response = requests.post(roles_api_url, json=role_info)
                if not response.ok:
                    raise DeploymentError(
                        _('Failed to create role %(role)s (%(error)s)')
                        % {'role': element['slug'], 'error': response.status_code},
                        details=response.text,
                    )
                # then force provisionning
                response = requests.post(provision_api_url, json={'role_uuid': response.json()['uuid']})
                if not response.ok:
                    raise DeploymentError(
                        _('Failed to provision role %(role)s (%(error)s)')
                        % {'role': element['slug'], 'error': response.status_code},
                        details=response.text,
                    )


STATUS_CHOICES = [
    ('registered', _('Registered')),
    ('running', _('Running')),
    ('waiting', _('Waiting for modules')),
    ('failed', _('Failed')),
    ('completed', _('Completed')),
]


class AsyncJob(models.Model):
    label = models.CharField(max_length=100)
    status = models.CharField(
        max_length=100,
        default='registered',
        choices=STATUS_CHOICES,
    )
    creation_timestamp = models.DateTimeField(default=now)
    last_update_timestamp = models.DateTimeField(auto_now=True)
    completion_timestamp = models.DateTimeField(default=None, null=True)
    exception = models.TextField()
    exception_details = models.TextField(null=True)

    application = models.ForeignKey(Application, on_delete=models.CASCADE)
    version = models.ForeignKey(Version, on_delete=models.CASCADE, null=True)
    action = models.CharField(max_length=100)
    progression_urls = JSONField(blank=True, default=dict)
    progression_cache = JSONField(blank=True, default=dict)
    details = JSONField(blank=True, default=dict)

    raise_exception = True

    def run(self, spool=False):
        if 'uwsgi' in sys.modules and spool:
            from hobo.applications.spooler import run_job

            tenant = getattr(connection, 'tenant', None)
            domain = getattr(tenant, 'domain_url', '')
            run_job.spool(domain=domain.encode(), job_id=str(self.pk).encode())
            return
        self.status = 'running'
        self.save()
        try:
            if self.action == 'scandeps':
                self.application.scandeps()
            elif self.action == 'create_bundle':
                self.version.create_bundle(self)
            elif self.action == 'deploy':
                self.version.deploy(self)
            elif self.action == 'check-install':
                self.version.check_install(job=self)
            elif self.action == 'check-first-install':
                self.version.check_install(job=self, initial=True)
        except ApplicationError as e:
            self.status = 'failed'
            self.exception = e.msg
            self.exception_details = e.details
            if self.raise_exception:
                raise
        except Exception:
            self.status = 'failed'
            self.exception = traceback.format_exc()
            if self.raise_exception:
                raise
        finally:
            if self.status == 'running':
                if not self.progression_urls:
                    self.status = 'completed'
                else:
                    self.status = 'waiting'
            self.completion_timestamp = now()
            self.save()
            if self.status == 'waiting':
                # the job is may be running in the same time that AsyncJobView is viewed
                # the SQL transaction is not the same, run modules completion check now
                self.check_modules_completion()

    def get_diff_details(self):
        # collect service bundle-check results
        diffs = collections.defaultdict(dict)
        not_found = collections.defaultdict(list)
        no_history = collections.defaultdict(list)
        uninstalled = collections.defaultdict(list)
        result_legacy = []
        for service in self.details.values():
            for data in service.values():
                if self.action == 'check-install':
                    # don't look at differences etc on first install
                    for diff in data.get('differences') or []:
                        diffs[diff['type']][diff['slug']] = diff['url']
                    for unknown in data.get('unknown_elements') or []:
                        not_found[unknown['type']].append(unknown['slug'])
                    for history in data.get('no_history_elements') or []:
                        no_history[history['type']].append(history['slug'])
                    for uninstalled_elem in data.get('uninstalled_elements') or []:
                        uninstalled[uninstalled_elem['type']].append(uninstalled_elem['slug'])
                result_legacy += data.get('legacy_elements') or []
        result_diffs = []
        result_not_found = []
        result_no_history = []
        result_uninstalled = []

        # get and sort relations, so differences will be correctly sorted
        relations = self.application.relation_set.all().select_related('element')
        type_labels = {}
        object_types = get_object_types()
        types = [o['id'] for o in object_types]
        for object_type in object_types:
            type_labels[object_type['id']] = object_type['singular']
        for relation in relations:
            relation.element.type_label = type_labels.get(relation.element.type)
        relations = sorted(
            relations,
            key=lambda a: (
                a.auto_dependency,
                get_object_type_index(a.element.type, types),
                slugify(a.element.name),
            ),
        )
        for legacy in result_legacy:
            legacy['type_label'] = type_labels.get(legacy['type'])
        result_legacy = sorted(
            result_legacy, key=lambda a: (get_object_type_index(a['type'], types), slugify(a['text']))
        )
        # build information to display
        for relation in relations:
            diff = diffs.get(relation.element.type, {}).get(relation.element.slug)
            if diff:
                result_diffs.append((relation, diffs[relation.element.type][relation.element.slug]))
            if relation.element.slug in not_found.get(relation.element.type, []):
                result_not_found.append(relation)
            if relation.element.slug in no_history.get(relation.element.type, []):
                result_no_history.append(relation)
            if relation.element.slug in uninstalled.get(relation.element.type, []):
                result_uninstalled.append(relation)
        return result_diffs, result_not_found, result_no_history, result_legacy, result_uninstalled

    def save_values(self, service_id, service_title, response_json, field_name, key, save=True):
        if response_json.get(key) is None:
            return
        if service_id not in getattr(self, field_name):
            getattr(self, field_name)[service_id] = {}
        getattr(self, field_name)[service_id][service_title] = response_json[key]
        if save:
            self.save()

    def check_modules_completion(self):
        if self.status != 'waiting':
            return {}
        if self.action in ['check-install', 'check-first-install']:
            exception_module_message = _('Failed to check local modifications for module %s')
            exception_modules_message = _('Failed to check local modifications for modules %s')
        elif self.action == 'deploy':
            exception_module_message = _('Failed to deploy module %s')
            exception_modules_message = _('Failed to deploy modules %s')
        elif self.action == 'scandeps':
            exception_module_message = _('Failed to scan module %s')
            exception_modules_message = _('Failed to scan modules %s')
        elif self.action == 'create_bundle':
            exception_module_message = _('Failed to get elements from module %s')
            exception_modules_message = _('Failed to get elements from modules %s')
        context = {'job_progression': {}}
        for service_id, services in self.progression_urls.items():
            for service, url in services.items():
                if service_id not in self.progression_cache:
                    self.progression_cache[service_id] = {}
                cache = self.progression_cache[service_id].get(service) or {}
                if (cache.get('data') or {}).get('status') == 'completed':
                    context['job_progression'][service] = copy.deepcopy(cache)
                    context['job_progression'][service].update({'service_id': service_id})
                    continue
                response = requests.get(url)
                if not response.ok:
                    continue
                try:
                    response_json = response.json()
                except json.JSONDecodeError:
                    continue
                context['job_progression'][service] = response_json
                context['job_progression'][service].update({'service_id': service_id})
                self.progression_cache[service_id][service] = response.json()
                if response_json.get('data'):
                    self.save_values(service_id, service, response_json['data'], 'details', 'job_result_data')
        self.save()
        context['services_all_completed'] = all(
            [s['data']['status'] == 'completed' for s in context['job_progression'].values()]
        )
        failed = [
            s['service_id'] for s in context['job_progression'].values() if s['data']['status'] == 'failed'
        ]
        if failed:
            self.status = 'failed'
            self.exception = ngettext(
                exception_module_message,
                exception_modules_message,
                len(failed),
            ) % ', '.join(failed)
            failure_labels = [
                '%s: %s' % (s['service_id'], s['data']['failure_label'])
                for s in context['job_progression'].values()
                if s['data']['status'] == 'failed' and s['data'].get('failure_label')
            ]
            if failure_labels:
                self.exception_details = '\n'.join(failure_labels)
            self.completion_timestamp = now()
            self.save()
        elif context['services_all_completed']:
            self.status = 'completed'
            self.completion_timestamp = now()
            self.save()
        context['wait_for_services'] = True
        context['service_job_status_choices'] = {c[0]: c[1] for c in STATUS_CHOICES}
        return context
