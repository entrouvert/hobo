# hobo - portal to configure and deploy applications
# Copyright (C) 2015-2022 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from uwsgidecorators import spool

from hobo.provisionning.spooler import ensure_db, set_connection

from .models import AsyncJob


@spool
@ensure_db
def run_job(args):
    set_connection(args['domain'])
    job = AsyncJob.objects.get(id=args['job_id'])
    job.raise_exception = False
    job.run()
    print('got job:', job)
