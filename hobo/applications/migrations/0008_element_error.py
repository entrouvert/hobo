from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('applications', '0007_asyncjob'),
    ]

    operations = [
        migrations.AddField(
            model_name='element',
            name='error',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='element',
            name='error_status',
            field=models.CharField(
                choices=[('notfound', 'Not Found'), ('error', 'Error')], max_length=100, null=True
            ),
        ),
    ]
