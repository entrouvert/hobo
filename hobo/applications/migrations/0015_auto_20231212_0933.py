# Generated by Django 3.2.16 on 2023-12-12 08:33

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('applications', '0014_check_install'),
    ]

    operations = [
        migrations.AddField(
            model_name='application',
            name='authors',
            field=models.TextField(blank=True, verbose_name='Authors'),
        ),
        migrations.AddField(
            model_name='application',
            name='license',
            field=models.CharField(
                blank=True,
                choices=[
                    ('', 'Unspecified'),
                    ('agplv3+', 'GNU Affero General Public License v3 or later (AGPLv3+)'),
                    ('gplv3+', 'GNU General Public License v3 or later (GPLv3+)'),
                    ('mit', 'MIT License'),
                ],
                max_length=200,
                verbose_name='License',
            ),
        ),
    ]
