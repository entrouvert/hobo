from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('applications', '0018_relation_config_options'),
    ]

    operations = [
        migrations.AlterField(
            model_name='application',
            name='slug',
            field=models.SlugField(max_length=100, unique=True),
        ),
    ]
