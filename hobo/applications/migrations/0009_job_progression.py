from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('applications', '0008_element_error'),
    ]

    operations = [
        migrations.AddField(
            model_name='asyncjob',
            name='progression_urls',
            field=models.JSONField(blank=True, default=dict),
        ),
    ]
