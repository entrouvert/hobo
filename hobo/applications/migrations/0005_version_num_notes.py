from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('applications', '0004_version_num_notes'),
    ]

    operations = [
        migrations.AlterField(
            model_name='version',
            name='number',
            field=models.CharField(max_length=100, verbose_name='Number'),
        ),
    ]
