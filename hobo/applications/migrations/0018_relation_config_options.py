import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('applications', '0017_relation_origins'),
    ]

    operations = [
        migrations.CreateModel(
            name='RelationConfig',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('element_type', models.CharField(max_length=100, verbose_name='Type')),
                ('element_slug', models.SlugField(max_length=500, verbose_name='Slug')),
                ('options', models.JSONField(blank=True, default=dict)),
                (
                    'application',
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to='applications.application'
                    ),
                ),
            ],
            options={
                'unique_together': {('application', 'element_type', 'element_slug')},
            },
        ),
    ]
