from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('applications', '0005_version_num_notes'),
    ]

    operations = [
        migrations.AddField(
            model_name='application',
            name='documentation_url',
            field=models.URLField(blank=True, verbose_name='Documentation URL'),
        ),
    ]
