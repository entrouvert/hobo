from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('applications', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='application',
            name='icon',
            field=models.FileField(
                blank=True,
                help_text='Icon file must be in JPEG or PNG format, and should be a square of at least 512×512 pixels.',
                null=True,
                upload_to='applications/icons/',
                verbose_name='Icon',
            ),
        ),
    ]
