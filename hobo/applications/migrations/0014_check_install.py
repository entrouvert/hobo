from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('applications', '0013_parameter'),
    ]

    operations = [
        migrations.AddField(
            model_name='asyncjob',
            name='details',
            field=models.JSONField(blank=True, default=dict),
        ),
    ]
