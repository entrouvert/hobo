from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('applications', '0009_job_progression'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='element',
            name='error',
        ),
        migrations.RemoveField(
            model_name='element',
            name='error_status',
        ),
        migrations.AddField(
            model_name='relation',
            name='error',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='relation',
            name='error_status',
            field=models.CharField(
                choices=[('notfound', 'Not Found'), ('error', 'Error'), ('not-installed', 'Not installed')],
                max_length=100,
                null=True,
            ),
        ),
    ]
