from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('applications', '0015_auto_20231212_0933'),
    ]

    operations = [
        migrations.AddField(
            model_name='asyncjob',
            name='progression_cache',
            field=models.JSONField(blank=True, default=dict),
        ),
        migrations.AlterField(
            model_name='asyncjob',
            name='status',
            field=models.CharField(
                choices=[
                    ('registered', 'Registered'),
                    ('running', 'Running'),
                    ('waiting', 'Waiting for modules'),
                    ('failed', 'Failed'),
                    ('completed', 'Completed'),
                ],
                default='registered',
                max_length=100,
            ),
        ),
    ]
