from django.db import migrations


def forwards(apps, schema_editor):
    Application = apps.get_model('applications', 'Application')
    for app in Application.objects.all():
        for i, version in enumerate(app.version_set.order_by('creation_timestamp')):
            if app.editable:
                version.number = '%s.0' % (i + 1)
            else:
                version.number = 'unknown'
            version.save(update_fields=['number'])


class Migration(migrations.Migration):
    dependencies = [
        ('applications', '0003_version_num_notes'),
    ]

    operations = [
        migrations.RunPython(forwards, reverse_code=migrations.RunPython.noop),
    ]
