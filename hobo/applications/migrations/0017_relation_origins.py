from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('applications', '0016_progression'),
    ]

    operations = [
        migrations.AddField(
            model_name='relation',
            name='origins',
            field=models.ManyToManyField(blank=True, to='applications.Relation'),
        ),
    ]
