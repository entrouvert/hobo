# Generated by Django 3.2.13 on 2022-10-30 12:47

import django.db.models.deletion
import django.utils.timezone
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('applications', '0006_documentation_url'),
    ]

    operations = [
        migrations.CreateModel(
            name='AsyncJob',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('label', models.CharField(max_length=100)),
                (
                    'status',
                    models.CharField(
                        choices=[
                            ('registered', 'Registered'),
                            ('running', 'Running'),
                            ('failed', 'Failed'),
                            ('completed', 'Completed'),
                        ],
                        default='registered',
                        max_length=100,
                    ),
                ),
                ('creation_timestamp', models.DateTimeField(default=django.utils.timezone.now)),
                ('last_update_timestamp', models.DateTimeField(auto_now=True)),
                ('completion_timestamp', models.DateTimeField(default=None, null=True)),
                ('exception', models.TextField()),
                ('action', models.CharField(max_length=100)),
                (
                    'application',
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to='applications.application'
                    ),
                ),
                (
                    'version',
                    models.ForeignKey(
                        null=True, on_delete=django.db.models.deletion.CASCADE, to='applications.version'
                    ),
                ),
            ],
        ),
    ]
