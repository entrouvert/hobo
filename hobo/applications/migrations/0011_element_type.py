from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('applications', '0010_relation_error'),
    ]

    operations = [
        migrations.AlterField(
            model_name='element',
            name='type',
            field=models.CharField(max_length=100, verbose_name='Type'),
        ),
    ]
