from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('applications', '0002_icon'),
    ]

    operations = [
        migrations.AddField(
            model_name='version',
            name='notes',
            field=models.TextField(blank=True, verbose_name='Notes'),
        ),
        migrations.AddField(
            model_name='version',
            name='number',
            field=models.CharField(max_length=100, null=True, verbose_name='Number'),
        ),
    ]
