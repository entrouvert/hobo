from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('applications', '0011_element_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='application',
            name='visible',
            field=models.BooleanField(
                default=True,
                help_text='If enabled, the application will be visible in the services where it is installed.',
                verbose_name='Visible',
            ),
        ),
    ]
