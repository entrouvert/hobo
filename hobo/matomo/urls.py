# hobo - portal to configure and deploy applications
# Copyright (C) 2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import path

from . import views

urlpatterns = [
    path('', views.home, name='matomo-home'),
    path('enable-manual', views.enable_manual, name='matomo-enable-manual'),
    path('enable-auto', views.enable_auto, name='matomo-enable-auto'),
    path('disable', views.disable, name='matomo-disable'),
]
