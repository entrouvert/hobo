# hobo - portal to configure and deploy applications
# Copyright (C) 2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import xml.etree.ElementTree as etree

from django import forms
from django.utils.translation import gettext_lazy as _


class SettingsForm(forms.Form):
    """
    According to publik-base-theme/templates/includes/tracking.html,
    the 2 tracking_js variables are merged into the unique below field.
    If JS code added is compliant to CNIL policy, we store it into
    'cnil_compliant_visits_tracking_js' else into 'visits_tracking_js'.
    The goal is to display a banner advertising users about intrusive JS.
    """

    tracking_js = forms.CharField(label=_('Tracking Javascript'), required=False, widget=forms.Textarea())

    def clean_tracking_js(self):
        value = self.cleaned_data['tracking_js']

        try:
            xml_node = etree.fromstring(value)
            if xml_node.tag == 'script':
                script_tag = xml_node
            else:
                script_tag = xml_node.find('script')
            if script_tag is not None:
                return script_tag.text
        except etree.ParseError:
            pass

        if '<script' in value:
            raise forms.ValidationError(
                _(
                    'This field should only contain the Javascript code. '
                    'You should remove the surrounding <script> markup.'
                )
            )
        return value


class EnableForm(forms.Form):
    pass
