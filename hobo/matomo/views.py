# hobo - portal to configure and deploy applications
# Copyright (C) 2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib import messages
from django.urls import reverse_lazy
from django.views.generic import FormView

from .forms import EnableForm, SettingsForm
from .utils import (
    MatomoError,
    MatomoException,
    MatomoWS,
    auto_configure_matomo,
    compute_cnil_acknowledgment_level,
    get_tracking_js,
    get_variable,
    get_variable_value,
    put_tracking_js,
)


class HomeView(FormView):
    template_name = 'hobo/matomo_home.html'
    form_class = SettingsForm
    success_url = reverse_lazy('matomo-home')

    def get_initial(self):
        initial = super().get_initial()
        initial['tracking_js'] = get_tracking_js()
        return initial

    def form_valid(self, form):
        tracking_js = form.cleaned_data['tracking_js']
        put_tracking_js(tracking_js)
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        tracking_js = get_tracking_js()
        logme_url = get_variable_value('matomo_logme_url')
        context['logme_url'] = logme_url
        context['tracking_js'] = tracking_js

        # compute contextual values
        context['cnil_ack_level'] = compute_cnil_acknowledgment_level(tracking_js)
        try:
            MatomoWS()
        except MatomoError:
            context['ws_available'] = False
        else:
            context['ws_available'] = True
        context['enabled'] = tracking_js != ''
        if logme_url != '':
            context['mode'] = 'auto'
        else:
            context['mode'] = 'manual'
        return context


home = HomeView.as_view()


class EnableManualView(FormView):
    form_class = SettingsForm
    template_name = 'hobo/matomo_enable_manual.html'
    success_url = reverse_lazy('matomo-home')

    def get_initial(self):
        initial = super().get_initial()
        initial['tracking_js'] = get_tracking_js()
        return initial

    def form_valid(self, form):
        tracking_js = form.cleaned_data['tracking_js']
        put_tracking_js(tracking_js)
        logme_url = get_variable('matomo_logme_url')
        logme_url.delete()
        return super().form_valid(form)


enable_manual = EnableManualView.as_view()


class EnableAutoView(FormView):
    form_class = EnableForm
    template_name = 'hobo/matomo_enable_auto.html'
    success_url = reverse_lazy('matomo-home')

    def form_valid(self, form):
        matomo = MatomoWS()
        try:
            id_site = auto_configure_matomo(matomo)
        except MatomoException as exc:
            messages.error(self.request, 'matomo: ' + str(exc))
        else:
            try:
                matomo.create_fake_first_tracking_visit(id_site)
            except MatomoException as exc:
                messages.warning(self.request, 'ping: ' + str(exc))
        return super().form_valid(form)


enable_auto = EnableAutoView.as_view()


class DisableView(FormView):
    form_class = EnableForm
    template_name = 'hobo/matomo_disable.html'
    success_url = reverse_lazy('matomo-home')

    def form_valid(self, form):
        put_tracking_js('')
        variable = get_variable('matomo_logme_url')
        variable.delete()
        return super().form_valid(form)


disable = DisableView.as_view()
