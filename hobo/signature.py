import base64
import datetime
import hashlib
import hmac
import random
import secrets
import urllib.parse

from django.utils.encoding import smart_bytes
from django.utils.http import quote, urlencode


class SignatureError(Exception):
    pass


def sign_url(url, key, algo='sha256', timestamp=None, nonce=None):
    parsed = urllib.parse.urlparse(url)
    new_query = sign_query(parsed.query, key, algo, timestamp, nonce)
    return urllib.parse.urlunparse(parsed[:4] + (new_query,) + parsed[5:])


def get_nonce():
    return hex(random.getrandbits(128))[2:]


def sign_query(query, key, algo='sha256', timestamp=None, nonce=None):
    if timestamp is None:
        timestamp = datetime.datetime.utcnow()
    timestamp = timestamp.strftime('%Y-%m-%dT%H:%M:%SZ')
    if nonce is None:
        nonce = get_nonce()
    new_query = query
    if new_query:
        new_query += '&'
    new_query += urlencode((('algo', algo), ('timestamp', timestamp)))
    if nonce:  # we don't add nonce if it's an empty string
        new_query += '&nonce=' + quote(nonce)
    signature = base64.b64encode(sign_string(new_query, key, algo=algo))
    new_query += '&signature=' + quote(signature)
    return new_query


def sign_string(s, key, algo='sha256'):
    digestmod = getattr(hashlib, algo)
    if isinstance(key, str):
        key = key.encode('utf-8')
    hash = hmac.HMAC(smart_bytes(key), digestmod=digestmod, msg=smart_bytes(s))
    return hash.digest()


def check_url(url, key, known_nonce=None, timedelta=30, raise_on_error=False):
    parsed = urllib.parse.urlparse(url, 'https')
    return check_query(
        parsed.query, key, known_nonce=known_nonce, timedelta=timedelta, raise_on_error=raise_on_error
    )


def check_query(query, key, known_nonce=None, timedelta=30, raise_on_error=False):
    parsed = urllib.parse.parse_qs(query)
    parsed = {key: value[0] if len(value) == 1 else value for key, value in parsed.items()}
    signature = parsed.get('signature')
    if not signature or not isinstance(signature, str):
        if raise_on_error:
            raise SignatureError('multiple/missing signature')
        return False
    algo = parsed.get('algo')
    if not algo or not isinstance(algo, str):
        if raise_on_error:
            raise SignatureError('multiple/missing algo')
        return False
    if algo != 'sha256':
        if raise_on_error:
            raise SignatureError('invalid algo, must be sha256')
        return False
    timestamp = parsed.get('timestamp')
    if not timestamp or not isinstance(timestamp, str):
        if raise_on_error:
            raise SignatureError('multiple/missing timestamp')
        return False
    if known_nonce is not None:
        nonce = parsed.get('nonce')
        if not nonce or not isinstance(nonce, str):
            if raise_on_error:
                raise SignatureError('multiple/missing nonce')
            return False
        if known_nonce(nonce):
            if raise_on_error:
                raise SignatureError('nonce replayed')
            return False
    unsigned_query, signature_content = query.split('&signature=', 1)
    if '&' in signature_content:
        if raise_on_error:
            raise SignatureError('signature is not the last parameter')
        return False  # signature must be the last parameter
    try:
        signature = base64.b64decode(signature)
    except ValueError:
        if raise_on_error:
            raise SignatureError('signature is invalid base64')
        return False
    try:
        timestamp = datetime.datetime.strptime(timestamp, '%Y-%m-%dT%H:%M:%SZ')
    except ValueError as e:
        if raise_on_error:
            raise SignatureError('invalid timestamp, %s' % e)
        return False
    delta = abs(datetime.datetime.utcnow() - timestamp)
    if delta > datetime.timedelta(seconds=timedelta):
        if raise_on_error:
            raise SignatureError('timestamp delta is more than %s seconds: %s' % (timedelta, delta))
        return False
    return check_string(unsigned_query, signature, key, algo=algo, raise_on_error=raise_on_error)


def check_string(s, signature, key, algo='sha256', raise_on_error=False):
    # constant time compare
    signature2 = sign_string(s, key, algo=algo)
    if not secrets.compare_digest(signature, signature2):
        if raise_on_error:
            raise SignatureError('HMAC hash is different')
        return False
    return True
