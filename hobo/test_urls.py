import logging

from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from django.middleware.csrf import get_token
from django.urls import path, re_path
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView


def helloworld(request):
    logging.getLogger(__name__).error('wat!')
    if 'raise' in request.GET:
        raise Exception('wat!')
    if 'forbidden' in request.GET:
        raise PermissionDenied('forbidden access')
    get_token(request)
    return HttpResponse('Hello world %s' % request.META['REMOTE_ADDR'])


class AuthenticatedTestView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request):
        return Response({'some': 'data'})


urlpatterns = [
    path('', helloworld),
    re_path(r'^authenticated-testview/', AuthenticatedTestView.as_view()),
]
