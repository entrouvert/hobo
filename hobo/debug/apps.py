# hobo - © Entr'ouvert

import datetime

import django.apps
from django.utils.timezone import now


class AppConfig(django.apps.AppConfig):
    name = 'hobo.debug'

    def hourly(self):
        from hobo.environment.utils import get_setting_variable

        debug_log_variable = get_setting_variable('DEBUG_LOG')
        debug_ips_variable = get_setting_variable('INTERNAL_IPS.extend')

        if debug_log_variable.json is True and (
            now() - debug_log_variable.last_update_timestamp > datetime.timedelta(hours=2)
        ):
            debug_log_variable.delete()

        if debug_ips_variable.json and (
            now() - debug_ips_variable.last_update_timestamp > datetime.timedelta(hours=2)
        ):
            debug_ips_variable.delete()
