# hobo - portal to configure and deploy applications
# Copyright (C) 2015-2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import reverse_lazy
from django.utils.functional import cached_property
from django.views.generic import FormView

from hobo.environment.utils import get_setting_variable

from .forms import SettingsForm


class HomeView(FormView):
    template_name = 'hobo/debug_home.html'
    form_class = SettingsForm
    success_url = reverse_lazy('debug-home')

    @cached_property
    def debug_log_variable(self):
        return get_setting_variable('DEBUG_LOG')

    @cached_property
    def debug_ips_variable(self):
        return get_setting_variable('INTERNAL_IPS.extend')

    def get_initial(self):
        initial = super().get_initial()
        initial['debug_log'] = bool(self.debug_log_variable.json)
        initial['debug_ips'] = self.debug_ips_variable.json
        return initial

    @property
    def current_ip(self):
        return self.request.META.get('REMOTE_ADDR') or None

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['current_ip_debug'] = self.current_ip in self.debug_ips_variable.json
        return ctx

    def toggle_value(self, l, value):
        if value in l:
            return [x for x in l if x != value]
        else:
            return l + [value]

    def form_valid(self, form):
        debug_log = form.cleaned_data['debug_log']
        self.debug_log_variable.json = debug_log
        self.debug_log_variable.save()

        debug_ips = form.cleaned_data['debug_ips']
        if 'toggle-current-ip' in self.request.POST:
            debug_ips = self.toggle_value(debug_ips, self.current_ip)
        self.debug_ips_variable.json = debug_ips
        self.debug_ips_variable.save()
        return super().form_valid(form)


home = HomeView.as_view()
