# hobo - portal to configure and deploy applications
# Copyright (C) 2015-2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import validate_ipv46_address
from django.utils.translation import gettext_lazy as _


def validate_space_separated_ips(value):
    errors = []
    for ip in value:
        try:
            validate_ipv46_address(ip)
        except ValidationError as e:
            errors.append(e)
    if errors:
        raise ValidationError(errors)


class MultipleIPAddressField(forms.CharField):
    default_validators = [validate_space_separated_ips]

    def to_python(self, value):
        if value in self.empty_values:
            return []
        return value.split()

    def prepare_value(self, value):
        if not value:
            return ''
        if not isinstance(value, list):
            return value
        return ' '.join(value)


class SettingsForm(forms.Form):
    debug_log = forms.BooleanField(
        required=False, label=_('Enable Debug Logs'), help_text=_('It will be disabled after 2 hours.')
    )
    debug_ips = MultipleIPAddressField(
        label=_('Internal IP addresses'),
        required=False,
        help_text=_('List of IP addresses for which to enable debugging. It is cleared after 2 hours.'),
    )
