# hobo - portal to configure and deploy applications
# Copyright (C) 2015-2022 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import hashlib
import logging
import urllib
import urllib.parse
from io import BytesIO

from django.conf import settings
from django.core.cache import cache
from django.utils.encoding import smart_bytes
from django.utils.http import urlencode
from requests import Response
from requests import Session as RequestsSession
from requests.auth import AuthBase

from hobo.signature import sign_url


class NothingInCacheException(Exception):
    pass


class PublikSignature(AuthBase):
    def __init__(self, orig, secret):
        self.orig = orig
        self.secret = secret

    def __call__(self, request):
        url = request.url

        parsed_url = urllib.parse.urlparse(url)
        query_params = urllib.parse.parse_qs(parsed_url.query)
        query_params['orig'] = [self.orig]
        query = urllib.parse.urlencode(query_params, doseq=True)
        url = urllib.parse.urlunparse(parsed_url._replace(query=query))

        request.url = sign_url(url, self.secret)
        return request


def get_known_service_for_url(url):
    netloc = urllib.parse.urlparse(url).netloc
    scheme = urllib.parse.urlparse(url).scheme

    for services in settings.KNOWN_SERVICES.values():
        for service in services.values():
            remote_url = service.get('url')
            assert remote_url is not None

            remote_netloc = urllib.parse.urlparse(remote_url).netloc
            remote_scheme = urllib.parse.urlparse(remote_url).scheme

            if remote_netloc == netloc and remote_scheme == scheme:
                return service
    return None


class ImplicitPublikSignature(AuthBase):
    def __call__(self, request):
        url = request.url

        remote_service = get_known_service_for_url(url)
        if (
            remote_service
            and (secret := remote_service.get('secret'))
            and (orig := remote_service.get('orig'))
        ):
            return PublikSignature(orig=orig, secret=secret)(request)

        return request


class Requests(RequestsSession):
    def get_cache_key(self, url, params):
        params = urlencode(params)
        return hashlib.md5(smart_bytes(url + params)).hexdigest()

    def request(self, method, url, **kwargs):
        if not kwargs.get('auth'):
            kwargs['auth'] = ImplicitPublikSignature()
        logger = logging.getLogger(__name__)
        cache_duration = kwargs.pop('cache_duration', 0)
        invalidate_cache = kwargs.pop('invalidate_cache', False)
        raise_if_not_cached = kwargs.pop('raise_if_not_cached', False)

        if method == 'GET' and cache_duration:
            # handle cache
            cache_key = self.get_cache_key(url=url, params=kwargs.get('params', {}))
            cache_content = cache.get(cache_key)
            if cache_content and not invalidate_cache:
                response = Response()
                response.status_code = 200
                response.raw = BytesIO(smart_bytes(cache_content))
                return response
            elif raise_if_not_cached:
                raise NothingInCacheException()

        kwargs['timeout'] = kwargs.get('timeout') or settings.REQUESTS_TIMEOUT

        response = super().request(method, url, **kwargs)
        if response.status_code // 100 != 2:
            logger.warning('failed to %s %s (%s)', method, response.request.url, response.status_code)
        if method == 'GET' and cache_duration and (response.status_code // 100 == 2):
            cache.set(cache_key, response.content, cache_duration)

        return response
