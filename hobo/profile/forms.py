# hobo - portal to configure and deploy applications
# Copyright (C) 2015-2023  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import forms
from django.utils.translation import gettext_lazy as _

from . import models, utils


class EditFullNameTemplateForm(forms.Form):
    user_full_name_template = forms.CharField(
        label=_('User full name template (Django)'),
        help_text=_(
            'Template expressions including user profile attributes are accepted, e.g. '
            '“{{ user.first_name }}{% if user.attributes.middle_name %} {{ user.attributes.middle_name }}{% endif %} {{ user.last_name }}”'
        ),
        widget=forms.Textarea,
        required=False,
    )


class AttributeDefinitionCreateForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance.name in ['first_name', 'last_name']:
            self.initial['searchable'] = True
            self.fields['searchable'].disabled = True
            self.fields['searchable'].help_text = _('"%s" field is always searchable.') % self.instance.name

    class Meta:
        model = models.AttributeDefinition
        fields = [
            'label',
            'name',
            'description',
            'required',
            'required_on_login',
            'asked_on_registration',
            'user_editable',
            'user_visible',
            'searchable',
            'disabled',
            'kind',
        ]


class AttributeDefinitionUpdateForm(AttributeDefinitionCreateForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        authn_info = utils.get_authn_information_from_idp()
        if self.instance.name == 'email' and authn_info.get('accept_email_authentication', False):
            self.fields['disabled'].disabled = True
            self.fields['disabled'].help_text = _(
                'The email attribute is an identifier on the identity provider hence can\'t be deactivated.'
            )
        if authn_info.get('accept_phone_authentication', False) and self.instance.name == authn_info.get(
            'phone_identifier_field', ''
        ):
            self.fields['disabled'].disabled = True
            self.fields['disabled'].help_text = (
                _(
                    'The "%s" phone attribute is an identifier on the identity provider hence can\'t be deactivated.'
                )
                % self.instance.name
            )

    class Meta(AttributeDefinitionCreateForm.Meta):
        model = models.AttributeDefinition
        exclude = ['name', 'kind']
