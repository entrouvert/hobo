import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = []

    operations = [
        migrations.CreateModel(
            name='AttributeDefinition',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('label', models.CharField(unique=True, max_length=63, verbose_name='label')),
                ('description', models.TextField(verbose_name='description', blank=True)),
                (
                    'name',
                    models.SlugField(
                        unique=True,
                        max_length=256,
                        verbose_name='name',
                        error_messages={b'unique': 'Field names must be unique.'},
                        validators=[
                            django.core.validators.RegexValidator(
                                b'^[a-z][a-z0-9_]*\\Z',
                                'Enter valid variable name starting with a letter and consisting of letters, numbers, or underscores.',
                            )
                        ],
                    ),
                ),
                ('required', models.BooleanField(blank=True, default=False, verbose_name='required')),
                (
                    'asked_on_registration',
                    models.BooleanField(blank=True, default=False, verbose_name='asked on registration'),
                ),
                (
                    'user_editable',
                    models.BooleanField(blank=True, default=True, verbose_name='user editable'),
                ),
                ('user_visible', models.BooleanField(blank=True, default=True, verbose_name='user visible')),
                (
                    'kind',
                    models.CharField(
                        default='string',
                        max_length=16,
                        verbose_name='kind',
                        choices=[
                            ('string', 'String'),
                            ('boolean', 'Boolean'),
                            ('date', 'Date'),
                            ('title', 'Civility'),
                            ('birthdate', 'Birthdate'),
                            ('fr_postcode', 'French Postcode'),
                            ('phone_number', 'Phone Number'),
                            ('profile_image', 'Profile Image'),
                        ],
                    ),
                ),
                ('disabled', models.BooleanField(default=False, verbose_name='disabled')),
                ('order', models.PositiveIntegerField()),
                ('last_update_timestamp', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ['order'],
            },
            bases=(models.Model,),
        ),
    ]
