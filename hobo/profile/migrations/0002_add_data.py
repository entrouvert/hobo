from django.db import migrations


def add_initial_data(apps, schema_editor):
    AttributeDefinition = apps.get_model('profile', 'AttributeDefinition')

    attributes = [
        {'label': 'Civilité', 'name': 'title', 'kind': 'title', 'disabled': True},
        {'label': 'Prénom', 'name': 'first_name', 'required': True, 'asked_on_registration': True},
        {'label': 'Nom', 'name': 'last_name', 'required': True, 'asked_on_registration': True},
        {'label': 'Adresse électronique', 'name': 'email', 'kind': 'email', 'required': True},
        {'label': 'Adresse', 'name': 'address'},
        {'label': 'Code postal', 'name': 'zipcode'},
        {'label': 'Commune', 'name': 'city'},
        {'label': 'Pays', 'name': 'country', 'disabled': True},
        {'label': 'Date de naissance', 'name': 'birthdate', 'kind': 'birthdate', 'disabled': True},
        {'label': 'Téléphone', 'name': 'phone', 'kind': 'phone_number'},
    ]

    for i, attribute_dict in enumerate(attributes):
        attribute_dict['order'] = i + 1
        attr = AttributeDefinition(**attribute_dict)
        attr.save()


class Migration(migrations.Migration):
    dependencies = [
        ('profile', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(add_initial_data),
    ]
