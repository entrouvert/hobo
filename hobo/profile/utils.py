# hobo - portal to configure and deploy applications
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import urllib.parse

import requests
from django.conf import settings
from django.core.cache import cache

from hobo.requests_wrapper import Requests

from .models import AttributeDefinition


def get_profile_dict():
    return {'profile': {'fields': [x.as_dict() for x in AttributeDefinition.objects.all()]}}


def get_authn_information_from_idp():
    logger = logging.getLogger(__name__)
    if cached := cache.get('authn-information'):
        return cached
    if 'authentic' not in getattr(settings, 'KNOWN_SERVICES', {}):
        return {}
    if 'idp' not in settings.KNOWN_SERVICES['authentic']:
        return {}
    if not (url := settings.KNOWN_SERVICES['authentic']['idp'].get('url', {})):
        return {}
    else:
        url = urllib.parse.urljoin(url, 'api/authn-healthcheck/')
        data = {}
        try:
            response = Requests().get(url, timeout=5)
            data = response.json().get('data', {}) if response.ok else {}
        except requests.exceptions.RequestException as e:
            logger.error('error while retrieving authn info on a2 (%s)', e)
        else:
            cache.set('authn-information', data, 10)
        return data
