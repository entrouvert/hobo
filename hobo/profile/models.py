# hobo - portal to configure and deploy applications
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.core.validators import RegexValidator
from django.db import models
from django.utils.translation import gettext_lazy as _

validate_attribute_name = RegexValidator(
    r'^[a-z][a-z0-9_]*\Z',
    _('Enter valid variable name starting with a letter and consisting of letters, numbers, or underscores.'),
)


class AttributeDefinition(models.Model):
    label = models.CharField(verbose_name=_('label'), max_length=63, unique=True)
    description = models.TextField(verbose_name=_('description'), blank=True)
    name = models.SlugField(
        verbose_name=_('name'),
        max_length=50,
        unique=True,
        validators=[validate_attribute_name],
        error_messages={'unique': _('Field names must be unique.')},
    )
    required = models.BooleanField(verbose_name=_('required'), blank=True, default=False)
    required_on_login = models.BooleanField(verbose_name=_('required on login'), blank=True, default=False)
    asked_on_registration = models.BooleanField(
        verbose_name=_('asked on registration'), blank=True, default=False
    )
    user_editable = models.BooleanField(verbose_name=_('user editable'), blank=True, default=True)
    user_visible = models.BooleanField(verbose_name=_('user visible'), blank=True, default=True)
    searchable = models.BooleanField(verbose_name=_('searchable'), blank=True, default=False)
    kind = models.CharField(
        max_length=16,
        verbose_name=_('kind'),
        default='string',
        choices=(
            ('string', _('String')),
            ('boolean', _('Boolean')),
            ('date', _('Date')),
            ('title', _('Civility')),
            ('birthdate', _('Birthdate')),
            ('fr_postcode', _('French Postcode')),
            ('phone_number', _('Phone Number')),
            ('fr_phone_number', _('French Phone Number')),
            ('profile_image', _('Profile Image')),
            ('address_auto', _('Address (autocomplete)')),
            ('language', _('Language')),
        ),
    )
    disabled = models.BooleanField(verbose_name=_('disabled'), default=False)
    order = models.PositiveIntegerField()
    last_update_timestamp = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['order']

    def as_dict(self):
        as_dict = {x: y for (x, y) in self.__dict__.items() if type(y) in (str, bool)}
        return as_dict

    def get_real_kind_display(self):
        if self.kind == 'email':  # not a real authentic type
            return _('Email')
        return self.get_kind_display()

    def save(self, *args, **kwargs):
        if self.order is None:
            self.order = max([0] + [x.order for x in AttributeDefinition.objects.all()]) + 1
        super().save(*args, **kwargs)
