# hobo - portal to configure and deploy applications
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import path, re_path

from . import views

urlpatterns = [
    path('', views.home, name='profile-home'),
    re_path(r'(?P<name>[\w-]+)/options', views.options, name='profile-attribute-options'),
    path('reorder', views.reorder, name='profile-reorder'),
    path('add-attribute', views.add_attribute, name='profile-add-attribute'),
    path(
        'edit-user-full-name-template',
        views.edit_user_full_name_template,
        name='profile-edit-user-full-name-template',
    ),
]
