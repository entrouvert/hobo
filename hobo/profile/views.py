# hobo - portal to configure and deploy applications
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.shortcuts import redirect
from django.urls import reverse, reverse_lazy
from django.utils.translation import gettext as _
from django.views.generic import CreateView, ListView, TemplateView, UpdateView

from hobo.environment.forms import VariablesFormMixin

from .forms import AttributeDefinitionCreateForm, AttributeDefinitionUpdateForm, EditFullNameTemplateForm
from .models import AttributeDefinition


class HomeView(ListView):
    model = AttributeDefinition


home = HomeView.as_view()


class AddAttributeView(CreateView):
    model = AttributeDefinition
    form_class = AttributeDefinitionCreateForm
    success_url = reverse_lazy('profile-home')


add_attribute = AddAttributeView.as_view()


class OptionsView(UpdateView):
    model = AttributeDefinition
    form_class = AttributeDefinitionUpdateForm
    slug_url_kwarg = 'name'
    slug_field = 'name'
    success_url = reverse_lazy('profile-home')


options = OptionsView.as_view()


class EditFullNameTemplateView(VariablesFormMixin, TemplateView):
    template_name = 'profile/edit_full_name_template.html'
    form_class = EditFullNameTemplateForm
    variables = ['user_full_name_template']
    success_message = _('User full name template has been updated.')


edit_user_full_name_template = EditFullNameTemplateView.as_view()


def reorder(request):
    new_order_list = [int(x) for x in request.GET['new-order'].split(',')]
    for attribute in AttributeDefinition.objects.all():
        old_order = attribute.order
        new_order = new_order_list.index(attribute.id) + 1
        if old_order != new_order:
            attribute.order = new_order_list.index(attribute.id) + 1
            attribute.save()
    return redirect(reverse('profile-home'))
