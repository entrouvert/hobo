# hobo - portal to configure and deploy applications
# Copyright (C) 2015-2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf import settings
from django.urls import include, path, re_path

from .applications.urls import urlpatterns as applications_urls
from .debug.urls import urlpatterns as debug_urls
from .emails.urls import urlpatterns as emails_urls
from .environment.urls import urlpatterns as environment_urls
from .maintenance.urls import urlpatterns as maintenance_urls
from .matomo.urls import urlpatterns as matomo_urls
from .profile.urls import urlpatterns as profile_urls
from .security.urls import urlpatterns as security_urls
from .seo.urls import urlpatterns as seo_urls
from .sms.urls import urlpatterns as sms_urls
from .theme.urls import urlpatterns as theme_urls
from .urls_utils import decorated_includes
from .views import admin_required, health_json, hobo, home, login, login_local, logout, menu_json

urlpatterns = [
    path('', home, name='home'),
    re_path(r'^sites/', decorated_includes(admin_required, include(environment_urls))),
    re_path(r'^profile/', decorated_includes(admin_required, include(profile_urls))),
    re_path(r'^visits-tracking/', decorated_includes(admin_required, include(matomo_urls))),
    re_path(r'^theme/', decorated_includes(admin_required, include(theme_urls))),
    re_path(r'^emails/', decorated_includes(admin_required, include(emails_urls))),
    re_path(r'^seo/', decorated_includes(admin_required, include(seo_urls))),
    re_path(r'^sms/', decorated_includes(admin_required, include(sms_urls))),
    re_path(r'^debug/', decorated_includes(admin_required, include(debug_urls))),
    re_path(r'^applications/', decorated_includes(admin_required, include(applications_urls))),
    re_path(r'^maintenance/', decorated_includes(admin_required, include(maintenance_urls))),
    path('', include(security_urls)),
    path('api/health/', health_json, name='health-json'),
    re_path(r'^menu.json$', menu_json, name='menu_json'),
    re_path(r'^hobos.json$', hobo),
]

# add authentication patterns
urlpatterns += [
    path('logout/', logout, name='logout'),
    path('login/', login, name='auth_login'),
    path('login/local/', login_local),  # to be used as backup, in case of idp down
    path('accounts/mellon/', include('mellon.urls')),
]

if settings.DEBUG and 'debug_toolbar' in settings.INSTALLED_APPS:
    import debug_toolbar  # pylint: disable=import-error

    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
