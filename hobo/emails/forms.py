# hobo - portal to configure and deploy applications
# Copyright (C) 2015-2016 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from django import forms
from django.core.validators import validate_email
from django.utils.translation import gettext_lazy as _

from hobo.emails.validators import validate_email_address, validate_email_domain, validate_email_spf


class ValidEmailField(forms.EmailField):
    def validate(self, value):
        validate_email(value)
        validate_email_domain(value)
        validate_email_address(value)
        validate_email_spf(value)


class EmailsForm(forms.Form):
    default_from_email = ValidEmailField(label=_('Default From'), help_text=_('Sender email address'))
    email_sender_name = forms.CharField(
        label=_('Sender Name'), required=False, help_text=_('Custom sender name (defaults to global title)')
    )
    global_email_prefix = forms.CharField(
        label=_('Prefix'),
        required=False,
        help_text=_('Custom prefix for emails subject (defaults to global title)'),
    )
    email_signature = forms.CharField(label=_('Signature'), required=False, widget=forms.Textarea)
    email_unsubscribe_info_url = forms.CharField(
        label=_('URL with informations about emails sent by the platform (List-Unsubscribe header)'),
        required=False,
        help_text=_('It should contain details such as what emails are sent and how not to receive them.'),
    )
    email_abuse_report_url = forms.CharField(
        label=_('URL for email abuse reports (X-Report-Abuse header)'),
        required=False,
        help_text=_('It should contain a form to submit email abuse reports'),
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['email_signature'].widget.attrs = {'rows': 4, 'cols': 80}
