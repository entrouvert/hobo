# hobo - portal to configure and deploy applications
# Copyright (C) 2015-2021 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import django.core.mail.backends.smtp
from django.conf import settings


class EmailBackend(django.core.mail.backends.smtp.EmailBackend):
    def _send(self, email_message):
        if getattr(settings, 'TEMPLATE_VARS', None):
            for var in ('email_unsubscribe_info_url', 'portal_url'):
                try:
                    url = settings.TEMPLATE_VARS[var]
                    if url:
                        email_message.extra_headers['List-Unsubscribe'] = '<%s>' % url
                        break
                except (KeyError, TypeError):
                    pass
            try:
                url = settings.TEMPLATE_VARS['email_abuse_report_url']
                if url:
                    email_message.extra_headers['X-Report-Abuse'] = (
                        'Please report abuse for this email here: %s' % url
                    )
            except (KeyError, TypeError):
                pass
        return super()._send(email_message)
