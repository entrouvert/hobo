# hobo - portal to configure and deploy applications
# Copyright (C) 2015-2016 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.utils.translation import gettext as _
from django.views.generic import TemplateView

from hobo.environment.forms import VariablesFormMixin

from .forms import EmailsForm


class HomeView(VariablesFormMixin, TemplateView):
    template_name = 'hobo/emails_home.html'
    variables = [
        'default_from_email',
        'email_sender_name',
        'global_email_prefix',
        'email_signature',
        'email_unsubscribe_info_url',
        'email_abuse_report_url',
    ]
    form_class = EmailsForm
    success_message = _('Emails settings have been updated. It will take a few seconds to be effective.')


home = HomeView.as_view()
