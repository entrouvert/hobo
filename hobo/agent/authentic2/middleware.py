from django.conf import settings
from django.utils.deprecation import MiddlewareMixin

# initialized in Authentic2AgentConfig.ready
from .provisionning import provisionning  # pylint: disable=no-name-in-module


class ProvisionningMiddleware(MiddlewareMixin):
    def process_request(self, request):
        provisionning.start()

    def process_exception(self, request, exception):
        provisionning.clear()

    def process_response(self, request, response):
        provisionning.stop(provision=True, wait=getattr(settings, 'HOBO_PROVISIONNING_SYNCHRONOUS', False))
        provisionning.clear()
        return response
