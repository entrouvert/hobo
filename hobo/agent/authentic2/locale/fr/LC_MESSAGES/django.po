# Translation of hobo
# Copyright (C) 2014 Entr'ouvert
# This file is distributed under the same license as the hobo package.
# Frederic Peters <fpeters@entrouvert.com>, 2014
#
msgid ""
msgstr ""
"Project-Id-Version: hobo 0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-10-31 09:36+0100\n"
"PO-Revision-Date: 2020-02-07 08:44+0100\n"
"Last-Translator: Frederic Peters <fpeters@entrouvert.com>\n"
"Language: French\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: hobo/agent/authentic2/management/commands/hobo_deploy.py
#, python-format
msgid "Superuser of %s"
msgstr "Administrateur de %s"
