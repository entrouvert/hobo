# hobo - portal to configure and deploy applications
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import importlib

from authentic2.a2_rbac.signals import post_soft_create, post_soft_delete
from django.apps import AppConfig
from django.conf import settings
from django.db.models.signals import m2m_changed, post_save, pre_delete, pre_save


class Plugin:
    def get_before_urls(self):
        from . import urls

        return urls.urlpatterns


class Authentic2AgentConfig(AppConfig):
    name = 'hobo.agent.authentic2'
    label = 'authentic2_agent'
    verbose_name = 'Authentic2 Agent'

    def get_a2_plugin(self):
        return Plugin()

    def ready(self):
        from . import provisionning

        engine = provisionning.Provisionning()
        provisionning.provisionning = engine

        if getattr(settings, 'HOBO_PROVISIONNING', True):
            pre_save.connect(engine.pre_save)
            post_save.connect(engine.post_save)
            pre_delete.connect(engine.pre_delete)
            m2m_changed.connect(engine.m2m_changed)
            post_soft_create.connect(engine.post_soft_create)
            post_soft_delete.connect(engine.post_soft_delete)

        # must use importlib.import_module as command's module names are not
        # proper python symbols
        commands_to_provision = [
            'authentic2.custom_user.management.commands.fix-attributes',
            'authentic2.management.commands.check-and-repair',
            'authentic2.management.commands.clean-unused-accounts',
            'authentic2.management.commands.cleanupauthentic',
            'authentic2.management.commands.deactivate-orphaned-ldap-users',
            'authentic2.management.commands.import_site',
            'authentic2.management.commands.sync-ldap-users',
            'authentic2_auth_oidc.management.commands.oidc-sync-provider',
            'django.contrib.auth.management.commands.createsuperuser',
            'hobo.multitenant.management.commands.runscript',
        ]

        def make_execute(old_execute):
            def new_execute(self, *args, **kwargs):
                with engine:
                    return old_execute(self, *args, **kwargs)

            return new_execute

        for command_to_provision in commands_to_provision:
            module = importlib.import_module(command_to_provision)
            module.Command.execute = make_execute(module.Command.execute)
