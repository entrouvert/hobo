from django.core.management.base import BaseCommand


class Command(BaseCommand):
    args = ['...']

    def handle(self, *args, **options):
        pass
