from django.apps import AppConfig


class HoboAgentConfig(AppConfig):
    name = 'hobo.agent.hobo'
    label = 'hobo_agent'
    verbose_name = 'Hobo Agent'
