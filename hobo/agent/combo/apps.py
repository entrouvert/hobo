from django.apps import AppConfig


class ComboAgentConfig(AppConfig):
    name = 'hobo.agent.combo'
    label = 'combo_agent'
    verbose_name = 'Combo Agent'
