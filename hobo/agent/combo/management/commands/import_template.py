# hobo - portal to configure and deploy applications
# Copyright (C) 2015-2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from hobo.agent.common.management.commands import import_template


class Command(import_template.Command):
    def handle(self, *args, **kwargs):
        try:
            return super().handle(*args, **kwargs)
        except import_template.UnknownTemplateError:
            # ignore errors if template name contains portal-user or portal-agent as
            # those names do not actually require an existing file to work.
            if not any(name in kwargs.get('template_name') for name in ('portal-user', 'portal-agent')):
                raise
