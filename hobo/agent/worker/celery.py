# hobo - portal to configure and deploy applications
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from celery import Celery
from kombu.common import Broadcast

from . import services, settings

app = Celery('hobo', broker=settings.BROKER_URL)
app.conf.update(
    task_serializer='json',
    accept_content=['json'],
    result_serializer='json',
    task_queues=(Broadcast('broadcast_tasks'),),
)

if hasattr(settings, 'CELERY_SETTINGS'):
    app.conf.update(settings.CELERY_SETTINGS)


@app.task(name='hobo-deploy', bind=True)
def deploy(self, environment):
    services.deploy(environment)


@app.task(name='hobo-notify', bind=True, acks_late=True)
def hobo_notify(self, notification):
    assert 'tenant' in notification
    assert 'data' in notification
    services.notify(notification['data'])
