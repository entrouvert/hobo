import os

# AMQP message broker
BROKER_URL = 'amqp://'

# It's possible to limit agents to particular applications, or particular
# hostnames, using the AGENT_HOST_PATTERNS configuration variable.
#
# The format is a dictionary with applications as keys and a list of hostnames as
# value. The hostnames can be prefixed by an exclamation mark to exclude them.
#
#  AGENT_HOST_PATTERNS = {
#      'wcs': ['*.example.net', '!  *.dev.example.net'],
#  }
#
# Will limit wcs deployments to *.example.net hostnames, while excluding
# *.dev.example.net.
AGENT_HOST_PATTERNS = None

WCS_MANAGE_COMMAND = '/usr/bin/wcs-manage'
AUTHENTIC_MANAGE_COMMAND = '/usr/bin/authentic2-multitenant-manage'
COMBO_MANAGE_COMMAND = '/usr/lib/combo/manage.py'
PASSERELLE_MANAGE_COMMAND = '/usr/lib/passerelle/manage.py'
FARGO_MANAGE_COMMAND = '/usr/bin/fargo-manage'
WELCO_MANAGE_COMMAND = '/usr/bin/welco-manage'
CHRONO_MANAGE_COMMAND = '/usr/bin/chrono-manage'
LINGO_MANAGE_COMMAND = '/usr/bin/lingo-manage'
HOBO_MANAGE_COMMAND = '/usr/bin/hobo-manage'
BIJOE_MANAGE_COMMAND = '/usr/bin/bijoe-manage'

WCS_MANAGE_TRY_COMMAND = WCS_MANAGE_COMMAND
AUTHENTIC_MANAGE_TRY_COMMAND = AUTHENTIC_MANAGE_COMMAND
COMBO_MANAGE_TRY_COMMAND = COMBO_MANAGE_COMMAND
PASSERELLE_MANAGE_TRY_COMMAND = PASSERELLE_MANAGE_COMMAND
FARGO_MANAGE_TRY_COMMAND = FARGO_MANAGE_COMMAND
CHRONO_MANAGE_TRY_COMMAND = CHRONO_MANAGE_COMMAND
LINGO_MANAGE_TRY_COMMAND = LINGO_MANAGE_COMMAND
WELCO_MANAGE_TRY_COMMAND = WELCO_MANAGE_COMMAND
HOBO_MANAGE_TRY_COMMAND = HOBO_MANAGE_COMMAND
BIJOE_MANAGE_TRY_COMMAND = BIJOE_MANAGE_COMMAND

PASSERELLE_TENANTS_DIRECTORY = '/var/lib/passerelle/tenants'
WCS_TENANTS_DIRECTORY = '/var/lib/wcs'
AUTHENTIC_TENANTS_DIRECTORY = '/var/lib/authentic2-multitenant/tenants'
CHRONO_TENANTS_DIRECTORY = '/var/lib/chrono/tenants'
COMBO_TENANTS_DIRECTORY = '/var/lib/combo/tenants'
FARGO_TENANTS_DIRECTORY = '/var/lib/fargo/tenants'
LINGO_TENANTS_DIRECTORY = '/var/lib/lingo/tenants'
WELCO_TENANTS_DIRECTORY = '/var/lib/welco/tenants'
BIJOE_TENANTS_DIRECTORY = '/var/lib/bijoe/tenants'
HOBO_TENANTS_DIRECTORY = '/var/lib/hobo/tenants'


local_settings_file = os.environ.get(
    'HOBO_AGENT_SETTINGS_FILE', os.path.join(os.path.dirname(__file__), 'local_settings.py')
)
if os.path.exists(local_settings_file):
    with open(local_settings_file) as f:
        exec(f.read())
