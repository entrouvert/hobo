from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('common', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='role',
            name='details',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='role',
            name='description',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
    ]
