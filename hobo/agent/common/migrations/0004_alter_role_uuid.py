# Generated by Django 3.2.13 on 2022-06-08 08:02

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('common', '0003_auto_20200707_1656'),
    ]

    operations = [
        migrations.AlterField(
            model_name='role',
            name='uuid',
            field=models.CharField(db_index=True, max_length=32),
        ),
    ]
