from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('auth', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Role',
            fields=[
                (
                    'group_ptr',
                    models.OneToOneField(
                        parent_link=True,
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        to='auth.Group',
                        on_delete=models.CASCADE,
                    ),
                ),
                ('uuid', models.CharField(max_length=32)),
                ('description', models.TextField()),
            ],
            options={},
            bases=('auth.group',),
        ),
    ]
