from celery import Celery
from django.conf import settings
from django.db import connection
from kombu.common import Broadcast


def notify_agents(data):
    '''Send notifications to all other tenants'''
    tenant = connection.tenant
    if not hasattr(tenant, 'domain_url'):
        # Fake tenant, certainly during migration, do nothing
        return
    notification = {
        'tenant': tenant.domain_url,
        'data': data,
    }
    with Celery('hobo', broker=settings.BROKER_URL) as app:
        app.conf.update(
            task_serializer='json',
            accept_content=['json'],
            result_serializer='json',
            task_queues=(Broadcast('broadcast_tasks'),),
            event_queue_ttl=10,
        )
        # see called method in hobo.agent.worker.celery
        app.send_task(
            'hobo-notify', (notification,), expires=settings.BROKER_TASK_EXPIRES, queue='broadcast_tasks'
        )
