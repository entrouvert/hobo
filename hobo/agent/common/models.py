from django.conf import settings
from django.contrib.auth.models import Group
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.db.models import JSONField


class Role(Group):
    uuid = models.CharField(max_length=32, db_index=True)
    slug = models.SlugField(max_length=256)
    description = models.TextField(default='')
    details = models.TextField(default='')
    emails = ArrayField(models.CharField(max_length=128), default=list)
    emails_to_members = models.BooleanField(default=True)

    objects = models.Manager()


class UserExtraAttributes(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='extra_attributes',
    )
    data = JSONField(default=dict)
