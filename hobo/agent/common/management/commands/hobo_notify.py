# hobo - portal to configure and deploy applications
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import logging
import os
import sys

from django.core.management.base import BaseCommand
from tenant_schemas.utils import tenant_context

from hobo.multitenant.middleware import TenantMiddleware
from hobo.provisionning.utils import NotificationProcessing, TryAgain

logger = logging.getLogger(__name__)


class Command(BaseCommand, NotificationProcessing):
    requires_system_checks = []

    def add_arguments(self, parser):
        parser.add_argument('notification', metavar='NOTIFICATION', type=str)

    @classmethod
    def load_notification(cls, notification):
        if notification == '-':
            # get environment definition from stdin
            return json.load(sys.stdin)
        else:
            return json.load(open(notification))

    def handle(self, notification, **kwargs):
        notification = self.load_notification(notification)
        for tenant in TenantMiddleware.get_tenants():
            if not os.path.exists(os.path.join(tenant.get_directory(), 'hobo.json')):
                continue
            with tenant_context(tenant):
                self.process_notification(tenant, notification)

    @classmethod
    def process_notification(cls, tenant, notification):
        assert cls.check_valid_notification(notification), 'invalid notification'
        service = tenant.get_service()
        action = notification['@type']
        audience = notification['audience']
        full = notification['full'] if 'full' in notification else False
        entity_id = service.get('saml-sp-metadata-url')
        issuer = notification.get('issuer')
        assert entity_id, 'service has no saml-sp-metadat-url field'
        if entity_id not in audience:
            return
        object_type = notification['objects']['@type']
        msg = 'received request for %sing %%d %%s objects (Celery)' % action
        logger.info(msg, len(notification['objects']['data']), object_type)
        for _ in range(20):
            try:
                getattr(cls, 'provision_' + object_type)(
                    issuer, action, notification['objects']['data'], full=full
                )
            except TryAgain:
                continue
            break
