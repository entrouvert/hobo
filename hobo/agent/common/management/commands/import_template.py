# hobo - portal to configure and deploy applications
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from django.conf import settings
from django.core.management import call_command, get_commands
from django.core.management.base import BaseCommand, CommandError


class UnknownTemplateError(CommandError):
    pass


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('template_name', type=str)
        parser.add_argument('--basepath', type=str)

    def handle(self, *args, **kwargs):
        app_name = settings.PROJECT_NAME
        basepath = kwargs.get('basepath') or '/var/lib/%s/skeletons' % app_name
        template = '%s/%s.json' % (basepath, kwargs.get('template_name'))

        if 'import_site' in get_commands():
            if not os.path.exists(template):
                raise UnknownTemplateError('unknown template (%r)' % template)
            call_command('import_site', template)
