# hobo - portal to configure and deploy applications
# Copyright (C) 2015-2021 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import decimal
import sys

from django import template
from django.utils.translation import get_language
from num2words import num2words

register = template.Library()


def unlazy(x):
    return x.get_value() if hasattr(x, 'get_value') else x


def parse_float(value):
    value = unlazy(value)
    if isinstance(value, str):
        # replace , by . for French users comfort
        value = value.replace(',', '.')
    try:
        return float(value)
    except (ValueError, TypeError, OverflowError):
        return ''


@register.filter
def as_numeral(number):
    number = parse_float(number)
    if not number:
        return ''
    number = int(number)
    try:
        return num2words(number, lang=get_language())
    except (TypeError, ValueError, decimal.InvalidOperation):
        return ''


@register.filter
def as_numeral_currency(number):
    number = parse_float(number)
    if not number:
        return ''
    try:
        # workaround newer num2words always specifiying cents, and outputting consecutive spaces
        return (
            num2words(unlazy(number), lang=get_language(), to='currency')
            .removesuffix(' et zéro centimes')
            .replace(' ', ' ')
        )
    except (TypeError, ValueError, decimal.InvalidOperation):
        return ''


@register.filter
def has_role_uuid(user, role_uuid):
    if not user or not user.is_authenticated:
        return False
    if 'authentic2' in sys.modules:
        return user.roles.filter(uuid=role_uuid).exists()
    elif 'wcs' in sys.modules:
        return role_uuid in user.roles
    return user.groups.filter(role__uuid=role_uuid).exists()
