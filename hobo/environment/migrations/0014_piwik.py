from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('environment', '0013_auto_20160226_1633'),
    ]

    operations = [
        migrations.CreateModel(
            name='Piwik',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(max_length=50, verbose_name='Title')),
                ('slug', models.SlugField(verbose_name='Slug')),
                ('base_url', models.CharField(max_length=200, verbose_name='Base URL')),
                ('secret_key', models.CharField(max_length=60, verbose_name='Secret Key')),
                ('template_name', models.CharField(max_length=60, verbose_name='Template', blank=True)),
                ('last_operational_check_timestamp', models.DateTimeField(null=True)),
                ('last_operational_success_timestamp', models.DateTimeField(null=True)),
                ('last_update_timestamp', models.DateTimeField(auto_now=True, null=True)),
                ('admin_emails', models.TextField(verbose_name='Admins Emails')),
            ],
            options={
                'ordering': ['title'],
                'verbose_name': 'Piwik',
                'verbose_name_plural': 'Piwik',
            },
            bases=(models.Model,),
        ),
    ]
