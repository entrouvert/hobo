from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('environment', '0004_fargo'),
    ]

    operations = [
        migrations.AddField(
            model_name='variable',
            name='label',
            field=models.CharField(default='', max_length=100, verbose_name='label', blank=True),
            preserve_default=False,
        ),
    ]
