from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('environment', '0002_authentic_use_as_idp_for_self'),
    ]

    operations = [
        migrations.AlterField(
            model_name='authentic',
            name='slug',
            field=models.SlugField(verbose_name='Slug'),
        ),
        migrations.AlterField(
            model_name='combo',
            name='slug',
            field=models.SlugField(verbose_name='Slug'),
        ),
        migrations.AlterField(
            model_name='passerelle',
            name='slug',
            field=models.SlugField(verbose_name='Slug'),
        ),
        migrations.AlterField(
            model_name='wcs',
            name='slug',
            field=models.SlugField(verbose_name='Slug'),
        ),
    ]
