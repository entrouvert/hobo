from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('environment', '0009_mandayejs'),
    ]

    operations = [
        migrations.AddField(
            model_name='variable',
            name='auto',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
