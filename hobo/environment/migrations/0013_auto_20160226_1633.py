from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('environment', '0012_mandayejs_site_app'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mandayejs',
            name='site_app',
            field=models.CharField(
                default=b'mandayejs.applications.Test',
                max_length=128,
                verbose_name='Site Application',
                choices=[
                    (b'mandayejs.applications.Test', b'Test'),
                    (b'mandayejs.applications.Duonet', b'Duonet'),
                    (b'mandayejs.applications.Sezhame', b'Sezhame'),
                    (b'mandayejs.applications.Archimed', b'Archimed'),
                    (b'mandayejs.applications.ImuseFamilyMontpellier', b'ImuseFamilyMontpellier'),
                ],
            ),
            preserve_default=True,
        ),
    ]
