from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('contenttypes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Authentic',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(max_length=50, verbose_name='Title')),
                ('slug', models.SlugField()),
                ('base_url', models.CharField(max_length=200, verbose_name='Base URL')),
                ('secret_key', models.CharField(max_length=60, verbose_name='Secret Key')),
                ('template_name', models.CharField(max_length=60, verbose_name='Template', blank=True)),
                ('last_operational_check_timestamp', models.DateTimeField(null=True)),
                ('last_operational_success_timestamp', models.DateTimeField(null=True)),
                ('last_update_timestamp', models.DateTimeField(auto_now=True, null=True)),
            ],
            options={
                'verbose_name': 'Authentic Identity Provider',
                'verbose_name_plural': 'Authentic Identity Providers',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Combo',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(max_length=50, verbose_name='Title')),
                ('slug', models.SlugField()),
                ('base_url', models.CharField(max_length=200, verbose_name='Base URL')),
                ('secret_key', models.CharField(max_length=60, verbose_name='Secret Key')),
                ('template_name', models.CharField(max_length=60, verbose_name='Template', blank=True)),
                ('last_operational_check_timestamp', models.DateTimeField(null=True)),
                ('last_operational_success_timestamp', models.DateTimeField(null=True)),
                ('last_update_timestamp', models.DateTimeField(auto_now=True, null=True)),
            ],
            options={
                'verbose_name': 'Combo Portal',
                'verbose_name_plural': 'Combo Portals',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Passerelle',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(max_length=50, verbose_name='Title')),
                ('slug', models.SlugField()),
                ('base_url', models.CharField(max_length=200, verbose_name='Base URL')),
                ('secret_key', models.CharField(max_length=60, verbose_name='Secret Key')),
                ('template_name', models.CharField(max_length=60, verbose_name='Template', blank=True)),
                ('last_operational_check_timestamp', models.DateTimeField(null=True)),
                ('last_operational_success_timestamp', models.DateTimeField(null=True)),
                ('last_update_timestamp', models.DateTimeField(auto_now=True, null=True)),
            ],
            options={
                'verbose_name': 'Passerelle',
                'verbose_name_plural': 'Passerelle',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Variable',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('name', models.CharField(max_length=100, verbose_name='name')),
                (
                    'value',
                    models.TextField(
                        help_text='start with [ or { for a JSON document', verbose_name='value', blank=True
                    ),
                ),
                ('service_pk', models.PositiveIntegerField(null=True)),
                ('last_update_timestamp', models.DateTimeField(auto_now=True, null=True)),
                (
                    'service_type',
                    models.ForeignKey(to='contenttypes.ContentType', null=True, on_delete=models.CASCADE),
                ),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Wcs',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(max_length=50, verbose_name='Title')),
                ('slug', models.SlugField()),
                ('base_url', models.CharField(max_length=200, verbose_name='Base URL')),
                ('secret_key', models.CharField(max_length=60, verbose_name='Secret Key')),
                ('template_name', models.CharField(max_length=60, verbose_name='Template', blank=True)),
                ('last_operational_check_timestamp', models.DateTimeField(null=True)),
                ('last_operational_success_timestamp', models.DateTimeField(null=True)),
                ('last_update_timestamp', models.DateTimeField(auto_now=True, null=True)),
            ],
            options={
                'verbose_name': 'w.c.s. Web Forms',
                'verbose_name_plural': '.w.c.s. Web Forms',
            },
            bases=(models.Model,),
        ),
    ]
