from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('environment', '0017_hobo'),
    ]

    operations = [
        migrations.AddField(
            model_name='authentic',
            name='secondary',
            field=models.BooleanField(default=False, verbose_name='Secondary Service'),
        ),
        migrations.AddField(
            model_name='bijoe',
            name='secondary',
            field=models.BooleanField(default=False, verbose_name='Secondary Service'),
        ),
        migrations.AddField(
            model_name='chrono',
            name='secondary',
            field=models.BooleanField(default=False, verbose_name='Secondary Service'),
        ),
        migrations.AddField(
            model_name='combo',
            name='secondary',
            field=models.BooleanField(default=False, verbose_name='Secondary Service'),
        ),
        migrations.AddField(
            model_name='corbo',
            name='secondary',
            field=models.BooleanField(default=False, verbose_name='Secondary Service'),
        ),
        migrations.AddField(
            model_name='fargo',
            name='secondary',
            field=models.BooleanField(default=False, verbose_name='Secondary Service'),
        ),
        migrations.AddField(
            model_name='hobo',
            name='secondary',
            field=models.BooleanField(default=False, verbose_name='Secondary Service'),
        ),
        migrations.AddField(
            model_name='mandayejs',
            name='secondary',
            field=models.BooleanField(default=False, verbose_name='Secondary Service'),
        ),
        migrations.AddField(
            model_name='passerelle',
            name='secondary',
            field=models.BooleanField(default=False, verbose_name='Secondary Service'),
        ),
        migrations.AddField(
            model_name='piwik',
            name='secondary',
            field=models.BooleanField(default=False, verbose_name='Secondary Service'),
        ),
        migrations.AddField(
            model_name='wcs',
            name='secondary',
            field=models.BooleanField(default=False, verbose_name='Secondary Service'),
        ),
        migrations.AddField(
            model_name='welco',
            name='secondary',
            field=models.BooleanField(default=False, verbose_name='Secondary Service'),
        ),
    ]
