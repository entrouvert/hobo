from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('environment', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='authentic',
            name='use_as_idp_for_self',
            field=models.BooleanField(default=False, verbose_name='Use as IdP'),
            preserve_default=True,
        ),
    ]
