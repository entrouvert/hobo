from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('environment', '0006_auto_20150708_0830'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='wcs',
            options={'verbose_name': 'w.c.s. Web Forms', 'verbose_name_plural': 'w.c.s. Web Forms'},
        ),
    ]
