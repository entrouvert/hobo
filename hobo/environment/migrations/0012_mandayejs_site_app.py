from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('environment', '0011_chrono'),
    ]

    operations = [
        migrations.AddField(
            model_name='mandayejs',
            name='site_app',
            field=models.CharField(
                default=b'mandayejs.applications.Test',
                max_length=128,
                verbose_name='App Settings',
                choices=[
                    (b'mandayejs.applications.Test', b'Test'),
                    (b'mandayejs.applications.Duonet', b'Duonet'),
                    (b'mandayejs.applications.Sezhame', b'Sezhame'),
                    (b'mandayejs.applications.Archimed', b'Archimed'),
                    (b'mandayejs.applications.ImuseFamilyMontpellier', b'ImuseFamilyMontpellier'),
                ],
            ),
            preserve_default=True,
        ),
    ]
