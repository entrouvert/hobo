# Generated by Django 2.2.28 on 2022-07-01 07:11

from django.db import migrations


def clean_internal_ips(apps, schema_editor):
    Variable = apps.get_model('environment', 'Variable')
    for var in Variable.objects.filter(name='SETTING_INTERNAL_IPS', service_pk__isnull=True, auto=True):
        if Variable.objects.filter(
            name='SETTING_INTERNAL_IPS.extend', service_pk__isnull=True, auto=True
        ).exists():
            var.delete()
        else:
            var.name = 'SETTING_INTERNAL_IPS.extend'
            var.save()


class Migration(migrations.Migration):
    dependencies = [
        ('environment', '0027_allow_long_slug'),
    ]

    operations = [
        migrations.RunPython(clean_internal_ips, migrations.RunPython.noop),
    ]
