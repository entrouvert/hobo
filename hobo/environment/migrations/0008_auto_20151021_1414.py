from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('environment', '0007_auto_20151008_1406'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='authentic',
            options={
                'ordering': ['title'],
                'verbose_name': 'Authentic Identity Provider',
                'verbose_name_plural': 'Authentic Identity Providers',
            },
        ),
        migrations.AlterModelOptions(
            name='combo',
            options={
                'ordering': ['title'],
                'verbose_name': 'Combo Portal',
                'verbose_name_plural': 'Combo Portals',
            },
        ),
        migrations.AlterModelOptions(
            name='fargo',
            options={
                'ordering': ['title'],
                'verbose_name': 'Fargo document box',
                'verbose_name_plural': 'Fargo document box',
            },
        ),
        migrations.AlterModelOptions(
            name='passerelle',
            options={
                'ordering': ['title'],
                'verbose_name': 'Passerelle',
                'verbose_name_plural': 'Passerelle',
            },
        ),
        migrations.AlterModelOptions(
            name='wcs',
            options={
                'ordering': ['title'],
                'verbose_name': 'w.c.s. Web Forms',
                'verbose_name_plural': 'w.c.s. Web Forms',
            },
        ),
        migrations.AlterModelOptions(
            name='welco',
            options={'ordering': ['title'], 'verbose_name': 'Welco Mail Channel'},
        ),
    ]
