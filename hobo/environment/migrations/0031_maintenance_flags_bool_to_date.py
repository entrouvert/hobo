import datetime
import json

from django.db import migrations

names = [
    ('MAINTENANCE_PAGE', 'MAINTENANCE_PAGE_EXPIRATION'),
    ('TENANT_DISABLE_CRON_JOBS', 'TENANT_DISABLED_CRON_JOBS_EXPIRATION'),
]


def bool_to_date(apps, schema_editor):
    Variable = apps.get_model('environment', 'Variable')
    expiration = (datetime.date.today() + datetime.timedelta(days=90)).isoformat()
    for oldname, newname in names:
        for var in Variable.objects.filter(auto=True, name='SETTING_%s' % oldname).all():
            try:
                value = json.loads(var.value)
            except ValueError:
                value = None
            if value:
                var.name = 'SETTING_%s' % newname
                var.value = expiration
                var.save()
            else:
                var.delete()


def date_to_bool(apps, schema_editor):
    Variable = apps.get_model('environment', 'Variable')
    for oldname, newname in names:
        for var in Variable.objects.filter(auto=True, name='SETTING_%s' % newname).all():
            try:
                value = datetime.date.fromisoformat(var.value)
            except (ValueError, AttributeError):
                value = None
            if value is not None and value >= datetime.date.today():
                var.name = 'SETTING_%s' % oldname
                var.value = json.dumps(True)
                var.save()
            else:
                var.delete()


class Migration(migrations.Migration):
    dependencies = [
        ('environment', '0030_delete_france_connect_settings'),
    ]

    operations = [
        migrations.RunPython(bool_to_date, reverse_code=date_to_bool),
    ]
