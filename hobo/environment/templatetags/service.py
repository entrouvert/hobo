from django.template import Library
from django.urls import reverse_lazy

from .. import forms

register = Library()


@register.filter(name='as_update_form')
def as_update_form(object):
    return getattr(forms, object.__class__.__name__ + 'Form')(instance=object).as_p()


@register.filter(name='save_url')
def save_url(object):
    return reverse_lazy('save-service', kwargs={'service': object.Extra.service_id, 'slug': object.slug})


@register.filter(name='delete_url')
def delete_url(object):
    return reverse_lazy('delete-service', kwargs={'service': object.Extra.service_id, 'slug': object.slug})
