import urllib.parse

from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from hobo.environment import models


def validate_service_url(url):
    if urllib.parse.urlparse(url).port is not None:
        raise ValidationError(
            _('Error: providing port is not supported on service url %(url)s'),
            code='invalid-url',
            params={'url': url},
        )
    if not models.is_resolvable(url):
        raise ValidationError(
            _('Error: %(netloc)s is not resolvable in URL %(url)s'),
            code='not-resolvable',
            params={'netloc': urllib.parse.urlsplit(url).netloc, 'url': url},
        )
    if not models.has_valid_certificate(url):
        raise ValidationError(
            _('Error: no valid certificate for %(url)s'), code='invalid-certificate', params={'url': url}
        )
