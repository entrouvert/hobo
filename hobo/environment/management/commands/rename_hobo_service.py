# hobo - portal to configure and deploy applications
# Copyright (C) 2022  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import urllib.parse

from django.core.management import call_command
from django.core.management.base import BaseCommand, CommandError
from tenant_schemas.utils import tenant_context

from hobo.deploy.signals import notify_agents
from hobo.environment.utils import get_or_create_local_hobo
from hobo.multitenant.middleware import TenantMiddleware, TenantNotFound


def normalize_url(url):
    if not url.endswith('/'):
        url += '/'
    return url


class Command(BaseCommand):
    help = 'Change domain name of a hobo service'

    def add_arguments(self, parser):
        parser.add_argument('src_url', type=str)
        parser.add_argument('target_url', type=str)

    def handle(self, src_url, target_url, *args, **kwargs):
        verbosity = kwargs.get('verbosity', 1)
        src_url, target_url = normalize_url(src_url), normalize_url(target_url)
        legacy_domain = urllib.parse.urlparse(src_url).netloc.split(':')[0]
        target_domain = urllib.parse.urlparse(target_url).netloc.split(':')[0]

        try:
            tenant = TenantMiddleware.get_tenant_by_hostname(legacy_domain)
        except TenantNotFound:
            raise CommandError('No service matches %s' % src_url)

        call_command('create_tenant', target_domain, legacy_hostname=legacy_domain)

        try:
            tenant = TenantMiddleware.get_tenant_by_hostname(target_domain)
        except TenantNotFound:
            raise CommandError('Could not find the new tenant %s' % target_domain)

        with tenant_context(tenant):
            local_hobo = get_or_create_local_hobo()
            local_hobo.change_base_url(target_url)
            local_hobo.save()
            notify_agents(None)

        if verbosity:
            print('Service renamed successfully, check it out: %s' % target_url)
