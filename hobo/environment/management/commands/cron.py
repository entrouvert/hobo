# hobo - © Entr'ouvert

from django.core.management.base import BaseCommand, CommandError

from hobo.deploy.signals import notify_agents
from hobo.utils import cron


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('frequency', metavar='FREQUENCY', type=str, help='only hourly for now')

    def handle(self, frequency, **options):
        if frequency != 'hourly':
            raise CommandError(f'unknown frequency "{frequency}"')
        cron(frequency)
        # notify agents if an environment model changed
        notify_agents(None)
