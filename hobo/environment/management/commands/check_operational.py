from django.core.management.base import BaseCommand

from hobo.environment import models


class Command(BaseCommand):
    help = '''Checks if all services are operational'''

    def handle(self, *args, **kwargs):
        for klass in models.AVAILABLE_SERVICES:
            for obj in klass.objects.all():
                obj.check_operational()
                if int(kwargs.get('verbosity')) > 1:
                    if obj.is_operational():
                        print('%s is operational' % obj.title)
                    else:
                        print(self.style.NOTICE('%s is NOT operational' % obj.title))
                        if obj.last_operational_success_timestamp:
                            print(
                                self.style.NOTICE(
                                    '  last operational success: %s' % obj.last_operational_success_timestamp
                                )
                            )
