# hobo - portal to configure and deploy applications
# Copyright (C) 2022  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.core.management.base import BaseCommand, CommandError

from hobo.deploy.signals import notify_agents
from hobo.environment.utils import get_installed_services, wait_operationals


def normalize_url(url):
    if not url.endswith('/'):
        url += '/'
    return url


class Command(BaseCommand):
    help = 'Change domain name of a service'

    def add_arguments(self, parser):
        parser.add_argument('src_url', type=str)
        parser.add_argument('target_url', type=str)
        parser.add_argument(
            '--timeout',
            type=int,
            action='store',
            default=120,
            help='set the timeout for the wait_operationals method',
        )

    def handle(self, src_url, target_url, *args, **kwargs):
        self.timeout = kwargs.get('timeout')
        self.verbosity = kwargs.get('verbosity', 1)
        self.terminal_width = 0

        src_url, target_url = normalize_url(src_url), normalize_url(target_url)
        target_service = None

        for service in get_installed_services():
            if service.get_base_url_path() == src_url:
                if service.secondary:
                    raise CommandError(
                        'Cannot rename a secondary service, you must run the command against the hobo tenant that is primary holding it'
                    )
                target_service = service
                break
        if target_service is None:
            raise CommandError('No service matches %s' % src_url)

        target_service.change_base_url(target_url)
        target_service.last_operational_check_timestamp = None
        target_service.last_operational_success_timestamp = None
        target_service.save()
        notify_agents(None)
        wait_operationals([target_service], self.timeout, self.verbosity, self.terminal_width, notify_agents)

        if self.verbosity:
            print('Service renamed successfully, check it out: %s' % target_url)
