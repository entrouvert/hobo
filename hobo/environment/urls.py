# hobo - portal to configure and deploy applications
# Copyright (C) 2015-2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import path, re_path

from . import views

urlpatterns = [
    path('variables', views.VariablesView.as_view(), name='environment-variables'),
    re_path(
        r'^variables-(?P<service>\w+)/(?P<slug>[\w-]+)$',
        views.VariablesView.as_view(),
        name='edit-variable-service',
    ),
    path(
        'new-variable',
        views.VariableCreateView.as_view(),
        name='new-variable',
    ),
    re_path(r'^update-variable/(?P<pk>\w+)$', views.VariableUpdateView.as_view(), name='update-variable'),
    re_path(r'^delete-variable/(?P<pk>\w+)$', views.VariableDeleteView.as_view(), name='delete-variable'),
    re_path(
        r'^check_operational/(?P<service>\w+)/(?P<slug>[\w-]+)$',
        views.operational_check_view,
        name='operational-check',
    ),
    path('select_create_service', views.ServiceSelectCreateView.as_view(), name='select-create-service'),
    re_path(r'^new-(?P<service>\w+)$', views.ServiceCreateView.as_view(), name='create-service'),
    re_path(
        r'^save-(?P<service>\w+)/(?P<slug>[\w-]+)$', views.ServiceUpdateView.as_view(), name='save-service'
    ),
    re_path(
        r'^delete-(?P<service>\w+)/(?P<slug>[\w-]+)$',
        views.ServiceDeleteView.as_view(),
        name='delete-service',
    ),
    re_path(
        r'^new-variable-(?P<service>\w+)/(?P<slug>[\w-]+)$',
        views.VariableCreateView.as_view(),
        name='new-variable-service',
    ),
    path('import/', views.ImportView.as_view(), name='environment-import'),
    path('export/', views.ExportView.as_view(), name='environment-export'),
    re_path(r'^debug.json$', views.debug_json, name='debug-json'),
]
