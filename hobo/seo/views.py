# hobo - portal to configure and deploy applications
# Copyright (C) 2020  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import FormView

from hobo.environment.utils import get_variable, set_variable

from .forms import RobotsTxtForm, SettingsForm

ALLOW = ''
DISALLOW = """User-agent: *
Disallow: /"""


def get_mode(content):
    content = content.strip().replace('\r', '')
    if content == ALLOW:
        return 'allow'
    if content == DISALLOW:
        return 'disallow'
    return 'customize'


class HomeView(FormView):
    template_name = 'hobo/seo_home.html'
    form_class = SettingsForm
    success_url = reverse_lazy('seo-home')

    def get_initial(self):
        initial = super().get_initial()
        initial['meta_description'] = get_variable('meta_description').value
        initial['meta_keywords'] = get_variable('meta_keywords').value
        initial['meta_robots'] = get_variable('meta_robots').value
        return initial

    def form_valid(self, form):
        set_variable('meta_description', form.cleaned_data['meta_description'])
        set_variable('meta_keywords', form.cleaned_data['meta_keywords'])
        set_variable('meta_robots', form.cleaned_data['meta_robots'])
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['robots_txt'] = get_variable('robots_txt').value
        context['mode'] = get_mode(context['robots_txt'])
        return context


home = HomeView.as_view()


def allow(request):
    set_variable('robots_txt', ALLOW)
    return HttpResponseRedirect(reverse_lazy('seo-home'))


def disallow(request):
    set_variable('robots_txt', DISALLOW)
    return HttpResponseRedirect(reverse_lazy('seo-home'))


class CustomizeView(FormView):
    template_name = 'hobo/robots_txt.html'
    form_class = RobotsTxtForm
    success_url = reverse_lazy('seo-home')

    def get_initial(self):
        initial = super().get_initial()
        initial['content'] = get_variable('robots_txt').value
        return initial

    def form_valid(self, form):
        set_variable('robots_txt', form.cleaned_data['content'])
        return super().form_valid(form)


customize = CustomizeView.as_view()
