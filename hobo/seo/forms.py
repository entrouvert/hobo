# hobo - portal to configure and deploy applications
# Copyright (C) 2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import forms
from django.utils.translation import gettext_lazy as _


class RobotsTxtForm(forms.Form):
    content = forms.CharField(label=_('Content of robots.txt file'), required=False, widget=forms.Textarea)


class SettingsForm(forms.Form):
    meta_description = forms.CharField(label=_('Description for indexing'), required=False)
    meta_keywords = forms.CharField(label=_('Keywords for indexing'), required=False)
    meta_robots = forms.CharField(
        label=_('Directives for indexing'),
        required=False,
        help_text=_('example: noindex, nofollow, noarchive, nosnippet, notranslate, noimageindex'),
    )


class EnableForm(forms.Form):
    pass
