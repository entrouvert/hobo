import json
import urllib.parse

import pkg_resources
from django.utils.deprecation import MiddlewareMixin

try:
    import apt_pkg
except ImportError:
    apt_pkg = None


class VersionMiddleware(MiddlewareMixin):
    ENTROUVERT_PACKAGES = [
        'wcs',
        'wcs-au-quotidien',
        'authentic2',
        'authentic2-idp-cas',
        'authentic2-idp-ltpa',
        'authentic2-idp-oauth2',
        'polynum',
        'appli_project',
        'passerelle',
        'docbow',
        'calebasse',
        'python-entrouvert',
        'nose',
        'portail-citoyen',
        'portail-citoyen2',
        'portail-citoyen-announces',
        'django-cms-ajax-text-plugin',
        'eopayment',
        'compte-meyzieu',
        'compte-agglo-montpellier',
        'compte-orleans',
        'hobo',
        'django-cmsplugin-blurp',
        'combo',
        'lingo',
        'fargo',
        'welco',
        'chrono',
    ]
    VERSION = 1
    _packages_version = None

    def __init__(self, application):
        self.application = application

    def __call__(self, environ, start_response):
        path = ''
        path += urllib.parse.quote(environ.get('SCRIPT_NAME', ''))
        path += urllib.parse.quote(environ.get('PATH_INFO', ''))
        method = environ.get('REQUEST_METHOD', 'GET')
        if method == 'GET' and (path in '/__version__', '/__version__/'):
            packages_version = self.get_packages_version()
            start_response('200 Ok', [('content-type', 'application/json')])
            return [json.dumps(packages_version)]
        return self.application(environ, start_response)

    @classmethod
    def get_packages_version(cls):
        if cls._packages_version is not None:
            return cls._packages_version
        packages_version = {}
        # get versions from setuptools
        for distribution in tuple(pkg_resources.WorkingSet()):
            project_name = distribution.project_name
            version = distribution.version
            version = version.replace('+', '-')  # for PEP440
            if project_name in cls.ENTROUVERT_PACKAGES:
                packages_version[project_name] = version
        # get versions from Debian packages
        if apt_pkg:
            for name, project_name, version in cls.pkgs_from_origin("Entr'ouvert"):
                version = version.split('-')[0]  # debian format == pythonversion-xxx
                version = version.replace('+', '-')  # for PEP440
                # alert if a version is already present and is different
                if (
                    project_name in packages_version
                    and version != packages_version[project_name].split('!!')[0]
                ):
                    packages_version[project_name] += '!!%s.deb:%s' % (name, version)
                else:
                    packages_version[project_name] = version
        cls._packages_version = packages_version
        return packages_version

    @staticmethod
    def pkgs_from_origin(wanted_origin):
        # Given a wanted origin, yield triple with :
        # package name, source package name, package version
        apt_pkg.init()
        cache = apt_pkg.Cache(None)
        pkg_record = apt_pkg.PackageRecords(cache)
        dep_cache = apt_pkg.DepCache(cache)
        for pkg in cache.packages:
            if pkg.current_state != apt_pkg.CURSTATE_INSTALLED:
                continue
            versions = {'installed': pkg.current_ver}
            cand = dep_cache.get_candidate_ver(pkg)
            if cand:
                versions['candidate'] = cand
            for version in versions.values():
                # When the same version is available from multiple release files
                # only take the first one : the one used for installation
                file, index = version.file_list[0]
                if file.origin == wanted_origin:
                    pkg_record.lookup((file, index))
                    yield pkg.name, pkg_record.source_pkg or pkg.name, versions['installed'].ver_str
                    break
