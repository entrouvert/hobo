# hobo - portal to configure and deploy applications
# Copyright (C) 2015-2018  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import django.utils.log
from django.conf import settings
from django.db import connection


class AdminEmailHandler(django.utils.log.AdminEmailHandler):
    def emit(self, record):
        maintenance_mode = getattr(settings, 'MAINTENANCE_PAGE', None)
        if maintenance_mode:
            return
        return super().emit(record)

    def format_subject(self, subject):
        subject = super().format_subject(subject)
        try:
            subject = '[%s] %s' % (connection.tenant.domain_url, subject)
        except AttributeError:
            pass
        return subject
