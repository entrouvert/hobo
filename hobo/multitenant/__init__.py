# import the context_processors module so it gets to compute the versions hash
# early on.
from hobo import context_processors

# install tenant aware thread classes to prevent other lib to use classic ones
from . import threads

threads.install_tenant_aware_threads()
