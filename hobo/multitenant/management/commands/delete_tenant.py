import sys

from django.core.management.base import BaseCommand, CommandError

from hobo.multitenant.middleware import TenantMiddleware


class Command(BaseCommand):
    help = 'Delete tenant(s) by hostname(s)'

    def add_arguments(self, parser):
        parser.add_argument(
            '--force-drop',
            action='store_true',
            default=False,
            help='If you want the schema to be deleted from database',
        )
        parser.add_argument('hostnames', metavar='HOSTNAME', nargs='+')

    def handle(self, hostnames, **options):
        if not hostnames:
            raise CommandError('you must give at least one tenant hostname')

        if '-' in hostnames:  # get additional list of hostnames from stdin
            hostnames = list(hostnames)
            hostnames.remove('-')
            hostnames.extend([x.strip() for x in sys.stdin.readlines()])
        for hostname in hostnames:
            tenant = TenantMiddleware.get_tenant_by_hostname(hostname)
            tenant.delete(force_drop=options.get('force_drop'))
