from django.core.management.commands import shell

from hobo.multitenant.management.commands import TenantWrappedCommand, disable_global_logging


class Command(TenantWrappedCommand):
    COMMAND = shell.Command

    def handle(self, *args, **kwargs):
        disable_global_logging()
        super().handle(*args, **kwargs)
