from django.core.management.base import BaseCommand
from django.db import connection

from hobo.multitenant.middleware import TenantMiddleware


class Command(BaseCommand):
    help = 'Create schemas for all declared tenants'

    def handle(self, *args, **options):
        verbosity = int(options.get('verbosity'))

        connection.set_schema_to_public()
        all_tenants = TenantMiddleware.get_tenants()
        for tenant in all_tenants:
            if verbosity >= 1:
                print()
                print(self.style.NOTICE('=== Creating schema ') + self.style.SQL_TABLE(tenant.schema_name))

            tenant.create_schema()
