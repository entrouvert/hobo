import sys

from django.core.management.base import CommandError

from hobo.agent.common.management.commands.hobo_deploy import Command as HoboDeployCommand
from hobo.multitenant.middleware import TenantMiddleware

from .create_tenant import Command as CreateTenantCommand


class Command(CreateTenantCommand):
    help = 'Create hobo tenant(s) by hostname(s)'

    def handle(self, hostnames, **options):
        if not hostnames:
            raise CommandError('you must give at least one tenant hostname')

        if '-' in hostnames:  # get additional list of hostnames from stdin
            hostnames = list(hostnames)
            hostnames.remove('-')
            hostnames.extend([x.strip() for x in sys.stdin.readlines()])

        super().handle(hostnames, **options)

        # create SP keys for each tenant
        for hostname in hostnames:
            tenant = TenantMiddleware.get_tenant_by_hostname(hostname)
            HoboDeployCommand().generate_saml_keys(tenant, prefix='sp-')
