from django.core.management.base import BaseCommand

from hobo.multitenant.middleware import TenantMiddleware


class Command(BaseCommand):
    requires_model_validation = True
    can_import_settings = True

    def handle(self, **options):
        all_tenants = TenantMiddleware.get_tenants()

        for tenant in all_tenants:
            print(f'{tenant.schema_name} {tenant.domain_url}')
