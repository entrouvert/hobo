# hobo - portal to configure and deploy applications
# Copyright (C) 2019 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf import settings
from django.core.management.commands.showmigrations import Command as ShowMigrationsCommand
from django.db import connection

from hobo.multitenant.management.commands import SyncCommon
from hobo.multitenant.middleware import TenantMiddleware, TenantNotFound


class ShowMigrationsSchemasCommand(SyncCommon):
    help = 'Show database schema migrations.'

    def add_arguments(self, parser):
        super().add_arguments(parser)
        command = ShowMigrationsCommand()
        command.add_arguments(parser)

    def handle(self, *args, **options):
        super().handle(*args, **options)
        if self.domain:
            try:
                tenant = TenantMiddleware.get_tenant_by_hostname(self.domain)
            except TenantNotFound:
                raise RuntimeError(f'Schema "{self.schema_name}" does not exist')
            self.run_showmigrations(tenant, settings.TENANT_APPS)
        else:
            for tenant in TenantMiddleware.get_tenants():
                self.run_showmigrations(tenant, settings.TENANT_APPS)

    def run_showmigrations(self, tenant, included_apps):
        self._notice('=== Show migrations for schema %s' % tenant.domain_url)
        connection.set_tenant(tenant, include_public=False)
        command = ShowMigrationsCommand()
        command.execute(*self.args, **self.options)
        connection.set_schema_to_public()


Command = ShowMigrationsSchemasCommand
