from django.apps import AppConfig

from . import settings, threads

_tenant_settings_wrapper = None


def clear_tenants_settings():
    if _tenant_settings_wrapper is not None:
        _tenant_settings_wrapper.__dict__['tenants_settings'] = {}


class MultitenantAppConfig(AppConfig):
    name = 'hobo.multitenant'
    verbose_name = 'Multitenant'

    def ready(self):
        # noqa pylint: disable=global-statement
        global _tenant_settings_wrapper

        from django import conf

        # Install tenant aware settings
        if not isinstance(conf.settings._wrapped, settings.TenantSettingsWrapper):
            _tenant_settings_wrapper = conf.settings._wrapped = settings.TenantSettingsWrapper(
                conf.settings._wrapped
            )
            # reset settings getattr method to a cache-less version, to cancel
            # https://code.djangoproject.com/ticket/27625.
            conf.LazySettings.__getattr__ = lambda self, name: getattr(self._wrapped, name)
        threads.install_tenant_aware_threads()
