import json
import logging
import os

from django.db import connection
from mellon.adapters import DefaultAdapter

from . import utils


class MellonAdapter(DefaultAdapter):
    def __init__(self, *args, **kwargs):
        self.logger = logging.getLogger(__name__)

    def get_identity_providers_setting(self):
        tenant_dir = connection.tenant.get_directory()
        hobo_json_path = os.path.join(tenant_dir, 'hobo.json')
        if not os.path.exists(hobo_json_path):
            return []
        with open(hobo_json_path) as fd:
            hobo_json = json.load(fd)
        # always look for the first active identity provider in the list of
        # services
        for service in hobo_json.get('services'):
            if not service.get('saml-idp-metadata-url'):
                continue
            metadata_filepath = os.path.join(tenant_dir, 'idp-metadata-%s.xml' % service.get('id'))
            if not os.path.exists(metadata_filepath):
                continue
            return [{'METADATA': metadata_filepath, 'SLUG': 'idp'}]
        return []

    def provision_groups(self, user, idp, saml_attributes):
        role_uuids = saml_attributes.get('role-slug', [])
        utils.provision_user_groups(user, role_uuids)
