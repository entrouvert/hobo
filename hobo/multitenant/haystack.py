# hobo - rapid remote deployment
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os
import shutil
import time

# noqa pylint: disable=import-error
import haystack.backends.whoosh_backend
from django.db import connection
from tenant_schemas.postgresql_backend.base import FakeTenant


class WhooshSearchBackend(haystack.backends.whoosh_backend.WhooshSearchBackend):
    @property
    def use_file_storage(self):
        return not (isinstance(connection.tenant, FakeTenant))

    @use_file_storage.setter
    def use_file_storage(self, value):
        pass

    def delete_index(self):
        renamed_path = None
        if os.path.exists(self.path):
            # rename existing path instead of removing, so it works on NFS
            # when there are opened files.
            renamed_path = self.path + '.deleted-%s' % time.time()
            os.rename(self.path, renamed_path)
        super().delete_index()
        if renamed_path:
            # remove afterwards and ignore errors (residual directories will
            # have to be cleaned manually)
            try:
                shutil.rmtree(renamed_path)
            except OSError:
                pass

    @property
    def path(self):
        tenant = connection.tenant
        return os.path.join(tenant.get_directory(), 'whoosh_index')

    @path.setter
    def path(self, value):
        pass

    def setup(self):
        super().setup()
        # make it be always reinitialized
        self.setup_complete = False


class WhooshEngine(haystack.backends.whoosh_backend.WhooshEngine):
    backend = WhooshSearchBackend
