import os

from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.db import connection
from django.utils._os import safe_join
from tenant_schemas.storage import TenantStorageMixin

__all__ = ('TenantFileSystemStorage',)


class TenantFileSystemStorage(FileSystemStorage, TenantStorageMixin):
    '''Lookup files in $TENANT_BASE/<tenant.schema>/media/'''

    # noqa pylint: disable=invalid-overridden-method
    @property
    def location(self):
        if connection.tenant:
            return safe_join(settings.TENANT_BASE, connection.tenant.domain_url, 'media')
        else:
            return os.path.abspath(self.base_location)
