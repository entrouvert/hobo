from django.conf import settings
from django.forms import ModelForm

if 'tenant_schemas' in settings.INSTALLED_APPS:
    from tenant_schemas.utils import get_tenant_model
else:
    get_tenant_model = lambda: None


class HoboForm(ModelForm):
    required_css_class = 'required'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['schema_name'].required = False

    def clean(self):
        from django.template.defaultfilters import slugify

        cleaned_data = super().clean()
        if not cleaned_data.get('schema_name') and cleaned_data.get('domain_url'):
            cleaned_data['schema_name'] = slugify(cleaned_data['domain_url'])
        return cleaned_data

    class Meta:
        model = get_tenant_model()
        fields = ['domain_url', 'schema_name']


class HoboUpdateForm(ModelForm):
    class Meta:
        model = get_tenant_model()
        fields = ['domain_url']
