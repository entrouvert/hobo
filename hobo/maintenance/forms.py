# hobo - portal to configure and deploy applications
# Copyright (C) 2015-2022  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import forms
from django.utils.translation import gettext_lazy as _

from .utils import validate_hostname


class MaintenanceForm(forms.Form):
    maintenance_page_expiration = forms.DateField(
        required=False,
        label=_('Enable maintenance until'),
    )
    confirm_maintenance_page = forms.BooleanField(
        label=_('Confirm maintenance page activation'), required=False, widget=forms.HiddenInput()
    )
    maintenance_page_message = forms.CharField(
        required=False, widget=forms.Textarea, label=_('Maintenance page message')
    )
    maintenance_pass_through_header = forms.CharField(
        required=False,
        label=_('Maintenance HTTP header pass through'),
        help_text=_('if this header is in requests, maintenance mode is avoided'),
    )
    maintenance_pass_through_dnswl = forms.CharField(
        required=False,
        label=_('Maintenance DNSWL pass through'),
        help_text=_('if the request origin IP is in the DNSWL, maintenance mode is avoided'),
        validators=(validate_hostname,),
    )
    disabled_cron_expiration = forms.DateField(
        required=False,
        label=_('Disable cron jobs until'),
        help_text=_('disables expiration jumps, appointment reminders, etc. until given date'),
    )

    def add_confirmation_checkbox(self):
        self.add_error(
            None,
            _(
                "No HTTP header pass through or DNSWL is configured, you won't be able to disable the maintenance page."
            ),
        )
        self.add_error(
            'confirm_maintenance_page',
            _('Check this box if you are sure to enable the maintenance page.'),
        )
        self.fields['confirm_maintenance_page'].widget = forms.CheckboxInput()

    def test_result(self, pass_through):
        self.add_error(
            None,
            (
                _('OK: You would have access in maintenance using this settings')
                if pass_through
                else _('KO: You would not have access in maintenance using this settings')
            ),
        )
