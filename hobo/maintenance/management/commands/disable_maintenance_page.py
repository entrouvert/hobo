from django.core.management.base import BaseCommand, CommandError

from hobo.environment.models import Variable
from hobo.environment.utils import get_setting_variable


class Command(BaseCommand):
    help = 'Toggle maintenance page'

    def handle(self, *args, **options):
        maintenance_page_variable = get_setting_variable('MAINTENANCE_PAGE')
        if not bool(maintenance_page_variable.json):
            self.stdout.write(self.style.SUCCESS('The maintenance page is already disabled.'))
            return
        maintenance_page_variable.json = False
        maintenance_page_variable.save()
        self.stdout.write(self.style.SUCCESS('Maintenance page disabled.'))
