# hobo - portal to configure and deploy applications
# Copyright (C) 2015-2022 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from datetime import date, timedelta

from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.urls import reverse_lazy
from django.utils.functional import cached_property
from django.utils.translation import gettext as _
from django.views.generic import FormView

from hobo.environment.utils import get_setting_variable

from ..middleware.maintenance import pass_through
from .forms import MaintenanceForm
from .utils import DNSWL, maintenance_is_activable


class HomeView(FormView):
    template_name = 'hobo/maintenance/home.html'
    form_class = MaintenanceForm
    success_url = reverse_lazy('maintenance-home')

    @cached_property
    def maintenance_page_expiration_variable(self):
        return get_setting_variable('MAINTENANCE_PAGE_EXPIRATION')

    @cached_property
    def maintenance_page_message_variable(self):
        return get_setting_variable('MAINTENANCE_PAGE_MESSAGE')

    @cached_property
    def maintenance_pass_through_header_variable(self):
        return get_setting_variable('MAINTENANCE_PASS_THROUGH_HEADER')

    @cached_property
    def maintenance_pass_through_dnswl_variable(self):
        return get_setting_variable('MAINTENANCE_PASS_THROUGH_DNSWL')

    @cached_property
    def tenant_disabled_cron_jobs_expiration_variable(self):
        return get_setting_variable('TENANT_DISABLED_CRON_JOBS_EXPIRATION')

    def setup(self, request, *args, **kwargs):
        if not maintenance_is_activable():
            raise PermissionDenied(
                _(
                    'Maintenance mode is not configured, you are not allowed to access to this page. Please contact an administrator.'
                )
            )
        return super().setup(request, *args, **kwargs)

    def get_initial(self):
        initial = super().get_initial()
        expiration = self.maintenance_page_expiration_variable.value
        if expiration:
            try:
                expiration = date.fromisoformat(expiration)
            except (TypeError, ValueError):
                expiration = date.today() - timedelta(days=1)

        cron_expiration = self.tenant_disabled_cron_jobs_expiration_variable.value
        if cron_expiration:
            try:
                cron_expiration = date.fromisoformat(cron_expiration)
            except (TypeError, ValueError):
                cron_expiration = date.today() - timedelta(days=1)

        initial['maintenance_page_expiration'] = expiration
        initial['maintenance_page_message'] = self.maintenance_page_message_variable.value
        initial['maintenance_pass_through_header'] = self.maintenance_pass_through_header_variable.value
        initial['maintenance_pass_through_dnswl'] = self.maintenance_pass_through_dnswl_variable.value
        initial['disabled_cron_expiration'] = cron_expiration
        return initial

    def form_valid(self, form):
        domain = form.cleaned_data['maintenance_pass_through_dnswl']
        if domain:
            try:
                DNSWL(domain)
            except ValueError:
                form.add_error('maintenance_pass_through_dnswl', _('This is not a valid domain'))
                return self.form_invalid(form)
        if 'test' in form.data:
            if pass_through(
                self.request,
                form.cleaned_data['maintenance_pass_through_dnswl'],
                form.cleaned_data['maintenance_pass_through_header'],
            ):
                form.test_result(True)
            else:
                form.test_result(False)
            return self.form_invalid(form)

        if (
            form.cleaned_data['maintenance_page_expiration']
            and not form.cleaned_data['maintenance_pass_through_header']
            and not form.cleaned_data['maintenance_pass_through_dnswl']
            and not form.cleaned_data['confirm_maintenance_page']
        ):
            form.add_confirmation_checkbox()
            return self.form_invalid(form)

        if (
            not form.cleaned_data['maintenance_page_expiration']
            or form.cleaned_data['maintenance_page_expiration'] < date.today()
        ):
            self.maintenance_page_expiration_variable.update_value('')
        else:
            self.maintenance_page_expiration_variable.update_value(
                form.cleaned_data['maintenance_page_expiration'].isoformat()
            )

        if (
            not form.cleaned_data['disabled_cron_expiration']
            or form.cleaned_data['disabled_cron_expiration'] < date.today()
        ):
            self.tenant_disabled_cron_jobs_expiration_variable.update_value('')
        else:
            self.tenant_disabled_cron_jobs_expiration_variable.update_value(
                form.cleaned_data['disabled_cron_expiration'].isoformat()
            )

        self.maintenance_page_message_variable.update_value(form.cleaned_data['maintenance_page_message'])
        self.maintenance_pass_through_header_variable.update_value(
            form.cleaned_data['maintenance_pass_through_header']
        )
        self.maintenance_pass_through_dnswl_variable.update_value(
            form.cleaned_data['maintenance_pass_through_dnswl']
        )

        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        pass_through_ips_setting = getattr(settings, 'MAINTENANCE_PASS_THROUGH_IPS', [])
        ctx['pass_through_ips'] = ', '.join(pass_through_ips_setting)
        return ctx


home = HomeView.as_view()
