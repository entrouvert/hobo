# hobo - portal to configure and deploy applications
# Copyright (C) 2015-2023  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import re

import dns.exception
import dns.resolver
from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.validators import validate_ipv46_address
from django.utils.translation import gettext as _

logger = logging.getLogger(__name__)


def maintenance_is_activable():
    return bool(
        getattr(settings, 'MAINTENANCE_PASS_THROUGH_IPS', [])
        or getattr(settings, 'MAINTENANCE_PASS_THROUGH_DNSWL', [])
    )


def _resolver_resolve(domain):
    try:
        method = dns.resolver.resolve
    except AttributeError:
        # support for dnspython 2.0.0 on bullseye, prevent deprecation warning on later versions
        method = dns.resolver.query
    return method(domain, 'A', lifetime=1)


def is_valid_hostname(hostname):
    if hostname[-1] == '.':
        # strip exactly one dot from the right, if present
        hostname = hostname[:-1]
    if len(hostname) > 253:
        return False

    labels = hostname.split('.')

    # the TLD must be not all-numeric
    if re.match(r'[0-9]+$', labels[-1]):
        return False

    allowed = re.compile(r'(?!-)[a-z0-9-]{1,63}(?<!-)$', re.IGNORECASE)
    return all(allowed.match(label) for label in labels)


def validate_hostname(hostname):
    if not is_valid_hostname(hostname):
        raise ValidationError(_('%(hostname)s is not a valid hostname'), params={'hostname': hostname})


def check_dnswl(dnswl, remote_addr):
    domain = '.'.join(reversed(remote_addr.split('.'))) + '.' + dnswl
    exception = None
    log = logger.debug
    try:
        answers = _resolver_resolve(domain)
        result = any(answer.address for answer in answers)
    except dns.resolver.NXDOMAIN as e:
        exception = e
        result = False
    except dns.resolver.NoAnswer as e:
        exception = e
        result = False
    except dns.exception.DNSException as e:
        exception = e
        log = logger.warning
        result = False
    log('utils: dnswl lookup of "%s", result=%s exception=%s', domain, result, exception)
    return result


class DNSWL:
    def __init__(self, domain):
        if not is_valid_hostname(domain):
            raise ValueError('%s is not a valid domain name' % domain)
        self.domain = domain

    def __contains__(self, remote_addr):
        if not remote_addr or not isinstance(remote_addr, str):
            return False
        validate_ipv46_address(remote_addr)
        return check_dnswl(self.domain, remote_addr)


def dnswl(domain):
    return DNSWL(domain)
