from django.apps import apps
from django.conf import settings
from django.template import Context, Template, TemplateSyntaxError, VariableDoesNotExist
from mellon.adapters import DefaultAdapter


class MellonAdapter(DefaultAdapter):
    def get_identity_providers_setting(self):
        from hobo.environment.models import Authentic

        try:
            self_idp = Authentic.objects.get(use_as_idp_for_self=True)
        except Authentic.DoesNotExist:
            return []
        return [{'METADATA_URL': self_idp.get_saml_idp_metadata_url(), 'SLUG': 'idp'}]


class TemplateError(Exception):
    def __init__(self, msg, params=()):
        self.msg = msg
        self.params = params

    def __str__(self):
        return self.msg % self.params


def get_templated_url(url, context=None):
    if '{{' not in url and '{%' not in url:
        return url
    template_vars = getattr(settings, 'TEMPLATE_VARS', {})
    if context:
        template_vars.update(context)
    template_vars = Context(template_vars, use_l10n=False)
    try:
        return Template(url).render(template_vars)
    except VariableDoesNotExist as e:
        raise TemplateError(e.msg, e.params)
    except TemplateSyntaxError:
        raise TemplateError('syntax error')


def cron(frequency):
    if frequency != 'hourly':
        raise ValueError(frequency)
    for app in apps.get_app_configs():
        method = getattr(app, frequency, None)
        if not method:
            continue
        method()
