import logging

import requests
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser
from django.core.exceptions import FieldDoesNotExist
from django.utils.module_loading import import_string
from rest_framework import authentication, exceptions, status

from hobo import signature
from hobo.requests_wrapper import Requests

try:
    from mellon.models import UserSAMLIdentifier
except ImportError:
    UserSAMLIdentifier = None

logger = logging.getLogger(__name__)


class AnonymousAuthenticServiceUser(AnonymousUser):
    '''This virtual user hold permissions for other publik services'''

    is_anonymous = True
    is_authenticated = True
    ou = None
    is_publik_service = True

    def has_perm(self, *args, **kwargs):
        return True

    def has_ou_perm(self, *args, **kwargs):
        return True

    def has_perm_any(self, *args, **kwargs):
        return True

    def filter_by_perm(self, perm_or_perms, qs):
        # all objects are reachable
        return qs

    def __unicode__(self):
        return 'Publik Service User'


class AnonymousAdminServiceUser(AnonymousUser):
    '''This virtual user hold permissions for other publik services'''

    is_staff = True
    is_anonymous = True
    is_authenticated = True
    ou = None
    is_publik_service = True

    def __unicode__(self):
        return 'Publik Service Admin'


class APIClientUser:
    is_active = True
    is_anonymous = False
    is_authenticated = True
    is_superuser = False
    roles = []
    service_superuser = {}

    def __init__(self, is_active, is_anonymous, is_authenticated, is_superuser, roles, service_superuser):
        self.is_active = is_active
        self.is_anonymous = is_anonymous
        self.is_authenticated = is_authenticated
        self.is_superuser = is_superuser
        self.roles = roles
        self.service_superuser = service_superuser

    @classmethod
    def from_dict(cls, data):
        if 'service_superuser' not in data:
            data['service_superuser'] = {}
        return cls(
            is_active=data['is_active'],
            is_anonymous=data['is_anonymous'],
            is_authenticated=data['is_authenticated'],
            is_superuser=data['is_superuser'],
            roles=data['roles'],
            service_superuser=data['service_superuser'],
        )


class PublikAuthenticationFailed(exceptions.APIException):
    status_code = status.HTTP_401_UNAUTHORIZED
    default_code = 'invalid-signature'

    def __init__(self, code):
        self.detail = {'err': 1, 'err_desc': code}


class PublikServiceAuthentication(authentication.BaseAuthentication):
    def resolve_user(self, request):
        if not hasattr(settings, 'HOBO_ANONYMOUS_SERVICE_USER_CLASS'):
            raise PublikAuthenticationFailed('no-user-found')

        klass = import_string(settings.HOBO_ANONYMOUS_SERVICE_USER_CLASS)
        logger.debug('publik-authentication: anonymous signature validated')
        return klass()

    def get_view_flag(self, request, name, default=True):
        view = request.parser_context.get('view')
        return getattr(view, name, default)

    def get_orig_key(self, orig):
        if not hasattr(settings, 'KNOWN_SERVICES'):
            logger.warning('publik-authentication: no known services')
            raise PublikAuthenticationFailed('publik-authentication: no-known-services-setting')
        for services in settings.KNOWN_SERVICES.values():
            for service in services.values():
                if service.get('verif_orig') == orig and service.get('secret'):
                    return service['secret']
        logger.warning('publik-authentication: no secret found for origin %r', orig)
        raise PublikAuthenticationFailed('no-secret-found-for-orig')

    def authenticate(self, request):
        full_path = request.get_full_path()
        if not request.GET.get('orig') or not request.GET.get('signature'):
            return None
        key = self.get_orig_key(request.GET['orig'])
        try:
            assert signature.check_url(
                full_path, key, raise_on_error=True
            ), 'signature.check_url should never return False with raise_on_error'
        except signature.SignatureError as e:
            logger.warning('publik-authentication: signature check failed with error "%s"', e)
            raise PublikAuthenticationFailed(str(e))
        user = self.resolve_user(request)
        logger.debug('publik-authentication: user authenticated with signature %s', user)
        return (user, None)


class PublikAuthentication(PublikServiceAuthentication):
    def resolve_user(self, request):
        user = self.resolve_user_by_nameid(request)
        if user is not None:
            return user

        return super().resolve_user(request)

    def resolve_user_by_nameid(self, request):
        if not self.get_view_flag(request, 'publik_authentication_resolve_user_by_nameid'):
            return None

        User = get_user_model()
        if 'NameID' not in request.GET:
            return None

        name_id = request.GET['NameID']
        is_authentic = True
        try:
            User._meta.get_field('uuid')
        except FieldDoesNotExist:
            is_authentic = False

        if is_authentic:
            try:
                return User.objects.get(uuid=name_id)
            except User.DoesNotExist:
                raise PublikAuthenticationFailed('user-not-found')

        elif UserSAMLIdentifier:
            try:
                return UserSAMLIdentifier.objects.get(name_id=name_id).user
            except UserSAMLIdentifier.DoesNotExist:
                raise PublikAuthenticationFailed('user-not-found')
        else:
            raise PublikAuthenticationFailed('no-usable-model')


class APIClientAuthenticationUnavailable(exceptions.APIException):
    status_code = status.HTTP_503_SERVICE_UNAVAILABLE
    default_code = 'IDP temporarily unavailable, try again later.'

    def __init__(self):
        self.detail = {'err': 1, 'err_desc': self.default_code}


class APIClientAuthentication(authentication.BasicAuthentication):
    def authenticate_credentials(self, userid, password, request=None):
        idp_services = list(getattr(settings, 'KNOWN_SERVICES', {}).get('authentic', {}).values())
        if not idp_services:
            return None
        authentic = idp_services[0]
        url = authentic['url'] + 'api/check-api-client/'
        payload = {'identifier': userid, 'password': password}

        if request:
            payload['ip'] = request.META['REMOTE_ADDR']

        try:
            response = Requests().post(url, json=payload)
        except requests.Timeout:
            raise APIClientAuthenticationUnavailable()
        except requests.RequestException:
            raise APIClientAuthenticationUnavailable()

        try:
            response.raise_for_status()
        except requests.exceptions.RequestException:
            return None

        result = response.json()
        if 'err' not in result or 'data' not in result or result['err'] == 1:
            return None
        try:
            api_client = APIClientUser.from_dict(result['data'])
        except Exception:
            return None
        return api_client, None
