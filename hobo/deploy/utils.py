# hobo - portal to configure and deploy applications
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import time

from django.contrib.auth.models import User
from django.db.models import Max

from hobo.environment.models import AVAILABLE_SERVICES, Variable
from hobo.environment.utils import get_installed_services_dict
from hobo.profile.models import AttributeDefinition
from hobo.profile.utils import get_profile_dict


def get_hobo_json():
    hobo_json = {}

    # include the list of admin users so an agent can create them when
    # deploying a service (according to its policy)
    users = []
    for user in User.objects.filter(is_superuser=True, is_active=True, password__isnull=False).exclude(
        password=''
    ):
        user_dict = {}
        for attribute in ('username', 'first_name', 'last_name', 'email', 'password'):
            user_dict[attribute] = getattr(user, attribute)
        users.append(user_dict)

    # add users, services and profile
    hobo_json['users'] = users
    hobo_json.update(get_installed_services_dict())
    hobo_json.update(get_profile_dict())

    # set a timestamp
    timestamp = None
    for klass in [AttributeDefinition, Variable] + AVAILABLE_SERVICES:
        ts = klass.objects.all().aggregate(Max('last_update_timestamp')).get('last_update_timestamp__max')
        if timestamp is None or (ts and ts > timestamp):
            timestamp = ts
    if timestamp is None:
        timestamp = datetime.datetime.now()

    hobo_json['timestamp'] = str(time.mktime(timestamp.timetuple()) + timestamp.microsecond / 1e6)
    return hobo_json
