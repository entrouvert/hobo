# hobo - portal to configure and deploy applications
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import threading

from celery import Celery
from django.conf import settings
from django.core.signals import request_finished, request_started
from django.db.models.signals import post_delete, post_migrate, post_save, pre_migrate
from django.dispatch import receiver
from kombu.common import Broadcast

from hobo.environment.models import AVAILABLE_SERVICES, Variable
from hobo.profile.models import AttributeDefinition

from .utils import get_hobo_json


class Local(threading.local):
    MUST_NOTIFY = False


tls = Local()


@receiver(post_save)
@receiver(post_delete)
def post_environment_save(sender, instance, **kwargs):
    if not sender in [Variable, AttributeDefinition] + AVAILABLE_SERVICES:
        return
    tls.MUST_NOTIFY = True


@receiver(pre_migrate)
@receiver(request_started)
def reset_must_notify(sender, **kwargs):
    tls.MUST_NOTIFY = False


@receiver(post_migrate)
@receiver(request_finished)
def notify_agents(sender, **kwargs):
    if not tls.MUST_NOTIFY:
        return
    tls.MUST_NOTIFY = False
    send_to_celery()


def send_to_celery():
    with Celery('hobo', broker=settings.BROKER_URL) as app:
        app.conf.update(
            task_serializer='json',
            accept_content=['json'],
            result_serializer='json',
            task_queues=(Broadcast('broadcast_tasks'),),
        )
        # see called method in hobo.agent.worker.celery
        app.send_task(
            'hobo-deploy', (get_hobo_json(),), expires=settings.BROKER_TASK_EXPIRES, queue='broadcast_tasks'
        )
