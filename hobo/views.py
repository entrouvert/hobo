import datetime
import json

from django.contrib.auth import logout as auth_logout
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import user_passes_test
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import resolve_url
from django.urls import reverse
from django.utils.encoding import force_str
from django.utils.translation import gettext as _
from django.views.generic.base import TemplateView

from .environment.models import Authentic, Variable
from .environment.utils import get_installed_services
from .maintenance.utils import maintenance_is_activable


def is_superuser(u):
    if not u.is_authenticated:
        return False
    if not u.is_superuser:
        raise PermissionDenied
    return True


admin_required = user_passes_test(is_superuser)


class Home(TemplateView):
    template_name = 'hobo/home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['services'] = [x for x in get_installed_services() if not x.secondary]
        for service in context['services']:
            for legacy_url in service.legacy_urls:
                try:
                    legacy_url['datetime'] = datetime.datetime.strptime(
                        legacy_url['timestamp'], '%Y-%m-%d %H:%M:%S'
                    )
                except ValueError:
                    pass
        context['has_authentic'] = bool(Authentic.objects.filter(secondary=False))
        context['has_global_title'] = Variable.objects.filter(name='global_title').exists()
        context['has_default_from_email'] = Variable.objects.filter(name='default_from_email').exists()
        context['show_maintenance_menu'] = maintenance_is_activable()
        return context


home = admin_required(Home.as_view())


def hobo(request, **kwargs):
    # The hobos URL is supposed to return a list of hobo websites, this
    # dummy implementation makes it possible to point deployment agents to
    # a single instance, that will announce itself as the only hobo around.
    response = HttpResponse(content_type='application/json')
    json.dump([request.build_absolute_uri('/')], response)
    return response


def has_idp():
    try:
        self_idp = Authentic.objects.get(use_as_idp_for_self=True)
    except Authentic.DoesNotExist:
        self_idp = None
    return self_idp and self_idp.is_operational()


def login(request, *args, **kwargs):
    if has_idp():
        return HttpResponseRedirect(resolve_url('mellon_login') + '?' + request.GET.urlencode())
    return login_local(request, *args, **kwargs)


def login_local(request, *args, **kwargs):
    return auth_views.LoginView.as_view(*args, **kwargs)(request)


def logout(request, next_page=None):
    if has_idp():
        return HttpResponseRedirect(resolve_url('mellon_logout') + '?' + request.GET.urlencode())
    auth_logout(request)
    return HttpResponseRedirect(request.GET.get('next') or request.build_absolute_uri('/'))


def health_json(request):
    data = {x.slug: x.get_health_dict() for x in get_installed_services() if not x.secondary}
    return JsonResponse({'data': data})


@admin_required
def menu_json(request):
    label = _('System')
    json_str = json.dumps(
        [{'label': force_str(label), 'slug': 'system', 'url': request.build_absolute_uri(reverse('home'))}]
    )
    content_type = 'application/json'
    for variable in ('jsonpCallback', 'callback'):
        if variable in request.GET:
            json_str = '%s(%s);' % (request.GET[variable], json_str)
            content_type = 'application/javascript'
            break
    response = HttpResponse(content_type=content_type)
    response.write(json_str)
    return response
