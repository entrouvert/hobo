# hobo - portal to configure and deploy applications
# Copyright (C) 2015-2022 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from datetime import date
from ipaddress import ip_address, ip_network

from django.conf import settings
from django.template.response import TemplateResponse
from django.utils.translation import gettext as _

from hobo.maintenance.utils import dnswl, is_valid_hostname


def pass_through(request, pass_through_dnswl=None, pass_through_header=None):
    if pass_through_dnswl is None:
        pass_through_dnswl = getattr(settings, 'MAINTENANCE_PASS_THROUGH_DNSWL', '')
    if pass_through_header is None:
        pass_through_header = getattr(settings, 'MAINTENANCE_PASS_THROUGH_HEADER', '')

    remote_addr = request.META.get('REMOTE_ADDR')
    if remote_addr:
        pass_through_ips = getattr(settings, 'MAINTENANCE_PASS_THROUGH_IPS', [])
        if remote_addr in pass_through_ips:
            return True
        else:
            for network in [x for x in pass_through_ips if '/' in x]:
                try:
                    if ip_address(remote_addr) in ip_network(network, strict=False):
                        return True
                except ValueError:  # bad remote_addr or network syntax
                    pass
        if (
            pass_through_dnswl
            and is_valid_hostname(pass_through_dnswl)
            and remote_addr in dnswl(pass_through_dnswl)
        ):
            return True
    if pass_through_header and pass_through_header in request.headers:
        return True
    return False


class MaintenanceMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        maintenance_expiration = getattr(settings, 'MAINTENANCE_PAGE_EXPIRATION', None)
        try:
            maintenance_expiration = date.fromisoformat(maintenance_expiration)
        except (TypeError, ValueError):
            maintenance_expiration = None
        if (
            maintenance_expiration is not None
            and maintenance_expiration >= date.today()
            and not pass_through(request)
        ):
            maintenance_message = getattr(settings, 'MAINTENANCE_PAGE_MESSAGE', '')
            context = {'maintenance_message': maintenance_message}
            response = TemplateResponse(
                request, 'hobo/maintenance/maintenance_page.html', context=context, status=503
            ).render()
            # from django.utils.log:230
            # if this attr is set, responses with status_code >= 400 won't be logged
            response._has_been_logged = True
            return response
        return self.get_response(request)
