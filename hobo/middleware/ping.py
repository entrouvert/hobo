from django.http import JsonResponse


class PingMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.method == 'GET' and request.path in ('/__ping__', '/__ping__/'):
            return JsonResponse({'err': 0})
        return self.get_response(request)
