import json

from django.http import HttpResponse
from django.utils.deprecation import MiddlewareMixin

from hobo.scrutiny.wsgi import middleware


class VersionMiddleware(MiddlewareMixin):
    def process_request(self, request):
        if request.method == 'GET' and request.path in ('/__version__', '/__version__/'):
            packages_version = middleware.VersionMiddleware.get_packages_version()
            return HttpResponse(json.dumps(packages_version), content_type='application/json')
        return None
