# hobo - portal to configure and deploy applications
# Copyright (C) 2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf import settings


class InternalIPMiddleware:
    def __init__(self, get_response=None):
        self.get_response = get_response

    def process_request(self, request):
        internal_ips = getattr(settings, 'INTERNAL_IPS', [])
        try:
            if request.META['REMOTE_ADDR'] in internal_ips:
                self.old_value = settings.DEBUG
                settings.DEBUG = True
        except TypeError:
            pass

    def process_response(self, request, response):
        if hasattr(self, 'old_value'):
            settings.DEBUG = self.old_value
            del self.old_value
        return response

    def __call__(self, request):
        old_value = settings.DEBUG
        internal_ips = getattr(settings, 'INTERNAL_IPS', [])
        set_debug = request.META['REMOTE_ADDR'] in internal_ips
        try:
            if set_debug:
                settings.DEBUG = True
            return self.get_response(request)
        finally:
            settings.DEBUG = old_value
