import threading

from django.utils.deprecation import MiddlewareMixin


class StoreRequestMiddleware(MiddlewareMixin):
    collection = {}

    def process_request(self, request):
        StoreRequestMiddleware.collection[threading.current_thread()] = request

    def process_response(self, request, response):
        StoreRequestMiddleware.collection.pop(threading.current_thread(), None)
        return response

    @classmethod
    def get_request(cls):
        return cls.collection.get(threading.current_thread())
