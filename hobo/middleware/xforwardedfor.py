from django.conf import settings
from django.utils.deprecation import MiddlewareMixin


class XForwardedForMiddleware(MiddlewareMixin):
    """If settings.USE_X_FORWARDED_FOR is True, copy the first address from
    X-Forwarded-For header to the REMOTE_ADDR meta.

    Other headers can be used with USE_X_FORWARDED_FOR_HEADERS settings.

    This middleware should only be used if you are sure the headers cannot be
    forged (behind a reverse proxy for example)."""

    def process_request(self, request):
        if not getattr(settings, 'USE_X_FORWARDED_FOR', False):
            return None
        headers = getattr(settings, 'USE_X_FORWARDED_FOR_HEADERS', None) or ('X-Forwarded-For',)
        for header in headers:
            if header in request.headers:
                ip = request.headers.get(header, '').split(',')[0].strip()
                if ip:
                    request.META['REMOTE_ADDR'] = ip
                    break
