# hobo - portal to configure and deploy applications
# Copyright (C) 2020  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf import settings
from django.http import HttpResponse
from django.utils.deprecation import MiddlewareMixin


class RobotsTxtMiddleware(MiddlewareMixin):
    def process_request(self, request):
        if request.method == 'GET' and request.path == '/robots.txt' and hasattr(settings, 'TEMPLATE_VARS'):
            return HttpResponse(settings.TEMPLATE_VARS.get('robots_txt', ''), content_type='text/plain')
        return None
