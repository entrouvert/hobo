# hobo - portal to configure and deploy applications
# Copyright (C) Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf import settings

CSP_HEADER_NAME = 'Content-Security-Policy'
CSP_REPORT_ONLY_HEADER_NAME = 'Content-Security-Policy-Report-Only'


def content_security_policy_middleware(get_response):
    def middleware(request):
        response = get_response(request)

        internal_ips = getattr(settings, 'INTERNAL_IPS', [])
        content_security_policy = getattr(settings, 'CONTENT_SECURITY_POLICY', None)
        content_security_policy_report_only = getattr(settings, 'CONTENT_SECURITY_POLICY_REPORT_ONLY', None)
        content_security_policy_report_uri = getattr(settings, 'CONTENT_SECURITY_POLICY_REPORT_URI', None)

        if content_security_policy and CSP_HEADER_NAME not in response.headers:
            response.headers[CSP_HEADER_NAME] = content_security_policy

        if content_security_policy_report_only and CSP_REPORT_ONLY_HEADER_NAME not in response.headers:
            response.headers[CSP_REPORT_ONLY_HEADER_NAME] = content_security_policy_report_only

        if content_security_policy_report_uri:
            if internal_ips and request.META['REMOTE_ADDR'] not in internal_ips:
                return response

            if policy := response.headers.get(CSP_HEADER_NAME):
                if 'report-uri' not in policy:
                    response.headers[CSP_HEADER_NAME] = (
                        policy + f'; report-uri {content_security_policy_report_uri}?error'
                    )

            if policy := response.headers.get(CSP_REPORT_ONLY_HEADER_NAME):
                if 'report-uri' not in policy:
                    response.headers[CSP_REPORT_ONLY_HEADER_NAME] = (
                        policy + f'; report-uri {content_security_policy_report_uri}'
                    )

        return response

    return middleware
