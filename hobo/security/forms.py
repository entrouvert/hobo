# hobo - portal to configure and deploy applications
# Copyright (C) Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re

from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

HEADS = [
    'child-src',
    'connect-src',
    'default-src',
    'font-src',
    'frame-src',
    'img-src',
    'manifest-src',
    'media-src',
    'object-src',
    'prefetch-src',
    'script-src',
    'style-src',
    'worker-src',
    'form-action',
    'frame-ancestors',
    'navigate-to',
    'report-uri',
]

SCHEME = r'(?:(?:https:)?//)?'
HOSTNAME = r'(?:\*\.)?[0-9a-z-]+(:?\.[0-9a-z-]+)+'
PORT = r'(?::[0-9]+)?'
PATH = r'(?:[/?#][^\s]*)?'

SOURCES = [
    "'self'",
    "'unsafe-eval'",
    "'unsafe-inline'",
    "'none'",
    'data:',
    re.compile(rf'^{SCHEME}{HOSTNAME}{PORT}{PATH}$'),
]


def validate_csp(policy):
    '''Validate a subset of possibles CSP policies'''

    policy = policy.replace('\n', ' ')
    directives = list(filter(None, (directive.strip() for directive in policy.split(';'))))
    for directive in directives:
        head, rest = directive.split(' ', 1)
        if head not in HEADS:
            raise ValidationError(_('Invalid CSP directive "%s"') % head)
        sources = rest.split(' ')
        for source in sources:
            for needle in SOURCES:
                if hasattr(needle, 'match'):
                    if needle.match(source):
                        break
                else:
                    if needle == source:
                        break
            else:
                raise ValidationError(_('Invalid CSP source "%s" in directive "%s"') % (source, head))


class SecurityForm(forms.Form):
    content_security_policy_report = forms.BooleanField(
        required=False,
        initial=False,
        label=_('Enable Content Security Policy reports'),
    )

    content_security_policy_report_only = forms.CharField(
        required=False,
        widget=forms.Textarea,
        label=_('Content Security Policy Report Only'),
        help_text=_('You should first test your policy by setting a "Report Only" content security policy'),
        validators=[validate_csp],
    )

    content_security_policy = forms.CharField(
        required=False,
        widget=forms.Textarea,
        label=_('Content Security Policy'),
        validators=[validate_csp],
    )

    def clean(self):
        cleaned_data = self.cleaned_data
        for key, value in cleaned_data.items():
            if key in ['content_security_policy_report_only', 'content_security_policy']:
                cleaned_data[key] = re.sub(r'\s+', ' ', value.strip())
        return cleaned_data
