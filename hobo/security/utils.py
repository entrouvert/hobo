# hobo - portal to configure and deploy applications
# Copyright (C) Entr'ouvert

import urllib.parse

from hobo.environment.utils import get_operational_services


def common_domains(urls):
    '''Compute fewest common domains between urls'''
    common = None
    for url in urls:
        parsed = urllib.parse.urlparse(url)
        new = parsed.netloc.split('.')
        if common is None:
            common = new
        else:
            j = len(common)
            for i, (a, b) in enumerate(zip(common[::-1], new[::-1])):
                if a != b:
                    j = i
                    break
            common = common[::-1][:j][::-1]

    new_urls = []
    if common:
        common_domain = '.' + '.'.join(common)
        common_wildcard_domain = 'https://*' + common_domain

    for url in urls:
        if common and common_domain in url:
            if common_wildcard_domain not in new_urls:
                new_urls.append(common_wildcard_domain)
        else:
            new_urls.append(url)
    return new_urls


def default_policy():
    service_base_urls = [service.base_url for service in get_operational_services()]
    policy = 'default-src '
    policy += ' '.join(common_domains(service_base_urls))
    policy += " 'unsafe-eval'"
    policy += " 'unsafe-inline'"
    return policy
