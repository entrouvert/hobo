# hobo - portal to configure and deploy applications
# Copyright (C) Entr'ouvert

import datetime
import json

from django.core.cache import cache
from django.db import models
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _


class CspReport(models.Model):
    first_seen = models.DateTimeField(auto_now_add=True, verbose_name=_('First seen'))
    last_seen = models.DateTimeField(auto_now_add=True, verbose_name=_('Last seen'))
    error = models.BooleanField(verbose_name=_('Error'))
    inline = models.BooleanField(verbose_name=_('Inline'))
    source = models.TextField(verbose_name=_('Source'))
    line = models.PositiveIntegerField(verbose_name=_('Line'))
    column = models.PositiveIntegerField(verbose_name=_('Column'))
    violated_directive = models.TextField(verbose_name=_('Violated directive'))
    content = models.JSONField(verbose_name=_('Content'))

    # period to update report.last_seen
    UPDATE_LAST_SEEN_PERIOD = datetime.timedelta(minutes=10)

    def formatted_content(self):
        return json.dumps(self.content, indent=2)

    @classmethod
    def record_from_report(cls, report, error=False):
        '''Create a new CspReport from the deserialized JSON pushed to a report-uri endpoint.'''
        try:
            csp_report = report['csp-report']
            violated_directive = csp_report['violated-directive']
            blocked_uri = csp_report['blocked-uri']
            document_uri = csp_report['document-uri']
            inline = blocked_uri.lower() == 'inline'
            line = csp_report.get('line-number', 0)
            column = csp_report.get('column-number', 0)
            if inline:
                source = document_uri
            else:
                source = blocked_uri
        except KeyError as e:
            raise ValueError(f'missing value {e}')

        # keep one report per policy violation
        csp_report, created = CspReport.objects.get_or_create(
            error=error,
            inline=inline,
            source=source,
            violated_directive=violated_directive,
            line=line,
            column=column,
            defaults={'content': csp_report},
        )

        # update CspReport.last_seen not faster than every 10 minutes...
        if not created:
            time_since_last_seen = now() - csp_report.last_seen
            if time_since_last_seen > cls.UPDATE_LAST_SEEN_PERIOD:
                CspReport.objects.filter(pk=csp_report.pk).update(last_seen=now())

        return csp_report

    @classmethod
    def clean_old_reports(cls, **kwargs):
        not_on_or_after = now() - datetime.timedelta(**kwargs)
        cls.objects.filter(last_seen__lt=not_on_or_after).delete()

    def __str__(self):
        if self.inline:
            return f'blocked inline asset in {self.source}, line {self.line}, column: {self.column} by directive {self.violated_directive}'
        else:
            return f'blocked asset {self.source} by directive {self.violated_directive}'

    class Meta:
        verbose_name = _('CSP report')
        verbose_name_plural = _('CSP reports')
        db_table = 'hobo_cspreport'
        unique_together = [
            ('error', 'inline', 'source', 'violated_directive', 'line', 'column'),
        ]

    @classmethod
    def too_much_reports(cls):
        '''Verify if too much reports have been reported'''
        cache_key = 'csp-too-much-reports'
        cache_value = cache.get(cache_key)
        if cache_value is None:
            cls.clean_old_reports(days=1)
            cache_value = cls.objects.count() > 1000
            cache.set(cache_key, cache_value, 60)
        return cache_value
