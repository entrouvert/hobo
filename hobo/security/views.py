# hobo - portal to configure and deploy applications
# Copyright (C) Entr'ouvert

import json
import logging
import re

from django.contrib import messages
from django.http import HttpResponse, HttpResponseBadRequest
from django.urls import reverse_lazy
from django.utils.functional import cached_property
from django.utils.translation import gettext as _
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import FormView

from hobo.environment.utils import get_setting_variable

from .forms import SecurityForm
from .models import CspReport
from .utils import default_policy

logger = logging.getLogger('hobo.security')


REPLACE_SEMICOMMAS_BY_NEWLINES_RE = re.compile(' *; *')


def add_newlines_to_policy(policy):
    '''Add some spacing to policies for presentation'''
    return re.sub(REPLACE_SEMICOMMAS_BY_NEWLINES_RE, ';\n', policy)


class HomeView(FormView):
    template_name = 'hobo/security/home.html'
    form_class = SecurityForm
    success_url = reverse_lazy('security-home')

    @cached_property
    def content_security_policy_variable(self):
        return get_setting_variable('CONTENT_SECURITY_POLICY')

    @cached_property
    def content_security_policy_report_only_variable(self):
        return get_setting_variable('CONTENT_SECURITY_POLICY_REPORT_ONLY')

    @cached_property
    def content_security_policy_report_uri_variable(self):
        return get_setting_variable('CONTENT_SECURITY_POLICY_REPORT_URI')

    def get_initial(self):
        initial = super().get_initial()
        initial['content_security_policy_report'] = bool(
            self.content_security_policy_report_uri_variable.value
        )
        initial['content_security_policy'] = add_newlines_to_policy(
            self.content_security_policy_variable.value,
        )
        initial['content_security_policy_report_only'] = add_newlines_to_policy(
            self.content_security_policy_report_only_variable.value,
        )
        return initial

    def form_valid(self, form):
        if 'clean-reports' in self.request.POST:
            CspReport.objects.all().delete()
            messages.info(self.request, _('All reports removed.'))
            return super().form_valid(form)

        self.content_security_policy_variable.update_value(form.cleaned_data['content_security_policy'])
        self.content_security_policy_report_only_variable.update_value(
            form.cleaned_data['content_security_policy_report_only']
        )
        if form.cleaned_data['content_security_policy_report']:
            self.content_security_policy_report_uri_variable.update_value(
                self.request.build_absolute_uri('/api/csp-report/')
            )
        else:
            self.content_security_policy_report_uri_variable.update_value('')
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['reports'] = CspReport.objects.order_by('-last_seen').only(
            'first_seen', 'last_seen', 'source', 'violated_directive'
        )
        ctx['default_policy'] = default_policy()
        return ctx


home = HomeView.as_view()


@csrf_exempt
def api_report(request):
    if not get_setting_variable('CONTENT_SECURITY_POLICY_REPORT_URI').value:
        return HttpResponseBadRequest('no CSP')

    content_security_policy_variable = get_setting_variable('CONTENT_SECURITY_POLICY')
    content_security_policy_report_only_variable = get_setting_variable('CONTENT_SECURITY_POLICY_REPORT_ONLY')

    # do not accept reports if no policy is published
    if not content_security_policy_variable.value and not content_security_policy_report_only_variable.value:
        return HttpResponseBadRequest('no CSP')

    if CspReport.too_much_reports():
        return HttpResponseBadRequest('too much reports')

    # gather values for the report
    try:
        error = 'error' in request.GET
        report = json.loads(request.body)
        csp_report = CspReport.record_from_report(report, error=error)
    except Exception as e:
        logger.warning('invalid CSP report: %s, %r', e, request.body[:1024])
        return HttpResponseBadRequest('invalid json')

    # delete reports last_seen more than one day ago
    CspReport.clean_old_reports(days=1)

    # log a warning if ?error is present, ?error is added to the
    # report-uri by the middleware for blocking CSP policies
    if error and content_security_policy_variable:
        hostname = request.headers['host']
        logger.warning(f'{hostname}: CSP reports %s', csp_report)
    return HttpResponse()
