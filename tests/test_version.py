import subprocess
import sys

import pytest

import hobo.scrutiny.wsgi.middleware
from hobo.scrutiny.wsgi.middleware import VersionMiddleware

# Not able to install apt or apt_pkg in virtualenv
# forcing use of system-dist-packages for them
path_bck = sys.path
apt_pkg = apt_cache = None
try:
    sys.path = ['/usr/lib/python3/dist-packages']
    import apt.cache as apt_cache
    import apt_pkg
except ImportError:
    pass
finally:
    sys.path = path_bck

pytestmark = pytest.mark.django_db


@pytest.fixture
def without_apt_pkg():
    apt_pkg_bck = hobo.scrutiny.wsgi.middleware.apt_pkg
    hobo.scrutiny.wsgi.middleware.apt_pkg = None
    yield None
    hobo.scrutiny.wsgi.middleware.apt_pkg = apt_pkg_bck


@pytest.fixture
def with_apt_pkg():
    if not apt_pkg:
        pytest.skip('apt_pkg is not available')
    apt_pkg_bck = hobo.scrutiny.wsgi.middleware.apt_pkg
    hobo.scrutiny.wsgi.middleware.apt_pkg = apt_pkg
    yield None
    hobo.scrutiny.wsgi.middleware.apt_pkg = apt_pkg_bck


@pytest.mark.skipif(apt_pkg, reason='Same test is done by test_version_middleware_debian')
def test_version_middleware(without_apt_pkg, settings, client):
    hobo.scrutiny.wsgi.middleware.VersionMiddleware.ENTROUVERT_PACKAGES = ['pytest', 'pytest-django']
    settings.MIDDLEWARE = ('hobo.middleware.version.VersionMiddleware',) + tuple(settings.MIDDLEWARE)
    json_response = client.get('/__version__').json()
    assert set(json_response.keys()) == {'pytest', 'pytest-django'}


@pytest.mark.skipif(not apt_pkg, reason='Unable to import apt_pkg')
def test_version_middleware_debian(settings, client):
    hobo.scrutiny.wsgi.middleware.VersionMiddleware.ENTROUVERT_PACKAGES = ['pytest', 'pytest-django']
    settings.MIDDLEWARE = ('hobo.middleware.version.VersionMiddleware',) + tuple(settings.MIDDLEWARE)
    hobo.scrutiny.wsgi.middleware.apt_pkg = None

    # fetch versions for python packages only
    json_response = client.get('/__version__').json()
    assert set(json_response.keys()) == {'pytest', 'pytest-django'}

    hobo.scrutiny.wsgi.middleware.apt_pkg = apt_pkg
    # fetch version for debian packages only
    versions = {
        src_name: version for dummy, src_name, version in VersionMiddleware.pkgs_from_origin("Entr'ouvert")
    }

    # fetch versions for python & debian packages
    VersionMiddleware._packages_version = None  # clear cache
    json_response = client.get('/__version__').json()
    # check that we have versions for both python & debian packages
    expected_pkgs = {'pytest', 'pytest-django'} | set(versions.keys())
    assert set(json_response.keys()) == expected_pkgs


@pytest.mark.skipif(not apt_cache, reason='Unable to import apt.cache')
def test_apt_pkg_vs_apt_cache(with_apt_pkg):
    # Testing that new way to fetch packages version with apt_pkg
    # returns the same info than the old way with apt.cache

    def process_version(vers):
        return {src_name: version for _, src_name, version in vers}

    versions = VersionMiddleware.pkgs_from_origin("Entr'ouvert")
    versions = process_version(versions)

    # old way to fetch versions using apt_cache
    debian_cache = apt_cache.Cache()
    old_versions = [
        (p.name, p.installed.source_name, p.installed.version)
        for p in debian_cache
        if p.installed
        and (
            (p.installed and p.installed.origins[0].origin == "Entr'ouvert")
            or (p.candidate and p.candidate.origins[0].origin == "Entr'ouvert")
        )
    ]
    old_versions = process_version(old_versions)
    assert old_versions == versions


def test_apt_pkg_vs_apt(with_apt_pkg):
    def repo_from_version(lines, version):
        # Parse apt-cache policy output
        repo = []
        priority = None
        for line in lines:
            line = line.strip()
            if not priority:
                if not line.startswith('*** %s' % version):
                    continue
                priority = line.split(' ')[2].strip()
                continue
            if line.startswith('***'):
                break  # another version table entry : too far
            if line.startswith(priority):
                repo.append(line.split(' ')[1])
        return repo

    def info_from_apt(pkg_name):
        # fetch repositories url (for installed & candidate versions)
        # and installed package version from apt-cache policy
        proc = subprocess.run(['apt-cache', 'policy', pkg_name], stdout=subprocess.PIPE, check=True)
        lines = proc.stdout.decode('utf-8').split('\n')
        installed = lines[1].split(':')[1].strip()
        candidate = lines[2].split(':')[1].strip()

        install_repo = repo_from_version(lines, installed)
        candidate_repo = repo_from_version(lines, candidate)

        return installed, install_repo + candidate_repo

    versions = VersionMiddleware.pkgs_from_origin("Entr'ouvert")
    for pkg_name, dummy, version in versions:
        installed_version, repos = info_from_apt(pkg_name)
        assert any(['deb.entrouvert.org' in repo for repo in repos])
        assert installed_version == version
