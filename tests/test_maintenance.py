from datetime import date, timedelta
from unittest import mock

import pytest
from django.test import override_settings
from django.urls import reverse
from dns.exception import DNSException
from dns.resolver import NXDOMAIN, NoAnswer

from hobo.environment.models import Variable
from hobo.environment.utils import get_setting_variable
from hobo.maintenance.management.commands.disable_maintenance_page import Command
from hobo.maintenance.utils import check_dnswl

from .test_manager import login


def test_maintenance_middleware(app, admin_user, db, monkeypatch, settings):
    app = login(app)
    resp = app.get('/')
    assert resp.status_code == 200

    tomorrow = date.today() + timedelta(days=1)
    settings.MAINTENANCE_PAGE_EXPIRATION = tomorrow.isoformat()
    resp = app.get('/', status=503)
    assert 'This site is currently unavailable.' in resp.text

    resp = app.get('/__ping__/', status=200)
    assert resp.json.get('err') == 0

    # check custom maintenance message
    settings.MAINTENANCE_PAGE_MESSAGE = 'foobar'
    resp = app.get('/', status=503)
    assert 'foobar' in resp.text

    settings.MAINTENANCE_PASS_THROUGH_IPS = ['127.0.0.1']
    resp = app.get('/')
    assert resp.status_code == 200

    settings.MAINTENANCE_PASS_THROUGH_IPS = ['127.0.0.100', '127.0.0.0/16']
    resp = app.get('/')
    assert resp.status_code == 200

    settings.MAINTENANCE_PASS_THROUGH_IPS = ['127.0.0.1/4']  # lenient ipaddress.ip_network parsing
    app.get('/')
    assert resp.status_code == 200

    settings.MAINTENANCE_PASS_THROUGH_IPS = ['128.0.0.0/24']
    resp = app.get('/', status=503)
    assert 'foobar' in resp.text

    resp = app.get('/__ping__/', status=200)
    assert resp.json.get('err') == 0

    settings.MAINTENANCE_PASS_THROUGH_IPS = []
    resp = app.get('/', status=503)
    assert 'foobar' in resp.text

    settings.MAINTENANCE_PASS_THROUGH_HEADER = 'X-Entrouvert'
    resp = app.get('/', headers={'X-Entrouvert': 'yes'})
    assert resp.status_code == 200

    settings.MAINTENANCE_PASS_THROUGH_HEADER = ''
    settings.MAINTENANCE_PASS_THROUGH_DNSWL = 'dnswl.example.com'
    with mock.patch('hobo.maintenance.utils.check_dnswl', return_value=True):
        resp = app.get('/')
        assert resp.status_code == 200
    with mock.patch('hobo.maintenance.utils.check_dnswl', return_value=False):
        resp = app.get('/', status=503)
        assert 'foobar' in resp.text

        resp = app.get('/__ping__/', status=200)
        assert resp.json.get('err') == 0


def test_maintenance_expiration(app, admin_user, db, settings, freezer):
    app = login(app)
    resp = app.get('/')
    assert resp.status_code == 200

    expire = date.today() + timedelta(days=3)
    settings.MAINTENANCE_PAGE_EXPIRATION = expire.isoformat()
    resp = app.get('/', status=503)

    freezer.move_to(expire - timedelta(days=2))
    resp = app.get('/', status=503)

    freezer.move_to(expire)
    resp = app.get('/', status=503)

    freezer.move_to(expire + timedelta(days=1))
    resp = app.get('/')


def test_maintenance_middleware_dnswl_validation(app, admin_user, db, settings):
    app = login(app)
    resp = app.get('/')
    assert resp.status_code == 200

    tomorrow = date.today() + timedelta(days=1)
    settings.MAINTENANCE_PAGE_EXPIRATION = tomorrow.isoformat()
    settings.MAINTENANCE_PASS_THROUGH_DNSWL = 'INVALID DOMAIN'
    resp = app.get('/', status=503)
    assert 'This site is currently unavailable.' in resp.text


def test_check_dnswl():
    # existing dnswl answers, ipv4
    with mock.patch('hobo.maintenance.utils._resolver_resolve') as mock_resolve:
        result = mock.Mock()
        result.address = '1.2.3.4'
        result2 = mock.Mock()
        result2.address = '4.5.6.7'
        mock_resolve.return_value = [result, result2]
        assert check_dnswl('dnswl.example.com', '127.0.0.1')

    # no known dnswl, ipv4
    with mock.patch('hobo.maintenance.utils._resolver_resolve') as mock_resolve:
        mock_resolve.return_value = []
        assert not check_dnswl('dnswl.example.com', '127.0.0.1')

    # existing dnswl answers, ipv6
    with mock.patch('hobo.maintenance.utils._resolver_resolve') as mock_resolve:
        result = mock.Mock()
        result.address = '2001:db8:3333:4444:CCCC:DDDD:EEEE:FFFF'
        result2 = mock.Mock()
        result2.address = '2001:db8:3333:4444:CCCC:DDDD:FFFF:FFFF'
        mock_resolve.return_value = [result, result2]
        assert check_dnswl('dnswl.example.com', '::1')

    # no known dnswl, ipv6
    with mock.patch('hobo.maintenance.utils._resolver_resolve') as mock_resolve:
        mock_resolve.return_value = []
        assert not check_dnswl('dnswl.example.com', '::1')

    # misc exceptions
    with mock.patch(
        'hobo.maintenance.utils._resolver_resolve', side_effect=NXDOMAIN('DNS query name does not exist')
    ) as mock_resolve:
        result = mock.Mock()
        result.address = '1.2.3.4'
        result2 = mock.Mock()
        result2.address = '4.5.6.7'
        mock_resolve.return_value = [result, result2]
        assert not check_dnswl('dnswl.example.com', '127.0.0.1')

    with mock.patch(
        'hobo.maintenance.utils._resolver_resolve',
        side_effect=NoAnswer('DNS response does not contain the answer'),
    ) as mock_resolve:
        result = mock.Mock()
        result.address = '1.2.3.4'
        result2 = mock.Mock()
        result2.address = '4.5.6.7'
        mock_resolve.return_value = [result, result2]
        assert not check_dnswl('dnswl.example.com', '127.0.0.1')

    with mock.patch(
        'hobo.maintenance.utils._resolver_resolve', side_effect=DNSException('Error while retrieving DNSWL')
    ) as mock_resolve:
        result = mock.Mock()
        result.address = '1.2.3.4'
        result2 = mock.Mock()
        result2.address = '4.5.6.7'
        mock_resolve.return_value = [result, result2]
        assert not check_dnswl('dnswl.example.com', '127.0.0.1')


def test_manage(app, admin_user, settings):
    assert Variable.objects.filter(name='SETTING_MAINTENANCE_PAGE_EXPIRATION').count() == 0
    assert Variable.objects.filter(name='SETTING_MAINTENANCE_MESSAGE').count() == 0
    assert Variable.objects.filter(name='SETTING_MAINTENANCE_PASS_THROUGH_HEADER').count() == 0
    assert Variable.objects.filter(name='TENANT_DISABLED_CRON_JOBS_EXPIRATION').count() == 0
    assert not getattr(settings, 'MAINTENANCE_PASS_THROUGH_IPS', [])

    login(app)
    # Maintenance page not authorized if no DNSWL nor PASS_THROUGH_IPS
    resp = app.get('/maintenance/', status=403)

    resp = app.get('/')
    assert 'Maintenance' not in resp.text
    settings.MAINTENANCE_PASS_THROUGH_IPS = ['127.0.0.1']
    resp = app.get('/')
    assert 'Maintenance' in resp.text

    tomorrow = date.today() + timedelta(days=1)
    resp = app.get('/maintenance/')
    resp.form.set('maintenance_page_expiration', tomorrow)
    resp.form.set('maintenance_page_message', 'Foo')
    resp.form.set('maintenance_pass_through_header', 'X-Entrouvert')
    resp.form.set('disabled_cron_expiration', tomorrow)
    resp = resp.form.submit().follow()
    assert (
        Variable.objects.filter(name='SETTING_MAINTENANCE_PAGE_EXPIRATION').get().value
        == tomorrow.isoformat()
    )
    assert Variable.objects.filter(name='SETTING_MAINTENANCE_PAGE_MESSAGE').get().value == 'Foo'
    assert (
        Variable.objects.filter(name='SETTING_MAINTENANCE_PASS_THROUGH_HEADER').get().value == 'X-Entrouvert'
    )
    assert (
        Variable.objects.filter(name='SETTING_TENANT_DISABLED_CRON_JOBS_EXPIRATION').get().value
        == tomorrow.isoformat()
    )

    resp.form.set('maintenance_page_expiration', date.today() - timedelta(days=2))
    resp.form.set('disabled_cron_expiration', date.today() - timedelta(days=2))
    resp.form.set('maintenance_page_message', '')
    resp.form.set('maintenance_pass_through_header', 'X-Entrouvert')
    resp = resp.form.submit().follow()
    assert Variable.objects.filter(name='SETTING_MAINTENANCE_PAGE_EXPIRATION').get().value == ''
    assert Variable.objects.filter(name='SETTING_TENANT_DISABLED_CRON_JOBS_EXPIRATION').get().value == ''
    assert Variable.objects.filter(name='SETTING_MAINTENANCE_PAGE_MESSAGE').get().value == ''
    assert (
        Variable.objects.filter(name='SETTING_MAINTENANCE_PASS_THROUGH_HEADER').get().value == 'X-Entrouvert'
    )

    resp.form.set('maintenance_page_expiration', '')
    resp.form.set('maintenance_page_message', '')
    resp.form.set('maintenance_pass_through_header', '')
    resp.form.set('disabled_cron_expiration', '')
    resp = resp.form.submit().follow()
    assert Variable.objects.filter(name='SETTING_MAINTENANCE_PAGE_EXPIRATION').get().value == ''
    assert Variable.objects.filter(name='SETTING_MAINTENANCE_PAGE_MESSAGE').get().value == ''
    assert Variable.objects.filter(name='SETTING_MAINTENANCE_PASS_THROUGH_HEADER').get().value == ''
    assert Variable.objects.filter(name='SETTING_TENANT_DISABLED_CRON_JOBS_EXPIRATION').get().value == ''

    # try to activate the maintenance page without pass through header
    resp.form.set('maintenance_page_expiration', tomorrow)
    resp.form.set('maintenance_page_message', '')
    resp.form.set('maintenance_pass_through_header', '')
    resp.form.set('maintenance_pass_through_dnswl', '')
    resp.form.set('disabled_cron_expiration', '')
    resp = resp.form.submit()
    assert 'No HTTP header pass through or DNSWL is configured' in resp.text
    assert 'Check this box if you are sure to enable the maintenance page.' in resp.text

    # check the confirmation checkbox
    resp.form.set('maintenance_page_expiration', tomorrow)
    resp.form.set('confirm_maintenance_page', True)
    resp.form.set('maintenance_page_message', '')
    resp.form.set('maintenance_pass_through_header', '')
    resp.form.set('maintenance_pass_through_dnswl', '')
    resp.form.set('disabled_cron_expiration', '')
    resp = resp.form.submit().follow()
    assert (
        Variable.objects.filter(name='SETTING_MAINTENANCE_PAGE_EXPIRATION').get().value
        == tomorrow.isoformat()
    )
    assert Variable.objects.filter(name='SETTING_MAINTENANCE_PASS_THROUGH_HEADER').get().value == ''


@pytest.mark.parametrize(
    'dnswl,is_valid',
    (
        ('1.2.3.4', False),
        ('-toto.foo', False),
        (('a' * 64) + '.example.com', False),
        ('gnu.org', True),
        ('a.b.c.d', True),
    ),
)
@override_settings(MAINTENANCE_PASS_THROUGH_IPS=['1.2.3.4'])
def test_manage_dnswl_validation(app, admin_user, settings, dnswl, is_valid):
    login(app)
    resp = app.get('/maintenance/')
    resp.form.set('maintenance_pass_through_dnswl', dnswl)
    resp = resp.form.submit()
    if is_valid:
        resp.follow()
        assert Variable.objects.filter(name='SETTING_MAINTENANCE_PASS_THROUGH_DNSWL').get().value == dnswl
    else:
        assert Variable.objects.filter(name='SETTING_MAINTENANCE_PASS_THROUGH_DNSWL').count() == 0
        assert f'<ul class="errorlist"><li>{dnswl} is not a valid hostname</li></ul>' in resp.text


def test_disable_maintenance_page_command(db):
    maintenance_page_variable = get_setting_variable('MAINTENANCE_PAGE')
    assert not bool(maintenance_page_variable.json)
    command = Command()
    command.handle()
    maintenance_page_variable = get_setting_variable('MAINTENANCE_PAGE')
    assert not bool(maintenance_page_variable.json)
    maintenance_page_variable.json = True
    maintenance_page_variable.save()
    assert bool(maintenance_page_variable.json)
    command.handle()
    maintenance_page_variable = get_setting_variable('MAINTENANCE_PAGE')
    assert not bool(maintenance_page_variable.json)


@override_settings(MAINTENANCE_PASS_THROUGH_IPS=['1.2.3.4'])
def test_maintenance_test_button(app, settings, admin_user):
    app = login(app)
    resp = app.get(reverse('maintenance-home'))

    # IP allowed in DNSWL
    resp.form.set('maintenance_pass_through_dnswl', 'dnswl.example.com')

    def check_dnswl(dnswl, remote_addr):
        return dnswl == 'dnswl.example.com' and remote_addr == '127.0.0.1'

    with mock.patch('hobo.maintenance.utils.check_dnswl', check_dnswl):
        resp = resp.form.submit('test')

    assert resp.status_code == 200

    assert Variable.objects.filter(name='SETTING_MAINTENANCE_PAGE_EXPIRATION').count() == 0
    assert Variable.objects.filter(name='SETTING_MAINTENANCE_MESSAGE').count() == 0
    assert Variable.objects.filter(name='SETTING_MAINTENANCE_PASS_THROUGH_HEADER').count() == 0
    assert Variable.objects.filter(name='TENANT_DISABLED_CRON_JOBS_EXPIRATION').count() == 0

    elts = resp.pyquery.find('ul.errorlist.nonfield li')
    assert len(elts)
    assert elts[0].text == 'OK: You would have access in maintenance using this settings'

    assert len(resp.pyquery.find('ul.errorlist:not(.nonfield) li')) == 0

    # Header authorized for maintenance
    resp.form.set('maintenance_pass_through_dnswl', '')
    resp.form.set('maintenance_pass_through_header', 'TESTH')
    resp = resp.form.submit('test', headers={'TESTH': ''})
    assert resp.status_code == 200

    assert Variable.objects.filter(name='SETTING_MAINTENANCE_PAGE_EXPIRATION').count() == 0
    assert Variable.objects.filter(name='SETTING_MAINTENANCE_MESSAGE').count() == 0
    assert Variable.objects.filter(name='SETTING_MAINTENANCE_PASS_THROUGH_HEADER').count() == 0
    assert Variable.objects.filter(name='TENANT_DISABLED_CRON_JOBS_EXPIRATION').count() == 0

    elts = resp.pyquery.find('ul.errorlist.nonfield li')
    assert len(elts)
    assert elts[0].text == 'OK: You would have access in maintenance using this settings'

    assert len(resp.pyquery.find('ul.errorlist:not(.nonfield) li')) == 0

    # resubmit without headers
    resp = resp.form.submit('test')
    assert resp.status_code == 200

    elts = resp.pyquery.find('ul.errorlist.nonfield li')
    assert len(elts)
    assert elts[0].text == 'KO: You would not have access in maintenance using this settings'

    assert Variable.objects.filter(name='SETTING_MAINTENANCE_PAGE_EXPIRATION').count() == 0
    assert Variable.objects.filter(name='SETTING_MAINTENANCE_MESSAGE').count() == 0
    assert Variable.objects.filter(name='SETTING_MAINTENANCE_PASS_THROUGH_HEADER').count() == 0
    assert Variable.objects.filter(name='TENANT_DISABLED_CRON_JOBS_EXPIRATION').count() == 0

    elts = resp.pyquery.find('ul.errorlist.nonfield li')
    assert len(elts)
    assert elts[0].text == 'KO: You would not have access in maintenance using this settings'

    resp = app.get(reverse('maintenance-home'))
    resp.form.set('maintenance_pass_through_dnswl', '127.0.0.1')
    resp = resp.form.submit('test')
    elts = resp.pyquery.find('ul.errorlist:not(.nonfield) li')
    assert len(elts) == 1
    assert elts[0].text == '127.0.0.1 is not a valid hostname'
    resp = resp.form.submit()
    elts = resp.pyquery.find('ul.errorlist:not(.nonfield) li')
    assert len(elts) == 1
    assert elts[0].text == '127.0.0.1 is not a valid hostname'

    tomorrow = date.today() + timedelta(days=1)
    resp.form.set('maintenance_page_expiration', tomorrow.isoformat())
    resp.form.set('maintenance_pass_through_dnswl', 'localhost')
    resp.form.set('maintenance_page_message', 'Foo')
    resp = resp.form.submit()

    assert (
        Variable.objects.filter(name='SETTING_MAINTENANCE_PAGE_EXPIRATION').get().value
        == tomorrow.isoformat()
    )
    assert Variable.objects.filter(name='SETTING_MAINTENANCE_PAGE_MESSAGE').get().value == 'Foo'
    assert Variable.objects.filter(name='SETTING_MAINTENANCE_PASS_THROUGH_DNSWL').get().value == 'localhost'
