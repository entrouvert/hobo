import pytest
from django.contrib.auth.models import User

from hobo.deploy.utils import get_hobo_json
from hobo.environment.models import AVAILABLE_SERVICES, Authentic, Variable, Wcs
from hobo.profile.models import AttributeDefinition

pytestmark = pytest.mark.django_db


@pytest.fixture
def empty_site():
    for klass in [User, Variable] + AVAILABLE_SERVICES:
        klass.objects.all().delete()


def test_empty_hobo(request, empty_site):
    if request.config.getvalue('nomigrations'):
        pytest.skip('skipping because of nomigrations')
    hobo_json = get_hobo_json()
    assert hobo_json['variables'] == {}
    assert hobo_json['services'] == []
    assert hobo_json['users'] == []
    assert hobo_json['profile']['fields'] != []


def test_variables(empty_site):
    v = Variable(name='foo', value='bar')
    v.save()
    hobo_json = get_hobo_json()
    assert hobo_json['variables']['foo'] == 'bar'


def test_services(empty_site):
    w = Wcs(title='foo', slug='foo', base_url='http://foo.example.net')
    w.save()
    a = Authentic(title='bar', slug='bar', base_url='http://bar.example.net')
    a.save()
    hobo_json = get_hobo_json()
    wcs_json = [x for x in hobo_json['services'] if x.get('service-id') == 'wcs'][0]
    authentic_json = [x for x in hobo_json['services'] if x.get('service-id') == 'authentic'][0]
    assert wcs_json['title'] == 'foo'
    assert wcs_json['saml-sp-metadata-url']
    assert authentic_json['title'] == 'bar'
    assert authentic_json['saml-idp-metadata-url']


def test_profile(request, empty_site):
    if request.config.getvalue('nomigrations'):
        pytest.skip('skipping because of nomigrations')
    hobo_json = get_hobo_json()
    assert hobo_json['profile']['fields'][0]['name'] == 'title'
    assert hobo_json['profile']['fields'][0]['required'] is False

    assert hobo_json['profile']['fields'][1]['name'] == 'first_name'
    assert hobo_json['profile']['fields'][1]['required'] is True


def test_required_on_login(request, empty_site):
    AttributeDefinition.objects.create(name='cgu', label='CGU', required_on_login=True)
    hobo_json = get_hobo_json()
    assert hobo_json['profile']['fields'][-1]['name'] == 'cgu'
    assert hobo_json['profile']['fields'][-1]['required_on_login'] is True
