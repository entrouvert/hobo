import logging

import pytest

from hobo.agent.common.models import Role
from hobo.multitenant.utils import provision_user_groups

pytestmark = pytest.mark.django_db


@pytest.fixture
def roles():
    return [Role.objects.create(uuid=str(i) * 8, name='Role %s' % i) for i in range(5)]


def test_provision_user_groups(db, roles, admin_user, caplog):
    caplog.set_level(logging.INFO)
    roles_uuids = [r.uuid for r in roles]
    provision_user_groups(admin_user, roles_uuids)
    assert len(caplog.records) == len(roles)
    assert list(admin_user.groups.all()) == [r.group_ptr for r in roles]

    caplog.clear()
    caplog.set_level(logging.WARNING)
    provision_user_groups(admin_user, roles_uuids)
    assert list(admin_user.groups.all()) == [r.group_ptr for r in roles]
    assert len(caplog.records) == 0

    provision_user_groups(admin_user, roles_uuids[:-1])
    assert list(admin_user.groups.all()) == [r.group_ptr for r in roles[:-1]]

    roles_uuids.append('unknown-uuid')
    provision_user_groups(admin_user, roles_uuids)
    assert len(caplog.records) == 1
