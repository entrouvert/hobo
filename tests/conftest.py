import os

import django_webtest
import pytest
from django.contrib.auth.models import User


@pytest.fixture
def app(request):
    wtm = django_webtest.WebTestMixin()
    wtm._patch_settings()
    request.addfinalizer(wtm._unpatch_settings)
    return django_webtest.DjangoTestApp()


@pytest.fixture
def admin_user(db):
    return User.objects.create_superuser('admin', email=None, password='password')


@pytest.fixture
def fake_themes(settings, tmpdir):
    THEMES = """
[
  {
    "id": "alfortville",
    "label": "Alfortville",
    "variables": {
      "css_variant": "alfortville",
      "no_extra_js": false,
      "theme_color": "#FFFFFF",
      "foo": "bar"
    },
    "overlay": "foobar"
  },
  {
    "id": "publik",
    "label": "Publik",
    "variables": {
      "css_variant": "publik",
      "no_extra_js": false,
      "theme_color": "#E80E89"
    }
  }
]
"""
    base_dir = str(tmpdir.mkdir('themes'))
    settings.THEMES_DIRECTORY = base_dir

    themes_dir = os.path.join(base_dir, 'publik-base')
    os.mkdir(themes_dir)
    with open(os.path.join(themes_dir, 'themes.json'), 'w') as handler:
        handler.write(THEMES)

    # populate 'foobar' overlay
    themes_dir = os.path.join(base_dir, 'foobar')
    os.mkdir(themes_dir)
    for part in ('static', 'templates'):
        os.mkdir(os.path.join(themes_dir, part))
