# hobo - portal to configure and deploy applications
# Copyright (C) 2020  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.import pytest

import pytest
from django.test import override_settings

from hobo.environment.utils import get_variable
from hobo.seo.views import ALLOW, DISALLOW

from .test_manager import login

pytestmark = pytest.mark.django_db


def test_middleware(app):
    with override_settings(TEMPLATE_VARS={'robots_txt': 'xxx'}):
        resp = app.get('/robots.txt', status=200)
        assert resp.text == 'xxx'

    with override_settings():
        resp = app.get('/robots.txt', status=404)


def test_unlogged_access(app):
    # connect while not being logged in
    resp = app.get('/seo/', status=302)
    assert resp.location.endswith('/login/?next=/seo/')


def test_home(app, admin_user):
    login(app)
    resp = app.get('/seo/', status=200)
    assert [x['href'] for x in resp.html.find('span', {'class': 'actions'}).find_all('a')] == [
        '/seo/customize',
        '/seo/disallow',
        '/seo/allow',
    ]

    variable = get_variable('robots_txt')
    variable.value = ALLOW
    variable.save()
    resp = app.get('/seo/', status=200)
    assert 'disabled' in resp.html.find('a', {'href': '/seo/allow'})['class']

    variable = get_variable('robots_txt')
    variable.value = DISALLOW
    variable.save()
    resp = app.get('/seo/', status=200)
    assert 'disabled' in resp.html.find('a', {'href': '/seo/disallow'})['class']
    assert not resp.html.button


def test_allow(app, admin_user):
    login(app)
    variable = get_variable('robots_txt')
    variable.value = 'some content'
    variable.save()
    resp = app.get('/seo/allow', status=302)
    assert resp.location.endswith('/seo/')
    assert get_variable('robots_txt').value == ALLOW


def test_disallow(app, admin_user):
    login(app)
    variable = get_variable('robots_txt')
    variable.value = 'some content'
    variable.save()
    resp = app.get('/seo/disallow', status=302)
    assert resp.location.endswith('/seo/')
    assert get_variable('robots_txt').value == DISALLOW
    resp = resp.follow()
    assert resp.html.pre.text == DISALLOW


def test_custom(app, admin_user):
    login(app)
    variable = get_variable('robots_txt')
    variable.value = 'some content'
    variable.save()
    resp = app.get('/seo/customize', status=200)
    assert resp.html.textarea['name'] == 'content'
    assert resp.html.textarea.text.strip() == 'some content'
    resp.form['content'] = 'some new content'
    resp = resp.form.submit()
    assert get_variable('robots_txt').value == 'some new content'
    resp = resp.follow()
    assert resp.html.pre.text == 'some new content'


def test_meta(app, admin_user):
    login(app)
    resp = app.get('/seo/')
    resp.form['meta_description'] = 'meta description'
    resp.form['meta_keywords'] = 'meta, keywords'
    resp = resp.form.submit()
