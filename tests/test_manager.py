# hobo - portal to configure and deploy applications
# Copyright (C) 2015-2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import pytest
from httmock import HTTMock

from hobo.environment.models import Authentic, Variable
from hobo.environment.utils import get_variable
from hobo.profile import models
from hobo.profile.models import AttributeDefinition
from hobo.profile.utils import get_authn_information_from_idp


def login(app, username='admin', password='password'):
    login_page = app.get('/login/')
    login_form = login_page.forms[0]
    login_form['username'] = username
    login_form['password'] = password
    resp = login_form.submit()
    assert resp.status_int == 302
    return app


@pytest.fixture
def logged_app(app, admin_user):
    return login(app)


def test_unlogged_access(app):
    # connect while not being logged in
    assert app.get('/', status=302).location.endswith('/login/?next=/')


def test_access(logged_app):
    resp = logged_app.get('/', status=200)
    assert 'User Profile' not in resp.text
    assert 'Services' in resp.text
    assert 'Variables' in resp.text

    Authentic(title='bar', slug='bar', base_url='http://bar.example.net').save()
    resp = logged_app.get('/', status=200)
    assert 'User Profile' in resp.text


def test_logout(logged_app):
    app = logged_app
    app.get('/logout/')
    assert app.get('/', status=302).location.endswith('/login/?next=/')


@pytest.mark.parametrize('kind', ['boolean', 'string'])
def test_add_attribute(logged_app, admin_user, kind):
    app = logged_app
    assert models.AttributeDefinition.objects.filter(kind=kind).filter(name='test').count() == 0
    page = app.get('/profile/add-attribute', status=200)
    page.form['label'] = 'test'
    page.form['name'] = 'test'
    page.form['description'] = 'test'
    page.form['required'] = True
    page.form['required_on_login'] = True
    page.form['asked_on_registration'] = False
    page.form['user_editable'] = False
    page.form['user_visible'] = False
    page.form['disabled'] = False
    page.form['kind'] = kind
    page.form.submit()
    assert models.AttributeDefinition.objects.filter(kind=kind).filter(name='test').count() == 1


def test_edit_user_full_name_template(logged_app, admin_user, settings):
    app = logged_app
    value = '{{ user.first_name }}'
    assert not Variable.objects.filter(name='user_full_name_template')
    page = app.get('/profile/edit-user-full-name-template', status=200)
    page.form['user_full_name_template'] = value
    page.form.submit()
    assert Variable.objects.get(name='user_full_name_template').value == value

    value = '{{ user.last_name }} etc.'
    page = app.get('/profile/edit-user-full-name-template', status=200)
    page.form['user_full_name_template'] = value
    page.form.submit()
    assert Variable.objects.get(name='user_full_name_template').value == value

    page = app.get('/profile/edit-user-full-name-template', status=200)
    page.form['user_full_name_template'] = 'whatever'
    page.click('Cancel')
    assert Variable.objects.get(name='user_full_name_template').value == value

    page = app.get('/profile/edit-user-full-name-template', status=200)
    page.form['user_full_name_template'] = ''
    page.form.submit()
    assert Variable.objects.get(name='user_full_name_template').value == ''


def test_attribute_kind_not_restricted_at_model_level(db):
    assert models.AttributeDefinition.objects.create(label='test', kind='somestring')


def test_profile_home_view(logged_app):
    resp = logged_app.get('/profile/', status=200)
    assert [x['href'] for x in resp.html.findAll('a', {'rel': 'popup'})][2:5] == [
        '/profile/title/options',
        '/profile/first_name/options',
        '/profile/last_name/options',
    ]


def mocked_http(url, request):
    if url.path == '/api/authn-healthcheck/':
        activated = bool(url.netloc == 'idp.example.com')
        return {
            'content': {
                'err': 0,
                'data': {
                    'accept_email_authentication': activated,
                    'accept_phone_authentication': activated,
                    'phone_identifier_field': 'phone',
                },
            },
            'status_code': 200,
        }


def test_cached_authn_information_from_idp(settings, freezer):
    settings.KNOWN_SERVICES = {
        'authentic': {'idp': {'url': 'https://idp.example.com/', 'orig': 'example.com'}}
    }

    with HTTMock(mocked_http):
        authn_info = get_authn_information_from_idp()
    assert authn_info['accept_email_authentication'] is True
    assert authn_info['accept_phone_authentication'] is True

    settings.KNOWN_SERVICES = {
        'authentic': {'idp': {'url': 'https://deactivated.example.com/', 'orig': 'example.com'}}
    }

    # cached result
    with HTTMock(mocked_http):
        authn_info = get_authn_information_from_idp()
    assert authn_info['accept_email_authentication'] is True
    assert authn_info['accept_phone_authentication'] is True

    freezer.tick(11)
    # stale cache
    with HTTMock(mocked_http):
        authn_info = get_authn_information_from_idp()
    assert authn_info['accept_email_authentication'] is False
    assert authn_info['accept_phone_authentication'] is False


def test_identifier_attributes_deactivation_disabled(settings, freezer, logged_app):
    settings.KNOWN_SERVICES = {
        'authentic': {'idp': {'url': 'https://idp.example.com/', 'orig': 'example.com'}}
    }

    with HTTMock(mocked_http):
        resp = logged_app.get('/profile/email/options', status=200)
        assert resp.form['disabled'].checked is False
        assert 'disabled' in resp.form['disabled'].attrs
        assert 'The email attribute is an identifier' in resp.form.text

        resp = logged_app.get('/profile/phone/options', status=200)
        assert resp.form['disabled'].checked is False
        assert 'disabled' in resp.form['disabled'].attrs
        assert 'The "phone" phone attribute is an identifier' in resp.form.text

    settings.KNOWN_SERVICES = {
        'authentic': {'idp': {'url': 'https://deactivated.example.com/', 'orig': 'example.com'}}
    }

    # cached result
    with HTTMock(mocked_http):
        resp = logged_app.get('/profile/email/options', status=200)
        assert resp.form['disabled'].checked is False
        assert 'disabled' in resp.form['disabled'].attrs
        assert 'The email attribute is an identifier' in resp.form.text

        resp = logged_app.get('/profile/phone/options', status=200)
        assert resp.form['disabled'].checked is False
        assert 'disabled' in resp.form['disabled'].attrs
        assert 'The "phone" phone attribute is an identifier' in resp.form.text

    freezer.tick(11)

    # stale cache
    with HTTMock(mocked_http):
        resp = logged_app.get('/profile/email/options', status=200)
        assert resp.form['disabled'].checked is False
        assert 'disabled' not in resp.form['disabled'].attrs
        assert 'The email attribute is an identifier' not in resp.form.text
        resp.form.set('disabled', True)
        resp.form.submit()
        assert AttributeDefinition.objects.get(name='email').disabled

        resp = logged_app.get('/profile/phone/options', status=200)
        assert resp.form['disabled'].checked is False
        assert 'disabled' not in resp.form['disabled'].attrs
        assert 'The "phone" phone attribute is an identifier' not in resp.form.text
        resp.form.set('disabled', True)
        resp.form.submit()
        assert AttributeDefinition.objects.get(name='phone').disabled


def test_reorder_view(logged_app):
    assert AttributeDefinition.objects.filter(name='first_name')[0].order == 2
    new_order = '3,2,1,4,5,6,7,8,9,10,11'
    resp = logged_app.get('/profile/reorder?new-order=%s' % new_order, status=302)
    assert resp.location == '/profile/'
    assert AttributeDefinition.objects.filter(name='last_name')[0].order == 1
    resp = resp.follow()
    assert [x['href'] for x in resp.html.findAll('a', {'rel': 'popup'})][2:5] == [
        '/profile/last_name/options',
        '/profile/first_name/options',
        '/profile/title/options',
    ]


def test_profile_attribute_option_view(logged_app):
    assert AttributeDefinition.objects.filter(name='first_name')[0].required
    resp = logged_app.get('/profile/first_name/options', status=200)
    assert resp.form['required'].checked is True
    resp.form['required'].checked = False
    resp = resp.form.submit()
    assert not AttributeDefinition.objects.filter(name='first_name')[0].required
    assert resp.location == '/profile/'


@pytest.mark.parametrize('name', ['first_name', 'last_name'])
def test_profile_attribute_searchable_is_disabled_on_name(logged_app, name):
    resp = logged_app.get(f'/profile/{name}/options')
    assert 'disabled' in resp.form['searchable'].attrs
    assert resp.form['searchable'].value


def test_profile_attribute_searchable_is_not_disabled_on_address(logged_app):
    resp = logged_app.get('/profile/address/options')
    assert 'disabled' not in resp.form['searchable'].attrs


def test_debug_home(logged_app):
    from hobo.environment.utils import get_installed_services_dict, get_setting_variable

    IPS = '99.99.99.99 77.77.77.77'
    IP_LIST = ['99.99.99.99', '77.77.77.77']

    page = logged_app.get('/debug/')
    page.form['debug_log'] = True
    page.form['debug_ips'] = IPS
    page = page.form.submit().follow()

    assert get_setting_variable('DEBUG_LOG').json is True
    assert get_setting_variable('INTERNAL_IPS.extend').json == IP_LIST
    hobo_json = get_installed_services_dict()
    assert hobo_json['variables']['SETTING_DEBUG_LOG'] is True
    assert hobo_json['variables']['SETTING_INTERNAL_IPS.extend'] == IP_LIST

    page.form['debug_log'] = False
    page.form['debug_ips'] = ''
    page = page.form.submit().follow()

    assert get_setting_variable('DEBUG_LOG').json is False
    assert get_setting_variable('INTERNAL_IPS.extend').json == []
    hobo_json = get_installed_services_dict()
    assert hobo_json['variables']['SETTING_DEBUG_LOG'] is False
    assert hobo_json['variables']['SETTING_INTERNAL_IPS.extend'] == []

    # toggle-current-ip button
    page = logged_app.get('/debug/')
    page.form['debug_ips'] = IPS
    page = page.form.submit(name='toggle-current-ip').follow()  # click
    assert get_setting_variable('INTERNAL_IPS.extend').json == IP_LIST + ['127.0.0.1']
    page = page.form.submit(name='toggle-current-ip').follow()  # click again
    assert get_setting_variable('INTERNAL_IPS.extend').json == IP_LIST

    # wrong ips are not returned as a list
    page = logged_app.get('/debug/')
    page.form['debug_ips'] = 'not_an_IP'
    page = page.form.submit()
    assert 'Enter a valid IPv4 or IPv6 address' in page.text
    assert page.form['debug_ips']._value == 'not_an_IP'  # get 'n o t _ a n _ I P'


def test_sms(logged_app):
    resp = logged_app.get('/sms/')
    resp.form['sms_sender'] = 'foo'
    resp.form['sms_url'] = 'https://foo.invalid'
    resp = resp.form.submit().follow()
    assert get_variable('sms_sender').value == 'foo'
    assert get_variable('sms_url').value == 'https://foo.invalid'

    resp.form['sms_sender'] = 'foo_ba.r'
    resp.form['sms_url'] = 'https://foo.invalid'
    resp = resp.form.submit()
    assert get_variable('sms_sender').value == 'foo_ba.r'
    assert get_variable('sms_url').value == 'https://foo.invalid'
