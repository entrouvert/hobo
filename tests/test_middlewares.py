def test_common_middleware(db, django_app_factory):
    app = django_app_factory(csrf_checks=False)

    resp = app.get('/__ping__/', status=200)
    assert resp.json.get('err') == 0

    resp = app.get('/api/health/', status=200)
    assert 'data' in resp.json

    resp = app.get('/api/health', status=301)
    assert resp.location == '/api/health/'

    for method in (app.post, app.put, app.patch):
        resp = method('/api/health/', status=200)
        assert 'data' in resp.json

        resp = method('/api/health', status=404)
        assert resp.json == {'err': 1, 'err_desc': 'URL must end with a slash.'}
