from unittest.mock import patch

import pytest

from hobo.environment.models import Variable

from .test_manager import login

pytestmark = pytest.mark.django_db


@patch('hobo.theme.views.random.random', return_value=0.1)
def test_theme_view(mocked_random, app, admin_user, fake_themes):
    app = login(app)
    resp = app.get('/theme').follow()
    assert [x['value'] for x in resp.html.findAll('input', {'type': 'radio'})] == ['alfortville', 'publik']
    resp.form['theme'].value = 'alfortville'
    resp = resp.form.submit()
    assert Variable.objects.filter(name='theme')[0].value == 'alfortville'
    assert Variable.objects.filter(name='foo')[0].value == 'bar'
    assert resp.location == '/theme/'
    resp = resp.follow()
    assert 'The theme has been changed' in str(resp.html)
    assert resp.form['theme'].value == 'alfortville'

    resp.form['theme'].value = 'publik'
    resp = resp.form.submit()
    assert resp.location == '/theme/'
    assert Variable.objects.filter(name='theme')[0].value == 'publik'
    assert Variable.objects.filter(name='foo')[0].value == ''

    # easter egg, sometimes it gets sorted by colour
    mocked_random.return_value = 0.09
    resp = app.get('/theme').follow()
    assert [x['value'] for x in resp.html.findAll('input', {'type': 'radio'})] == ['publik', 'alfortville']


def test_theme_view_empty(app, admin_user, settings):
    del settings.THEMES_DIRECTORY
    app = login(app)
    resp = app.get('/theme').follow()
    assert 'Theme' in resp.text


def test_theme_option_view(app, admin_user, fake_themes):
    app = login(app)
    resp = app.get('/theme').follow()
    resp = resp.click('Options')
    assert resp.html.find('label').text == 'Global Title:'
    resp.form['global_title'] = 'foo'
    resp = resp.form.submit()
    assert Variable.objects.all()[0].name == 'global_title'
    assert Variable.objects.all()[0].value == 'foo'

    assert resp.location == '.'
    resp = resp.follow()
    assert resp.html.find('li').text == 'foo'


def test_theme_select_view_without_theme_selected(app, admin_user, fake_themes):
    assert Variable.objects.filter(name='theme').count() == 0
    app = login(app)
    resp = app.get('/theme/select')
    assert resp.location == '/theme/'
