# Copyright (C) Entr'ouvert

import unittest.mock
import urllib.parse

import pytest
import responses

from hobo.requests_wrapper import Requests


@pytest.fixture
def known_services(settings):
    settings.KNOWN_SERVICES = {
        'x': {
            'y': {
                'url': 'https://remote.example.com/',
                'secret': 'foobar',
                'orig': 'remote.example.com',
            }
        }
    }


@pytest.fixture
def fix_signature_nonce():
    with unittest.mock.patch('hobo.signature.get_nonce', return_value='b7474791a1a04e9fc1b1b5da30515de5'):
        yield


@responses.activate
def test_get_on_remote_service(known_services, fix_signature_nonce, freezer):
    # check real url is signed and original query string is conserved

    freezer.move_to('2024-01-01T01:01:01Z')

    remote_mock = responses.add(responses.GET, 'https://remote.example.com/api/endpoint/', json={'err': 0})

    response = Requests().get('https://remote.example.com/api/endpoint/?foo=bar')
    assert response.json() == {'err': 0}
    real_url = remote_mock.calls[-1].request.url
    parsed_real_url = urllib.parse.urlparse(real_url)
    real_url_qs = urllib.parse.parse_qs(parsed_real_url.query)

    assert (
        urllib.parse.urlunparse(parsed_real_url._replace(query=''))
        == 'https://remote.example.com/api/endpoint/'
    )
    assert real_url_qs == {
        'algo': [
            'sha256',
        ],
        'foo': [
            'bar',
        ],
        'nonce': [
            'b7474791a1a04e9fc1b1b5da30515de5',
        ],
        'orig': [
            'remote.example.com',
        ],
        'signature': [
            '5RGhucpQPGAbW6nvzris6eOcYuCd6AIDNZunj39A+bs=',
        ],
        'timestamp': [
            '2024-01-01T01:01:01Z',
        ],
    }


@responses.activate
def test_get_on_external_url(known_services, freezer):
    # check real url is not signed for unknown remote host
    freezer.move_to('2024-01-01T01:01:01Z')

    remote_mock = responses.add(responses.GET, 'https://other.example.com/api/endpoint/', json={'err': 0})

    response = Requests().get('https://other.example.com/api/endpoint/?foo=bar')
    assert response.json() == {'err': 0}
    real_url = remote_mock.calls[-1].request.url
    parsed_real_url = urllib.parse.urlparse(real_url)
    real_url_qs = urllib.parse.parse_qs(parsed_real_url.query)

    assert (
        urllib.parse.urlunparse(parsed_real_url._replace(query=''))
        == 'https://other.example.com/api/endpoint/'
    )
    assert real_url_qs == {
        'foo': [
            'bar',
        ],
    }
