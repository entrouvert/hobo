from unittest import mock

import pytest

from hobo.requests_wrapper import NothingInCacheException, Requests


def test_requests_cache(settings):
    settings.KNOWN_SERVICES = {
        'chrono': {
            'foobar': {
                'title': 'Foo',
                'url': 'https://chrono.example.com/',
                'orig': 'example.com',
                'secret': 'xxx',
            }
        },
        'hobo': {
            'hobo': {
                'title': 'Hobo',
                'url': 'https://hobo.example.com/',
                'orig': 'example.com',
                'secret': 'xxx',
            }
        },
    }
    kwargs = {'cache_duration': 15}
    with mock.patch('hobo.requests_wrapper.RequestsSession.request') as requests_get:
        requests = Requests()
        requests_get.return_value = mock.Mock(content=b'hello world', status_code=200)
        # default cache, nothing in there
        assert requests.get('http://chrono.example.com/', **kwargs).content == b'hello world'
        assert requests_get.call_count == 1
        # now there's something in cache
        assert requests.get('http://chrono.example.com/', **kwargs).content == b'hello world'
        assert requests_get.call_count == 1
        # passing parameters triggers new request
        assert (
            requests.get('http://chrono.example.com/', params={'test': 'test'}, **kwargs).content
            == b'hello world'
        )
        assert requests_get.call_count == 2
        # if parameters are the same, cache is used
        assert (
            requests.get('http://chrono.example.com/', params={'test': 'test'}, **kwargs).content
            == b'hello world'
        )
        assert requests_get.call_count == 2
        # value changed
        requests_get.return_value = mock.Mock(content=b'hello second world', status_code=200)
        assert requests.get('http://chrono.example.com/', **kwargs).content == b'hello world'
        assert requests_get.call_count == 2
        # force cache invalidation
        assert (
            requests.get('http://chrono.example.com/', invalidate_cache=True, **kwargs).content
            == b'hello second world'
        )
        assert requests_get.call_count == 3
        # check raise_if_not_cached
        with pytest.raises(NothingInCacheException):
            requests.get('http://chrono.example.com/other', raise_if_not_cached=True, **kwargs)

        # check with unicode url
        assert requests.get('http://chrono.example.com/éléphant', **kwargs).content == b'hello second world'

        # check that 'cache_duration' keyword absence deactivate cache hit
        requests_get.return_value = mock.Mock(content=b'hello world again', status_code=200)
        assert requests.get('http://chrono.example.com/').content == b'hello world again'

        requests_get.return_value = mock.Mock(content=b'goodbye world', status_code=200)
        assert requests.get('http://chrono.example.com/').content == b'goodbye world'
