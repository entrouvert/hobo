from hobo.environment.models import Variable

from .test_manager import login


def test_sms_view(app, admin_user):
    app = login(app)
    resp = app.get('/')
    resp = resp.click('SMS')
    resp.form['sms_url'] = 'https://example.com/send/'
    resp.form['sms_sender'] = 'Sender Name'
    resp = resp.form.submit().follow()

    assert Variable.objects.filter(name='sms_url')[0].value == 'https://example.com/send/'
    assert Variable.objects.filter(name='sms_sender')[0].value == 'Sender Name'

    resp.form['sms_sender'] = 'Entr\'ouvert'
    resp = resp.form.submit()
    assert 'Only alphanumeric' in resp.text

    resp.form['sms_sender'] = 'Sender Name'
    resp.form['sms_url'] = '{{syntax{error}}'
    resp = resp.form.submit()
    assert 'Invalid template' in resp.text
