import json
import re
import threading
import time
from unittest import mock

import pytest
import requests
import responses
from django.core.cache import cache
from django.test import override_settings
from django.test.client import RequestFactory
from django.utils import translation

from hobo.context_processors import theme_base, user_urls

TEMPLATE = 'Feeling lucky, punk?'
TEMPLATE_PAGE2 = 'Response for page 2'


def wait_other_threads():
    time.sleep(0.1)
    for thread in threading.enumerate()[1:]:
        thread.join(timeout=10)
    assert len(threading.enumerate()) == 1


@pytest.fixture
def portal_mock():
    def callback(request):
        headers = {
            'X-Combo-Skeleton-Pages': json.dumps(
                {'1': 'http://testserver/foo', '2': 'http://testserver/page1/page2/'}
            )
        }
        if 'page2' in request.url:
            return 200, headers, TEMPLATE_PAGE2
        else:
            return 200, headers, TEMPLATE

    with (
        responses.RequestsMock() as rsps,
        override_settings(INSTALLED_APPS=[], THEME_SKELETON_URL='http://combo.example.com/_skeleton_/'),
    ):
        rsps.add_callback(responses.GET, 'http://combo.example.com/_skeleton_/', callback=callback)
        yield rsps


@pytest.fixture
def theme_base_source(rf):
    def theme_base_source(path):
        context = theme_base(rf.get(path))
        return context['theme_base']().source

    return theme_base_source


def test_theme_base(portal_mock, theme_base_source):
    cache.clear()

    calls = portal_mock.calls

    assert theme_base_source('/') == TEMPLATE
    assert calls[0].request.url == 'http://combo.example.com/_skeleton_/?source=http%3A%2F%2Ftestserver%2F'
    wait_other_threads()

    # requested page + root + pages from X-Combo-Skeleton-Pages header
    assert len(calls) == 4

    calls.reset()

    assert theme_base_source('/') == TEMPLATE
    assert not calls

    assert theme_base_source('/page1/page2/') == TEMPLATE_PAGE2
    assert not calls

    assert theme_base_source('/page1/') == TEMPLATE
    assert not calls

    assert theme_base_source('/page1/page2/') == TEMPLATE_PAGE2
    assert not calls


@pytest.fixture
def portal_mock_error():
    def callback(request):
        headers = {
            'X-Combo-Skeleton-Pages': json.dumps(
                {'1': 'http://testserver/foo', '2': 'http://testserver/page1/page2/'}
            )
        }
        if 'page2' in request.url:
            raise requests.RequestException('page2 boom!')
        return 200, headers, TEMPLATE

    with (
        responses.RequestsMock() as rsps,
        override_settings(INSTALLED_APPS=[], THEME_SKELETON_URL='http://combo.example.com/_skeleton_/'),
    ):
        rsps.add_callback(responses.GET, 'http://combo.example.com/_skeleton_/', callback=callback)
        yield rsps


def test_theme_base_error(portal_mock_error, theme_base_source, caplog, freezer):
    calls = portal_mock_error.calls
    cache.clear()
    caplog.set_level('WARNING')

    assert not caplog.messages
    assert theme_base_source('/') == TEMPLATE
    wait_other_threads()
    assert len(calls) == 3

    calls.reset()
    portal_mock_error.replace(
        responses.GET, 'http://combo.example.com/_skeleton_/', body=requests.RequestException('boom!')
    )

    assert theme_base_source('/') == TEMPLATE
    assert not calls
    assert not caplog.messages

    # move 5 minutes later...
    freezer.tick(301)
    assert theme_base_source('/') == TEMPLATE
    wait_other_threads()

    assert calls
    assert re.match('WARNING.*failed to retrieve theme.*boom!', caplog.text)
    caplog.clear()
    calls.reset()

    # move 1 hours later...
    freezer.tick(3300)
    assert theme_base_source('/') == TEMPLATE
    wait_other_threads()
    assert len(calls) == 1
    assert re.match('WARNING.*failed to retrieve theme.*boom!', caplog.text)
    caplog.clear()
    calls.reset()

    freezer.tick(301)

    assert theme_base_source('/') == TEMPLATE
    wait_other_threads()
    assert len(calls) == 1
    assert re.match('WARNING.*failed to retrieve theme.*boom!', caplog.text)
    caplog.clear()
    calls.reset()

    freezer.tick(301)

    assert theme_base_source('/') == TEMPLATE
    wait_other_threads()
    assert len(calls) == 1
    assert re.match('ERROR.*failed to retrieve theme.*boom!', caplog.text)
    caplog.clear()
    calls.reset()

    # after some times cache is evicted and...
    cache.clear()
    with pytest.raises(Exception, match=r'Failed to retrieve theme:.*boom'):
        theme_base_source('/')


@pytest.fixture
def portal_mock_language():
    def callback(request):
        language = request.headers['Accept-Language']
        headers = {'X-Combo-Skeleton-Pages': json.dumps({'1': 'http://testserver/foo'})}
        if language == 'en':
            return 200, headers, 'Skeleton for English'
        elif language == 'fr':
            return 200, headers, 'Skeleton for French'

    with (
        responses.RequestsMock() as rsps,
        override_settings(
            INSTALLED_APPS=[],
            LANGUAGES=[('en', 'English'), ('fr', 'French')],
            THEME_SKELETON_URL='http://combo.example.com/_skeleton_/',
        ),
    ):
        rsps.add_callback(responses.GET, 'http://combo.example.com/_skeleton_/', callback=callback)
        yield rsps


def test_theme_base_language(portal_mock_language, theme_base_source):
    cache.clear()

    calls = portal_mock_language.calls

    assert theme_base_source(path='/') == 'Skeleton for English'

    wait_other_threads()

    assert len(calls) == 5

    language_and_urls = {(call.request.headers['accept-language'], call.request.url) for call in calls}
    assert {
        ('en', 'http://combo.example.com/_skeleton_/?source=http%3A%2F%2Ftestserver%2F'),
        ('fr', 'http://combo.example.com/_skeleton_/?source=http%3A%2F%2Ftestserver%2F'),
        ('en', 'http://combo.example.com/_skeleton_/?source=http%3A%2F%2Ftestserver%2Ffoo'),
        ('fr', 'http://combo.example.com/_skeleton_/?source=http%3A%2F%2Ftestserver%2Ffoo'),
    } == language_and_urls

    assert theme_base_source(path='/') == 'Skeleton for English'

    with translation.override('fr'):
        assert theme_base_source(path='/') == 'Skeleton for French'


class IdPRequestFactory(RequestFactory):
    def _base_environ(self, **request):
        environ = super()._base_environ(**request)
        environ['SCRIPT_NAME'] = 'idp'
        environ['wsgi.url_scheme'] = 'https'
        return environ


def test_user_urls(settings, rf):
    settings.TEMPLATE_VARS = {
        'idp_registration_url': 'https://idp/register/',
        'idp_account_url': 'https://idp/accounts/',
    }

    request = rf.get('/page/')
    assert user_urls(request) == {
        'login_url': '/login/?next=%2Fpage%2F',
        'logout_url': '/logout/?next=%2F',
        'registration_url': 'https://idp/register/?next=http%3A%2F%2Ftestserver%2Fpage%2F',
        'account_url': 'https://idp/accounts/?next=http%3A%2F%2Ftestserver%2Fpage%2F',
    }

    request = IdPRequestFactory().get('/accounts/?next=http%3A%2F%2Ftestserver%2Fpage%2F')
    assert user_urls(request)['account_url'] == 'https://idp/accounts/?next=http%3A%2F%2Ftestserver%2Fpage%2F'

    with mock.patch('hobo.context_processors._authentic2_get_next_url', return_value='coin'):
        # simulate the real get_next_url of authentic2
        from django.conf import settings as real_settings

        real_settings.PROJECT_NAME = 'authentic2-multitenant'
        request = rf.get('/login/?next=coin&nonce=2')
        assert user_urls(request) == {
            'login_url': '#',
            'logout_url': '/logout/?next=%2F',
            'registration_url': '/register/?next=coin&nonce=2',
            'account_url': 'https://idp/accounts/?next=http%3A%2F%2Ftestserver%2Flogin%2F%3Fnext%3Dcoin%26nonce%3D2',
        }

        request = rf.get('/register/?next=coin&nonce=2')
        assert user_urls(request) == {
            'login_url': '/login/?next=coin&nonce=2',
            'logout_url': '/logout/?next=%2F',
            'registration_url': '#',
            'account_url': 'https://idp/accounts/?next=http%3A%2F%2Ftestserver%2Fregister%2F%3Fnext%3Dcoin%26nonce%3D2',
        }
