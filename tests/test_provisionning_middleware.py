from django.contrib.auth import get_user_model

from hobo.signature import sign_url


def test_provisionning(app, db, settings):
    settings.HOBO_ANONYMOUS_SERVICE_USER_CLASS = 'hobo.rest_authentication.AnonymousAdminServiceUser'
    settings.KNOWN_SERVICES = {
        'chrono': {
            'foobar': {
                'title': 'Foo',
                'url': 'https://chrono.example.invalid/',
                'verif_orig': 'chrono.example.invalid',
                'secret': 'xxx',
                'provisionning-url': 'https://chrono.example.invalid/__provision__/',
            }
        },
        'hobo': {
            'hobo': {
                'title': 'Hobo',
                'url': 'https://hobo.example.invalid/',
                'verif_orig': 'hobo.example.invalid',
                'secret': 'xxx',
                'provisionning-url': 'https://hobo.example.invalid/__provision__/',
            }
        },
    }
    notification = {
        '@type': 'provision',
        'issuer': 'http://idp.example.net/idp/saml/metadata',
        'objects': {
            '@type': 'user',
            'data': [
                {
                    'id': 1,
                    'uuid': 'a' * 32,
                    'username': 'johndoe',
                    'first_name': 'John',
                    'last_name': 'Doe',
                    'email': 'john.doe@example.net',
                    'is_superuser': True,
                    'is_staff': True,
                    'is_active': True,
                    'roles': [],
                    'ou': {'foo': 'bar'},
                    'date_joined': '2022-07-19T15:07:54.649675+02:00',
                    'last_login': '2023-01-03T10:56:11.552280+01:00',
                    'password': 'pbkdf2_sha256$150000$afVbUpBWl3v7$6D9xCdIf0lnjSGHm+BxPmkWvRvIq0vvP4c/FTFhZnkY=',
                    'email_verified': False,
                    'email_verified_date': None,
                    'phone': '123456',
                    'phone_verified_on': None,
                    'modified': '2023-01-03T10:59:45.103274+01:00',
                    'last_account_deletion_alert': None,
                    'deactivation': None,
                    'deactivation_reason': None,
                    'first_name_verified': False,
                    'last_name_verified': False,
                    'address': 'Somewhere',
                    'address_verified': False,
                    'zipcode': '13333',
                    'zipcode_verified': False,
                    'city': 'Marseille',
                    'city_verified': False,
                    'phone_verified': False,
                    'mobile': '56789',
                    'mobile_verified': False,
                    'preferred_username': 'Blue',
                    'preferred_username_verified': False,
                    'custom_attr': 'foo',
                    'custom_attr_verified': False,
                }
            ],
        },
    }
    User = get_user_model()
    assert User.objects.count() == 0
    app.put_json('/__provision__/', notification, status=403)
    assert User.objects.count() == 0
    app.put_json(
        sign_url('/__provision__/?orig=%s' % 'hobo.example.invalid', 'xxx'), notification, status=200
    )
    assert User.objects.count() == 1

    user = User.objects.latest('pk')
    assert user.email == 'john.doe@example.net'

    excluded_attrs = ['password', 'roles']
    expected_data = {k: v for k, v in notification['objects']['data'][0].items() if k not in excluded_attrs}
    assert user.extra_attributes.data == expected_data
