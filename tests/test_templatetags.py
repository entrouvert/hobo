# hobo - portal to configure and deploy applications
# Copyright (C) 2015-2021 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.template import Context, Template


def test_as_numeral(settings):
    t = Template('{{ number|as_numeral }}')
    assert t.render(Context({'number': 42})) == 'forty-two'
    assert t.render(Context({'number': '42'})) == 'forty-two'
    assert t.render(Context({'number': 42.15})) == 'forty-two'
    assert t.render(Context({'number': '42,15'})) == 'forty-two'
    assert t.render(Context({'number': -42})) == 'minus forty-two'
    assert t.render(Context({'number': 100200})) == 'one hundred thousand, two hundred'
    assert t.render(Context({'number': '100200'})) == 'one hundred thousand, two hundred'
    assert t.render(Context({'number': None})) == ''
    assert t.render(Context({'number': 'foo'})) == ''
    assert t.render(Context({'number': ['foo', 'bar']})) == ''
    assert t.render(Context({'number': {'foo': 'bar'}})) == ''
    assert t.render(Context({'number': 10**500})) == ''

    settings.LANGUAGE_CODE = 'fr'
    assert t.render(Context({'number': '42'})) == 'quarante-deux'
    assert t.render(Context({'number': 42.15})) == 'quarante-deux'
    assert t.render(Context({'number': 100200})) == 'cent mille deux cents'


def test_as_numeral_currency(settings):
    t = Template('{{ number|as_numeral_currency }}')
    assert t.render(Context({'number': 42})) == 'forty-two euro, zero cents'
    assert t.render(Context({'number': '42'})) == 'forty-two euro, zero cents'
    assert t.render(Context({'number': 42.15})) == 'forty-two euro, fifteen cents'
    assert t.render(Context({'number': '42,15'})) == 'forty-two euro, fifteen cents'
    assert t.render(Context({'number': -42})) == 'minus forty-two euro, zero cents'
    assert t.render(Context({'number': 100200})) == 'one hundred thousand, two hundred euro, zero cents'
    assert t.render(Context({'number': '100200'})) == 'one hundred thousand, two hundred euro, zero cents'
    assert t.render(Context({'number': None})) == ''
    assert t.render(Context({'number': 'foo'})) == ''
    assert t.render(Context({'number': ['foo', 'bar']})) == ''
    assert t.render(Context({'number': {'foo': 'bar'}})) == ''
    assert t.render(Context({'number': 10**500})) == ''

    settings.LANGUAGE_CODE = 'fr'
    assert t.render(Context({'number': 42})) == 'quarante-deux euros'
    assert t.render(Context({'number': 42.15})) == 'quarante-deux euros et quinze centimes'
    assert t.render(Context({'number': '1'})) == 'un euro'
    assert t.render(Context({'number': 100200})) == 'cent mille deux cents euros'
