import json
import re
from unittest import mock

import pytest
from django.contrib.auth.models import User

from hobo.environment.models import Authentic, Variable

from .test_manager import login

pytestmark = pytest.mark.django_db


@pytest.fixture
def user(db):
    return User.objects.create_user('jhon', email='jhon@doe.love', password='xxx')


def test_home_view(app, admin_user):
    app = login(app)
    resp = app.get('/')
    assert resp.html.find('h1').text == 'System'
    assert resp.html.find('h2').text == 'System'


def test_home_view_with_normal_user(app, user):
    app = login(app, 'jhon', 'xxx')
    app.get('/', status=403)


def test_home_view_with_not_auth(app):
    resp = app.get('/')
    assert resp.location == '/login/?next=/'


def test_hobo_view(app, user):
    app = login(app, 'jhon', 'xxx')
    resp = app.get('/hobos.json')
    assert resp.json == ['http://testserver/']


def test_idp_login(app, user):
    Authentic.objects.create(
        title='bar',
        slug='bar',
        base_url='http://bar.example.net',
        use_as_idp_for_self=True,
        last_operational_success_timestamp='2022-2-22',
        last_operational_check_timestamp='2022-2-22',
    )
    resp = app.get('/login/')
    assert resp.location == '/accounts/mellon/login/?'


@mock.patch('hobo.views.auth_logout')
def test_logout_view(mocked_logout, app):
    resp = app.get('/logout/')
    assert mocked_logout.called
    assert resp.location == 'http://testserver/'

    Authentic.objects.create(
        title='bar',
        slug='bar',
        base_url='http://bar.example.net',
        use_as_idp_for_self=True,
        last_operational_success_timestamp='2022-2-22',
        last_operational_check_timestamp='2022-2-22',
    )
    resp = app.get('/logout/')
    assert resp.location == '/accounts/mellon/logout/?'


def test_healt_view(app):
    resp = app.get('/api/health/')
    assert resp.json == {'data': {}}
    Authentic.objects.create(
        title='bar',
        slug='bar',
        base_url='http://bar.example.net',
        last_operational_success_timestamp='2022-2-22',
        last_operational_check_timestamp='2022-2-22',
    )
    resp = app.get('/api/health/')
    assert resp.json == {
        'data': {
            'bar': {
                'has_valid_certificate': False,
                'is_operational': True,
                'is_resolvable': False,
                'is_running': False,
                'security_data': {'level': 'NaN', 'label': ''},
            }
        }
    }


def test_menu_view(app, admin_user):
    expected = [{'slug': 'system', 'label': 'System', 'url': 'http://testserver/'}]
    app = login(app)
    resp = app.get('/menu.json')
    assert resp.content_type == 'application/json'
    assert resp.json == expected

    resp = app.get('/menu.json?callback=foo')
    assert resp.content_type == 'application/javascript'
    json_str = re.match(r'foo\((.*)\)', resp.text).group(1)
    assert json.loads(json_str) == expected


def test_warning_notifications(app, admin_user):
    app = login(app)
    resp = app.get('/')
    assert len(resp.html.find_all('p', {'class': 'warning'})) == 2

    assert resp.html.find('p', {'class': 'warning'}).parent['href'] == '/theme/'
    Variable.objects.create(name='global_title', value='Publik')
    resp = app.get('/')
    assert len(resp.html.find_all('p', {'class': 'warning'})) == 1

    assert resp.html.find('p', {'class': 'warning'}).parent['href'] == '/emails/'
    Variable.objects.create(name='default_from_email', value='publik@example.com')
    resp = app.get('/')
    assert not resp.html.find_all('p', {'class': 'warning'})
