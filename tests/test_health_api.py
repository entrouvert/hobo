import json
import socket
from unittest.mock import MagicMock

import pytest
import requests
from django.core.cache import cache
from django.utils import timezone
from httmock import HTTMock, remember_called, urlmatch

from hobo.environment.models import Authentic, Combo

pytestmark = pytest.mark.django_db


@pytest.fixture
def services(request):
    now = timezone.now()
    a = Authentic(
        title='blues',
        slug='blues',
        base_url='https://blues.example.publik',
        last_operational_check_timestamp=now,
        last_operational_success_timestamp=now,
    )
    a.save()
    c = Combo(title='jazz', slug='jazz', base_url='https://jazz.example.publik')
    c.save()


def test_response(app, admin_user, services, monkeypatch):
    cache.clear()
    monkeypatch.setattr(socket, 'gethostbyname', lambda x: '176.31.123.109')
    monkeypatch.setattr(requests, 'get', lambda x, *args, **kwargs: MagicMock(status_code=200))
    response = app.get('/api/health/')
    assert response.status_code == 200
    content = json.loads(response.text)
    assert 'blues' in content['data'].keys()
    assert 'jazz' in content['data'].keys()


def test_is_resolvable(app, admin_user, services, monkeypatch):
    cache.clear()

    def gethostname(netloc):
        if netloc == 'jazz.example.publik':
            return '176.31.123.109'
        else:
            raise socket.gaierror

    monkeypatch.setattr(socket, 'gethostbyname', gethostname)
    monkeypatch.setattr(requests, 'get', lambda x, *args, **kwargs: MagicMock(status_code=200))
    response = app.get('/api/health/')
    content = json.loads(response.text)
    blues = content['data']['blues']
    jazz = content['data']['jazz']
    assert not blues['is_resolvable']
    assert jazz['is_resolvable']


def test_is_running(app, admin_user, services, monkeypatch):
    cache.clear()
    monkeypatch.setattr(socket, 'gethostbyname', lambda x: '176.31.123.109')

    @urlmatch(netloc='jazz.example.publik')
    @remember_called
    def jazz_mock(url, request):
        return {'status_code': 200}

    @urlmatch(netloc='blues.example.publik')
    @remember_called
    def blues_mock(url, request):
        return {'status_code': 404}

    with HTTMock(blues_mock, jazz_mock):
        response = app.get('/api/health/')
        content = json.loads(response.text)
        blues = content['data']['blues']
        jazz = content['data']['jazz']
        assert not blues['is_running']
        assert jazz['is_running']
        assert blues_mock.call['count'] == 3
        assert jazz_mock.call['count'] == 3

        # check it gets results from cache
        response = app.get('/api/health/')
        content = json.loads(response.text)
        blues = content['data']['blues']
        jazz = content['data']['jazz']
        assert not blues['is_running']
        assert jazz['is_running']
        assert blues_mock.call['count'] == 3
        assert jazz_mock.call['count'] == 3


def test_security_data(app, admin_user, services, monkeypatch):
    cache.clear()
    monkeypatch.setattr(socket, 'gethostbyname', lambda x: '176.31.123.109')

    @urlmatch(netloc='jazz.example.publik')
    @remember_called
    def jazz_mock(url, request):
        return {
            'status_code': 200,
            'headers': {
                'X-Content-Type-Options': 'nosniff',
                'X-Frame-Options': 'SAMEORIGIN',
                'X-XSS-Protection': '1; mode=block',
                'Strict-Transport-Security': 'max-age=63072000',
            },
        }

    @urlmatch(netloc='blues.example.publik')
    @remember_called
    def blues_mock(url, request):
        return {'status_code': 200}

    with HTTMock(blues_mock, jazz_mock):
        response = app.get('/api/health/')
        content = json.loads(response.text)
        blues = content['data']['blues']
        jazz = content['data']['jazz']
        assert blues_mock.call['count'] == 3
        assert jazz_mock.call['count'] == 3
        assert blues['security_data']['level'] == 4
        assert jazz['security_data']['level'] == 0

        # check it gets results from cache
        response = app.get('/api/health/')
        content = json.loads(response.text)
        blues = content['data']['blues']
        jazz = content['data']['jazz']
        assert blues_mock.call['count'] == 3
        assert jazz_mock.call['count'] == 3
        assert blues['security_data']['level'] == 4
        assert jazz['security_data']['level'] == 0


def test_is_operational(app, admin_user, services, monkeypatch):
    cache.clear()
    monkeypatch.setattr(socket, 'gethostbyname', lambda x: '176.31.123.109')
    monkeypatch.setattr(requests, 'get', lambda x, *args, **kwargs: MagicMock(status_code=200))
    response = app.get('/api/health/')
    content = json.loads(response.text)
    blues = content['data']['blues']
    jazz = content['data']['jazz']
    assert blues['is_operational']
    assert not jazz['is_operational']


def test_has_valid_certificate(app, admin_user, services, monkeypatch):
    cache.clear()

    def get(url, verify, *args, **kwargs):
        if 'blues.example.publik' in url or not verify:
            return MagicMock(status_code=200)
        else:
            raise requests.exceptions.SSLError

    monkeypatch.setattr(socket, 'gethostbyname', lambda x: '176.31.123.109')
    monkeypatch.setattr(requests, 'get', get)
    response = app.get('/api/health/')
    content = json.loads(response.text)
    blues = content['data']['blues']
    jazz = content['data']['jazz']
    assert blues['has_valid_certificate']
    assert not jazz['has_valid_certificate']
