import pytest
from django.contrib.auth.models import User
from django.test import override_settings

from hobo.agent.common.models import UserExtraAttributes
from hobo.user_name.apps import get_full_name


@pytest.fixture
def user(db):
    u = User.objects.create(
        first_name='Jane',
        last_name='Doe',
    )
    UserExtraAttributes.objects.create(user=u, data={'foo': 'bar'})
    return u


def test_user_original_get_full_name(user):
    assert user.original_get_full_name() == 'Jane Doe'


def test_user_cached_extra_attributes(user):
    assert user.attributes == {'foo': 'bar'}


def test_user_cached_extra_attributes_missing_fallbacks_to_empty_dict(user):
    user.extra_attributes.delete()
    user.refresh_from_db()
    assert user.attributes == {}


def test_user_get_full_name_from_template(user):
    with override_settings(
        TEMPLATE_VARS={'user_full_name_template': '{{ user.first_name }} {{ user.attributes.foo }}'}
    ):
        assert get_full_name(user) == 'Jane bar'

    with override_settings(TEMPLATE_VARS={'user_full_name_template': ''}):
        assert get_full_name(user) == 'Jane Doe'


def test_user_get_full_name(user):
    with override_settings(
        TEMPLATE_VARS={'user_full_name_template': '{{ user.first_name }} {{ user.attributes.foo }}'}
    ):
        assert user.get_full_name() == 'Jane bar'

    with override_settings(TEMPLATE_VARS={'user_full_name_template': ''}):
        assert user.get_full_name() == 'Jane Doe'
