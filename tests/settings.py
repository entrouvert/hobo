import os

import hobo.test_utils

PROJECT_NAME = 'hobo'

LANGUAGE_CODE = 'en-us'
BROKER_URL = 'memory://'
LANGUAGES = [('en', 'English')]

# noqa pylint: disable=undefined-variable
INSTALLED_APPS += ('hobo.agent.common', 'hobo.user_name.apps.UserNameConfig')

# noqa pylint: disable=undefined-variable
ALLOWED_HOSTS.append('localhost')

# noqa pylint: disable=undefined-variable
TEMPLATES[0]['OPTIONS']['debug'] = True

# noqa pylint: disable=used-before-assignment
MIDDLEWARE = MIDDLEWARE + (
    'hobo.middleware.ping.PingMiddleware',
    'hobo.middleware.RobotsTxtMiddleware',
    'hobo.provisionning.middleware.ProvisionningMiddleware',
    'hobo.middleware.maintenance.MaintenanceMiddleware',
    'hobo.middleware.security.content_security_policy_middleware',
)

common_middleware_index = MIDDLEWARE.index('django.middleware.common.CommonMiddleware')
MIDDLEWARE = (
    MIDDLEWARE[:common_middleware_index]
    + ('hobo.middleware.common.HoboCommonMiddleware',)
    + MIDDLEWARE[common_middleware_index + 1 :]
)

HOBO_MANAGER_HOMEPAGE_URL_VAR = 'portal_agent_url'
DATABASES = {
    'default': {
        'ENGINE': os.environ.get('DB_ENGINE', 'django.db.backends.sqlite3'),
        'NAME': hobo.test_utils.get_safe_db_name(),
    }
}

TEMPLATES[0]['OPTIONS'].setdefault('builtins', []).append('hobo.templatetags.hobo')

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'hobo.rest_authentication.PublikAuthentication',
        'hobo.rest_authentication.APIClientAuthentication',
    ),
    'DEFAULT_RENDERER_CLASSES': ('rest_framework.renderers.JSONRenderer',),
}
