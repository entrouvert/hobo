from unittest import mock

import pytest
from django.core.management import load_command_class
from django.test import override_settings

from hobo.agent.common.management.commands.import_template import Command, UnknownTemplateError


@mock.patch('hobo.agent.common.management.commands.import_template.os.path.exists')
@mock.patch('hobo.agent.common.management.commands.import_template.call_command')
@mock.patch('hobo.agent.common.management.commands.import_template.get_commands')
def test_import_template_default(mocked_get_commands, mocked_call_command, mocked_exists):
    """do import a template from default path"""
    mocked_get_commands.return_value = 'import_site'
    mocked_call_command.return_value = mock.MagicMock()
    mocked_exists.return_value = True

    # simulate:
    # $ XXX-manage import-template import_me
    command = Command()
    with override_settings(PROJECT_NAME='bobo'):
        command.handle(template_name='import_me')

    # assert 'import_site' command is run
    assert mocked_call_command.mock_calls == [
        mock.call('import_site', '/var/lib/bobo/skeletons/import_me.json')
    ]


@mock.patch('hobo.agent.common.management.commands.import_template.os.path.exists')
@mock.patch('hobo.agent.common.management.commands.import_template.call_command')
@mock.patch('hobo.agent.common.management.commands.import_template.get_commands')
def test_import_template_basepath(mocked_get_commands, mocked_call_command, mocked_exists):
    """do import a template having basepath parameter provided"""
    mocked_get_commands.return_value = 'import_site'
    mocked_call_command.return_value = mock.MagicMock()
    mocked_exists.return_value = True

    # simulate:
    # $ XXX-manage import-template import_me --basepath /tmp
    command = Command()
    with override_settings(PROJECT_NAME='bobo'):
        command.handle(template_name='import_me', basepath='/tmp')

    # assert 'import_site' command is run
    assert mocked_call_command.mock_calls == [mock.call('import_site', '/tmp/import_me.json')]


@mock.patch('hobo.agent.common.management.commands.import_template.os.path.exists')
@mock.patch('hobo.agent.common.management.commands.import_template.call_command')
@mock.patch('hobo.agent.common.management.commands.import_template.get_commands')
def test_import_template_warning(mocked_get_commands, mocked_call_command, mocked_exists):
    """returns if template is not found"""
    mocked_get_commands.return_value = 'import_site'
    mocked_call_command.return_value = mock.MagicMock()
    mocked_exists.return_value = False

    # simulate:
    # $ XXX-manage import-template import_me
    command = Command()
    with override_settings(PROJECT_NAME='bobo'):
        with pytest.raises(UnknownTemplateError):
            command.handle(template_name='import_me')

    # assert 'import_site' command is not run
    assert mocked_call_command.mock_calls == []


@mock.patch('hobo.agent.common.management.commands.import_template.Command.handle')
def test_import_template_on_combo_agent(mocked_super):
    """overloaded import-template for combo"""
    command = load_command_class('hobo.agent.combo', 'import_template')

    # template specified and found
    command.handle(template_name='my-template')
    assert mocked_super.mock_calls == [mock.call(template_name='my-template')]

    # do nothing on internal templates
    mocked_super.side_effect = UnknownTemplateError
    for template in 'portal-user', 'portal-agent', 'x-portal-user-x', 'x-portal-agent-x':
        command.handle(template_name=template)

    # template no found
    with pytest.raises(UnknownTemplateError):
        command.handle(template_name='unknown-template')
