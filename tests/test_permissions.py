import json

import pytest
import responses
import rest_framework.permissions
from django.contrib.auth.models import Group, User
from django.db import connection, models
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.template import engines
from django.urls import path
from rest_framework.authentication import SessionAuthentication
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.permissions import SAFE_METHODS, BasePermission
from rest_framework.views import APIView

from hobo.agent.common.models import Role
from hobo.rest_authentication import APIClientAuthentication
from hobo.rest_permissions import IsAdminUser, IsAPIClient, PermissionContextError, role_permission
from hobo.urls import urlpatterns as urlpatterns_orig

from .test_manager import login


@pytest.fixture(scope='module')
def FakeCollectionModel(django_db_setup, django_db_blocker):
    Meta = type('Meta', (type,), {'app_label': 'hobo'})
    attrs = {
        'name': models.CharField(max_length=16),
        'objects': models.Manager(),
        'Meta': Meta,
        '__module__': 'hobo.models',
    }

    FakeModel_cls = type('FakeCollectionModel', (models.Model,), attrs)
    django_db_blocker.unblock()
    with connection.schema_editor() as schema_editor:
        schema_editor.create_model(FakeModel_cls)
        yield FakeModel_cls
        schema_editor.delete_model(FakeModel_cls)


@pytest.fixture(scope='module')
def FakeModel(django_db_setup, django_db_blocker, FakeCollectionModel):
    Meta = type('Meta', (type,), {'app_label': 'hobo', 'default_related_name': 'fake'})
    attrs = {
        'name': models.CharField(max_length=16),
        'admin_role': models.ForeignKey(
            Group, blank=True, null=True, default=None, on_delete=models.SET_NULL
        ),
        'edit_role': models.ForeignKey(Group, blank=True, null=True, default=None, on_delete=models.SET_NULL),
        'view_role': models.ForeignKey(Group, blank=True, null=True, default=None, on_delete=models.SET_NULL),
        'collection': models.ForeignKey(
            FakeCollectionModel, blank=True, null=True, default=None, on_delete=models.SET_NULL
        ),
        'objects': models.Manager(),
        'Meta': Meta,
        '__module__': 'hobo.models',
    }

    FakeModel_cls = type('FakeModel', (models.Model,), attrs)
    django_db_blocker.unblock()
    with connection.schema_editor() as schema_editor:
        schema_editor.create_model(FakeModel_cls)
        yield FakeModel_cls
        schema_editor.delete_model(FakeModel_cls)


# View allowing to test IsAdminUser
@api_view(['GET'])
@authentication_classes([SessionAuthentication, APIClientAuthentication])
@permission_classes([IsAdminUser])
def fake_view_apiclient_admin(request, format=None):
    return rest_framework.response.Response({'err': 0})


@api_view(['GET'])
@authentication_classes([SessionAuthentication, APIClientAuthentication])
@permission_classes([rest_framework.permissions.IsAuthenticated])
def fake_view_apiclient_auth(request, format=None):
    return rest_framework.response.Response({'err': 0})


@api_view(['GET'])
@authentication_classes([SessionAuthentication, APIClientAuthentication])
@permission_classes(
    [(IsAPIClient & IsAdminUser) | ((~IsAPIClient) & rest_framework.permissions.IsAuthenticated)]
)
def fake_view_apiclient_admin_or_auth(request, format=None):
    return rest_framework.response.Response({'err': 0})


# class & view for testing the or combination given as exemple in
# https://www.django-rest-framework.org/api-guide/permissions/#setting-the-permission-policy
class ReadOnly(BasePermission):
    def has_permission(self, request, view):
        return request.method in SAFE_METHODS


@api_view(['GET', 'POST'])
@authentication_classes([SessionAuthentication, APIClientAuthentication])
@permission_classes([IsAdminUser | ReadOnly])
def fake_getpost(request):
    # When using SessionAuthentication there is no way (?) to exempt_csrf token
    if request.method == 'POST':
        return rest_framework.response.Response({'err': 0})

    # generating form with csrf token on GET
    django_engine = engines['django']
    template = django_engine.from_string(
        '<form method="post" action="{% url "fakeform" %}">{% csrf_token %}</form>'
    )
    return HttpResponse(template.render({}, request))


@pytest.fixture
def model_view(FakeModel):
    class FakeView(APIView):
        authentication_classes = [SessionAuthentication, APIClientAuthentication]
        permission_classes = (
            (IsAPIClient & role_permission('edit_role'))
            | (~IsAPIClient & rest_framework.permissions.IsAuthenticated),
        )

        def get_object(self):
            obj = get_object_or_404(FakeModel, id=self.kwargs['identifier'])
            self.check_object_permissions(self.request, obj)
            return obj

        def get(self, request, identifier):
            obj = self.get_object()
            return rest_framework.response.Response({'err': 0, 'name': obj.name})

    return FakeView.as_view()


@pytest.fixture
def model_role_view(FakeModel):
    class FakeView(APIView):
        authentication_classes = [SessionAuthentication, APIClientAuthentication]
        permission_classes = (role_permission('edit_role') | IsAdminUser,)

        def get_object(self):
            obj = get_object_or_404(FakeModel, id=self.kwargs['identifier'])
            self.check_object_permissions(self.request, obj)
            return obj

        def get(self, request, identifier):
            obj = self.get_object()
            return rest_framework.response.Response({'err': 0, 'name': obj.name})

    return FakeView.as_view()


@pytest.fixture
def collection_view(FakeModel, FakeCollectionModel):
    def fake_from_collection(collec):
        return FakeModel.objects.filter(collection=collec).all()

    class FakeCollectionView(APIView):
        authentication_classes = [SessionAuthentication, APIClientAuthentication]
        permission_classes = (
            (IsAPIClient & role_permission('edit_role', fake_from_collection))
            | (~IsAPIClient & rest_framework.permissions.IsAuthenticated),
        )

        def get_object(self):
            obj = get_object_or_404(FakeCollectionModel, id=self.kwargs['identifier'])
            self.check_object_permissions(self.request, obj)
            return obj

        def get(self, request, identifier):
            obj = self.get_object()
            return rest_framework.response.Response({'err': 0, 'names': [elt.name for elt in obj.fake.all()]})

    return FakeCollectionView.as_view()


# Setup fake URLs
A2_URL = 'https://idp.example.org/'
A2_CHECK_APICLIENT_URL = A2_URL + 'api/check-api-client/'
IS_ADMIN_URL = 'TESTURL/is_admin'
IS_AUTH_URL = 'TESTURL/is_auth'
IS_API_ADMIN_OR_AUTH = 'TESTURL/apiadmin_or_auth'
IS_AUTH_OR_RO_URL = 'TESTURL/ro'
MODEL_VIEW_URL = 'TESTURL/modelview'
MODEL_ROLE_VIEW_URL = 'TESTURL/modelroleview'
COLLECTION_VIEW_URL = 'TESTURL/collecview'


class TestURLs:
    urlpatterns = urlpatterns_orig + [
        path(IS_ADMIN_URL, fake_view_apiclient_admin),
        path(IS_AUTH_URL, fake_view_apiclient_auth),
        path(IS_API_ADMIN_OR_AUTH, fake_view_apiclient_admin_or_auth),
        path(IS_AUTH_OR_RO_URL, fake_getpost, name='fakeform'),
    ]


def make_service(slug, this=False, service_type='chrono', ou_slug=None):
    service = {'title': slug.title(), 'url': f'https://{ou_slug}.example.org', 'secret': 'xxx'}
    if this:
        service['this'] = True
    if ou_slug:
        service['variables'] = {'ou-slug': ou_slug}
    return (service_type, {slug: service})


@pytest.fixture
def is_admin_url(request):
    return '/%s' % IS_ADMIN_URL


@pytest.fixture
def is_auth_url(request):
    return '/%s' % IS_AUTH_URL


@pytest.fixture
def is_admin_or_ro_url():
    return '/%s' % IS_AUTH_OR_RO_URL


@pytest.fixture
def is_apiadmin_or_auth():
    return '/%s' % IS_API_ADMIN_OR_AUTH


@pytest.fixture
def model_view_url():
    def _generate(model_id):
        return '/%s/%d/' % (MODEL_VIEW_URL, model_id)

    return _generate


@pytest.fixture
def model_role_view_url():
    def _generate(model_id):
        return '/%s/%d/' % (MODEL_ROLE_VIEW_URL, model_id)

    return _generate


@pytest.fixture
def collection_view_url():
    def _generate(model_id):
        return '/%s/%d/' % (COLLECTION_VIEW_URL, model_id)

    return _generate


@pytest.fixture
def setup_services(settings):
    def _setup_services(extra_services=None):
        SERVICES = {
            'authentic': {
                'idp': {
                    'title': 'a2',
                    'url': A2_URL,
                    'orig': 'auth.example.org',
                    'secret': 'xxx',
                },
            },
            'wcs': {
                'foo': {
                    'title': 'some title',
                    'url': 'https://wcs.example.org/',
                    'orig': 'example.org',
                    'secret': 'xxx',
                },
            },
        }
        for service_type, service in extra_services or []:
            if service_type not in SERVICES:
                SERVICES[service_type] = {}
            SERVICES[service_type].update(service)
        settings.KNOWN_SERVICES = SERVICES

    return _setup_services


@responses.activate
@pytest.fixture
def make_api_client():
    def _make_api_client(identifier, password, service_superuser=None, roles=None):
        roles = roles if roles else []
        payload = {'identifier': identifier, 'password': password, 'ip': '127.0.0.1'}
        data = {
            'is_active': True,
            'is_anonymous': False,
            'is_authenticated': True,
            'is_superuser': False,
            'roles': [role.uuid for role in roles],
            'service_superuser': service_superuser,
        }
        responses.reset()
        responses.post(
            A2_CHECK_APICLIENT_URL,
            match=[responses.matchers.json_params_matcher(payload)],
            body=json.dumps({'err': 0, 'data': data}),
        )
        return ('Basic', (identifier, password))

    return _make_api_client


@pytest.fixture
def fake_views(settings, model_view, model_role_view, collection_view):
    url_with_arg = '%s/<int:identifier>/' % MODEL_VIEW_URL
    TestURLs.urlpatterns.append(path(url_with_arg, model_view))

    url_with_arg = '%s/<int:identifier>/' % MODEL_ROLE_VIEW_URL
    TestURLs.urlpatterns.append(path(url_with_arg, model_role_view))

    url_with_arg = '%s/<int:identifier>/' % COLLECTION_VIEW_URL
    TestURLs.urlpatterns.append(path(url_with_arg, collection_view))

    settings.ROOT_URLCONF = TestURLs
    return settings


@responses.activate
@pytest.fixture
def app_auth(app, make_api_client):
    def _app_auth(identifier='foo', password='bar', service_superuser=None, roles=None):
        if identifier:
            auth = make_api_client(identifier, password, service_superuser, roles)
            app.authorization = auth
        return app

    return _app_auth


@responses.activate
def test_permissions_superuser_or_auth(
    admin_user,
    app_auth,
    fake_views,
    setup_services,
    is_admin_url,
    is_auth_url,
    is_admin_or_ro_url,
    is_apiadmin_or_auth,
):
    service_slug = 'agenda'
    agenda = make_service(service_slug, this=True)
    setup_services(extra_services=[agenda])

    # not authenticated
    app = app_auth(None)
    app.get(is_admin_url, status=403)
    app.get(is_auth_url, status=403)

    app.get(is_admin_or_ro_url)
    app.post(is_admin_or_ro_url, status=403)

    app.get(is_apiadmin_or_auth, status=403)

    # authenticated without service superuser information
    app = app_auth()
    app.get(is_admin_url, status=403)
    app.get(is_auth_url)

    app.get(is_admin_or_ro_url)
    app.post(is_admin_or_ro_url, status=403)

    app.get(is_apiadmin_or_auth, status=403)

    # authenticated admin on service
    app = app_auth(service_superuser={'dummy_ou': {service_slug: True}})
    app.get(is_admin_url)
    app.get(is_auth_url)

    app.get(is_admin_or_ro_url)
    app.post(is_admin_or_ro_url)

    app.get(is_apiadmin_or_auth)

    # not admin on service
    app = app_auth(service_superuser={'dummy_ou': {service_slug: False}})
    app.get(is_admin_url, status=403)
    app.get(is_auth_url)

    app.get(is_admin_or_ro_url)
    app.post(is_admin_or_ro_url, status=403)

    app.get(is_apiadmin_or_auth, status=403)

    # testing fallback on standard auth/permissions with non APIClient user
    app.authorization = None
    app = login(app)
    app.get(is_admin_url)
    app.get(is_auth_url)

    app.get(is_apiadmin_or_auth)

    resp = app.get(is_admin_or_ro_url)
    # using session authentication, we need a csrf token to post
    resp.form.submit()

    app.get('/logout/')
    # creating an unprivilegied user
    User.objects.create_user('foo', email=None, password='bar', is_staff=False)
    app = login(app, 'foo', 'bar')

    app.get(is_auth_url)
    app.get(is_admin_url, status=403)
    app.get(is_apiadmin_or_auth, status=200)


@responses.activate
def test_permissions_multi_ou(admin_user, app_auth, fake_views, setup_services, is_admin_url, is_auth_url):
    service_slug = 'agenda'
    agenda = make_service(service_slug, ou_slug='dummy_ou')
    agenda2 = make_service(service_slug, this=True, ou_slug='ou1')
    setup_services(extra_services=[agenda, agenda2])
    # admin in another OU
    app = app_auth(service_superuser={'dummy_ou': {service_slug: True}, 'ou1': {service_slug: False}})
    app.get(is_admin_url, status=403)

    # missing OU superuser information
    app = app_auth(service_superuser={'dummy_ou': {service_slug: True}})
    app.get(is_admin_url, status=403)

    # authenticated admin on service
    app = app_auth(service_superuser={'dummy_ou': {service_slug: False}, 'ou1': {service_slug: True}})
    app.get(is_admin_url)

    # testing fallback on standard auth/permissions with non APIClient user
    app.authorization = None
    app = login(app)
    app.get(is_admin_url)
    app.get(is_auth_url)


@responses.activate
def test_permissions_ou_missconfiguration(
    admin_user, app_auth, fake_views, setup_services, is_admin_url, is_auth_url
):
    # no OU defined for service but multiple OUs in apiclient permissions
    service_slug = 'agenda1'
    agenda = make_service(service_slug, this=True)
    setup_services(extra_services=[agenda])
    app = app_auth(service_superuser={'dummy_ou': {service_slug: False}, 'ou1': {service_slug: True}})
    app.get(is_admin_url, status=403)
    app.get(is_auth_url)

    # testing fallback on standard auth/permissions with non APIClient user
    app.authorization = None
    app = login(app)
    app.get(is_admin_url)
    app.get(is_auth_url)


@responses.activate
def test_permissions_no_active_service(
    admin_user, app_auth, fake_views, setup_services, is_admin_url, is_auth_url
):
    service_slug = 'agenda1'
    agenda = make_service(service_slug)
    setup_services(extra_services=[agenda])
    app = app_auth(service_superuser={'dummy_ou': {service_slug: True}})
    app.get(is_admin_url, status=403)
    app.get(is_auth_url)

    # testing fallback on standard auth/permissions with non APIClient user
    app.authorization = None
    app = login(app)
    app.get(is_admin_url)
    app.get(is_auth_url)


@responses.activate
def test_role_permissions(
    admin_user,
    app,
    app_auth,
    fake_views,
    setup_services,
    FakeModel,
    FakeCollectionModel,
    model_view_url,
    model_role_view_url,
    collection_view_url,
):
    admin_role = Role.objects.create(uuid='1234', name='admin', slug='admin')
    edit_role = Role.objects.create(uuid='12345', name='edit', slug='edit')
    view_role = Role.objects.create(uuid='123456', name='view', slug='view')

    fake_obj1 = FakeModel.objects.create(
        name='foo1', edit_role=edit_role, admin_role=admin_role, view_role=view_role
    )
    fake_obj2 = FakeModel.objects.create(
        name='foo2', edit_role=admin_role, admin_role=admin_role, view_role=admin_role
    )

    fake_obj3 = FakeModel.objects.create(
        name='foo3', edit_role=edit_role, admin_role=admin_role, view_role=view_role
    )

    collec1 = FakeCollectionModel.objects.create(name='c1')
    collec2 = FakeCollectionModel.objects.create(name='c1')
    for fake in (fake_obj1, fake_obj3):
        fake.collection = collec1
        fake.save()
    for fake in (fake_obj1, fake_obj2, fake_obj3):
        fake.collection = collec2
        fake.save()

    service_slug = 'foobar'
    service = make_service(service_slug, this=True)
    setup_services(extra_services=[service])

    # not logged in
    app.get(model_view_url(fake_obj1.id), status=403)
    app.get(model_view_url(fake_obj3.id), status=403)
    app.get(model_view_url(fake_obj2.id), status=403)

    app.get(model_role_view_url(fake_obj1.id), status=403)
    app.get(model_role_view_url(fake_obj3.id), status=403)
    app.get(model_role_view_url(fake_obj2.id), status=403)

    app.get(collection_view_url(collec1.id), status=403)
    app.get(collection_view_url(collec2.id), status=403)

    # APIClient with edit_role
    app = app_auth(roles=[edit_role])
    app.get(model_view_url(fake_obj1.id))
    app.get(model_view_url(fake_obj3.id))
    app.get(model_view_url(fake_obj2.id), status=403)

    app.get(model_role_view_url(fake_obj1.id))
    app.get(model_role_view_url(fake_obj3.id))
    app.get(model_role_view_url(fake_obj2.id), status=403)

    app.get(collection_view_url(collec1.id))
    app.get(collection_view_url(collec2.id), status=403)
    app.get('/logout/')

    # Simple user without any role
    User.objects.create_user('simple_user', email=None, password='passwd', is_staff=False)
    app = login(app, 'simple_user', 'passwd')

    app.get(model_view_url(fake_obj1.id))
    app.get(model_view_url(fake_obj3.id))
    app.get(model_view_url(fake_obj2.id))

    app.get(model_role_view_url(fake_obj1.id), status=403)
    app.get(model_role_view_url(fake_obj3.id), status=403)
    app.get(model_role_view_url(fake_obj2.id), status=403)

    app.get(collection_view_url(collec1.id))
    app.get(collection_view_url(collec2.id))
    app.get('/logout/')

    # testing accesses with admin user
    app = login(app)

    app.get(model_view_url(fake_obj1.id))
    app.get(model_view_url(fake_obj3.id))
    app.get(model_view_url(fake_obj2.id))

    app.get(model_role_view_url(fake_obj1.id))
    app.get(model_role_view_url(fake_obj3.id))
    app.get(model_role_view_url(fake_obj2.id))

    app.get(collection_view_url(collec1.id))
    app.get(collection_view_url(collec2.id))
    app.get('/logout/')


def test_bad_role_permission(FakeModel):
    fake_obj1 = FakeModel.objects.create(name='foo1', edit_role=None, admin_role=None, view_role=None)

    perm = role_permission('foobar')()
    with pytest.raises(PermissionContextError):
        perm.has_object_permission(None, None, fake_obj1)
