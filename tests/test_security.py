# hobo - portal to configure and deploy applications
# Copyright (C) Entr'ouvert

import unittest.mock

import pytest
from django.core.cache import cache
from django.core.exceptions import ValidationError

from hobo.environment.models import Variable
from hobo.environment.utils import get_setting_variable
from hobo.security.forms import HEADS, validate_csp
from hobo.security.models import CspReport
from hobo.security.utils import common_domains

from .test_manager import login

CSP_HEADER_NAME = 'Content-Security-Policy'
CSP_REPORT_ONLY_HEADER_NAME = 'Content-Security-Policy-Report-Only'

POLICY = "default-src *.example.com 'unsafe-eval' 'unsafe-inline'"
POLICY2 = "default-src https://*.example.com 'unsafe-eval' 'unsafe-inline'"

# Copied from https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy-Report-Only
CSP_REPORT = {
    'csp-report': {
        'blocked-uri': 'http://example.com/css/style.css',
        'disposition': 'report',
        'document-uri': 'http://example.com/signup.html',
        'effective-directive': 'style-src-elem',
        'original-policy': "default-src 'none'; style-src cdn.example.com; report-uri /_/csp-reports",
        'referrer': '',
        'status-code': 200,
        'violated-directive': 'style-src-elem',
    }
}

CSP_REPORT_VERY_LONG = {
    'csp-report': {
        'blocked-uri': 'http://example.com/foo/bar/?id=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
        'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
        'disposition': 'report',
        'document-uri': 'http://example.com/signup.html',
        'effective-directive': 'style-src-elem',
        'original-policy': "default-src 'none'; style-src cdn.example.com; report-uri /_/csp-reports",
        'referrer': '',
        'status-code': 200,
        'violated-directive': 'style-src-elem',
    }
}

CSP_REPORT_INLINE = {
    'csp-report': {
        'blocked-uri': 'inline',
        'disposition': 'report',
        'document-uri': 'http://example.com/signup.html',
        'effective-directive': 'style-src-elem',
        'original-policy': "default-src 'none'; style-src cdn.example.com; report-uri /_/csp-reports",
        'referrer': '',
        'status-code': 200,
        'violated-directive': 'style-src-elem',
    }
}


def test_forms_validate_bad_csp():
    with pytest.raises(ValidationError, match=r'Invalid CSP directive'):
        validate_csp('truc muche')
    with pytest.raises(ValidationError, match=r'Invalid CSP source'):
        validate_csp('default-src zob')


SOURCE_EXAMPLES = [
    "'self'",
    "'unsafe-eval'",
    "'unsafe-inline'",
    "'none'",
    'data:',
    'https://*.example.com',
    'mail.example.com:443',
    'https://store.example.com',
    '*.example.com',
    'https://*.example.com:12/path/to/file.js',
    'https://example.com/subdirectory',
    'https://example.com/subdirectory/',
]


@pytest.mark.parametrize('directive', HEADS)
@pytest.mark.parametrize('source', SOURCE_EXAMPLES)
def test_forms_validate_good_csp(directive, source):
    validate_csp(f'{directive} \'self\' {source} data:')


def test_middleware(app, settings):
    resp = app.get('/')
    assert CSP_HEADER_NAME not in resp.headers
    assert CSP_REPORT_ONLY_HEADER_NAME not in resp.headers

    settings.CONTENT_SECURITY_POLICY = POLICY
    resp = app.get('/')
    assert CSP_HEADER_NAME in resp.headers
    assert CSP_REPORT_ONLY_HEADER_NAME not in resp.headers
    assert resp.headers[CSP_HEADER_NAME] == POLICY

    settings.CONTENT_SECURITY_POLICY_REPORT_ONLY = POLICY
    resp = app.get('/')
    assert CSP_HEADER_NAME in resp.headers
    assert CSP_REPORT_ONLY_HEADER_NAME in resp.headers
    assert resp.headers[CSP_HEADER_NAME] == POLICY
    assert resp.headers[CSP_REPORT_ONLY_HEADER_NAME] == POLICY

    settings.CONTENT_SECURITY_POLICY_REPORT_URI = 'https://csp.example.com/csp-report/'
    resp = app.get('/')
    assert CSP_HEADER_NAME in resp.headers
    assert CSP_REPORT_ONLY_HEADER_NAME in resp.headers
    assert resp.headers[CSP_HEADER_NAME] == POLICY + '; report-uri https://csp.example.com/csp-report/?error'
    assert (
        resp.headers[CSP_REPORT_ONLY_HEADER_NAME]
        == POLICY + '; report-uri https://csp.example.com/csp-report/'
    )

    settings.INTERNAL_IPS = ['1.1.1.1']
    resp = app.get('/')
    assert 'report-uri' not in resp.headers[CSP_HEADER_NAME]
    assert 'report-uri' not in resp.headers[CSP_REPORT_ONLY_HEADER_NAME]


def test_manage(app, admin_user, settings):
    assert Variable.objects.filter(name='SETTING_CONTENT_SECURITY_POLICY').count() == 0
    assert Variable.objects.filter(name='SETTING_CONTENT_SECURITY_POLICY_REPORT_ONLY').count() == 0

    login(app)
    resp = app.get('/')
    resp = resp.click('Security')

    form = resp.forms['setting-form']
    form.set('content_security_policy_report', True)
    form.set('content_security_policy', POLICY)
    form.set('content_security_policy_report_only', POLICY2)
    resp = form.submit()

    assert 'errorlist' not in resp
    resp = resp.follow()

    assert Variable.objects.filter(name='SETTING_CONTENT_SECURITY_POLICY').get().value == POLICY
    assert Variable.objects.filter(name='SETTING_CONTENT_SECURITY_POLICY_REPORT_ONLY').get().value == POLICY2
    assert Variable.objects.filter(name='SETTING_CONTENT_SECURITY_POLICY_REPORT_URI').get().value == (
        'http://testserver/api/csp-report/'
    )

    form = resp.forms['setting-form']
    form.set('content_security_policy_report', False)
    resp = form.submit().follow()

    assert Variable.objects.filter(name='SETTING_CONTENT_SECURITY_POLICY').get().value == POLICY
    assert Variable.objects.filter(name='SETTING_CONTENT_SECURITY_POLICY_REPORT_ONLY').get().value == POLICY2
    assert Variable.objects.filter(name='SETTING_CONTENT_SECURITY_POLICY_REPORT_URI').get().value == ''

    form = resp.forms['setting-form']
    form.set('content_security_policy', '')
    form.set('content_security_policy_report_only', '')
    resp = form.submit().follow()

    assert Variable.objects.filter(name='SETTING_CONTENT_SECURITY_POLICY').get().value == ''
    assert Variable.objects.filter(name='SETTING_CONTENT_SECURITY_POLICY_REPORT_ONLY').get().value == ''
    assert Variable.objects.filter(name='SETTING_CONTENT_SECURITY_POLICY_REPORT_URI').get().value == ''


def test_report_table(app, admin_user, settings, freezer):
    freezer.move_to('2024-01-17 10:00:00+01:00')

    CspReport.record_from_report(CSP_REPORT)
    CspReport.record_from_report(CSP_REPORT_VERY_LONG)
    CspReport.record_from_report(CSP_REPORT_INLINE, error=True)

    login(app)
    resp = app.get('/security/')
    rows = resp.pyquery.items('tr')
    assert [[cell.text() for cell in row.items('td,th')] for row in rows] == [
        ['Last seen', 'First seen', 'Source', 'Violated directive'],
        [
            'Jan. 17, 2024, 9 a.m.',
            'Jan. 17, 2024, 9 a.m.',
            'http://example.com/css/style.css',
            'style-src-elem',
        ],
        [
            'Jan. 17, 2024, 9 a.m.',
            'Jan. 17, 2024, 9 a.m.',
            'http://example.com/foo/bar/?id=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx…',
            'style-src-elem',
        ],
        [
            'Jan. 17, 2024, 9 a.m.',
            'Jan. 17, 2024, 9 a.m.',
            'Inline: http://example.com/signup.html Line: 0 Column: 0 ⚠',
            'style-src-elem',
        ],
    ]

    resp = resp.forms['csp-reports-form'].submit(name='clean-reports').follow()
    rows = resp.pyquery.items('tr')
    assert [[cell.text() for cell in row.items('td,th')] for row in rows][1:] == []


def test_api_report(app, db, caplog, freezer):
    caplog.set_level('WARNING')
    assert app.post_json('/api/csp-report/', params=CSP_REPORT, status=400).text == 'no CSP'

    get_setting_variable('CONTENT_SECURITY_POLICY').update_value(POLICY)
    assert app.post_json('/api/csp-report/', params=CSP_REPORT, status=400).text == 'no CSP'

    get_setting_variable('CONTENT_SECURITY_POLICY_REPORT_URI').update_value('https://foobar')

    assert app.post('/api/csp-report/', params=b'', status=400).text == 'invalid json'
    caplog.clear()

    assert app.post('/api/csp-report/', params=b'{}', status=400).text == 'invalid json'
    caplog.clear()

    app.post_json('/api/csp-report/', params=CSP_REPORT, status=200)
    assert CspReport.objects.get().content == CSP_REPORT['csp-report']
    assert not caplog.records

    app.post_json('/api/csp-report/?error', params=CSP_REPORT, status=200)
    assert caplog.messages == [
        'testserver: CSP reports blocked asset http://example.com/css/style.css by directive style-src-elem'
    ]

    cache.clear()

    # set too-much-reports condition
    with unittest.mock.patch('hobo.security.models.CspReport.objects.count', lambda *args: 1001):
        app.post_json('/api/csp-report/', params=CSP_REPORT, status=400)
    # too-much-reports is cached
    app.post_json('/api/csp-report/', params=CSP_REPORT, status=400)
    freezer.tick(61)
    # too-much-reports cache is expired
    app.post_json('/api/csp-report/', params=CSP_REPORT, status=200)
    freezer.tick(540)
    # update last_time
    app.post_json('/api/csp-report/', params=CSP_REPORT, status=200)


def test_utils_common_domain():
    assert common_domains(
        [
            'https://a.b.example.com',
            'https://b.example.com',
            'https://c.example.com',
        ]
    ) == ['https://*.example.com']

    assert set(
        common_domains(
            [
                'https://a.example.com',
                'https://b.example.com',
                'https://example.com',
            ]
        )
    ) == {'https://*.example.com', 'https://example.com'}
