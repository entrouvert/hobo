import json
import os

import pytest
from django.conf import UserSettingsHolder
from django.test import override_settings

from hobo.deploy.utils import get_hobo_json
from hobo.environment.models import Authentic, Combo
from hobo.multitenant.settings_loaders import Authentic as AuthenticLoader
from hobo.multitenant.settings_loaders import BackofficeLoginHint, TemplateVars
from hobo.profile.models import AttributeDefinition

pytestmark = pytest.mark.django_db


def test_authentic_update_settings_from_path(tmpdir):
    a = Authentic(title='bar', slug='bar', base_url='http://bar.example.net')
    a.save()

    env = get_hobo_json()
    fields = env['profile']['fields']
    assert [x['name'] for x in fields] == [
        'title',
        'first_name',
        'last_name',
        'email',
        'address',
        'zipcode',
        'city',
        'country',
        'birthdate',
        'phone',
    ]

    # swap title and phone fields
    title = AttributeDefinition.objects.get(name='title')
    phone = AttributeDefinition.objects.get(name='phone')
    (title.order, phone.order) = (phone.order, title.order)
    title.save()
    phone.save()

    env = get_hobo_json()
    fields = env['profile']['fields']
    assert [x['name'] for x in fields] == [
        'phone',
        'first_name',
        'last_name',
        'email',
        'address',
        'zipcode',
        'city',
        'country',
        'birthdate',
        'title',
    ]

    assert [x['name'] for x in fields if x['disabled']] == ['country', 'birthdate', 'title']
    profile_fields = [x['name'] for x in fields if not x['disabled']]
    assert profile_fields == [
        'phone',
        'first_name',
        'last_name',
        'email',
        'address',
        'zipcode',
        'city',
    ]

    # serialize hobo.json
    path = os.path.join(str(tmpdir), 'hobo.json')
    with open(path, 'w') as fd:
        json.dump(env, fd)

    # call settings loaders
    tenant_settings = UserSettingsHolder({})
    assert not getattr(tenant_settings, 'A2_PROFILE_FIELDS', False)
    assert not getattr(tenant_settings, 'A2_REQUIRED_FIELDS', False)
    assert not getattr(tenant_settings, 'A2_REGISTRATION_FIELDS', False)
    loader = AuthenticLoader()
    loader.update_settings_from_path(tenant_settings, path)
    assert tenant_settings.A2_PROFILE_FIELDS == profile_fields
    assert tenant_settings.A2_REQUIRED_FIELDS == ['first_name', 'last_name', 'email']
    assert tenant_settings.A2_REGISTRATION_FIELDS == ['first_name', 'last_name']


def test_mellon_backoffice_login_hint_setting_from_path(tmpdir):
    c = Combo(title='combo', slug='portal', base_url='http://portal.example.net')
    c.save()

    env = get_hobo_json()
    # simulate hobo.json for this instance
    env['services'][0]['this'] = True

    path = os.path.join(str(tmpdir), 'hobo.json')
    with open(path, 'w') as fd:
        json.dump(env, fd)

    # call settings loaders
    tenant_settings = UserSettingsHolder({})
    assert not getattr(tenant_settings, 'MELLON_LOGIN_HINTS', False)

    loader = BackofficeLoginHint()
    loader.update_settings_from_path(tenant_settings, path)

    assert tenant_settings.MELLON_LOGIN_HINTS == ['backoffice']


def test_mellon_always_backoffice_login_hint_setting_from_path(tmpdir):
    c = Combo(
        title='combo', slug='portal-agent', base_url='http://agent.example.net', template_name='portal-agent'
    )
    c.save()

    env = get_hobo_json()
    # simulate hobo.json for this instance
    env['services'][0]['this'] = True

    path = os.path.join(str(tmpdir), 'hobo.json')
    with open(path, 'w') as fd:
        json.dump(env, fd)

    # call settings loaders
    tenant_settings = UserSettingsHolder({})
    assert not getattr(tenant_settings, 'MELLON_LOGIN_HINTS', False)

    loader = BackofficeLoginHint()
    loader.update_settings_from_path(tenant_settings, path)
    assert tenant_settings.MELLON_LOGIN_HINTS == ['always_backoffice']


def test_email_update_settings_from_path(tmpdir):
    tenant_settings = UserSettingsHolder({})
    loader = TemplateVars()

    def update_settings(env):
        path = os.path.join(str(tmpdir), 'hobo.json')
        with open(path, 'w') as fd:
            json.dump(env, fd)
        loader.update_settings_from_path(tenant_settings, path)

    tenant_settings.DEFAULT_FROM_EMAIL = 'webmaster@hobo.example.org'
    env = {'services': [], 'variables': {}}
    update_settings(env)
    assert tenant_settings.DEFAULT_FROM_EMAIL == 'webmaster@hobo.example.org'
    env['variables']['default_from_email'] = 'john.doe@example.com'
    update_settings(env)
    assert tenant_settings.DEFAULT_FROM_EMAIL == 'john.doe@example.com'
    env['variables']['global_title'] = 'Publik'
    update_settings(env)
    assert tenant_settings.DEFAULT_FROM_EMAIL == '"Publik" <john.doe@example.com>'
    env['variables']['email_sender_name'] = 'Jhon Doe'
    update_settings(env)
    assert tenant_settings.DEFAULT_FROM_EMAIL == '"Jhon Doe" <john.doe@example.com>'
    env['variables']['default_from_email'] = 'foo@example.com'
    update_settings(env)
    assert tenant_settings.DEFAULT_FROM_EMAIL == '"Jhon Doe" <foo@example.com>'

    # default_from_email needed for '"foo" <bar.example.com>' address format
    tenant_settings.DEFAULT_FROM_EMAIL = '<webmaster@hobo.example.org>'
    update_settings({'services': [], 'variables': {'global_title': 'Publik'}})
    assert tenant_settings.DEFAULT_FROM_EMAIL == '<webmaster@hobo.example.org>'


def test_sms_update_settings_from_path(tmpdir):
    tenant_settings = UserSettingsHolder({})
    loader = TemplateVars()

    variables = {
        'sms_url': 'https://example.com/send/',
        'sms_sender': 'Sender',
        'local_country_code': '262',
        'phone_country_codes': {
            '262': {'region': 'RE', 'region_desc': 'Réunion'},
            '666': {'region': 'WLD', 'region_desc': 'Wonderland'},
        },
    }
    env = {'services': [], 'variables': variables}
    path = os.path.join(str(tmpdir), 'hobo.json')
    with open(path, 'w') as fd:
        json.dump(env, fd)
    loader.update_settings_from_path(tenant_settings, path)
    assert tenant_settings.SMS_URL == 'https://example.com/send/'
    assert tenant_settings.SMS_SENDER == 'Sender'
    assert tenant_settings.LOCAL_COUNTRY_CODE == '262'
    assert tenant_settings.PHONE_COUNTRY_CODES == {
        '262': {'region': 'RE', 'region_desc': 'Réunion'},
        '666': {'region': 'WLD', 'region_desc': 'Wonderland'},
    }

    variables.pop('phone_country_codes')
    PHONE_COUNTRY_CODES = {
        '32': {'region': 'BE', 'region_desc': 'Belgium'},
        '777': {'region': 'WLD', 'region_desc': 'Wonderland'},
    }

    with override_settings(PHONE_COUNTRY_CODES=PHONE_COUNTRY_CODES):
        env = {'services': [], 'variables': variables}
        path = os.path.join(str(tmpdir), 'hobo.json')
        with open(path, 'w') as fd:
            json.dump(env, fd)
        loader.update_settings_from_path(tenant_settings, path)
        assert tenant_settings.PHONE_COUNTRY_CODES == {
            '32': {'region': 'BE', 'region_desc': 'Belgium'},
            '777': {'region': 'WLD', 'region_desc': 'Wonderland'},
        }
