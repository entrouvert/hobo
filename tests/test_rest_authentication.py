import pytest
import requests
import responses


@pytest.fixture
def settings_with_idp(settings):
    settings.ROOT_URLCONF = 'hobo.test_urls'
    settings.KNOWN_SERVICES = {
        'authentic': {
            'idp': {
                'title': 'Foobar',
                'url': 'https://idp.example.invalid/',
                'orig': 'example.org',
                'secret': 'xxx',
            }
        }
    }
    return settings


@pytest.fixture
def app_with_auth(app):
    app.authorization = ('Basic', ('foo', 'bar'))
    return app


def test_no_known_services(app_with_auth, db, settings):
    settings.ROOT_URLCONF = 'hobo.test_urls'
    app_with_auth.get('/authenticated-testview/', status=403)


def test_no_idp_in_known_services(app_with_auth, db, settings):
    settings.ROOT_URLCONF = 'hobo.test_urls'
    settings.KNOWN_SERVICES = {}
    app_with_auth.get('/authenticated-testview/', status=403)


def test_idp_connection_error(app_with_auth, db, settings_with_idp):
    with responses.RequestsMock() as rsps:
        rsps.post('https://idp.example.invalid/api/check-api-client/', status=403)
        app_with_auth.get('/authenticated-testview/', status=403)


def test_idp_timeout(app_with_auth, db, settings_with_idp):
    with responses.RequestsMock() as rsps:
        rsps.post('https://idp.example.invalid/api/check-api-client/', body=requests.Timeout('...'))
        resp = app_with_auth.get('/authenticated-testview/', status=503)
        assert resp.json == {'err': 1, 'err_desc': 'IDP temporarily unavailable, try again later.'}


def test_idp_unavailable_error(app_with_auth, db, settings_with_idp):
    with responses.RequestsMock() as rsps:
        rsps.post('https://idp.example.invalid/api/check-api-client/', body=requests.RequestException('...'))
        resp = app_with_auth.get('/authenticated-testview/', status=503)
        assert resp.json == {'err': 1, 'err_desc': 'IDP temporarily unavailable, try again later.'}


def test_idp_no_err_key(app_with_auth, db, settings_with_idp):
    with responses.RequestsMock() as rsps:
        rsps.post(
            'https://idp.example.invalid/api/check-api-client/',
            json={'foo': 'bar'},
            status=200,
        )
        app_with_auth.get('/authenticated-testview/', status=403)


def test_idp_app_error(app_with_auth, db, settings_with_idp):
    with responses.RequestsMock() as rsps:
        rsps.post(
            'https://idp.example.invalid/api/check-api-client/',
            json={'err': 1},
            status=200,
        )
        app_with_auth.get('/authenticated-testview/', status=403)


def test_idp_wrong_serialization(app_with_auth, db, settings_with_idp):
    with responses.RequestsMock() as rsps:
        rsps.post(
            'https://idp.example.invalid/api/check-api-client/',
            json={'err': 0, 'data': {'foo': 'bar'}},
            status=200,
        )
        app_with_auth.get('/authenticated-testview/', status=403)


def test_no_credentials(app, db, settings_with_idp):
    # test that the '/authenticated-testview/' really requires authentication,
    # otherwise all the others tests are meaningless.
    app.get('/authenticated-testview/', status=403)


@pytest.mark.parametrize('client_ip', ('127.0.0.1', '::1', '1.2.3.4', '2001:a:b:c:d::'))
def test_access_granted(app_with_auth, db, settings_with_idp, client_ip):
    expected_params = {'identifier': 'foo', 'password': 'bar', 'ip': client_ip}
    with responses.RequestsMock() as rsps:
        rsps.post(
            'https://idp.example.invalid/api/check-api-client/',
            json={
                'err': 0,
                'data': {
                    'is_active': True,
                    'is_anonymous': False,
                    'is_authenticated': True,
                    'is_superuser': False,
                    'restrict_to_anonymised_data': False,
                    'roles': [],
                },
            },
            match=[responses.matchers.json_params_matcher(expected_params)],
            status=200,
        )
        app_with_auth.get('/authenticated-testview/', extra_environ={'REMOTE_ADDR': client_ip})
