# hobo - © Entr'ouvert

from django.core.management import call_command

from hobo.deploy.signals import notify_agents
from hobo.environment.utils import get_setting_variable


def test_reset_debug_log(db, freezer, mocker):
    assert get_setting_variable('DEBUG_LOG').json is not True

    debug_log_variable = get_setting_variable('DEBUG_LOG')
    debug_log_variable.json = True
    debug_log_variable.save()
    notify_agents(None)

    send_to_celery = mocker.patch('hobo.deploy.signals.send_to_celery')

    freezer.tick(7199)
    call_command('cron', 'hourly')
    assert get_setting_variable('DEBUG_LOG').json is True
    assert send_to_celery.call_count == 0

    freezer.tick(2)
    call_command('cron', 'hourly')
    assert get_setting_variable('DEBUG_LOG').json is not True
    # check new environment is notified to agents
    assert send_to_celery.call_count == 1

    call_command('cron', 'hourly')
    assert get_setting_variable('DEBUG_LOG').json is not True
    assert send_to_celery.call_count == 1


def test_reset_debug_ips(db, freezer, mocker):
    assert get_setting_variable('INTERNAL_IPS.extend').json is not True

    debug_ips_variable = get_setting_variable('INTERNAL_IPS.extend')
    debug_ips_variable.json = ['1.2.3.4']
    debug_ips_variable.save()
    notify_agents(None)

    send_to_celery = mocker.patch('hobo.deploy.signals.send_to_celery')

    freezer.tick(7199)
    call_command('cron', 'hourly')
    assert get_setting_variable('INTERNAL_IPS.extend').json
    # check new environment is notified to agents
    assert send_to_celery.call_count == 0

    freezer.tick(2)
    call_command('cron', 'hourly')
    assert not get_setting_variable('INTERNAL_IPS.extend').json
    assert send_to_celery.call_count == 1

    call_command('cron', 'hourly')
    assert not get_setting_variable('INTERNAL_IPS.extend').json
    assert send_to_celery.call_count == 1
