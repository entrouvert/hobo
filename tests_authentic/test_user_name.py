from authentic2.models import Attribute
from django.contrib.auth import get_user_model
from django.test import override_settings

from hobo.user_name.apps import get_full_name

User = get_user_model()


def test_get_full_name_from_template_utils_from_multiple_attrs(tenant_ctx, settings):
    user = User.objects.create(
        first_name='Jane',
        last_name='Doe',
    )
    Attribute.objects.create(
        name='foo',
        label='Foo',
        kind='string',
        required=False,
        multiple=False,
        user_visible=True,
        user_editable=True,
    )
    Attribute.objects.create(
        name='nicknames',
        label='Nicknames',
        kind='string',
        required=False,
        multiple=True,
        user_visible=True,
        user_editable=True,
    )
    user.attributes.nicknames = ['Milly', 'Molly', 'Minnie']
    user.attributes.foo = 'bar'
    user.save()
    user.refresh_from_db()

    with override_settings(
        TEMPLATE_VARS={'user_full_name_template': '{{ user.first_name }} {{ user.attributes.foo }}'}
    ):
        assert get_full_name(user) == 'Jane bar'

    with override_settings(
        TEMPLATE_VARS={
            'user_full_name_template': '{{ user.first_name }} {{ user.attributes.nicknames.0 }} {{ user.attributes.nicknames.2 }}'
        }
    ):
        assert get_full_name(user) == 'Jane Milly Minnie'

    with override_settings(TEMPLATE_VARS={'user_full_name_template': ''}):
        assert get_full_name(user) == 'Jane Doe'


def test_get_full_name_from_template_accessor_from_multiple_attrs(tenant_ctx, settings):
    user = User.objects.create(
        first_name='Jane',
        last_name='Doe',
    )
    Attribute.objects.create(
        name='foo',
        label='Foo',
        kind='string',
        required=False,
        multiple=False,
        user_visible=True,
        user_editable=True,
    )
    Attribute.objects.create(
        name='nicknames',
        label='Nicknames',
        kind='string',
        required=False,
        multiple=True,
        user_visible=True,
        user_editable=True,
    )
    user.attributes.nicknames = ['Milly', 'Molly', 'Minnie']
    user.attributes.foo = 'bar'
    user.save()
    user.refresh_from_db()

    with override_settings(
        TEMPLATE_VARS={'user_full_name_template': '{{ user.first_name }} {{ user.attributes.foo }}'}
    ):
        assert user.get_full_name() == 'Jane bar'

    with override_settings(
        TEMPLATE_VARS={
            'user_full_name_template': '{{ user.first_name }} {{ user.attributes.nicknames.0 }} {{ user.attributes.nicknames.2 }}'
        }
    ):
        assert user.get_full_name() == 'Jane Milly Minnie'

    with override_settings(TEMPLATE_VARS={'user_full_name_template': ''}):
        assert user.get_full_name() == 'Jane Doe'
