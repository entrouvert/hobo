import builtins
import os
from unittest.mock import mock_open, patch

import hobo.test_utils

# Debian defaults
DEBUG = False

PROJECT_NAME = 'authentic2-multitenant'


open_backup = open
with patch.object(builtins, 'open', mock_open(read_data=b'xxx')):
    with open_backup(os.environ['DEBIAN_CONFIG_COMMON']) as fd:
        exec(fd.read())

# noqa pylint: disable=undefined-variable
DATABASES['default']['NAME'] = hobo.test_utils.get_safe_db_name()

# Avoid conflic with real tenants
# that might exist in /var/lib/authentic2_multitenant/tenants
TENANT_BASE = '/that/path/does/not/exist'


# Add the XForwardedForMiddleware
# noqa pylint: disable=used-before-assignment
MIDDLEWARE = ('authentic2.middleware.XForwardedForMiddleware',) + MIDDLEWARE

# Add authentic settings loader
# noqa pylint: disable=used-before-assignment
TENANT_SETTINGS_LOADERS = ('hobo.multitenant.settings_loaders.Authentic',) + TENANT_SETTINGS_LOADERS

# Add authentic2 hobo agent
# noqa pylint: disable=used-before-assignment
INSTALLED_APPS = ('hobo.agent.authentic2',) + INSTALLED_APPS

CACHES = {
    'default': {
        'BACKEND': 'hobo.multitenant.cache.TenantCache',
        'REAL_BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    }
}

HOBO_ROLE_EXPORT = True

SESSION_COOKIE_SECURE = False
CSRF_COOKIE_SECURE = False

LANGUAGE_CODE = 'en'
