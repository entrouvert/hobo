import os
from unittest.mock import ANY, call, patch

import lasso
import requests
from authentic2.a2_rbac.models import OrganizationalUnit, Role
from authentic2.a2_rbac.utils import get_default_ou
from authentic2.models import Attribute, AttributeValue
from authentic2.saml.models import LibertyProvider
from django.contrib.auth import get_user_model
from django.db import transaction
from tenant_schemas.utils import tenant_context

from hobo import signature

User = get_user_model()


def _api_role(role):
    val = {
        'description': role.description,
        'name': role.name,
        'ou': role.ou.slug,
        'service': None,
        'slug': role.slug,
        'uuid': role.uuid,
    }
    if role.service:
        val['service'] = {'name': role.service.name, 'ou': role.service.ou.slug, 'slug': role.service.slug}
    return val


def test_provision_role(tenant_ctx, caplog, notify_agents, settings, provisionning):
    LibertyProvider.objects.create(
        ou=get_default_ou(),
        name='wcs',
        slug='wcs',
        entity_id='http://more.example.net/metadata/',
        protocol_conformance=lasso.PROTOCOL_SAML_2_0,
    )
    LibertyProvider.objects.create(
        ou=get_default_ou(),
        name='whatever',
        slug='whatever',
        entity_id='http://provider.com',
        protocol_conformance=lasso.PROTOCOL_SAML_2_0,
    )
    with provisionning:
        role = Role.objects.create(name='coin', ou=get_default_ou())

    assert notify_agents.call_count == 1
    arg = notify_agents.call_args
    assert arg == call(ANY, sync=ANY)
    arg = arg[0][0]
    assert isinstance(arg, dict)
    assert set(arg.keys()) == {'audience', '@type', 'objects', 'full'}
    assert arg['audience'] == ['http://more.example.net/metadata/']
    assert arg['@type'] == 'provision'
    assert arg['full'] is False
    objects = arg['objects']
    assert isinstance(objects, dict)
    assert set(objects.keys()) == {'data', '@type'}
    assert objects['@type'] == 'role'
    data = objects['data']
    assert isinstance(data, list)
    assert len(data) == 1
    o = data[0]
    assert set(o.keys()) == {
        'details',
        'emails_to_members',
        'description',
        'uuid',
        'name',
        'slug',
        'emails',
    }
    assert o['details'] == ''
    assert o['emails_to_members'] is True
    assert o['emails'] == []

    notify_agents.reset_mock()
    emails = ['john.doe@example.com', 'toto@entrouvert.com']
    with provisionning:
        role.emails = emails
        role.emails_to_members = True
        role.save()

    assert notify_agents.call_count == 1
    arg = notify_agents.call_args
    assert arg == call(ANY, sync=ANY)
    arg = arg[0][0]
    assert isinstance(arg, dict)
    assert set(arg.keys()) == {'audience', '@type', 'objects', 'full'}
    assert arg['audience'] == ['http://more.example.net/metadata/']
    assert arg['@type'] == 'provision'
    assert arg['full'] is False
    objects = arg['objects']
    assert isinstance(objects, dict)
    assert set(objects.keys()) == {'data', '@type'}
    assert objects['@type'] == 'role'
    data = objects['data']
    assert isinstance(data, list)
    assert len(data) == 1
    o = data[0]
    assert set(o.keys()) == {
        'details',
        'emails_to_members',
        'description',
        'uuid',
        'name',
        'slug',
        'emails',
    }
    assert o['details'] == ''
    assert o['emails_to_members'] is True
    assert o['emails'] == emails

    notify_agents.reset_mock()
    with provisionning:
        role.delete()

    assert notify_agents.call_count == 1
    arg = notify_agents.call_args
    assert arg == call(ANY, sync=ANY)
    arg = arg[0][0]
    assert isinstance(arg, dict)
    assert set(arg.keys()) == {'audience', '@type', 'objects', 'full'}
    assert arg['audience'] == ['http://more.example.net/metadata/']
    assert arg['@type'] == 'deprovision'
    assert arg['full'] is False
    objects = arg['objects']
    assert isinstance(objects, dict)
    assert set(objects.keys()) == {'data', '@type'}
    assert objects['@type'] == 'role'
    data = objects['data']
    assert isinstance(data, list)
    assert len(data) == 1
    o = data[0]
    assert set(o.keys()) == {'uuid'}


def test_technical_role_unprovisionned(tenant_ctx, notify_agents, caplog, provisionning):
    LibertyProvider.objects.create(
        ou=get_default_ou(),
        name='provider',
        entity_id='http://provider.com',
        protocol_conformance=lasso.PROTOCOL_SAML_2_0,
    )
    with provisionning:
        role = Role.objects.create(slug='_a2-whatever', name='a2-whatever', ou=get_default_ou())
    assert notify_agents.call_count == 0

    notify_agents.reset_mock()
    emails = ['john.doe@example.com', 'toto@entrouvert.com']
    with provisionning:
        role.emails = emails
        role.emails_to_members = True
        role.save()
    assert notify_agents.call_count == 0

    notify_agents.reset_mock()
    with provisionning:
        role.delete()
    assert notify_agents.call_count == 0


def test_provision_user(tenant_ctx, tenant, notify_agents, caplog, provisionning):
    LibertyProvider.objects.create(
        slug='other',
        ou=get_default_ou(),
        name='Other',
        entity_id='http://other.example.net/metadata/',
        protocol_conformance=lasso.PROTOCOL_SAML_2_0,
    )
    wcs = LibertyProvider.objects.create(
        slug='more',
        ou=get_default_ou(),
        name='More',
        entity_id='http://more.example.net/metadata/',
        protocol_conformance=lasso.PROTOCOL_SAML_2_0,
    )
    service = LibertyProvider.objects.create(
        slug='combo',
        ou=get_default_ou(),
        name='Combo',
        entity_id='http://combo.example.net/metadata/',
        protocol_conformance=lasso.PROTOCOL_SAML_2_0,
    )
    role = Role.objects.create(name='coin', service=service, ou=get_default_ou(), is_superuser=True)
    role2 = Role.objects.create(name='zob', service=service, ou=get_default_ou(), emails=['zob@example.net'])
    child_role = Role.objects.create(name='child', ou=get_default_ou())
    notify_agents.reset_mock()
    attribute = Attribute.objects.create(label='Code postal', name='code_postal', kind='string')
    with provisionning:
        user1 = User.objects.create(
            username='Étienne',
            email='etienne.dugenou@example.net',
            first_name='Étienne',
            last_name='Dugenou',
            ou=get_default_ou(),
        )
        user2 = User.objects.create(
            username='john.doe',
            email='iohn.doe@example.net',
            first_name='John',
            last_name='Doe',
            is_active=False,
            ou=get_default_ou(),
        )
        role2.members.add(user2)
    users = {user.uuid: user for user in [user1, user2]}
    assert notify_agents.call_count == 2
    assert notify_agents.call_args_list[0][0][0]['objects']['@type'] == 'role'
    assert set(notify_agents.call_args_list[0][0][0]['audience']) == {
        'http://combo.example.net/metadata/',
        'http://more.example.net/metadata/',
        'http://other.example.net/metadata/',
    }
    arg = notify_agents.call_args
    assert arg == call(ANY, sync=ANY)
    arg = arg[0][0]
    assert isinstance(arg, dict)
    assert set(arg.keys()) == {'issuer', 'audience', '@type', 'objects', 'full'}
    assert arg['issuer'] == 'https://%s/idp/saml2/metadata' % tenant.domain_url
    assert set(arg['audience']) == {
        'http://combo.example.net/metadata/',
        'http://more.example.net/metadata/',
    }
    assert arg['@type'] == 'provision'
    assert arg['full'] is False
    objects = arg['objects']
    assert isinstance(objects, dict)
    assert set(objects.keys()) == {'data', '@type'}
    assert objects['@type'] == 'user'
    data = objects['data']
    assert isinstance(data, list)
    assert len(data) == 2
    assert len({o['uuid'] for o in data}) == 2
    for o in data:
        assert set(o.keys()) >= {
            'uuid',
            'username',
            'first_name',
            'is_superuser',
            'last_name',
            'email',
            'roles',
        }
        assert o['uuid'] in users
        user = users[o['uuid']]
        assert o['username'] == user.username
        assert o['first_name'] == user.first_name
        assert o['last_name'] == user.last_name
        assert o['email'] == user.email
        assert o['is_active'] is user.is_active
        assert o['roles'] == [_api_role(r) for r in user.roles.all()]
        assert o['is_superuser'] is False

    notify_agents.reset_mock()
    attribute.set_value(user1, '13400')
    user1.is_superuser = True
    with provisionning:
        user1.save()
        user2.save()

    assert notify_agents.call_count == 1
    arg = notify_agents.call_args
    assert arg == call(ANY, sync=ANY)
    arg = arg[0][0]
    assert isinstance(arg, dict)
    assert set(arg.keys()) == {'issuer', 'audience', '@type', 'objects', 'full'}
    assert arg['issuer'] == 'https://%s/idp/saml2/metadata' % tenant.domain_url
    assert set(arg['audience']) == {
        'http://combo.example.net/metadata/',
        'http://more.example.net/metadata/',
    }
    assert arg['@type'] == 'provision'
    assert arg['full'] is False
    objects = arg['objects']
    assert isinstance(objects, dict)
    assert set(objects.keys()) == {'data', '@type'}
    assert objects['@type'] == 'user'
    data = objects['data']
    assert isinstance(data, list)
    assert len(data) == 2
    for o in data:
        assert set(o.keys()) >= {
            'code_postal',
            'uuid',
            'username',
            'first_name',
            'is_superuser',
            'last_name',
            'email',
            'roles',
        }
        assert o['uuid'] in users
        user = users[o['uuid']]
        assert o['uuid'] == user.uuid
        assert o['username'] == user.username
        assert o['first_name'] == user.first_name
        assert o['last_name'] == user.last_name
        assert o['email'] == user.email
        assert o['roles'] == [_api_role(r) for r in user.roles.all()]
        assert o['code_postal'] is None or o['code_postal'] == '13400'
        assert o['is_superuser'] is user.is_superuser

    # test a service in a second OU also get the provisionning message
    notify_agents.reset_mock()
    ou2 = OrganizationalUnit.objects.create(name='ou2', slug='ou2')
    wcs.ou = ou2
    wcs.save()
    attribute.set_value(user1, '13500')
    with provisionning:
        user1.save()
        user2.save()

    assert notify_agents.call_count == 2
    assert set(
        notify_agents.mock_calls[0][1][0]['audience'] + notify_agents.mock_calls[1][1][0]['audience']
    ) == {'http://more.example.net/metadata/', 'http://combo.example.net/metadata/'}
    ou2.delete()

    notify_agents.reset_mock()
    with provisionning:
        AttributeValue.objects.get(attribute=attribute).delete()

    assert notify_agents.call_count == 1
    arg = notify_agents.call_args
    assert arg == call(ANY, sync=ANY)
    arg = arg[0][0]
    assert isinstance(arg, dict)
    assert set(arg.keys()) == {'issuer', 'audience', '@type', 'objects', 'full'}
    assert arg['issuer'] == 'https://%s/idp/saml2/metadata' % tenant.domain_url
    assert arg['audience'] == ['http://combo.example.net/metadata/']
    assert arg['@type'] == 'provision'
    assert arg['full'] is False
    objects = arg['objects']
    assert isinstance(objects, dict)
    assert set(objects.keys()) == {'data', '@type'}
    assert objects['@type'] == 'user'
    data = objects['data']
    assert isinstance(data, list)
    assert len(data) == 1
    for o in data:
        assert set(o.keys()) >= {
            'uuid',
            'username',
            'first_name',
            'is_superuser',
            'last_name',
            'email',
            'roles',
        }
        assert o['uuid'] == user1.uuid
        assert o['username'] == user1.username
        assert o['first_name'] == user1.first_name
        assert o['last_name'] == user1.last_name
        assert o['email'] == user1.email
        assert o['roles'] == []
        assert o['is_superuser'] is True

    user1.is_superuser = False
    user1.save()

    notify_agents.reset_mock()
    with provisionning:
        role.members.add(user1)
        user2.save()

    assert notify_agents.call_count == 2
    assert notify_agents.call_args_list[0][0][0]['objects']['@type'] == 'role'
    arg = notify_agents.call_args_list[1]
    assert arg == call(ANY, sync=ANY)
    arg = arg[0][0]
    assert isinstance(arg, dict)
    assert set(arg.keys()) == {'issuer', 'audience', '@type', 'objects', 'full'}
    assert arg['issuer'] == 'https://%s/idp/saml2/metadata' % tenant.domain_url
    assert arg['audience'] == ['http://combo.example.net/metadata/']
    assert arg['@type'] == 'provision'
    assert arg['full'] is False
    objects = arg['objects']
    assert isinstance(objects, dict)
    assert set(objects.keys()) == {'data', '@type'}
    assert objects['@type'] == 'user'
    data = objects['data']
    assert isinstance(data, list)
    assert len(data) == 2
    for o in data:
        assert set(o.keys()) >= {
            'uuid',
            'username',
            'first_name',
            'is_superuser',
            'last_name',
            'email',
            'roles',
        }
        assert o['uuid'] in users
        user = users[o['uuid']]
        assert o['uuid'] == user.uuid
        assert o['username'] == user.username
        assert o['first_name'] == user.first_name
        assert o['last_name'] == user.last_name
        assert o['email'] == user.email
        assert o['roles'] == [_api_role(r) for r in user.roles.all()]
        assert o['is_superuser'] is (user == user1)
    assert len({x['uuid'] for x in notify_agents.call_args_list[1][0][0]['objects']['data']}) == 2

    notify_agents.reset_mock()
    with provisionning:
        user1.roles.remove(role)

    assert notify_agents.call_count == 2
    arg = notify_agents.call_args
    assert arg == call(ANY, sync=ANY)
    arg = arg[0][0]
    assert isinstance(arg, dict)
    assert set(arg.keys()) == {'issuer', 'audience', '@type', 'objects', 'full'}
    assert arg['issuer'] == 'https://%s/idp/saml2/metadata' % tenant.domain_url
    assert arg['audience'] == ['http://combo.example.net/metadata/']
    assert arg['@type'] == 'provision'
    assert arg['full'] is False
    objects = arg['objects']
    assert isinstance(objects, dict)
    assert set(objects.keys()) == {'data', '@type'}
    assert objects['@type'] == 'user'
    data = objects['data']
    assert isinstance(data, list)
    assert len(data) == 1
    for o in data:
        assert set(o.keys()) >= {
            'uuid',
            'username',
            'first_name',
            'is_superuser',
            'last_name',
            'email',
            'roles',
        }
        assert o['uuid'] == user1.uuid
        assert o['username'] == user1.username
        assert o['first_name'] == user1.first_name
        assert o['last_name'] == user1.last_name
        assert o['email'] == user1.email
        assert o['roles'] == []
        assert o['is_superuser'] is False

    notify_agents.reset_mock()
    with provisionning:
        user1.roles.add(child_role)
        child_role.add_parent(role)

    assert notify_agents.call_count == 2
    arg = notify_agents.call_args
    assert arg == call(ANY, sync=ANY)
    arg = arg[0][0]
    assert isinstance(arg, dict)
    assert set(arg.keys()) == {'issuer', 'audience', '@type', 'objects', 'full'}
    assert arg['issuer'] == 'https://%s/idp/saml2/metadata' % tenant.domain_url
    assert arg['audience'] == ['http://combo.example.net/metadata/']
    assert arg['@type'] == 'provision'
    assert arg['full'] is False
    objects = arg['objects']
    assert isinstance(objects, dict)
    assert set(objects.keys()) == {'data', '@type'}
    assert objects['@type'] == 'user'
    data = objects['data']
    assert isinstance(data, list)
    assert len(data) == 1
    for o in data:
        assert set(o.keys()) >= {
            'uuid',
            'username',
            'first_name',
            'is_superuser',
            'last_name',
            'email',
            'roles',
        }
        assert o['uuid'] == user1.uuid
        assert o['username'] == user1.username
        assert o['first_name'] == user1.first_name
        assert o['last_name'] == user1.last_name
        assert o['email'] == user1.email
        assert len(o['roles']) == 2
        for r in o['roles']:
            r1 = _api_role(role)
            r2 = _api_role(child_role)
            assert r in (r1, r2)
        assert len({r['uuid'] for r in o['roles']}) == 2
        assert o['is_superuser'] is True

    notify_agents.reset_mock()
    with provisionning:
        child_role.remove_parent(role)

    assert notify_agents.call_count == 1
    arg = notify_agents.call_args
    assert arg == call(ANY, sync=ANY)
    arg = arg[0][0]
    assert isinstance(arg, dict)
    assert set(arg.keys()) == {'issuer', 'audience', '@type', 'objects', 'full'}
    assert arg['issuer'] == 'https://%s/idp/saml2/metadata' % tenant.domain_url
    assert arg['audience'] == ['http://combo.example.net/metadata/']
    assert arg['@type'] == 'provision'
    assert arg['full'] is False
    objects = arg['objects']
    assert isinstance(objects, dict)
    assert set(objects.keys()) == {'data', '@type'}
    assert objects['@type'] == 'user'
    data = objects['data']
    assert isinstance(data, list)
    assert len(data) == 1
    for o in data:
        assert set(o.keys()) >= {
            'uuid',
            'username',
            'first_name',
            'is_superuser',
            'last_name',
            'email',
            'roles',
        }
        assert o['uuid'] == user1.uuid
        assert o['username'] == user1.username
        assert o['first_name'] == user1.first_name
        assert o['last_name'] == user1.last_name
        assert o['email'] == user1.email
        assert o['roles'] == [_api_role(child_role)]
        assert o['is_superuser'] is False

    notify_agents.reset_mock()
    ou2 = OrganizationalUnit.objects.create(name='ou2', slug='ou2')
    LibertyProvider.objects.create(
        ou=get_default_ou(),
        name='provider2',
        slug='provider2',
        entity_id='http://provider2.com',
        protocol_conformance=lasso.PROTOCOL_SAML_2_0,
    )
    with provisionning:
        user1.delete()
        user2.delete()
    assert notify_agents.call_count == 1
    arg = notify_agents.call_args
    assert arg == call(ANY, sync=ANY)
    arg = arg[0][0]
    assert isinstance(arg, dict)
    assert set(arg.keys()) == {'issuer', 'audience', '@type', 'objects', 'full'}
    assert arg['issuer'] == 'https://%s/idp/saml2/metadata' % tenant.domain_url
    assert set(arg['audience']) == {'http://combo.example.net/metadata/'}
    assert arg['@type'] == 'deprovision'
    assert arg['full'] is False
    objects = arg['objects']
    assert isinstance(objects, dict)
    assert set(objects.keys()) == {'data', '@type'}
    assert objects['@type'] == 'user'
    data = objects['data']
    assert isinstance(data, list)
    assert len(data) == 2
    assert len({o['uuid'] for o in data}) == 2
    for o in data:
        assert set(o.keys()) == {'uuid'}
        assert o['uuid'] in users


def test_provision_createsuperuser(tenant_ctx, tenant, notify_agents, caplog, call_command):
    # create a provider so notification messages have an audience.
    LibertyProvider.objects.create(
        ou=get_default_ou(),
        name='wcs',
        slug='wcs',
        entity_id='http://more.example.net/metadata/',
        protocol_conformance=lasso.PROTOCOL_SAML_2_0,
    )
    LibertyProvider.objects.create(
        ou=get_default_ou(),
        name='whatever',
        slug='whatever',
        entity_id='http://provider.com',
        protocol_conformance=lasso.PROTOCOL_SAML_2_0,
    )

    call_command(
        'createsuperuser',
        domain=tenant.domain_url,
        username='coin',
        email='coin@coin.org',
        interactive=False,
    )
    assert notify_agents.call_count == 1


def test_command_hobo_provision(tenant_ctx, notify_agents, caplog, call_command):
    ou = get_default_ou()
    LibertyProvider.objects.create(
        ou=ou,
        name='wcs',
        slug='wcs',
        entity_id='http://more.example.net/metadata/',
        protocol_conformance=lasso.PROTOCOL_SAML_2_0,
    )
    for i in range(10):
        Role.objects.create(name='role-%s' % i, ou=ou)
    for i in range(10):
        User.objects.create(
            username='user-%s' % i,
            first_name='John',
            last_name='Doe %s' % i,
            ou=ou,
            email='jone.doe-%s@example.com',
        )

    # call_command('tenant_command', 'hobo_provision', ...) doesn't work
    # https://github.com/bernardopires/django-tenant-schemas/issues/495
    # so we call the command from the tenant context.
    call_command('hobo_provision', roles=True, users=True)

    msg_1 = notify_agents.call_args_list[0][0][0]
    msg_2 = notify_agents.call_args_list[1][0][0]
    assert msg_1['@type'] == 'provision'
    assert msg_1['full'] is True
    assert msg_1['objects']['@type'] == 'role'
    assert len(msg_1['objects']['data']) == 10
    assert msg_2['@type'] == 'provision'
    assert msg_2['full'] is False
    assert msg_2['objects']['@type'] == 'user'
    assert len(msg_2['objects']['data']) == 10


def test_middleware(notify_agents, app_factory, tenant, settings):
    settings.HOBO_PROVISIONNING_SYNCHRONOUS = True

    with tenant_context(tenant):
        user = User.objects.create(username='john', ou=get_default_ou())
        user.set_password('password')
        user.save()
        LibertyProvider.objects.create(
            ou=get_default_ou(),
            name='More',
            slug='more',
            entity_id='http://more.example.net/metadata/',
            protocol_conformance=lasso.PROTOCOL_SAML_2_0,
        )
        LibertyProvider.objects.create(
            ou=get_default_ou(),
            name='Combo',
            slug='combo',
            entity_id='http://combo.example.net/metadata/',
            protocol_conformance=lasso.PROTOCOL_SAML_2_0,
        )
    assert notify_agents.call_count == 0

    app = app_factory(tenant)
    resp = app.get('/login/')
    form = resp.form
    form.set('username', 'john')
    form.set('password', 'password')
    resp = form.submit(name='login-password-submit').follow()
    resp = resp.click('Your account')
    resp = resp.click('Edit')
    resp.form.set('first_name', 'John')
    resp.form.set('last_name', 'Doe')
    assert notify_agents.call_count == 0
    resp = resp.form.submit().follow()
    assert notify_agents.call_count == 1


def test_provision_using_http(tenant, settings, caplog, call_command, provisionning):
    # first attempt with legacy provisionning mode
    settings.HOBO_HTTP_PROVISIONNING = False
    with tenant_context(tenant):
        # create providers so notification messages have an audience.
        LibertyProvider.objects.create(
            name='provider',
            slug='provider',
            entity_id='http://more.example.net/metadata/',
            protocol_conformance=lasso.PROTOCOL_SAML_2_0,
        )
        LibertyProvider.objects.create(
            name='provider2',
            slug='provider2',
            entity_id='http://combo.example.net/metadata/',
            protocol_conformance=lasso.PROTOCOL_SAML_2_0,
        )
    with patch('hobo.agent.authentic2.provisionning.notify_agents') as notify_agents:
        call_command(
            'createsuperuser',
            domain=tenant.domain_url,
            username='coin',
            email='coin@coin.org',
            interactive=False,
        )
        assert notify_agents.call_count == 1
        assert set(notify_agents.call_args[0][0]['audience']) == {
            'http://more.example.net/metadata/',
            'http://combo.example.net/metadata/',
        }

    # then with http provisionning, now used by default
    with tenant_context(tenant):
        del settings.KNOWN_SERVICES['wcs']['more']['provisionning-url']
        del settings.KNOWN_SERVICES['combo']['combo']['provisionning-url']
    settings.HOBO_HTTP_PROVISIONNING = True
    with patch('hobo.agent.authentic2.provisionning.notify_agents') as notify_agents:
        call_command(
            'createsuperuser',
            domain=tenant.domain_url,
            username='coin2',
            email='coin2@coin.org',
            interactive=False,
        )
        assert notify_agents.call_count == 1
        assert set(notify_agents.call_args[0][0]['audience']) == {
            'http://more.example.net/metadata/',
            'http://combo.example.net/metadata/',
        }

    with tenant_context(tenant):
        settings.KNOWN_SERVICES['wcs']['more'][
            'provisionning-url'
        ] = 'http:///more.example.net/__provisionning__/'
    with patch('hobo.agent.authentic2.provisionning.notify_agents') as notify_agents:
        with patch('hobo.agent.authentic2.provisionning.requests.put') as requests_put:
            call_command(
                'createsuperuser',
                domain=tenant.domain_url,
                username='coin2',
                email='coin2@coin.org',
                interactive=False,
            )
            assert notify_agents.call_count == 1
            assert notify_agents.call_args[0][0]['audience'] == ['http://combo.example.net/metadata/']
            assert requests_put.call_count == 1
            assert all('&sync=1' in call.args[0] for call in requests_put.mock_calls if call.args)
            # cannot check audience passed to requests.put as it's the same
            # dictionary that is altered afterwards and would thus also contain
            # http://example.com.

    with tenant_context(tenant):
        settings.KNOWN_SERVICES['combo']['combo'][
            'provisionning-url'
        ] = 'http:///combo.example.net/__provisionning__/'
    with patch('hobo.agent.authentic2.provisionning.notify_agents') as notify_agents:
        with patch('hobo.agent.authentic2.provisionning.requests.put') as requests_put:
            call_command(
                'createsuperuser',
                domain=tenant.domain_url,
                username='coin3',
                email='coin3@coin.org',
                interactive=False,
            )
            assert notify_agents.call_count == 0
            assert requests_put.call_count == 2
            assert all('&sync=1' in call.args[0] for call in requests_put.mock_calls if call.args)

    with patch('hobo.agent.authentic2.provisionning.notify_agents') as notify_agents:
        with patch('hobo.agent.authentic2.provisionning.requests.put') as requests_put:
            with provisionning:
                User.objects.create(username='foo')
                User.objects.create(username='bar')
            assert notify_agents.call_count == 0
            assert requests_put.call_count == 2
            assert all('&sync=1' not in call.args[0] for call in requests_put.mock_calls if call.args)


def test_provisionning_api(tenant, app_factory, settings, caplog):
    with tenant_context(tenant):
        # create providers so notification messages have an audience.
        LibertyProvider.objects.create(
            ou=get_default_ou(),
            name='provider',
            slug='provider',
            entity_id='http://other.example.net/metadata/',
            protocol_conformance=lasso.PROTOCOL_SAML_2_0,
        )
        LibertyProvider.objects.create(
            ou=get_default_ou(),
            name='provider2',
            slug='provider2',
            entity_id='http://more.example.net/metadata/',
            protocol_conformance=lasso.PROTOCOL_SAML_2_0,
        )
        LibertyProvider.objects.create(
            ou=get_default_ou(),
            name='provider3',
            slug='provider3',
            entity_id='http://combo.example.net/metadata/',
            protocol_conformance=lasso.PROTOCOL_SAML_2_0,
        )

        role = Role.objects.create(name='coin', ou=get_default_ou())
        user = User.objects.create(
            username='Étienne',
            email='etienne.dugenou@example.net',
            first_name='Étienne',
            last_name='Dugenou',
            ou=get_default_ou(),
        )

    app = app_factory(tenant)
    resp = app.post_json('/api/provision/', {}, status=403)

    orig = settings.KNOWN_SERVICES['welco']['other']['verif_orig']
    key = settings.KNOWN_SERVICES['welco']['other']['secret']
    resp = app.post_json(signature.sign_url('/api/provision/?orig=%s' % orig, key), {}, status=400)
    assert resp.json['errors']['__all__'] == ['must provide user_uuid or role_uuid']

    resp = app.post_json(
        signature.sign_url('/api/provision/?orig=%s' % orig, key),
        {'user_uuid': 'xxx', 'role_uuid': 'yyy'},
        status=400,
    )
    assert resp.json['errors']['__all__'] == ['cannot provision both user & role']

    resp = app.post_json(
        signature.sign_url('/api/provision/?orig=%s' % orig, key), {'user_uuid': 'xxx'}, status=200
    )
    assert resp.json == {'err': 1, 'err_desc': 'unknown user UUID'}

    resp = app.post_json(
        signature.sign_url('/api/provision/?orig=%s' % orig, key), {'role_uuid': 'xxx'}, status=200
    )
    assert resp.json == {'err': 1, 'err_desc': 'unknown role UUID'}

    with patch('hobo.agent.authentic2.provisionning.requests.put') as requests_put:
        resp = app.post_json(
            signature.sign_url('/api/provision/?orig=%s' % orig, key), {'user_uuid': user.uuid}
        )
        assert requests_put.call_count == 2
        assert '&sync=1' in requests_put.call_args[0][0]
        assert not resp.json['leftover_audience']
        assert set(resp.json['reached_audience']) == {
            'http://combo.example.net/metadata/',
            'http://more.example.net/metadata/',
        }

    with patch('hobo.agent.authentic2.provisionning.requests.put') as requests_put:
        resp = app.post_json(
            signature.sign_url('/api/provision/?orig=%s' % orig, key),
            {'user_uuid': user.uuid, 'service_type': 'wcs'},
        )
        assert requests_put.call_count == 1
        assert '&sync=1' in requests_put.call_args[0][0]
        assert not resp.json['leftover_audience']
        assert set(resp.json['reached_audience']) == {'http://more.example.net/metadata/'}

    with patch('hobo.agent.authentic2.provisionning.requests.put') as requests_put:
        resp = app.post_json(
            signature.sign_url('/api/provision/?orig=%s' % orig, key),
            {'user_uuid': user.uuid, 'service_url': 'example.net'},
        )
        assert requests_put.call_count == 2
        assert '&sync=1' in requests_put.call_args[0][0]

    with patch('hobo.agent.authentic2.provisionning.requests.put') as requests_put:
        resp = app.post_json(
            signature.sign_url('/api/provision/?orig=%s' % orig, key), {'role_uuid': role.uuid}
        )
        assert requests_put.call_count == 3
        assert resp.json['err'] == 0
        assert not resp.json['leftover_audience']

    with patch('hobo.agent.authentic2.provisionning.requests.put') as requests_put:
        requests_put.side_effect = requests.RequestException
        resp = app.post_json(
            signature.sign_url('/api/provision/?orig=%s' % orig, key), {'role_uuid': role.uuid}
        )
        assert resp.json['err'] == 1
        assert resp.json['leftover_audience']

    with patch('hobo.agent.authentic2.provisionning.requests.put') as requests_put:
        role.slug = '_technical'
        role.save()
        resp = app.post_json(
            signature.sign_url('/api/provision/?orig=%s' % orig, key), {'role_uuid': role.uuid}
        )
        assert requests_put.call_count == 3
        assert resp.json['err'] == 0
        assert not resp.json['leftover_audience']
        assert set(resp.json['reached_audience']) == {
            'http://combo.example.net/metadata/',
            'http://other.example.net/metadata/',
            'http://more.example.net/metadata/',
        }

    with patch('hobo.agent.authentic2.provisionning.requests.put') as requests_put:
        role.slug = '_a2-technical'
        role.save()
        resp = app.post_json(
            signature.sign_url('/api/provision/?orig=%s' % orig, key), {'role_uuid': role.uuid}
        )
        assert requests_put.call_count == 0
        assert resp.json['err'] == 0
        assert not resp.json['leftover_audience']
        assert not resp.json['reached_audience']


def test_provision_debug(tenant, caplog, settings, tmpdir, provisionning):
    log_path = str(tmpdir / 'debug-provisionning.log')
    settings.DEBUG_PROVISIONNING_LOG_PATH = log_path
    settings.HOBO_PROVISIONNING_DEBUG = True

    assert not os.path.exists(log_path)

    with patch('hobo.agent.authentic2.provisionning.notify_agents') as notify_agents:
        with tenant_context(tenant):
            LibertyProvider.objects.create(
                ou=get_default_ou(),
                name='wcs',
                slug='wcs',
                entity_id='http://more.example.net/metadata/',
                protocol_conformance=lasso.PROTOCOL_SAML_2_0,
            )
            with provisionning:
                Role.objects.create(name='coin', ou=get_default_ou())

            assert notify_agents.call_count == 1
            assert os.path.exists(log_path)


def test_transaction_save_delete(transactional_db, tenant_ctx, notify_agents, provisionning):
    user1 = User(
        username='Étienne',
        email='etienne.dugenou@example.net',
        first_name='Étienne',
        last_name='Dugenou',
        ou=get_default_ou(),
    )

    with provisionning:
        with transaction.atomic():
            user1.save()
            user1.delete()
