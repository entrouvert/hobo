# hobo - portal to configure and deploy applications
# Copyright (C) 2015-2020  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
import unittest.mock
import urllib.parse

import pytest
from django.contrib.auth import get_user_model
from django.test import RequestFactory
from rest_framework import permissions
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from hobo import rest_authentication, signature


class TestPublikAuthentication:
    @pytest.fixture(autouse=True)
    def setup(self, tenant_ctx, settings):
        settings.HOBO_ANONYMOUS_SERVICE_USER_CLASS = 'hobo.rest_authentication.AnonymousAdminServiceUser'

    @pytest.fixture
    def user(self, tenant_ctx):
        User = get_user_model()
        return User.objects.create(username='foo', password='foo')

    @pytest.fixture
    def publik_authentication(self):
        return rest_authentication.PublikAuthentication()

    ORIG = 'other.example.net'
    URL = '/api/?coucou=zob'

    @pytest.fixture
    def authenticate(self, settings, publik_authentication):
        def _authenticate(query, key='', **kwargs):
            key = key or settings.KNOWN_SERVICES['welco']['other']['secret']
            factory = RequestFactory()
            django_request = factory.get(signature.sign_url(self.URL + query, key))
            request = Request(django_request, **kwargs)
            return publik_authentication.authenticate(request)

        return _authenticate

    def test_resolve_user_by_nameid(self, user, authenticate):
        result = authenticate('&NameID=%s&orig=%s' % (user.uuid, urllib.parse.quote(self.ORIG)))
        assert result is not None
        assert isinstance(result, tuple)
        assert len(result) == 2
        assert result[0] == user
        assert result[1] is None

    def test_disable_resolve_user_by_nameid(self, user, authenticate):
        view = unittest.mock.Mock()
        view.publik_authentication_resolve_user_by_nameid = False

        result = authenticate(
            '&NameID=%s&orig=%s' % (user.uuid, urllib.parse.quote(self.ORIG)), parser_context={'view': view}
        )

        assert result is not None
        assert isinstance(result, tuple)
        assert len(result) == 2
        assert result[0].__class__ is rest_authentication.AnonymousAdminServiceUser
        assert result[0].is_authenticated
        assert result[0].is_authenticated
        assert result[0].is_anonymous
        assert result[0].is_anonymous
        assert result[0].is_staff
        assert result[1] is None

    def test_anonymous_service_user(self, authenticate):
        result = authenticate('&orig=%s' % urllib.parse.quote(self.ORIG))

        assert result is not None
        assert isinstance(result, tuple)
        assert len(result) == 2
        assert result[0].__class__ is rest_authentication.AnonymousAdminServiceUser
        assert result[0].is_authenticated
        assert result[0].is_authenticated
        assert result[0].is_anonymous
        assert result[0].is_anonymous
        assert result[0].is_staff
        assert result[1] is None

    def test_anonymous_service_user_failure(self, authenticate):
        with pytest.raises(rest_authentication.PublikAuthenticationFailed) as exc_info:
            authenticate('&orig=%s' % urllib.parse.quote(self.ORIG), key='zob')
        assert exc_info.value.detail['err'] == 1
        assert exc_info.value.detail['err_desc'] == 'HMAC hash is different'


def test_response(tenant_ctx, rf, settings, monkeypatch):
    request = rf.get('/')

    del settings.KNOWN_SERVICES

    class View(APIView):
        authentication_classes = (rest_authentication.PublikAuthentication,)
        permission_classes = (permissions.IsAuthenticated,)

        def get(self, request, format=None):
            return Response({'err': 0, 'user': str(request.user)})

    view = View.as_view()

    response = view(request)
    assert response.status_code == 403

    request = rf.get('/?signature=aaa&orig=zzz')

    response = view(request)
    assert response.status_code == 401
    assert response.data['err'] == 1
    assert response.data['err_desc'] == 'publik-authentication: no-known-services-setting'

    secret_key = 'bbb'
    settings.KNOWN_SERVICES = {
        'whatever': {
            'whatever': {
                'verif_orig': 'zzz',
            }
        }
    }

    response = view(request)
    assert response.status_code == 401
    assert response.data['err'] == 1
    assert response.data['err_desc'] == 'no-secret-found-for-orig'

    settings.KNOWN_SERVICES['whatever']['whatever']['secret'] = secret_key
    response = view(request)
    assert response.status_code == 401
    assert response.data['err'] == 1
    assert response.data['err_desc'] == 'multiple/missing algo'

    # User authentication
    request = rf.get(signature.sign_url('/?orig=zzz&NameID=1234', secret_key))

    response = view(request)
    assert response.status_code == 401
    assert response.data == {'err': 1, 'err_desc': 'user-not-found'}

    # Service authentication, wrong timestamp
    request = rf.get(re.sub('timestamp=[^&]*', 'timestamp=xxx', signature.sign_url('/?orig=zzz', secret_key)))

    response = view(request)
    assert response.status_code == 401
    assert response.data == {
        'err': 1,
        'err_desc': "invalid timestamp, time data 'xxx' does not match format '%Y-%m-%dT%H:%M:%SZ'",
    }

    # Service authentication
    request = rf.get(signature.sign_url('/?orig=zzz', secret_key))

    response = view(request)
    assert response.status_code == 200
    assert response.data == {'err': 0, 'user': 'AnonymousUser'}

    del settings.HOBO_ANONYMOUS_SERVICE_USER_CLASS
    response = view(request)
    assert response.status_code == 401
    assert response.data == {'err': 1, 'err_desc': 'no-user-found'}


def test_known_service_this(tenant_ctx, settings):
    assert settings.KNOWN_SERVICES['authentic']['test'].get('this') is True
    for service_type, services in settings.KNOWN_SERVICES.items():
        for slug, infos in services.items():
            if service_type == 'authentic' and slug == 'test':
                assert infos.get('this') is True
            else:
                assert infos.get('this', False) is False
