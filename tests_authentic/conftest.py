import contextlib
import json
import os

import pytest
from django.core.management import call_command as management_call_command
from django.db import connection
from django_webtest import DjangoTestApp, WebTestMixin
from tenant_schemas.utils import tenant_context

# initialized in Authentic2AgentConfig.ready
# pylint: disable=no-name-in-module
from hobo.agent.authentic2.provisionning import provisionning as base_provisionning
from hobo.multitenant.models import Tenant


@pytest.fixture
def tenant_base(tmpdir, settings):
    from hobo.multitenant.apps import clear_tenants_settings

    clear_tenants_settings()

    base = str(tmpdir.mkdir('authentic-tenant-base'))
    settings.TENANT_BASE = base
    yield base


@pytest.fixture(scope='session')
def create_schema(django_db_setup, django_db_blocker):
    def _create_schema(name):
        with django_db_blocker.unblock():
            schema_name = name.replace('-', '_').replace('.', '_')
            t = Tenant(domain_url=name, schema_name=schema_name)
            t.create_schema()
            return t

    return _create_schema


@pytest.fixture(scope='session')
def tenant_schema(create_schema):
    return create_schema('authentic.example.net')


@pytest.fixture
def tenant(settings, db, tenant_schema, tenant_base):
    name = 'authentic.example.net'
    settings.ALLOWED_HOSTS = [name]

    tenant_dir = os.path.join(tenant_base, name)
    os.mkdir(tenant_dir)
    with open(os.path.join(tenant_dir, 'settings.json'), 'w') as fd:
        json.dump({'HOBO_TEST_VARIABLE': name}, fd)
    with open(os.path.join(tenant_dir, 'hobo.json'), 'w') as fd:
        json.dump(
            {
                'variables': {
                    'hobo_test_variable': True,
                    'other_variable': 'foo',
                },
                'services': [
                    {
                        'slug': 'test',
                        'service-id': 'authentic',
                        'title': 'Test',
                        'this': True,
                        'secret_key': '12345',
                        'base_url': 'https://%s' % name,
                        'variables': {
                            'other_variable': 'bar',
                        },
                    },
                    {
                        'slug': 'other',
                        'title': 'Other',
                        'service-id': 'welco',
                        'secret_key': 'abcdef',
                        'url': 'http://other.example.net',
                        'base_url': 'http://other.example.net',
                        'provisionning-url': 'http://other.example.net/__provision__/',
                        'saml-sp-metadata-url': 'http://other.example.net/metadata/',
                    },
                    {
                        'slug': 'combo',
                        'title': 'Combo',
                        'service-id': 'combo',
                        'secret_key': 'abcdef',
                        'url': 'http://combo.example.net',
                        'base_url': 'http://combo.example.net',
                        'provisionning-url': 'http://combo.example.net/__provision__/',
                        'saml-sp-metadata-url': 'http://combo.example.net/metadata/',
                    },
                    {
                        'slug': 'more',
                        'title': 'More',
                        'service-id': 'wcs',
                        'secret_key': 'abcdef',
                        'url': 'http://more.example.net',
                        'base_url': 'http://more.example.net',
                        'provisionning-url': 'http://more.example.net/__provision__/',
                        'saml-sp-metadata-url': 'http://more.example.net/metadata/',
                    },
                ],
            },
            fd,
        )
    try:
        yield tenant_schema
    finally:
        pass


@pytest.fixture
def tenant_ctx(tenant):
    with tenant_context(tenant):
        yield


@contextlib.contextmanager
def run_on_commit_hooks():
    yield
    do_run_on_commit_hooks()


def do_run_on_commit_hooks():
    current_run_on_commit = connection.run_on_commit
    connection.run_on_commit = []
    while current_run_on_commit:
        func = current_run_on_commit.pop(0)[1]
        func()


class MyDjangoTestApp(DjangoTestApp):
    @run_on_commit_hooks()
    def do_request(self, *args, **kwargs):
        return super().do_request(*args, **kwargs)


@pytest.fixture
def app_factory(request):
    wtm = WebTestMixin()
    wtm._patch_settings()

    def factory(hostname='testserver'):
        if hasattr(hostname, 'domain_url'):
            hostname = hostname.domain_url
        return MyDjangoTestApp(extra_environ={'HTTP_HOST': hostname})

    try:
        yield factory
    finally:
        wtm._unpatch_settings()


@pytest.fixture
def notify_agents(mocker):
    yield mocker.patch('hobo.agent.authentic2.provisionning.Provisionning.notify_agents')


@pytest.fixture
def call_command():
    def call_command(*args, **kwargs):
        with run_on_commit_hooks():
            return management_call_command(*args, **kwargs)

    return call_command


class Provisionning:
    def __init__(self, provisionning):
        self.provisionning = provisionning

    def __enter__(self):
        return self.provisionning.__enter__()

    def __exit__(self, *args):
        try:
            return self.provisionning.__exit__(*args)
        finally:
            do_run_on_commit_hooks()


@pytest.fixture
def provisionning():
    return Provisionning(base_provisionning)
