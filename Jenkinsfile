@Library('eo-jenkins-lib@main') import eo.Utils

pipeline {
    agent any
    options {
        disableConcurrentBuilds()
        timeout(time: 60, unit: 'MINUTES')
    }
    parameters {
        string(name: 'AUTHENTIC_VERSION', defaultValue: 'main')
        string(name: 'DJANGO_TENANT_SCHEMAS_VERSION', defaultValue: 'main')
        string(name: 'PASSERELLE_VERSION', defaultValue: 'main')
    }
    environment {
        AUTHENTIC_VERSION = "${params.AUTHENTIC_VERSION}"
        DJANGO_TENANT_SCHEMAS_VERSION = "${params.DJANGO_TENANT_SCHEMAS_VERSION}"
        PASSERELLE_VERSION = "${params.PASSERELLE_VERSION}"
    }
    stages {
        stage('Unit Tests') {
            steps {
                sh 'nox --no-reuse-existing-virtualenvs'
            }
            post {
                always {
                    script {
                        utils = new Utils()
                        utils.publish_coverage('coverage.xml')
                        utils.publish_coverage_native('index.html', 'htmlcov', 'Coverage')
                        utils.publish_pylint('pylint.out')
                    }
                    mergeJunitResults()
                }
            }
        }
        stage('Packaging') {
            steps {
                script {
                    env.SHORT_JOB_NAME=sh(
                        returnStdout: true,
                        // given JOB_NAME=gitea/project/PR-46, returns project
                        // given JOB_NAME=project/main, returns project
                        script: '''
                            echo "${JOB_NAME}" | sed "s/gitea\\///" | awk -F/ '{print $1}'
                        '''
                    ).trim()
                    if (env.GIT_BRANCH == 'main' || env.GIT_BRANCH == 'origin/main') {
                        sh "sudo -H -u eobuilder /usr/local/bin/eobuilder -d bookworm ${SHORT_JOB_NAME}"
                    } else if (env.GIT_BRANCH.startsWith('hotfix/')) {
                        sh "sudo -H -u eobuilder /usr/local/bin/eobuilder -d bookworm --branch ${env.GIT_BRANCH} --hotfix ${SHORT_JOB_NAME}"
                    }
                }
            }
        }
    }
    post {
        always {
            script {
                utils = new Utils()
                utils.mail_notify(currentBuild, env, 'ci+jenkins-hobo@entrouvert.org')
            }
        }
        success {
            cleanWs()
        }
    }
}
