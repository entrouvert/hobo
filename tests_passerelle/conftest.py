import shutil
import tempfile

import pytest


@pytest.fixture
def tenant_base(request, settings):
    base = tempfile.mkdtemp('passerelle-tenant-base')
    settings.TENANT_BASE = base

    def fin():
        shutil.rmtree(base)

    request.addfinalizer(fin)
    return tenant_base
