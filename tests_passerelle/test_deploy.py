import json
import os
import sys
from io import StringIO

from django.core.management import call_command
from django.db import connection
from passerelle.utils import export_site  # noqa pylint: disable=import-error

from hobo.multitenant.middleware import TenantMiddleware

os.sys.path.append('%s/tests' % os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


def test_deploy_specifics(db, tenant_base):
    hobo_json = {
        'variables': {
            'hobo_test_variable': True,
            'other_variable': 'foo',
        },
        'services': [
            {
                'slug': 'test',
                'title': 'Test',
                'service-id': 'welco',
                'this': True,
                'secret_key': '12345',
                'base_url': 'http://passerelle.example.net',
                'saml-sp-metadata-url': 'http://passerelle.example.net/saml/metadata',
                'variables': {
                    'other_variable': 'bar',
                },
            },
            {
                'slug': 'other',
                'title': 'Other',
                'secret_key': 'abcde',
                'service-id': 'wcs',
                'base_url': 'http://wcs.example.net',
            },
        ],
    }
    old_stdin = sys.stdin
    sys.stdin = StringIO(json.dumps(hobo_json))
    try:
        call_command('hobo_deploy', 'http://passerelle.example.net', '-')
    finally:
        sys.stdin = old_stdin

    assert len(list(TenantMiddleware.get_tenants())) == 1


def test_import_template(db, tenant_base):
    call_command('create_tenant', 'passerelle.example.net')
    tenant = TenantMiddleware.get_tenant_by_hostname('passerelle.example.net')
    connection.set_tenant(tenant)
    call_command(
        'import_template', '--basepath=%s' % os.path.dirname(__file__), 'data_passerelle_export_site'
    )
    with open('%s/data_passerelle_export_site.json' % os.path.dirname(__file__)) as fd:
        content = fd.read()
    assert export_site() == json.loads(content)
