import builtins
import os
from unittest.mock import mock_open, patch

import hobo.test_utils

# Debian defaults
DEBUG = False

PROJECT_NAME = 'passerelle'

#
# hobotization (multitenant)
#
open_backup = open
with patch.object(builtins, 'open', mock_open(read_data=b'xxx')):
    with open_backup(os.environ['DEBIAN_CONFIG_COMMON']) as fd:
        exec(fd.read())

BRANCH_NAME = os.environ.get('BRANCH_NAME', '').replace('/', '-')[:15]
# noqa pylint: disable=undefined-variable
DATABASES['default']['NAME'] = hobo.test_utils.get_safe_db_name()

# Avoid conflic with real tenants
# that might exist in /var/lib/passerelle/tenants
TENANT_BASE = '/that/path/does/not/exist'

# suds logs are buggy
# noqa pylint: disable=undefined-variable
LOGGING['loggers']['suds'] = {
    'level': 'ERROR',
    'handlers': ['mail_admins'],
    'propagate': True,
}
