DEBUG = True

SECRET_KEY = 'changeme'

MEDIA_ROOT = 'media'

STATIC_ROOT = 'collected-static'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'db.sqlite3',
    }
}

## Django Mellon configuration
# you need to generate SSL certificates in your current directory to make it functionnal :
#
#   openssl genpkey -algorithm rsa -pkeyopt rsa_keygen_bits:2048 -out key.cert
#   openssl req -x509 -new -out cert.pem -subj '/CN=whocaresIdo' -key key.cert -days 3650
#
# you also need to get the idp metadata and call it idp-metadata.xml
# Uncomment the following lines to enable SAML support
# INSTALLED_APPS += ('mellon',)
# AUTHENTICATION_BACKENDS = ( 'mellon.backends.SAMLBackend',)
# LOGIN_URL = '/accounts/mellon/login/'
# LOGOUT_URL = '/accounts/mellon/logout/'
# MELLON_PUBLIC_KEYS = ['cert.pem']
# MELLON_PRIVATE_KEY = 'key.cert'
# MELLON_IDENTITY_PROVIDERS = [
#            {'METADATA': 'idp-metadata.xml',
#             'GROUP_ATTRIBUTE': 'role'},
#        ]
